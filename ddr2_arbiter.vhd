------------------------------------------------------------------
--
-- Title       : Parametrized scheduler
-- Design      : DDR2 memory controller for video compression
-- Author      : Maciej Trochimiuk
-- Company     : none
-- Start Date  : 01.03.2010
--
---------------------------------------------------------------------
-- VERSION 0.2
-----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ddr2_pkg.all; 
use work.motion_compensation_dec_pkg.all;

entity ddr2_arbiter is 
  port (
    --zegar i reset
    clk               : in std_logic;
    rst_n             : in std_logic;
	mem_ready         : out std_logic;
    
    --wej�cia i wyj�cia do urz�dze�
    devices_i         : in SCHEDULER_DEVICES_IN;
    devices_o         : out SCHEDULER_DEVICES_OUT;
    devices_mem_i     : out MULTIPLEXER_ALTERA_DDR2_OUT;
    devices_mem_o     : in  MULTIPLEXER_ALTERA_DDR2_IN;
    
    --kontroler altery
    local_init_done       : in  std_logic;    
    local_rdata           : in  std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
    local_rdata_valid     : in  std_logic;
    local_ready           : in  std_logic;
    local_refresh_ack     : in  std_logic;
    local_wdata_req       : in  std_logic;
    local_addr            : out std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
    local_be              : out std_logic_vector(ALTERA_BYTE_ENABLE-1 downto 0);
    local_size            : out std_logic_vector(ALTERA_BURST_WIDTH-1 downto 0);
    local_wdata           : out std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
    local_read_req        : out std_logic;
    local_write_req       : out std_logic
  );
 end entity;
 
 architecture rtl of ddr2_arbiter is
 
  type STATE_TYPE is (INIT, CHECK, BUSY);
  
  signal ready                 : std_logic_vector(PORTS_TOTAL-1 downto 0); 
  signal state_r               : state_type;
  signal state_next            : state_type;
  signal ready_r               : std_logic;
  signal ready_next            : std_logic;
  signal port_idx_r            : std_logic_vector(log2_ceil(PORTS_TOTAL) downto 0);
  signal port_idx_next         : std_logic_vector(log2_ceil(PORTS_TOTAL) downto 0);
  signal port_enable_r         : std_logic_vector(PORTS_TOTAL-1 downto 0);
  signal port_enable_next      : std_logic_vector(PORTS_TOTAL-1 downto 0);
	
begin
 
  mem_ready <= ready_r;
    
  PIN_GEN: for i in (PORTS_TOTAL - 1) downto 0 generate
    ready(i) <= devices_i(i).device_ready;
  end generate;
  
  SCHEDULER_CLK_RST_PROC: process(clk, rst_n)
	begin
		if rst_n = '0' then
			state_r <= INIT;
			port_idx_r <= (others => '0');	  
			port_enable_r <= (others => '0'); 
			ready_r <= '0';
		elsif rising_edge(clk) then
			state_r <= state_next;
			ready_r <= ready_next;
			port_idx_r <= port_idx_next;  
			port_enable_r <= port_enable_next;
		end if;
	end process;
	     
	MAIN_PROC:	process(state_r, devices_i, ready, port_idx_r, port_enable_r, 
	local_init_done, local_refresh_ack, local_rdata, local_rdata_valid, local_ready, local_wdata_req, devices_mem_o)
    variable port_idx_v              : std_logic_vector(log2_ceil(PORTS_TOTAL) downto 0);
    variable ones_v                  : std_logic_vector(PORTS_TOTAL-1 downto 0);
    variable state_v                 : STATE_TYPE;
    variable devices_o_v             : SCHEDULER_DEVICES_OUT;
    variable devices_mem_i_v         : MULTIPLEXER_ALTERA_DDR2_OUT;
    variable local_addr_v            : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
    variable local_be_v              : std_logic_vector(ALTERA_BYTE_ENABLE-1 downto 0);
    variable local_size_v            : std_logic_vector(ALTERA_BURST_WIDTH-1 downto 0);
    variable local_wdata_v           : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
    variable local_read_req_v        : std_logic;
    variable local_write_req_v       : std_logic;
	variable ready_v                 : std_logic;
	variable port_enable_v           : std_logic_vector(PORTS_TOTAL-1 downto 0);
  begin
    ---------------
    -- VARIABLES --
    ---------------
    
    ones_v := (others => '1');
    state_v := state_r;
    port_idx_v := port_idx_r;
	port_enable_v := port_enable_r;
	ready_v := '0';
        
    -------------------
    -- STATE MACHINE --
    -------------------
    
    case state_r is
      when INIT =>
        port_idx_v := (others => '0');
        if ready = ones_v and local_init_done = '1' then
          state_v := CHECK;
        end if;
		port_enable_v := (others => '0');
        
      when CHECK =>
		ready_v := '1';
        if local_refresh_ack = '1' then
          state_v := CHECK;
        else
          for i in 0 to PORTS_TOTAL-1 loop
            if devices_i(i).request_lp = '1' then
              port_idx_v := conv_std_logic_vector(i,log2_ceil(PORTS_TOTAL)+1);
              state_v := BUSY;	
			  port_enable_v := (others => '0');
			  port_enable_v(i) := '1';
            end if;
          end loop;
		  
		  for i in 0 to PORTS_TOTAL-1 loop
            if devices_i(i).request_np = '1' then
              port_idx_v := conv_std_logic_vector(i,log2_ceil(PORTS_TOTAL)+1);
              state_v := BUSY;
			  port_enable_v := (others => '0');
			  port_enable_v(i) := '1';
            end if;
          end loop;
		  
		  for i in 0 to PORTS_TOTAL-1 loop
            if devices_i(i).request_hp = '1' then
              port_idx_v := conv_std_logic_vector(i,log2_ceil(PORTS_TOTAL)+1);
              state_v := BUSY;
			  port_enable_v := (others => '0');
			  port_enable_v(i) := '1';
            end if;
          end loop;
        end if;
        
      when BUSY =>
	    ready_v := '1';
        if devices_i(conv_integer(port_idx_r)).done = '1' then
          state_v := CHECK;
		  port_enable_v := (others => '0');
       elsif devices_i(conv_integer(port_idx_r)).request_hp = '0' and devices_i(conv_integer(port_idx_r)).request_np = '0' and devices_i(conv_integer(port_idx_r)).request_lp = '0' then
          --forcing reset, error
          state_v := INIT;
        end if; 
    end case;
    
    ------------------------------------
    -- DEVICE OUTPUTS AND MULTIPLEXER --
    ------------------------------------
    
    local_addr_v := devices_mem_o(0).addr;
    local_be_v := devices_mem_o(0).be;
    local_size_v := devices_mem_o(0).size;
    local_wdata_v := devices_mem_o(PORTS_BIDIR + PORTS_READ).wdata;
	
    local_read_req_v := devices_mem_o(0).read_req;
    local_write_req_v := devices_mem_o(0).write_req;
	
    for i in 0 to PORTS_TOTAL-1 loop
      devices_mem_i_v(i).rdata := local_rdata;
      devices_o_v(i).enable := port_enable_r(i);
      devices_mem_i_v(i).rdata_valid := local_rdata_valid and port_enable_r(i);
      devices_mem_i_v(i).ready := local_ready and port_enable_r(i);
      devices_mem_i_v(i).wdata_req := local_wdata_req and port_enable_r(i);
    end loop;
    for i in 1 to PORTS_TOTAL-1 loop
      if port_enable_r(i) = '1' then
        local_addr_v := devices_mem_o(i).addr;
        local_be_v := devices_mem_o(i).be;
        local_size_v := devices_mem_o(i).size;
        local_read_req_v := devices_mem_o(i).read_req;
        local_write_req_v := devices_mem_o(i).write_req;
        if PORTS_BIDIR + PORTS_READ < i then
          local_wdata_v := devices_mem_o(i).wdata;
        end if;
      end if;
    end loop;
       
    -----------------
    -- ASSIGNMENTS --
    -----------------    
        
    state_next <= state_v;
    port_idx_next <= port_idx_v; 
	port_enable_next <= port_enable_v;
    devices_o <= devices_o_v;
    local_addr <= local_addr_v;
    local_be <= local_be_v;
    local_size <= local_size_v;
    local_wdata <= local_wdata_v;
    local_read_req <= local_read_req_v;
    local_write_req <= local_write_req_v;
    devices_o <= devices_o_v;
    devices_mem_i <= devices_mem_i_v;
	ready_next <= ready_v;
    
  end process; 
end rtl;