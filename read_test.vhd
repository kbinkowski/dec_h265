-------------------------------------------------------------------------------
--                                                                           --
-- Title       : MOTION COMPENSATION DECODER - H.265/HEVC                    --
-- Design      : the H.265/HEVC decoder                                      --
-- Author      : Kamil Binkowski, University of Technology in Warsaw         --
-- Start Date  : 02.03.2013                                                  --
--                                                                           --
-------------------------------------------------------------------------------  
--                                                                           --
-- File        : read_test.vhd                                               --
--                                                                           --
-------------------------------------------------------------------------------
-- Description: Module used to read from files input data for top entity.    --
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.motion_compensation_dec_pkg.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;  

entity read_test is   
port(  
  rst                  : in std_logic;
  clk                  : in std_logic;  
  start                : in std_logic;  
  ready                : in std_logic;   
  read_info            : out INPUT_DATA_TYPE;
  chroma_format        : out std_logic;
  weight_denom         : out WEIGHT_DENOM_TYPE;
  weighted_pred_flag   : out std_logic;  
  weighted_bipred_flag : out std_logic
  );
end read_test;


architecture read_test_arch of read_test is  

file seqParam_file      : text open read_mode is "/test/seqParam_file.txt";
file res_file           : text open read_mode is "/test/res_file.txt";   
file mv_file            : text open read_mode is "/test/mv_file.txt";
file ctu_file           : text open read_mode is "/test/ctu_file.txt";

type INTERNAL_TYPE is record
  read_mv      : std_logic;
  read_samples : std_logic; 
end record;

signal internal_r                : INTERNAL_TYPE;
signal internal_next             : INTERNAL_TYPE; 

signal chroma_format_r           : std_logic;
signal chroma_format_next        : std_logic;	
signal weight_denom_r            : WEIGHT_DENOM_TYPE;
signal weight_denom_next         : WEIGHT_DENOM_TYPE;
signal weighted_pred_flag_r      : std_logic;
signal weighted_pred_flag_next   : std_logic;
signal weighted_bipred_flag_r    : std_logic;
signal weighted_bipred_flag_next : std_logic;

signal read_info_r               : INPUT_DATA_TYPE;
signal read_info_next            : INPUT_DATA_TYPE;

begin  
  
read_info <= read_info_r;
chroma_format <= chroma_format_r;
weight_denom <= weight_denom_r;
weighted_pred_flag <= weighted_pred_flag_r;  
weighted_bipred_flag <= weighted_bipred_flag_r;

--Process of read data from files
PROC_READ: process(ready, clk, start) 
variable read_info_v : INPUT_DATA_TYPE;
variable internal_v  : INTERNAL_TYPE;  
variable line_v      : line;
variable integer_v   : integer;    
variable bit_v       : std_logic;  
variable string_v    : string(3 downto 1);
variable chroma_format_v        : std_logic;
variable weight_denom_v         : WEIGHT_DENOM_TYPE;
variable weighted_pred_flag_v   : std_logic;
variable weighted_bipred_flag_v : std_logic;

begin                
  read_info_v            := read_info_r;
  internal_v             := internal_r; 
  chroma_format_v        := chroma_format_r;
  weight_denom_v         := weight_denom_r;
  weighted_pred_flag_v   := weighted_pred_flag_r;
  weighted_bipred_flag_v := weighted_bipred_flag_r;
  
  if falling_edge(clk) then 
    if start = '1' then
      if not endfile(seqParam_file) then 
	 
        readline(seqParam_file, line_v);
        read(line_v, bit_v); 
        chroma_format_v := bit_v;	 
	  
        readline(seqParam_file, line_v);
        read(line_v, bit_v);  
        weighted_pred_flag_v := bit_v;
	  
	    readline(seqParam_file, line_v);
        read(line_v, bit_v);  
        weighted_bipred_flag_v := bit_v; 
	  
	    readline(seqParam_file, line_v);
        read(line_v, integer_v);  
        weight_denom_v.luma_log_weight_denom := std_logic_vector(to_unsigned(integer_v, weight_denom_v.luma_log_weight_denom'length));  
	  
        readline(seqParam_file, line_v);
        read(line_v, integer_v);  
        weight_denom_v.chroma_log_weight_denom := std_logic_vector(to_unsigned(integer_v, weight_denom_v.chroma_log_weight_denom'length)); 
    
      end if;
    end if;
  
    read_info_v.prediction_parameter_dav := '0';
    read_info_v.residuals_dav := '0';
    
      if (ready = '1' and internal_r.read_mv='0' and internal_r.read_samples='0') then
      internal_v.read_mv := '1';
      internal_v.read_samples := '1';
      
      if not endfile(ctu_file) then 
        readline(ctu_file, line_v);  
        read(line_v, string_v);    --CTU
        read(line_v, integer_v);   --posx
        read_info_v.ctu_position.position_x := std_logic_vector(to_unsigned(integer_v, read_info_v.ctu_position.position_x'length));
        read(line_v, integer_v);   --posy
        read_info_v.ctu_position.position_y := std_logic_vector(to_unsigned(integer_v, read_info_v.ctu_position.position_y'length)); 
        read(line_v, integer_v);   -- frame number - not used
				read(line_v, integer_v);   -- slice type
				read_info_v.slice_type := std_logic_vector(to_unsigned(integer_v, read_info_v.slice_type'length)); 
				readline(ctu_file, line_v);
        read(line_v, integer_v);   --cu64
        read_info_v.ctu_tree.cu_64 := std_logic_vector(to_unsigned(integer_v, read_info_v.ctu_tree.cu_64'length));   
        readline(ctu_file, line_v);
        for i in 0 to 3 loop 
        read(line_v, integer_v); --cu32
        read_info_v.ctu_tree.cu_32(i) := std_logic_vector(to_unsigned(integer_v, read_info_v.ctu_tree.cu_32(i)'length));    
        end loop;
        readline(ctu_file, line_v);
        for i in 0 to 15 loop 
        read(line_v, integer_v); --cu16
        read_info_v.ctu_tree.cu_16(i) := std_logic_vector(to_unsigned(integer_v, read_info_v.ctu_tree.cu_16(i)'length));    
        end loop; 
        readline(ctu_file, line_v);
        for i in 0 to 63 loop 
        read(line_v, integer_v); --cu8
        read_info_v.ctu_tree.cu_8(i) := std_logic_vector(to_unsigned(integer_v, read_info_v.ctu_tree.cu_8(i)'length));    
        end loop;
        end if;
      end if;  
    
    if (internal_r.read_mv = '1' and not endfile(mv_file)) then 
      readline(mv_file, line_v);
      read(line_v, string_v);   --MV:
      if (string_v = "END") then
        internal_v.read_mv := '0';
      else 
        read_info_v.prediction_parameter_dav := '1';
        read(line_v, bit_v);      -- inter prediction flag
        read_info_v.prediction_parameter(0) := bit_v;
				if (bit_v = '1') then
	        read(line_v, bit_v);      -- biprediction flag
	        read_info_v.prediction_parameter(1) := bit_v;
	        read(line_v, integer_v);  -- MV x
	        read_info_v.prediction_parameter(LOG_MOTION_VECTOR+1 downto 2) := std_logic_vector(to_signed(integer_v, LOG_MOTION_VECTOR));    
	        read(line_v, integer_v);  -- MV y
	        read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+1 downto LOG_MOTION_VECTOR+2) := std_logic_vector(to_signed(integer_v, LOG_MOTION_VECTOR));
	        read(line_v, integer_v);  -- referenced frame id
	        read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+1 downto LOG_MOTION_VECTOR*2+2) := std_logic_vector(to_unsigned(integer_v, LOG_REFERENCED_FRAME_ID));
	        read(line_v, integer_v);  -- luma weight
	        read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+LOG_WEIGHT+1 downto LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+2) := std_logic_vector(to_signed(integer_v, LOG_WEIGHT));
	        read(line_v, integer_v);  -- luma offset
	        read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+LOG_WEIGHT+LOG_OFFSET+1 downto LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+LOG_WEIGHT+2) := std_logic_vector(to_signed(integer_v, LOG_OFFSET));	
	        read(line_v, integer_v);  -- chroma cb weight      
					read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+2*LOG_WEIGHT+LOG_OFFSET+1 downto LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+LOG_WEIGHT+LOG_OFFSET+2) := std_logic_vector(to_signed(integer_v, LOG_WEIGHT));
	        read(line_v, integer_v);  -- chroma cb offset
	        read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+2*LOG_WEIGHT+2*LOG_OFFSET+1 downto LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+2*LOG_WEIGHT+LOG_OFFSET+2) := std_logic_vector(to_signed(integer_v, LOG_OFFSET));
	        read(line_v, integer_v);  -- chroma cr weight      
					read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+3*LOG_WEIGHT+2*LOG_OFFSET+1 downto LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+2*LOG_WEIGHT+2*LOG_OFFSET+2) := std_logic_vector(to_signed(integer_v, LOG_WEIGHT));
	        read(line_v, integer_v);  -- chroma cr offset
	        read_info_v.prediction_parameter(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+3*LOG_WEIGHT+3*LOG_OFFSET+1 downto LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+3*LOG_WEIGHT+2*LOG_OFFSET+2) := std_logic_vector(to_signed(integer_v, LOG_OFFSET));	
				else
					read(line_v, integer_v);  -- chromaPredMode
					read_info_v.prediction_parameter(LOG_INTRA_CHROMA_PRED_MODE downto 1) := std_logic_vector(to_signed(integer_v, LOG_INTRA_CHROMA_PRED_MODE));  
	    		read(line_v, integer_v);  -- lumaPredMode_0 
					read_info_v.prediction_parameter(LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE downto LOG_INTRA_CHROMA_PRED_MODE+1) := std_logic_vector(to_signed(integer_v, LOG_INTRA_LUMA_PRED_MODE)); 
					read(line_v, integer_v);  -- lumaPredMode_1
					read_info_v.prediction_parameter(LOG_INTRA_LUMA_PRED_MODE*2+LOG_INTRA_CHROMA_PRED_MODE downto LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE+1) := std_logic_vector(to_signed(integer_v, LOG_INTRA_LUMA_PRED_MODE)); 
					read(line_v, integer_v);  -- lumaPredMode_2
					read_info_v.prediction_parameter(LOG_INTRA_LUMA_PRED_MODE*3+LOG_INTRA_CHROMA_PRED_MODE downto LOG_INTRA_LUMA_PRED_MODE*2+LOG_INTRA_CHROMA_PRED_MODE+1) := std_logic_vector(to_signed(integer_v, LOG_INTRA_LUMA_PRED_MODE)); 
					read(line_v, integer_v);  -- lumaPredMode_3
					read_info_v.prediction_parameter(LOG_INTRA_LUMA_PRED_MODE*4+LOG_INTRA_CHROMA_PRED_MODE downto LOG_INTRA_LUMA_PRED_MODE*3+LOG_INTRA_CHROMA_PRED_MODE+1) := std_logic_vector(to_signed(integer_v, LOG_INTRA_LUMA_PRED_MODE)); 
				end if;
			end if;
    end if;
    
    if (internal_r.read_samples = '1' and not endfile(res_file)) then
      readline(res_file, line_v);
      read(line_v, string_v);   --RES
      if (string_v = "END") then
        internal_v.read_samples := '0';
      else
        read_info_v.residuals_dav := '1';
        for i in 0 to 7 loop
          read(line_v, integer_v);   
          read_info_v.residuals(i) := std_logic_vector(to_signed(integer_v, read_info_v.residuals(i)'length));  
        end loop;
      end if;
    end if;
  end if;  
  
  internal_next             <= internal_v;
  read_info_next            <= read_info_v;
  chroma_format_next        <= chroma_format_v;
  weight_denom_next         <= weight_denom_v;
  weighted_pred_flag_next   <= weighted_pred_flag_v;
  weighted_bipred_flag_next <= weighted_bipred_flag_v;
end process;
    
   
   
--Main clk process.
PROC_CLK: process (clk, rst)
begin  
  if (rst = '0') then    
    internal_r.read_mv <= '0';
    internal_r.read_samples <= '0';
    read_info_r.ctu_position.position_x <= (others=>'0');
    read_info_r.ctu_position.position_y <= (others=>'0');  
	read_info_r.slice_type <= (others=>'0');
    read_info_r.ctu_tree.cu_64 <= (others=>'0');
    read_info_r.ctu_tree.cu_32 <= (others=>(others=>'0'));
    read_info_r.ctu_tree.cu_16 <= (others=>(others=>'0'));
    read_info_r.ctu_tree.cu_8 <= (others=>(others=>'0'));
    read_info_r.residuals_dav <= '0';
    read_info_r.residuals <= (others=>(others=>'0'));
    read_info_r.prediction_parameter_dav <= '0';
	read_info_r.prediction_parameter <= (others=>'0');
    chroma_format_r <= '1';
	weight_denom_r.luma_log_weight_denom <= (others=>'0');
    weight_denom_r.chroma_log_weight_denom <= (others=>'0');
    weighted_pred_flag_r <= '0';
	weighted_bipred_flag_r <= '0';
  elsif rising_edge(clk) then  
    internal_r <= internal_next;  
    read_info_r <= read_info_next; 
    chroma_format_r <= chroma_format_next;
	weight_denom_r <= weight_denom_next;
    weighted_pred_flag_r <= weighted_pred_flag_next;
	weighted_bipred_flag_r <= weighted_bipred_flag_next;
  end if;
end process;
    
end read_test_arch;
