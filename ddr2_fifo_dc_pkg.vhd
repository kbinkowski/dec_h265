library IEEE;
use IEEE.std_logic_1164.all;

package ddr2_fifo_dc_pkg is
component ddr2_fifo_dc is
  generic 
  (
    BIT_WIDTH        : integer := 8;
    ADDR_WIDTH       : integer := 10;
    LP_FULLNESS      : integer := 32;
    WARN_LENGTH      : integer := 20
  );
  port
  (   
    rst_n            : in   std_logic;
    
    w_clk            : in   std_logic;
    w_data           : in   std_logic_vector (BIT_WIDTH-1 downto 0);
    w_enable         : in   std_logic;
    r_clk            : in   std_logic;
    r_data           : out  std_logic_vector (BIT_WIDTH-1 downto 0);
    r_ack            : in   std_logic;
    empty            : out  std_logic;
    almost_empty     : out  std_logic;  
    half_full        : out  std_logic;  
    almost_full      : out  std_logic;
    full             : out  std_logic
  );
end component;
end;