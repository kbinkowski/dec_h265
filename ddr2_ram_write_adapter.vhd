-------------------------------------------------------------------------------
-- Title      : Module transforming data from smaller bit width into larger.
-- Project    : 
-------------------------------------------------------------------------------
-- File       : ddr2_ram_write_adapter.vhd
-- Author     :   <Roszkowski@STORK>
-- Company    : 
-- Created    : 2010-06-11
-- Last update: 2010-06-11
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: Module that multiplexes intput of one bit width into inputs
--              of larger bit width. It also transforms block widths, heights
--              and addresses appropriately (in most simple cases).
-------------------------------------------------------------------------------
-- Copyright (c) 2010 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2010-06-11  1.0      Roszkowski	Created
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

use work.ddr2_pkg.all;
use work.adapter_pkg.all;
use work.motion_compensation_dec_pkg.all;

entity ddr2_ram_write_adapter is
  port (
    clk               : in  std_logic;  -- clock
    rst_n             : in  std_logic;  -- reset (active low)
    dram_write_in     : in  WRITE_PORT_OUT;  -- things coming from dram controller
    dram_write_out    : out WRITE_PORT_IN;  -- things going to dram controller
    adapted_write_out : out WRITE_PORT_OUT;  -- things going to DRAM user
    adapted_write_in  : in  WRITE_PORT_IN_ADAPTER  -- things from DRAM user
    );
end ddr2_ram_write_adapter;

architecture rtl of ddr2_ram_write_adapter is
  -- thing below tells us how many times DRAM controller input bus is wider form our normal data bus
  constant MULTIPLY_FACTOR : natural := (ALTERA_DATA_WIDTH / DRAM_MEMORY_DATA_WIDTH);
  type RAM_INPUT_ARRAY_TYPE is array(MULTIPLY_FACTOR-2 downto 0) of std_logic_vector(DRAM_MEMORY_DATA_WIDTH-1 downto 0);

  type RAM_INPUT_CONTROLLER_TYPE is record
    data : RAM_INPUT_ARRAY_TYPE;        -- data to be stored into memory
    num_parts_valid : std_logic_vector(log2_ceil(MULTIPLY_FACTOR)-1 downto 0);  -- how many parts of data in data are valid
  end record;
  
  signal ram_input_r : RAM_INPUT_CONTROLLER_TYPE;  -- registers 
  signal ram_input_next : RAM_INPUT_CONTROLLER_TYPE;
begin  -- rtl


  COMB_PROC: process (adapted_write_in, dram_write_in, ram_input_r)
    variable dram_write_out_v    : WRITE_PORT_IN;
    variable adapted_write_out_v : WRITE_PORT_OUT;
    variable ram_input_next_v    : RAM_INPUT_CONTROLLER_TYPE;
    variable width_padding_v     : std_logic_vector(log2_ceil(MULTIPLY_FACTOR)-1 downto 0);
    variable write_in_data_v     : std_logic_vector(DRAM_MEMORY_DATA_WIDTH-1 downto 0);
  begin  -- process COMB_PROC
    width_padding_v := (others => '0');
    ram_input_next_v := ram_input_r;
    -- initialise memory
    -- pass some things unchanged
    dram_write_out_v.start := adapted_write_in.start;
    dram_write_out_v.hei   := adapted_write_in.hei;    
    -- all bit depth difference will be compensated using width - so cut it appropriately
    dram_write_out_v.wid := width_padding_v & adapted_write_in.wid(adapted_write_in.wid'high downto log2_ceil(MULTIPLY_FACTOR));
    dram_write_out_v.jum := width_padding_v & adapted_write_in.jum(adapted_write_in.wid'high downto log2_ceil(MULTIPLY_FACTOR));
    -- after cutting width related things it is also necessary to cut address
    dram_write_out_v.address := adapted_write_in.address(ALTERA_ADDRESS_WIDTH+ALTERA_ADDRESS_CORRECTION-1 downto ALTERA_ADDRESS_CORRECTION);
    -- and finally it is necessary to set select sigal when data buffer is full
    dram_write_out_v.sel   := '0';
    
    write_in_data_v := (others => '0');
    for i in 0 to DRAM_MEMORY_DATA_WIDTH/8-1 loop -- GP 
      write_in_data_v(7+8*i downto 8*i) := adapted_write_in.data(7+i*(8+DELTA_BIT_DEPTH) downto i*(8+DELTA_BIT_DEPTH));
    end loop;

    if adapted_write_in.sel = '1' then
      -- store data for further use
      if conv_integer(ram_input_r.num_parts_valid) = MULTIPLY_FACTOR-1 then
        -- all data blocks has been gathered - send them to DRAM controller
        dram_write_out_v.sel             := '1';
        ram_input_next_v.num_parts_valid := (others => '0');
      else
        -- patiently gather data
        ram_input_next_v.data(conv_integer(ram_input_r.num_parts_valid)) := write_in_data_v;--adapted_write_in.data; -- GP
        ram_input_next_v.num_parts_valid                                 := ram_input_r.num_parts_valid + 1;
      end if;
    end if;

    -- generate multiplexed DRAM data entry signal
    for i in 0 to ram_input_r.data'length-1 loop
      dram_write_out_v.data((I+1)*DRAM_MEMORY_DATA_WIDTH-1 downto I*DRAM_MEMORY_DATA_WIDTH) := ram_input_r.data(I);
    end loop;
    dram_write_out_v.data(MULTIPLY_FACTOR*DRAM_MEMORY_DATA_WIDTH-1 downto (MULTIPLY_FACTOR-1)*DRAM_MEMORY_DATA_WIDTH) := write_in_data_v;--adapted_write_in.data; --GP 
    
    
    adapted_write_out_v.ready := dram_write_in.ready;
    if dram_write_in.empty = '1' and ram_input_r.num_parts_valid = 0 then
      adapted_write_out_v.empty := '1';
    else
      adapted_write_out_v.empty := '0';
    end if;

    adapted_write_out_v.almost_empty := dram_write_in.almost_empty;
    adapted_write_out_v.almost_full := dram_write_in.almost_full;
    adapted_write_out_v.full := dram_write_in.full;
    
    dram_write_out <= dram_write_out_v;
    adapted_write_out <= adapted_write_out_v;
    ram_input_next <= ram_input_next_v;
  end process COMB_PROC;
  
  REG_PROC : process (clk, rst_n)
  begin  -- process REG_PROC 
    if rst_n = '0' then                 -- asynchronous reset (active low)
      for I in 0 to ram_input_r.data'length-1 loop
        ram_input_r.data(I) <= (others => '0');
      end loop;
      ram_input_r.num_parts_valid <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      
      ram_input_r <= ram_input_next;
    end if;
  end process REG_PROC ;

end rtl;
