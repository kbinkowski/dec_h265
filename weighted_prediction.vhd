library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.motion_compensation_dec_pkg.all;	   

entity weighted_prediction is  
  port(
  clk                  : in  std_logic;
  rst                  : in  std_logic;  --Reset, active LOW 
  
  weight0              : in  std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset0              : in  std_logic_vector(LOG_OFFSET-1 downto 0);
  weight1              : in  std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset1              : in  std_logic_vector(LOG_OFFSET-1 downto 0);
  
  log_weight_denom     : in  std_logic_vector(2 downto 0);
  weighted_pred_flag   : in  std_logic;  
  bi_pred_flag         : in  std_logic;	  -- when 0 we only use data with index 1, data with index 0 should be equal zero
  
  input_samples0       : in  INTERPOLATED_SAMPLES_TYPE;
  input_samples1       : in  INTERPOLATED_SAMPLES_TYPE;
  
  output_samples       : out DATA_SAMPLES_DEC_TYPE
  );
end weighted_prediction;   

architecture weighted_prediction_arch of weighted_prediction is 		  

type ADDITION_TYPE       is array (NUMBER_OF_SAMPLES_DEC-1 downto 0) of std_logic_vector(LOG_WEIGHT+LOG_INTERPOLATED_SAMPLE downto 0);
type ACCUMULATE_TYPE     is array (NUMBER_OF_SAMPLES_DEC-1 downto 0) of std_logic_vector(LOG_WEIGHT+LOG_INTERPOLATED_SAMPLE downto 0);
type MULTIPLICATION_TYPE is array (NUMBER_OF_SAMPLES_DEC-1 downto 0) of std_logic_vector(LOG_WEIGHT+LOG_INTERPOLATED_SAMPLE-1 downto 0);

type REG_TYPE is record
  tap0             : std_logic_vector(LOG_WEIGHT-1 downto 0);
  tap1             : std_logic_vector(LOG_WEIGHT-1 downto 0);
  samples0         : INTERPOLATED_SAMPLES_TYPE;
  samples1         : INTERPOLATED_SAMPLES_TYPE;
  multiplication0  : MULTIPLICATION_TYPE;
  multiplication1  : MULTIPLICATION_TYPE;
  addition         : ADDITION_TYPE;
  shifted_offset   : std_logic_vector(LOG_OFFSET+13 downto 0);
  accumulate       : ADDITION_TYPE; 
end record;

signal r           : REG_TYPE;
signal r_next      : REG_TYPE; 

begin
	
--Weighted Prediction process
PROC_WP: process (input_samples0, input_samples1, weight0, offset0, weight1, offset1, log_weight_denom, weighted_pred_flag, bi_pred_flag, r)
variable r_v                      : REG_TYPE;
variable temp_v                   : signed(LOG_WEIGHT+LOG_INTERPOLATED_SAMPLE downto 0);
variable output_samples_v         : DATA_SAMPLES_DEC_TYPE;

begin 

  r_v := r;
  
  r_v.tap0     := weight0;
  r_v.tap1     := weight1;
  r_v.samples0 := input_samples0;
  r_v.samples1 := input_samples1;
  
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    r_v.multiplication0(i) := std_logic_vector( resize( signed(r.tap0) * signed(r.samples0(i)), r_v.multiplication0(i)'length)); 
	r_v.multiplication1(i) := std_logic_vector( resize( signed(r.tap1) * signed(r.samples1(i)), r_v.multiplication1(i)'length)); 
	r_v.addition(i) := std_logic_vector( resize( signed(r_v.multiplication0(i)) + signed(r_v.multiplication1(i)), r_v.addition(i)'length));
  end loop;
    
  -- OFFSET_WD LUT: - default offset adding to eliminate rounding error 
  -- position: | 0 | 1 | 2 | 3 | 4 |  5 |  6 |  7 |   8 |   9 |  10 |   11 |   12 |   13 |
  --    value: | 0 | 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 | 512 | 1024 | 2048 | 4096 |
  
  if weighted_pred_flag = '0' then   -- Implicit Weighted Prediction 
    if bi_pred_flag = '0' then 
	
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        r_v.accumulate(i) := std_logic_vector( resize( signed(r.samples1(i)), r_v.accumulate(i)'length) + OFFSET_WD(6-DELTA_BIT_DEPTH));
		temp_v := resize( shift_right( signed(r.accumulate(i)), 6 - DELTA_BIT_DEPTH), temp_v'length);
		if temp_v > 2**(8+DELTA_BIT_DEPTH)-1 then -- over-range
          output_samples_v(i) := (others => '1');
        elsif temp_v < 0 then
          output_samples_v(i) := (others => '0'); 
        else
          output_samples_v(i) := std_logic_vector(temp_v(7+DELTA_BIT_DEPTH downto 0));
        end if;
      end loop;
	  
    else 
	
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        r_v.accumulate(i) := std_logic_vector( resize(signed(r.samples0(i)), r_v.accumulate(i)'length) + resize(signed(r.samples1(i)), r_v.accumulate(i)'length) + OFFSET_WD(7-DELTA_BIT_DEPTH) );
        temp_v := resize( shift_right( signed(r.accumulate(i)), 7 - DELTA_BIT_DEPTH), temp_v'length);
        if temp_v > 2**(8+DELTA_BIT_DEPTH)-1 then -- over-range
          output_samples_v(i) := (others => '1');
        elsif temp_v < 0 then
          output_samples_v(i) := (others => '0'); 
        else
          output_samples_v(i) := std_logic_vector(temp_v(7+DELTA_BIT_DEPTH downto 0));
        end if;
      end loop;
	  
    end if;
  else                               -- Explicit Weighted Prediction
    if bi_pred_flag = '0' then 
	  
	  r_v.shifted_offset := std_logic_vector( shift_left( resize( signed(offset1), r_v.shifted_offset'length), to_integer(unsigned(log_weight_denom))+6) );
	  	
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        r_v.accumulate(i) := std_logic_vector( signed(r.addition(i)) + signed(r.shifted_offset) + OFFSET_WD(to_integer(unsigned(log_weight_denom))+6-DELTA_BIT_DEPTH) );
		temp_v := resize( shift_right( signed(r.accumulate(i)), to_integer(unsigned(log_weight_denom)) + 6 - DELTA_BIT_DEPTH), temp_v'length);
		if temp_v > 2**(8+DELTA_BIT_DEPTH)-1 then -- over-range
          output_samples_v(i) := (others => '1');
        elsif temp_v < 0 then
          output_samples_v(i) := (others => '0'); 
        else
          output_samples_v(i) := std_logic_vector(temp_v(7+DELTA_BIT_DEPTH downto 0));
        end if;
      end loop;
	  
	else
    
	  r_v.shifted_offset := std_logic_vector( shift_left( shift_left( resize( signed(offset0), r_v.shifted_offset'length), DELTA_BIT_DEPTH ) + shift_left( resize( signed(offset1), r_v.shifted_offset'length), DELTA_BIT_DEPTH ) + 1, to_integer(unsigned(log_weight_denom))+6-DELTA_BIT_DEPTH) );  
     
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        r_v.accumulate(i) := std_logic_vector( signed(r.addition(i)) + signed(r.shifted_offset) );
		temp_v := resize( shift_right( signed(r.accumulate(i)), to_integer(unsigned(log_weight_denom)) + 7 - DELTA_BIT_DEPTH), temp_v'length);
		if temp_v > 2**(8+DELTA_BIT_DEPTH)-1 then -- over-range
          output_samples_v(i) := (others => '1');
        elsif temp_v < 0 then
          output_samples_v(i) := (others => '0'); 
        else
          output_samples_v(i) := std_logic_vector(temp_v(7+DELTA_BIT_DEPTH downto 0));
        end if;
      end loop;	
	  
    end if;	 
	
  end if;	
 
  -- Assignment  
  r_next <= r_v;
  output_samples     <= output_samples_v; 
end process;



--Main clock process.
PROC_REG: process (clk, rst)
begin  
  if rst = '0' then
    r.tap0            <= (others=>'0');
    r.tap1            <= (others=>'0');
    r.samples0        <= (others=>(others=>'0'));
    r.samples1        <= (others=>(others=>'0')); 
    r.multiplication0 <= (others=>(others=>'0'));
    r.multiplication1 <= (others=>(others=>'0'));
    r.addition        <= (others=>(others=>'0'));
    r.shifted_offset  <= (others=>'0');
    r.accumulate      <= (others=>(others=>'0'));
  elsif rising_edge(clk) then
    r <= r_next;
  end if;
end process;



end weighted_prediction_arch;