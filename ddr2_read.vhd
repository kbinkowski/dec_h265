library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ddr2_pkg.all;
use work.ddr2_fifo_dc_pkg.all;	 
use work.motion_compensation_dec_pkg.all;

entity ddr2_read is
	generic (
		FIFO_LENGTH          : natural := 128;
		ACCESS_CYCLE_LENGTH  : natural := 64
	); port (
		clk_altera      : in std_logic;
		clk_port        : in std_logic;
		rst_n           : in std_logic;
 		local_init_done : in  std_logic;    

		enc_i           : in READ_PORT_IN;
		enc_o           : out READ_PORT_OUT;

		sch_i           : in SCHEDULER_OUT;
		sch_o           : out SCHEDULER_IN;
		
		ddr2_i          : in ALTERA_DDR2_OUT;
		ddr2_o          : out ALTERA_DDR2_IN
	);
end entity;

architecture rtl of ddr2_read is
	--enumeration types
	--memory state machine states
	type STATE_TYPE_ALTERA is (READY, WAITING, DATA_READ, FINISHED, YIELD);
	--port state machine states
	type STATE_TYPE_ENCODER is (READY, BUSY, FINISHED);
	--scheduler output
	type SCHEDULER_TYPE is record
    	request                     : std_logic;
		done      	                : std_logic;
		device_ready                : std_logic;
	end record;
	
	--output registers
	signal scheduler_r            : SCHEDULER_TYPE;
	signal scheduler_next         : SCHEDULER_TYPE;
	signal encoder_ready_r        : std_logic;
	signal encoder_ready_next     : std_logic;
	signal controller_r           : ALTERA_DDR2_IN;
	signal controller_next        : ALTERA_DDR2_IN;

	--transaction parameters register
	signal address_base_r         : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
	signal address_base_next      : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
	signal address_jump_r         : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal address_jump_next      : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal width_m_r              : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal width_m_next           : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal height_m_r             : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal height_m_next          : std_logic_vector(PARAM_WIDTH-1 downto 0);
	
	--access cycle registers
	signal queued_width_r          : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal queued_width_next       : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal queued_height_r         : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal queued_height_next      : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal address_line_r          : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
	signal address_line_next       : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
	signal acc_req_r               : std_logic_vector(log2_ceil(FIFO_LENGTH) downto 0);
	signal acc_req_next            : std_logic_vector(log2_ceil(FIFO_LENGTH) downto 0);
	signal acc_r                   : std_logic_vector(log2_ceil(FIFO_LENGTH) downto 0);
	signal acc_next                : std_logic_vector(log2_ceil(FIFO_LENGTH) downto 0);
	signal width_r                 : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal width_next              : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal height_r                : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal height_next             : std_logic_vector(PARAM_WIDTH-1 downto 0);
	signal requests_finished_r     : std_logic;
	signal requests_finished_next  : std_logic;
	signal read_done_r             : std_logic;
	signal read_done_next          : std_logic;
	signal read_done_delay_r       : std_logic;
	signal read_done_delay_next    : std_logic;
	signal request_delay_r    : std_logic;
	signal request_delay_next : std_logic;
	
	--state machine state registers
	signal altera_state_r         : STATE_TYPE_ALTERA;
	signal altera_state_next      : STATE_TYPE_ALTERA;

	--fifo input register
	signal fifo_rst_r             : std_logic;
	signal fifo_rst_next          : std_logic;
	
	signal empty                  : std_logic;
	signal almost_empty           : std_logic;
	signal almost_empty_r         : std_logic;
	signal half_full              : std_logic;
	signal half_full_r            : std_logic;
	signal almost_full            : std_logic;
	signal full                   : std_logic;
	signal local_init_done_r      : std_logic;
	
begin

	--output assignment
	enc_o.ready <= encoder_ready_r and local_init_done_r;
	enc_o.empty <= empty;
	enc_o.almost_empty <= almost_empty;
	enc_o.half_full <= half_full;
	enc_o.almost_full <= almost_full;
	enc_o.full <= full;
	sch_o.request_lp <= scheduler_r.request;
	sch_o.request_np <= scheduler_r.request when almost_empty_r = '1' else '0';-- bag poprawiony 07.05.2011: almost_empty -> almost_empty_r 
	sch_o.request_hp <= '0';
	sch_o.done <= scheduler_r.done;
	sch_o.device_ready <= scheduler_r.device_ready;
	ddr2_o <= controller_r;
	
	--inter-domain communication
	request_delay_next <= not encoder_ready_r;--request_r;
	read_done_delay_next <= read_done_r;


	--fifo port assignment
	FIFO: ddr2_fifo_dc generic map (
		BIT_WIDTH => ALTERA_DATA_WIDTH, 
		ADDR_WIDTH => log2_ceil(FIFO_LENGTH),
		LP_FULLNESS => ACCESS_CYCLE_LENGTH,
		WARN_LENGTH => 2*log2_ceil(FIFO_LENGTH) 
	) port map (
		rst_n => fifo_rst_r, 
		
		w_clk => clk_altera,
		w_data => ddr2_i.rdata,
		w_enable => ddr2_i.rdata_valid,
		
		r_clk => clk_port,
		r_data => enc_o.data,
		r_ack => enc_i.sel,
		
		empty => empty,
		almost_empty => almost_empty,
		half_full => half_full,
		almost_full => almost_full,
		full => full
	);

	--memory state machine
	PROC_CLK_RST_ALTERA: process(clk_altera, rst_n)
	begin
		if rst_n = '0' then
			altera_state_r <= READY;
			requests_finished_r <= '0';
			controller_r.addr <= (others => '0');
			controller_r.be <= (others => '1');
			controller_r.size <= conv_std_logic_vector(ALTERA_MAX_BURST, ALTERA_BURST_WIDTH);
			controller_r.wdata <= (others => '0');
			controller_r.read_req <= '0';
			controller_r.write_req <= '0';
			scheduler_r.request <= '0';
			scheduler_r.done <= '0';
			scheduler_r.device_ready <= '0';
			address_line_r <= (others => '0'); 
			acc_req_r <= (others => '0'); 
			width_r <= (others => '0');
			height_r <= (others => '0');  
			read_done_r <= '0';
			request_delay_r <= '0';
			half_full_r <= '0';			
			almost_empty_r <= '0';
		elsif rising_edge(clk_altera) then
			altera_state_r <= altera_state_next;
			requests_finished_r <= requests_finished_next;
			controller_r <= controller_next;
			scheduler_r <= scheduler_next;
			address_line_r <= address_line_next;
			acc_req_r <= acc_req_next;
			acc_r <= acc_next;
			width_r <= width_next;
			height_r <= height_next;
			read_done_r <= read_done_next;
			request_delay_r <= request_delay_next;
			half_full_r <= half_full;
			almost_empty_r <= almost_empty;
		end if;
	end process;

	--port state machine
	PROC_CLK_RST_PORT: process(clk_port, rst_n)
	begin
		if rst_n = '0' then
			encoder_ready_r <= '1';
			address_base_r <= (others => '0');
			address_jump_r <= (others => '0');
			fifo_rst_r <= '0';
			queued_width_r <= (others => '0');
			queued_height_r <= (others => '0');
			width_m_r <= (others => '0');
			height_m_r <= (others => '0');
			read_done_delay_r <= '0';
			local_init_done_r <= '0';
		elsif rising_edge(clk_port) then
			encoder_ready_r <= encoder_ready_next;
			address_base_r <= address_base_next;
			address_jump_r <= address_jump_next;
			fifo_rst_r <= fifo_rst_next;
			queued_width_r <= queued_width_next;
			queued_height_r <= queued_height_next;
			width_m_r <= width_m_next;
			height_m_r <= height_m_next;
			read_done_delay_r <= read_done_delay_next;
			local_init_done_r <= local_init_done;
		end if;
	end process;

	--port state machine process
	PROC_PORT: process(encoder_ready_r, enc_i, altera_state_r, address_base_r, address_jump_r, width_m_r, height_m_r, 
	acc_r, acc_req_r, address_line_r, width_r, read_done_delay_r, empty, queued_width_r, queued_height_r)
		--total number of written blocks
		variable queued_width_v      : std_logic_vector(PARAM_WIDTH-1 downto 0);
		variable queued_height_v     : std_logic_vector(PARAM_WIDTH-1 downto 0);
		--base address and jump offset
		variable address_base_v     : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
		variable address_jump_v     : std_logic_vector(PARAM_WIDTH-1 downto 0);
		--current address, width and height counters
		variable width_v            : std_logic_vector(PARAM_WIDTH-1 downto 0);
		variable height_v           : std_logic_vector(PARAM_WIDTH-1 downto 0);
		--state machine and output variables
		variable encoder_ready_v    : std_logic;
		variable fifo_rst_v         : std_logic;
	begin
		address_base_v := address_base_r;
		address_jump_v := address_jump_r;
		width_v := width_m_r;
		height_v := height_m_r;
		fifo_rst_v := '1';
		encoder_ready_v := encoder_ready_r;
		queued_width_v := queued_width_r;
		queued_height_v := queued_height_r;

    if encoder_ready_r = '1' then
			--waiting for read request                
				queued_width_v := (others => '0');
				queued_height_v := (others => '0');
				address_base_v := enc_i.address;
				address_jump_v := enc_i.jum;
				width_v := enc_i.wid;
				height_v := enc_i.hei;
				if enc_i.start = '1' then
					encoder_ready_v := '0';
					fifo_rst_v := '0';
				end if;
			else
				--fifo_rst_v := '1';
--				if enc_i.sel = '1' and empty = '0' then
				if enc_i.sel = '1' then
					queued_width_v := queued_width_v + 1;
					if queued_width_v = width_v then
						queued_width_v := (others => '0');
						queued_height_v := queued_height_v + 1;
    				if queued_height_v = height_v then
    					encoder_ready_v := '1';
    				end if;
					end if;
				end if;
  		end if;
	
		encoder_ready_next <= encoder_ready_v;
		fifo_rst_next <= fifo_rst_v;
		address_base_next <= address_base_v;
		address_jump_next <= address_jump_v;
		width_m_next <= width_v;
		height_m_next <= height_v;
		queued_width_next <= queued_width_v;
		queued_height_next <= queued_height_v;
		
	end process;

--memory state machine process
PROC_ALTERA: process(altera_state_r, scheduler_r, controller_r, sch_i, ddr2_i, address_base_r, 
request_delay_r, acc_r, acc_req_r, address_line_r, width_r, height_r, width_m_r, height_m_r, address_jump_r, 
requests_finished_r, half_full_r, read_done_r, read_done_delay_r)
		constant ACC_MAX_C           : std_logic_vector(log2_ceil(FIFO_LENGTH)-1 downto 0) := conv_std_logic_vector(ACCESS_CYCLE_LENGTH , log2_ceil(FIFO_LENGTH));
		variable state_v             : STATE_TYPE_ALTERA;
		variable scheduler_v         : SCHEDULER_TYPE;
		variable controller_v        : ALTERA_DDR2_IN;
		variable acc_v               : std_logic_vector(log2_ceil(FIFO_LENGTH) downto 0);
		variable acc_incr_v          : std_logic_vector(log2_ceil(FIFO_LENGTH) downto 0);
		variable acc_req_v           : std_logic_vector(log2_ceil(FIFO_LENGTH) downto 0);
		variable address_line_v      : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
		variable width_v             : std_logic_vector(PARAM_WIDTH-1 downto 0);
		variable height_v            : std_logic_vector(PARAM_WIDTH-1 downto 0);
		variable requests_finished_v : std_logic; 
		variable read_done_v         : std_logic;
		--variable size_incr_v         : 
		
		
	begin
		state_v := altera_state_r;
		scheduler_v := scheduler_r;
		controller_v := controller_r;
		acc_v := acc_r;
		acc_req_v := acc_req_r;
		address_line_v := address_line_r;
		width_v := width_r;
		height_v := height_r;
		requests_finished_v := requests_finished_r;
		read_done_v := read_done_r and not read_done_delay_r;
		scheduler_v.done := '0';
		
		acc_incr_v := acc_r + 1;
		if ddr2_i.rdata_valid = '1' then
			--prepare data to be read
			acc_v := acc_incr_v;
		end if;	
			
		case altera_state_r is
			--waiting for read request
			when READY =>
				scheduler_v.device_ready := '1';
				state_v := READY;
				acc_v := (others => '0');
				acc_req_v := (others => '0');
				width_v := (others => '0');
				height_v := (others => '0');
				controller_v.addr := address_base_r;
				controller_v.size := conv_std_logic_vector(ALTERA_MAX_BURST, ALTERA_BURST_WIDTH);
				address_line_v := address_base_r;
				if request_delay_r = '1' then
					controller_v.read_req := '0';
					scheduler_v.request := '1';
					requests_finished_v := '0';
				end if;
				if ddr2_i.ready = '1' then -- sch_i.enable = '1' and 
					state_v := DATA_READ;
				end if;

			when DATA_READ =>
				state_v := DATA_READ;
				scheduler_v.request := '1';
				controller_v.read_req := '0';

				if controller_r.read_req = '1' and ddr2_i.ready = '1' then
					--request is being sent in current cycle, increment address for the next cycle
					acc_req_v := acc_req_r + controller_r.size;
					width_v := width_r + controller_r.size;
					controller_v.addr := controller_r.addr + controller_r.size;
--					if ACC_MAX_C - acc_req_v < width_m_r - width_v then
					if ACC_MAX_C - acc_req_r < width_m_r - width_r then
--						if ACC_MAX_C - acc_req_v > ALTERA_MAX_BURST + 2 then
						if ACC_MAX_C - ALTERA_MAX_BURST - 2 > acc_req_v then
							controller_v.size := conv_std_logic_vector(ALTERA_MAX_BURST, ALTERA_BURST_WIDTH);
						else
							controller_v.size := conv_std_logic_vector(1, ALTERA_BURST_WIDTH);
						end if;
					else
--						if width_m_r - width_v > ALTERA_MAX_BURST + 2 then
						if width_m_r - ALTERA_MAX_BURST - 2 > width_v then
							controller_v.size := conv_std_logic_vector(ALTERA_MAX_BURST, ALTERA_BURST_WIDTH);
						else
							controller_v.size := conv_std_logic_vector(1, ALTERA_BURST_WIDTH);
						end if;
					end if;					
					
					if width_v = width_m_r then
--					if width_r = width_m_r - controller_r.size then
						--new line
						controller_v.addr := address_line_r + address_jump_r;
						address_line_v := address_line_r + address_jump_r;
						height_v := height_r + 1;
						width_v := (others => '0');
						if height_v = height_m_r then
							--transaction almost finished, wait until all data is written
							requests_finished_v := '1';
						end if;
					end if;
				end if;
----------------------------------------------------------------------				
--				if requests_finished_r = '1' and acc_v = acc_req_v then
				if requests_finished_r = '1' and acc_r = acc_req_r then
					--transaction completed
					state_v := FINISHED;
					scheduler_v.done := '1';
				elsif acc_r = ACC_MAX_C then -- ??? acc_v = ACC_MAX_C
					--yield controller for other devices
					state_v := YIELD;
					scheduler_v.done := '1';
				end if;
				
				if requests_finished_v = '0' and acc_req_v < ACC_MAX_C then -- and acc_req_v /= ACC_MAX_C + ALTERA_BURST_WIDTH ??
					--request write in the next cycle
					controller_v.read_req := '1';
				end if;
				
			when YIELD =>
				state_v := YIELD;
				scheduler_v.request := '0';--?????!!!!!!!!!!!!
				if half_full_r = '0' then
					state_v := WAITING;
				end if;				
				
			--waiting for memory access
			when WAITING =>
				state_v := WAITING;
				controller_v.read_req := '0';
				scheduler_v.request := '1';
				acc_v := (others => '0');
				acc_req_v := (others => '0');
				if ddr2_i.ready = '1' then -- sch_i.enable = '1' and 
					state_v := DATA_READ;
				end if;

			when FINISHED =>
				state_v := FINISHED;
				scheduler_v.request := '0';
				controller_v.read_req := '0';
				if request_delay_r = '0' then
					state_v := READY;
				end if;
				read_done_v := '1';
		end case;
		
		read_done_next <= read_done_v;
		altera_state_next <= state_v;
		scheduler_next <= scheduler_v;
		controller_next <= controller_v;
		acc_next <= acc_v;
		acc_req_next <= acc_req_v;
		address_line_next <= address_line_v;
		width_next <= width_v;
		height_next <= height_v;
		requests_finished_next <= requests_finished_v;
		
	end process;
end rtl;