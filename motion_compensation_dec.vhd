-------------------------------------------------------------------------------
--                                                                           --
-- Title       : MOTION COMPENSATION DECODER - H.265/HEVC                    --
-- Design      : the H.265/HEVC decoder                                      --
-- Author      : Kamil Binkowski, University of Technology in Warsaw         --
-- Start Date  : 02.03.2013                                                  --
--                                                                           --
-------------------------------------------------------------------------------  
--                                                                           --
-- File        : motion_compensation_dec.vhd                                 --
-- Generated   : Sun Mar 10 00:10:58 2013                                    --
-- From        : interface description file                                  --
-- By          : Itf2Vhdl ver. 1.22                                          --
--                                                                           --
-------------------------------------------------------------------------------
-- Description: MOTION COMPENSATION module from H.265/HEVC decoder.          --
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use work.motion_compensation_dec_pkg.all; 
use work.ram_pkg.all;  
use work.fifo_dc_pkg.all;	
use work.interpolator_pkg.INPUT_OUTPUT_ROW_TYPE;
use work.interpolator_pkg.OUTPUT_ROW_TYPE;
use work.interpolator_pkg.CODING_UNIT_SIZE;   
use work.intra_dec_pkg.SET_OF_4_SAMPLES_TYPE;
use work.intra_dec_pkg.PU_32X32;
use work.intra_dec_pkg.PU_16X16;
use work.intra_dec_pkg.PU_8X8;
use work.intra_dec_pkg.PU_4X4;
use work.intra_dec_pkg.MAX_PIC_WIDTH_LOG2;
use work.intra_dec_pkg.MAX_PIC_HEIGHT_LOG2;




entity motion_compensation_dec is  
  port(
  -- -------------------------------------------------------------------------
  --                         CONTROL SIGNALS                                --
  -- ------------------------------------------------------------------------- 
  clk                      : in  std_logic;
  rst                      : in  std_logic; --Reset, active LOW
  start                    : in  std_logic;
  -- -------------------------------------------------------------------------
  --               INPUT DATA INTERFACE - CURRENT CODING_TREE_UNIT          --
  -- -------------------------------------------------------------------------
  input_data               : in INPUT_DATA_TYPE; 
  -- Sequence parameters
  chroma_format            : in std_logic; -- '0' when 4:2:2
                                           -- '1' when 4:2:0  
  weight_denom             : in WEIGHT_DENOM_TYPE; 
  weighted_pred_flag       : in std_logic;
  weighted_bipred_flag     : in std_logic;
  -- -------------------------------------------------------------------------
  --              OUTPUT DATA INTERFACE - CURRENT CODING_UNIT_TREE          --
  -- ------------------------------------------------------------------------- 
  output_data              : out OUTPUT_DATA_TYPE;             
  -- -------------------------------------------------------------------------
  --               DDR2 ADAPTER - REFERENCE FRAME INTERFACE                 --
  -- -------------------------------------------------------------------------
  referenced_data_port_in  : out REF_PORT_IN_TYPE;
  referenced_data_port_out : in  REF_PORT_OUT_TYPE
);
end motion_compensation_dec;



architecture motion_compensation_dec_arch of motion_compensation_dec is

  -- -------------------------------------------------------------------------
  --                                TYPES                                   --
  -- -------------------------------------------------------------------------

-- MACHINE STATES types
type STATE_FSM1_TYPE     is (IDLE, CHECK, WORK_TREE, WORK_BLOCK);
type STATE_FSM2_TYPE     is (IDLE, CHECK, INTER, INTRA);
type STATE_FSM3_TYPE     is (IDLE, WORK, BUSY);

-- TYPE of registers CTU's data
type CTU_TREE_ARRAY      is array(1 downto 0) of CTU_TREE_TYPE;  
type CTU_POSITION_ARRAY  is array(1 downto 0) of CTU_POSITION_TYPE; 

-- Support types
type RESIDUALS_WRITE_ADDR_TYPE            is array(3 downto 0) of std_logic_vector(8 downto 0);                               -- RESIDUALS_WRITE_ADDR_FSM_TYPE + one pingpong bit for each vector
type RESIDUALS_WRITE_ADDR_FSM_TYPE        is array(3 downto 0) of std_logic_vector(7 downto 0);
type RESIDUALS_READ_TYPE                  is array(15 downto 0) of std_logic_vector(2*(9+DELTA_BIT_DEPTH)-1 downto 0);
type RESIDUAL_READ_ADDR_TYPE              is array(1 downto 0) of std_logic_vector(LOG_ADDR_RESIDUAL-1 downto 0);             -- RESIDUAL_READ_ADDR_FSM_TYPE + one pingpong bit for each vector 
type RESIDUAL_READ_ADDR_FSM_TYPE          is array(1 downto 0) of std_logic_vector(LOG_ADDR_RESIDUAL-2 downto 0); 
type RECONSTRUCTED_SAMPLES_ADDR_TYPE      is array(1 downto 0) of std_logic_vector(LOG_ADDR_RECONSTRUCTED_SAMPLE-1 downto 0);
type RECONSTRUCTED_SAMPLES_ADDR_FSM_TYPE  is array(1 downto 0) of std_logic_vector(LOG_ADDR_RECONSTRUCTED_SAMPLE-2 downto 0); -- RECONSTRUCTED_SAMPLES_ADDR_FSM_TYPE + one pingpong bit for each vector 

-- FSM types:

type INTERNAL_FSM1_TYPE is record
  state                          : STATE_FSM1_TYPE;
  pingpong                       : std_logic;  
  -- Prediction information address
  prediction_parameter_addr      : std_logic_vector(LOG_ADDR_PREDICTION_PARAMETER-2 downto 0); 
  -- Counters and pointers
  cnt32                          : std_logic_vector(1 downto 0); -- indicates CU 32x32 within CU 64x64 
  cnt16                          : std_logic_vector(1 downto 0); -- indicates CU 16x16 within CU 32x32 
  cnt8                           : std_logic_vector(1 downto 0); -- indicates CU   8x8 within CU 16x16 
  cnt4                           : std_logic_vector(1 downto 0); -- indicates PU   4x4 within CU   8x8, used only for intra prediction for luma blocks
  intra4x4                       : std_logic;                    -- indicates intra prediction mode for 4x4 blocks 
  bi_pred_block_ptr              : std_logic;                    -- indicates the Referenced Block during bidirectional prediction
  cu_partition_ptr               : std_logic;                    -- indicates the Prediction Unit within Coding Unit (partition of CU) 
  -- Prediction Unit information
  chroma                         : std_logic_vector(1 downto 0);
  pu_position_x                  : std_logic_vector(5 downto 0); 
  pu_position_y                  : std_logic_vector(5 downto 0); 
  pu_size_x                      : std_logic_vector(6 downto 0); 
  pu_size_y                      : std_logic_vector(6 downto 0);
  -- Support of External Memory DDR2 containing Referenced Data
  referenced_data_port_start     : std_logic;
  -- Support of internal FIFO_ORDER
  fifo_order_en                  : std_logic;
end record;
   
type INTERNAL_FSM2_TYPE is record
  state                          : STATE_FSM2_TYPE;
  pingpong                       : std_logic; 
  chroma                         : std_logic_vector(1 downto 0);
  -- Residual samples
  residuals_addr                 : RESIDUAL_READ_ADDR_FSM_TYPE;
  residuals_memory_ptr           : std_logic_vector(1 downto 0);  -- indicates in which CU 32x32 is current PU ("00" top-left, "01" top_right, "10" bottom_left, "11" bottom_right)
  swap_samples_flag              : std_logic;                     -- indicates whether predicted samples should be reverse by couples before adding to residuals and saving in memory, 1 and 2 reverse with 3 and 4 samples 
  residual_valid_ptr             : std_logic;                     -- pointer indicates which residual form internal memory is used to reconstruct sample in this cycle (we read two residuals from each memory residuals_BUFF)
  -- Counters
  residuals_x_cnt                : std_logic_vector(6 downto 0);  -- support of reading residuals -- number of pixel in width
  residuals_y_cnt                : std_logic_vector(4 downto 0);  -- support of reading residuals -- number of rows (contains 4 pixels) in height
  referenced_samples_x_cnt       : std_logic_vector(6 downto 0);  -- support of reading referenced samples -- number of pixel in width
  referenced_samples_y_cnt       : std_logic_vector(4 downto 0);  -- support of reading referenced samples -- number of rows (contains  4 pixels) in height
  reconstructed_samples_x_cnt    : std_logic_vector(6 downto 0);  -- support of saving reconstructed samples -- number of pixel in width
  reconstructed_samples_y_cnt    : std_logic_vector(4 downto 0);  -- support of saving reconstructed samples -- number of rows (contains  4 pixels) in height
  -- Interpolator  settings
  interpolator_start             : std_logic;
  referenced_samples_dav         : std_logic; 
  --FIFO order 
  fifo_order_ack                 : std_logic;  
  -- Weighted prediction
  weighted_prediction_parameter_addr : std_logic_vector(LOG_ADDR_WEIGHTED_PREDICTION_PARAMETER-2 downto 0); 
  -- Bidirectional prediction
  bi_pred_block_ptr              : std_logic;
  bi_pred_delay_read_addr        : std_logic_vector(9 downto 0);    
  bi_pred_delay_write_addr       : std_logic_vector(9 downto 0);
  -- Reconstructed samples 
  reconstructed_samples_write_en    : std_logic_vector(1 downto 0);
  reconstructed_samples_write_addr  : RECONSTRUCTED_SAMPLES_ADDR_FSM_TYPE; 
  -- reconstructed_samples_read_addr   : std_logic_vector(LOG_ADDR_RECONSTRUCTED_SAMPLE-2 downto 0); 
  -- Intra prediction decoder
  intra_decoder_start               : std_logic;
  intra_predicted_samples_ready     : std_logic;
  intra_reconstructed_samples_x_cnt : std_logic_vector(6 downto 0);
  intra_reconstructed_samples_y_cnt : std_logic_vector(4 downto 0);
  intra_reconstructed_samples_dav   : std_logic;
end record; 

type INTERNAL_FSM3_TYPE is record
  state                          : STATE_FSM3_TYPE;
  -- Ping_pong parameters
  pingpong_dav                   : std_logic_vector(1 downto 0);  -- informs which data in pingpong architecture is valid at the moment 
  pingpong                       : std_logic;                     
  -- Output flags
  ready                          : std_logic;                     -- informs that FSM is ready to save next CODING_TREE_UNIT's data 
  -- Residual samples
  residuals_write_en             : std_logic_vector(3 downto 0);
  residuals_addr                 : RESIDUALS_WRITE_ADDR_FSM_TYPE;
  -- Prediction Information
  prediction_parameter_addr      : std_logic_vector(LOG_ADDR_PREDICTION_PARAMETER-2 downto 0);      
  weighted_prediction_parameter_addr : std_logic_vector(LOG_ADDR_WEIGHTED_PREDICTION_PARAMETER-2 downto 0); 
  -- Reconstructed Samples
  reconstructed_samples_addr     : std_logic_vector(LOG_ADDR_RECONSTRUCTED_SAMPLE-2 downto 0);
  reconstructed_samples_dav      : std_logic;   
  -- Counter 
  output_ctu_valid_cnt           : std_logic_vector(1 downto 0);  -- used to set delay between first valid CTU in input and output, when third CTU is read then first CTU's reconstructed data is in the output, used only at the start of module work
end record;

-- Other types:

type fifo_order_settings_TYPE is record 
  full         : std_logic;
  almost_full  : std_logic;
  empty        : std_logic;
  almost_empty : std_logic;
  data_in      : std_logic_vector(log2_ceil(NUMBER_OF_SAMPLES_DEC) + 36 downto 0); 
  data_out     : std_logic_vector(log2_ceil(NUMBER_OF_SAMPLES_DEC) + 36 downto 0); 
end record;

type FIFO_DATA_READ_TYPE is record
  -- Actual pingpong pointer related to CodingTreeUnit from which is actual PredictionBlock
  pingpong                      : std_logic;
  -- PredictionBlock information
  pb_position_x                 : std_logic_vector(5 downto 0);
  pb_position_y                 : std_logic_vector(5 downto 0);
  pb_size_x                     : std_logic_vector(6 downto 0);
  pb_size_y                     : std_logic_vector(6 downto 0);
  -- Chroma information
  chroma                        : std_logic_vector(1 downto 0); -- "0-" -> luma, "10" -> Cb, "11" -> Cr 
  -- Type of prediction
  inter_pred_flag               : std_logic;                    -- if equal 1 then inter prediction else intra prediction 
  -- Inter prediction interface:
  -- Bidirectional prediction
  bi_pred_flag                  : std_logic;                    -- informs whether PU is predicted in bidirectional prediction mode
  -- motion_vector information
  motion_vector_fract_x         : std_logic_vector(2 downto 0);
  motion_vector_fract_y         : std_logic_vector(2 downto 0);
  -- Pointer to first valid referenced samples
  referenced_sample_valid_ptr   : std_logic_vector(log2_ceil(NUMBER_OF_SAMPLES_DEC)-1 downto 0);
  -- Intra prediction interface:
  intra_pred_mode               : std_logic_vector(LOG_INTRA_PRED_MODE-1 downto 0);
end record;  

type PU_PARAMETER_TYPE is record
  -- Prediction Block information
  chroma                    : std_logic_vector(1 downto 0);
  pb_size_x                 : std_logic_vector(6 downto 0);
  pb_size_y                 : std_logic_vector(6 downto 0);
  round_pb_size_y           : std_logic_vector(6 downto 0); -- used in specific situation when block hight is not multiple of 4 (NUMBER_OF_SAMPLES)
  pb_position_x             : std_logic_vector(5 downto 0);
  pb_position_y             : std_logic_vector(5 downto 0);
  -- Prediction parameters
  inter_pred_flag           : std_logic;
	-- Inter prediction interface
	bi_pred_flag              : std_logic;
  weighted_pred_flag        : std_logic;
  weight0                   : std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset0                   : std_logic_vector(LOG_OFFSET-1 downto 0);
  weight1                   : std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset1                   : std_logic_vector(LOG_OFFSET-1 downto 0);
  log_weight_denom          : std_logic_vector(2 downto 0);
  -- Interpolator settings
  motion_vector_fract_x     : std_logic_vector(2 downto 0);
  motion_vector_fract_y     : std_logic_vector(2 downto 0);	
  -- Intra prediction interface
  intra_pred_mode           : std_logic_vector(LOG_INTRA_PRED_MODE-1 downto 0);
  pu_avail                  : std_logic_vector(LOG_PU_AVAIL-1 downto 0);
--  pu_addr_x                 : std_logic_vector(LOG_FRAME_SIZE_X - 1 downto 0);
--  pu_addr_y                 : std_logic_vector(LOG_FRAME_SIZE_Y - 1 downto 0);
  pu_size_idx               : std_logic_vector(1 downto 0);
end record; 	
	
type PU_PARAMETER_REGISTER_TYPE is record
  -- Prediction Block information
  chroma                    : std_logic_vector(1 downto 0);
  pb_size_x                 : std_logic_vector(6 downto 0);
  pb_size_y                 : std_logic_vector(6 downto 0);
  round_pb_size_y           : std_logic_vector(6 downto 0); -- used in specific situation when block hight is not multiple of 4 (NUMBER_OF_SAMPLES)
  pb_position_x             : std_logic_vector(5 downto 0);
  pb_position_y             : std_logic_vector(5 downto 0);
  -- Prediction parameters
  inter_pred_flag           : std_logic;  
end record; 

type PREDICTION_INFORMATION_TYPE is record   
  inter_pred_flag          : std_logic;
  bi_pred_flag             : std_logic;
  motion_vector            : MOTION_VECTOR_TYPE;
  referenced_frame_id      : std_logic_vector(LOG_REFERENCED_FRAME_ID-1 downto 0);
  intra_chroma_pred_mode   : std_logic_vector(LOG_INTRA_CHROMA_PRED_MODE-1 downto 0);
  intra_luma_pred_mode_0   : std_logic_vector(LOG_INTRA_LUMA_PRED_MODE-1 downto 0);   
  intra_luma_pred_mode_1   : std_logic_vector(LOG_INTRA_LUMA_PRED_MODE-1 downto 0); 
  intra_luma_pred_mode_2   : std_logic_vector(LOG_INTRA_LUMA_PRED_MODE-1 downto 0);
  intra_luma_pred_mode_3   : std_logic_vector(LOG_INTRA_LUMA_PRED_MODE-1 downto 0);
end record;   

  -- -------------------------------------------------------------------------
  --                        SIGNALS AND REGISTERS                           --
  -- -------------------------------------------------------------------------

-- Residuals_Buffer signals 
signal residuals_write_en                    : std_logic_vector(3 downto 0);
signal residuals_write_addr                  : RESIDUALS_WRITE_ADDR_TYPE;
signal residuals_write                       : std_logic_vector(2*NUMBER_OF_SAMPLES_DEC*(9+DELTA_BIT_DEPTH)-1 downto 0); 
signal residuals_read_addr                   : RESIDUAL_READ_ADDR_TYPE;
signal residuals_read                        : RESIDUALS_READ_TYPE;
-- Residuals register  
signal residuals_r                           : RESIDUALS_TYPE;
signal residuals_next                        : RESIDUALS_TYPE;

-- Reconstructed_Samples_Buffer signals      
signal reconstructed_samples_write           : std_logic_vector(NUMBER_OF_SAMPLES_DEC*(8+DELTA_BIT_DEPTH)-1 downto 0);
signal reconstructed_samples_write_addr      : RECONSTRUCTED_SAMPLES_ADDR_TYPE; 
signal reconstructed_samples_write_en        : std_logic_vector(1 downto 0);
signal reconstructed_samples                 : SET_OF_4_SAMPLES_TYPE;
signal reconstructed_samples_read            : std_logic_vector(NUMBER_OF_SAMPLES_DEC*(8+DELTA_BIT_DEPTH)-1 downto 0);
signal reconstructed_samples_read_addr       : std_logic_vector(LOG_ADDR_RECONSTRUCTED_SAMPLE-1 downto 0); 
signal reconstructed_samples_read_priority   : std_logic; -- informs which fsm is available to read from reconstructed samples buffer, when 1 then FSM2, otherwise FSM3

-- Pred_Parameter_Buffer signals
signal prediction_paramater_write_en         : std_logic;
signal prediction_paramater_write_addr       : std_logic_vector(LOG_ADDR_PREDICTION_PARAMETER-1 downto 0);
signal prediction_paramater_read_addr        : std_logic_vector(LOG_ADDR_PREDICTION_PARAMETER-1 downto 0);
signal prediction_paramater_read             : std_logic_vector(LOG_PREDICTION_PARAMETER-1 downto 0); 
signal prediction_information                : PREDICTION_INFORMATION_TYPE;

-- Weighted_Prediction_Parameter_Buffer signals
signal weighted_prediction_parameter_write_en   : std_logic;
signal weighted_prediction_parameter_write_addr : std_logic_vector(LOG_ADDR_WEIGHTED_PREDICTION_PARAMETER-1 downto 0);
signal weighted_prediction_parameter_read_addr  : std_logic_vector(LOG_ADDR_WEIGHTED_PREDICTION_PARAMETER-1 downto 0);
signal weighted_prediction_parameter_read       : std_logic_vector(3*(LOG_WEIGHT+LOG_OFFSET)-1 downto 0);

-- Coding_Tree_Unit structure registers  
signal ctu_tree_r                            : CTU_TREE_ARRAY;
signal ctu_tree_next                         : CTU_TREE_ARRAY;

-- Coding_Tree_Unit position information registers 
signal ctu_position_r                        : CTU_POSITION_ARRAY;
signal ctu_position_next                     : CTU_POSITION_ARRAY;

-- Referenced_Samples signals and registers 
signal referenced_samples_read               : std_logic_vector(NUMBER_OF_SAMPLES_DEC*(8+DELTA_BIT_DEPTH)-1 downto 0);
signal referenced_samples_r                  : DATA_SAMPLES_DEC_TYPE;
signal referenced_samples_next               : DATA_SAMPLES_DEC_TYPE;

-- Internal Fifo_Order signals
signal fifo_order_settings                   : fifo_order_settings_TYPE;
signal fifo_order_data                       : FIFO_DATA_READ_TYPE;
signal referenced_sample_valid_ptr           : std_logic_vector(log2_ceil(NUMBER_OF_SAMPLES_DEC)-1 downto 0); -- pointer to first valid sample from 7 samples read from external memory containing referenced frames

-- Referenced Data External Memory ports signal
signal referenced_data_port_sel              : std_logic;

-- Prediction Parameter register and its translation to intra or inter prediction signals 
signal pu_parameter													 : PU_PARAMETER_TYPE;
signal pu_parameter_r                        : PU_PARAMETER_REGISTER_TYPE;
signal pu_parameter_next                     : PU_PARAMETER_REGISTER_TYPE;
signal pred_parameter_r                      : std_logic_vector(LOG_PRED_PARAMETER_REGISTER-1 downto 0);
signal pred_parameter_next                   : std_logic_vector(LOG_PRED_PARAMETER_REGISTER-1 downto 0);

-- Interpolated samples, signals and registers
signal interpolator_ready                    : std_logic; 
signal interpolated_samples_r                : INTERPOLATED_SAMPLES_TYPE;
signal interpolated_samples_next             : INTERPOLATED_SAMPLES_TYPE;
signal interpolated_samples_dav_r            : std_logic; 
signal interpolated_samples_dav_next         : std_logic; 
signal interpolated_samples_dav_delay1_r     : std_logic; 
signal interpolated_samples_dav_delay1_next  : std_logic;
signal interpolated_samples_dav_delay2_r     : std_logic; 
signal interpolated_samples_dav_delay2_next  : std_logic; 
signal interpolated_samples_dav_delay3_r     : std_logic; 
signal interpolated_samples_dav_delay3_next  : std_logic; 

signal interpolator_samples_in               : INPUT_OUTPUT_ROW_TYPE;   
signal interpolator_samples_out              : OUTPUT_ROW_TYPE; 

-- Bidirectional Prediction Delay Buffer signals 
signal bi_pred_delay_write_en                : std_logic;
signal bi_pred_delay_write_samples           : std_logic_vector(NUMBER_OF_SAMPLES_DEC*(LOG_INTERPOLATED_SAMPLE)-1 downto 0);
signal bi_pred_delay_read_samples            : std_logic_vector(NUMBER_OF_SAMPLES_DEC*(LOG_INTERPOLATED_SAMPLE)-1 downto 0);
signal bi_pred_delay_samples                 : INTERPOLATED_SAMPLES_TYPE;

-- Weighted Samples signal  
signal weighted_samples                      : DATA_SAMPLES_DEC_TYPE;

-- Intra prediction decoder signals
signal intra_reconstructed_samples_ack       : std_logic;
signal intra_decoder_start_ack               : std_logic;
signal intra_predicted_samples_dav           : std_logic;
signal intra_predicted_samples               : SET_OF_4_SAMPLES_TYPE;
signal intra_predicted_samples_ack           : std_logic;
signal intra_pu_addr_x                       : std_logic_vector(MAX_PIC_WIDTH_LOG2 - 1 downto 0);
signal intra_pu_addr_y                       : std_logic_vector(MAX_PIC_HEIGHT_LOG2 - 1 downto 0);

-- Predicted Samples registers 
signal predicted_samples_r                   : DATA_SAMPLES_DEC_TYPE;
signal predicted_samples_next                : DATA_SAMPLES_DEC_TYPE;
signal predicted_samples_dav_r               : std_logic;
signal predicted_samples_dav_next            : std_logic;
signal predicted_samples_dav_delay1_r        : std_logic;
signal predicted_samples_dav_delay1_next     : std_logic; 

-- Reconstructed Samples registers
signal reconstructed_samples_r               : DATA_SAMPLES_DEC_TYPE;
signal reconstructed_samples_next            : DATA_SAMPLES_DEC_TYPE;

-- Finite State Machines  registers
signal fsm1_r                                : INTERNAL_FSM1_TYPE;
signal fsm1_next                             : INTERNAL_FSM1_TYPE;
signal fsm2_r                                : INTERNAL_FSM2_TYPE;
signal fsm2_next                             : INTERNAL_FSM2_TYPE;
signal fsm3_r                                : INTERNAL_FSM3_TYPE;
signal fsm3_next                             : INTERNAL_FSM3_TYPE;    

  -- -------------------------------------------------------------------------
  --                              Components                                --
  -- -------------------------------------------------------------------------

--Interpolator module
component interpolator_dec is
port (
  clk             : in  std_logic;
  rst_n           : in  std_logic;			 
  samples_in_dav  : in  std_logic;
  samples_in      : in  INPUT_OUTPUT_ROW_TYPE;
  start           : in  std_logic;
  chroma          : in  std_logic;
  mv_fract_x      : in  std_logic_vector(2 downto 0); 
  mv_fract_y      : in  std_logic_vector(2 downto 0);
  cu_size_x       : in  std_logic_vector(log2_ceil(CODING_UNIT_SIZE) downto 0);
  cu_size_y       : in  std_logic_vector(log2_ceil(CODING_UNIT_SIZE) downto 0);
  samples_out_dav : out std_logic;
  samples_out     : out INPUT_OUTPUT_ROW_TYPE;
  samples_out_wp  : out OUTPUT_ROW_TYPE;
  ready           : out std_logic
);
end component; 

--Weighted Prediction module
component weighted_prediction is  
  port(
  clk                : in  std_logic;
  rst                : in  std_logic;  --Reset, active LOW 
  weight0            : in  std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset0            : in  std_logic_vector(LOG_OFFSET-1 downto 0);
  weight1            : in  std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset1            : in  std_logic_vector(LOG_OFFSET-1 downto 0);
  log_weight_denom   : in  std_logic_vector(2 downto 0);
  weighted_pred_flag : in  std_logic;  -- appriopriate flag for actual slice type (weighted_pred_flag or weighted_bipred_flag)
  bi_pred_flag       : in  std_logic;  -- when 0 we only use data with index 1, data with index 0 should be equal zero
  input_samples0     : in  INTERPOLATED_SAMPLES_TYPE;
  input_samples1     : in  INTERPOLATED_SAMPLES_TYPE;
  output_samples     : out DATA_SAMPLES_DEC_TYPE
  );
end component; 

--Intra prediction decoder
component intra_pred_dec
  port (
  clk                : in  std_logic;
  rst_n              : in  std_logic;
  bit_depth_luma     : in  std_logic_vector(2 downto 0);
  bit_depth_chroma   : in  std_logic_vector(2 downto 0);
  refs_smooth_en     : in  std_logic;
  cu_size            : in  std_logic_vector(6 downto 0);
  pu_addr_x          : in  std_logic_vector(MAX_PIC_WIDTH_LOG2 - 1 downto 0);
  pu_addr_y          : in  std_logic_vector(MAX_PIC_HEIGHT_LOG2 - 1 downto 0);
  pu_avail           : in  std_logic_vector(4 downto 0);
  img_comp           : in  std_logic_vector(1 downto 0);
  pu_size_idx        : in  std_logic_vector(1 downto 0);
  mode_idx           : in  std_logic_vector(5 downto 0);
  pred_params_en     : in  std_logic;
  pred_params_ack    : out std_logic;
  pred_samples       : out SET_OF_4_SAMPLES_TYPE;
  pred_samples_en    : out std_logic;
  pred_samples_ack   : in  std_logic;
  rec_addr_x         : in  std_logic_vector(MAX_PIC_WIDTH_LOG2 - 1 downto 0);
  rec_addr_y         : in  std_logic_vector(MAX_PIC_HEIGHT_LOG2 - 1 downto 0);
  rec_img_comp       : in  std_logic_vector(1 downto 0);
  rec_width          : in  std_logic_vector(6 downto 0);
  rec_height         : in  std_logic_vector(6 downto 0);
  rec_samples        : in  SET_OF_4_SAMPLES_TYPE;
  rec_samples_en     : in  std_logic;
  rec_samples_ack    : out std_logic
  );
end component;

  -- -------------------------------------------------------------------------
  --                        Begin of the architecture                       --
  -- -------------------------------------------------------------------------

begin  
  

  -- -------------------------------------------------------------------------
  --                        Blocks of internal memory                       --
  -- -------------------------------------------------------------------------
  
-- Memory containing residuals and related process
PROC_RESIDUALS_BUFFER: process (input_data, fsm3_r, fsm2_r)
begin
  for i in 0 to NUMBER_OF_SAMPLES-1 loop
    residuals_write((9+DELTA_BIT_DEPTH)*(i+1)-1 downto (9+DELTA_BIT_DEPTH)*i) <= input_data.residuals(i);
  end loop; 
  --Signal assignments
  residuals_write_en      <= not fsm3_r.residuals_write_en;
  residuals_write_addr(0) <= fsm3_r.pingpong & fsm3_r.residuals_addr(0);
  residuals_write_addr(1) <= fsm3_r.pingpong & fsm3_r.residuals_addr(1);  
  residuals_write_addr(2) <= fsm3_r.pingpong & fsm3_r.residuals_addr(2);  
  residuals_write_addr(3) <= fsm3_r.pingpong & fsm3_r.residuals_addr(3);  
  residuals_read_addr(0)  <= fsm2_r.pingpong & fsm2_r.residuals_addr(0);
  residuals_read_addr(1)  <= fsm2_r.pingpong & fsm2_r.residuals_addr(1);  
end process;
LEFT_RIGHT_RESIDUALS_BUFFER:
for i in 0 to 1 generate  
  TOP_BOTTOM_TWO_LINES_RESIDUALS_BUFFER:
  for j in 0 to 1 generate 
    EVEN_ODD_LINE_RESIDUALS_BUFFER:
    for k in 0 to 1 generate
      TOP_RESIDUALS_BUFFER: RAMT 
      generic map (
        BIT_WIDTH  => 2*(9+DELTA_BIT_DEPTH), -- two (9+DELTA_BIT_DEPTH) bits samples in each cell
        ADDR_WIDTH => LOG_ADDR_RESIDUAL      -- each memory block contains: residuals from two CODING_TREE_UNIT (pingpong), for each CODING_TREE_UNIT there are 128 luma pixels and 128 or 64 chroma pixels (it depends on CHROMA FORMAT)
      )
      port map(
        clk(0) => clk,
        clk(1) => clk,
        wren => residuals_write_en(j*2+k),
        data_a => residuals_write((i*2+2)*(9+DELTA_BIT_DEPTH)-1 downto (i*2)*(9+DELTA_BIT_DEPTH)),
        address_a => residuals_write_addr(i),
        address_b => residuals_read_addr(k),
        q_b => residuals_read(i*4+j*2+k) 
      );
    BOTTOM_RESIDUALS_BUFFER: RAMT 
      generic map (
        BIT_WIDTH  => 2*(9+DELTA_BIT_DEPTH), -- two (9+DELTA_BIT_DEPTH) bits samples in each cell
        ADDR_WIDTH => LOG_ADDR_RESIDUAL      -- each memory block contains: residuals from two CODING_TREE_UNIT (pingpong), for each CODING_TREE_UNIT in one memory block there are 128 luma samples and 128 or 64 chroma samples (it depends on CHROMA FORMAT)
      )
      port map(
        clk(0) => clk,
        clk(1) => clk,
        wren => residuals_write_en(3 - (j*2+k)),
        data_a => residuals_write((4+i*2+2)*(9+DELTA_BIT_DEPTH)-1 downto (4+i*2)*(9+DELTA_BIT_DEPTH)),
        address_a => residuals_write_addr(2+i),
        address_b => residuals_read_addr(k),
        q_b => residuals_read(8+i*4+j*2+k) 
      );
    end generate;
  end generate;
end generate;



-- Memory containing reconstructed samples and related process 
PROC_RECONSTRUCTED_SAMPLES_BUFFER: process (reconstructed_samples_r, fsm2_r, fsm2_next, fsm3_r, pu_parameter, fifo_order_data, reconstructed_samples_read_priority)   
variable temp1_v                               : std_logic_vector(6 downto 0);
variable reconstructed_samples_read_addr_v     : std_logic_vector(LOG_ADDR_RECONSTRUCTED_SAMPLE-1 downto 0); 
begin 

  if reconstructed_samples_read_priority = '1' then 
    
    reconstructed_samples_read_addr_v(11 downto 10) := fsm2_r.pingpong & pu_parameter.chroma(1);
    if fifo_order_data.chroma(1) = '0' then
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_next.intra_reconstructed_samples_y_cnt)&"00")); 
      reconstructed_samples_read_addr_v(9 downto 0) := std_logic_vector((unsigned(temp1_v(5 downto 2))&"000000") + unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_next.intra_reconstructed_samples_x_cnt));  
    else
      reconstructed_samples_read_addr_v(9) := pu_parameter.chroma(0);
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_next.intra_reconstructed_samples_y_cnt)&"00")); 
      reconstructed_samples_read_addr_v(8 downto 0) := std_logic_vector((unsigned(temp1_v(5 downto 2))&"00000") + unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_next.intra_reconstructed_samples_x_cnt));  
    end if;

  else
    reconstructed_samples_read_addr_v := fsm3_r.pingpong & fsm3_r.reconstructed_samples_addr;
  end if;
  
  reconstructed_samples_read_addr     <= reconstructed_samples_read_addr_v;
  reconstructed_samples_write_en      <= not fsm2_r.reconstructed_samples_write_en;
  reconstructed_samples_write_addr(0) <= fsm2_r.pingpong & fsm2_r.reconstructed_samples_write_addr(0); 
  reconstructed_samples_write_addr(1) <= fsm2_r.pingpong & fsm2_r.reconstructed_samples_write_addr(1); 
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    reconstructed_samples_write((i+1)*(8+DELTA_BIT_DEPTH)-1 downto i*(8+DELTA_BIT_DEPTH)) <= reconstructed_samples_r(i);
  end loop;
  
end process; 

BUFFERS_RECONSTRUCTED_SAMPLES: 
for i in 0 to 1 generate
  BUFFER_RECONSTRUCTED_SAMPLES: RAMT
  generic map(  
    BIT_WIDTH  => 2*(8+DELTA_BIT_DEPTH),         -- two (8+DELTA_BIT_DEPTH) bits samples in each cell
    ADDR_WIDTH => LOG_ADDR_RECONSTRUCTED_SAMPLE  -- each memory block contains: reconstructed samples from two CODING_TREE_UNIT (pingpong), for each CODING_TREE_UNIT in one memory there are 2048 luma samples and 2048 or 1024 chroma samples (it depends on CHROMA FORMAT)   
  )
  port map(
    clk(0) => clk,
    clk(1) => clk,
    wren => reconstructed_samples_write_en(i),
    data_a => reconstructed_samples_write(2*(i+1)*(8+DELTA_BIT_DEPTH)-1 downto 2*i*(8+DELTA_BIT_DEPTH)),
    address_a => reconstructed_samples_write_addr(i),
    address_b => reconstructed_samples_read_addr,
    q_b => reconstructed_samples_read(2*(i+1)*(8+DELTA_BIT_DEPTH)-1 downto 2*i*(8+DELTA_BIT_DEPTH))
  );
end generate;



-- Memory containing Prediction Parameters 
-- For inter prediction: prediction flags, Motion Vectors and referenced frames identity
-- For intra prediction: prediction flags, intra prediction mode for luma and chroma blocks
prediction_paramater_write_en   <= input_data.prediction_parameter_dav;
prediction_paramater_write_addr <= fsm3_r.pingpong & fsm3_r.prediction_parameter_addr;
prediction_paramater_read_addr  <= fsm1_r.pingpong & fsm1_r.prediction_parameter_addr; 
PREDICTION_PARAMETER_BUFFER: RAMT 
  generic map (
    BIT_WIDTH  => LOG_PREDICTION_PARAMETER,
    ADDR_WIDTH => LOG_ADDR_PREDICTION_PARAMETER
  )
  port map(
    clk(0) => clk,
    clk(1) => clk,
    wren  => prediction_paramater_write_en,
    data_a => input_data.prediction_parameter(LOG_PREDICTION_PARAMETER-1 downto 0),
    address_a => prediction_paramater_write_addr,
    address_b => prediction_paramater_read_addr,
    q_b => prediction_paramater_read 
  );  
  
  

-- Memory containing Weighted Prediction Parameters    
weighted_prediction_parameter_write_en  <= input_data.prediction_parameter_dav;
weighted_prediction_parameter_write_addr <= fsm3_r.pingpong & fsm3_r.weighted_prediction_parameter_addr;
weighted_prediction_parameter_read_addr  <= fsm2_r.pingpong & fsm2_r.weighted_prediction_parameter_addr; 
WEIGHTED_PREDICTION_PARAMETER_BUFFER: RAMT 
  generic map (
  BIT_WIDTH  => 3*(LOG_WEIGHT + LOG_OFFSET),           -- luma_weight, luma_offset, chroma_cb_weight, chroma_cb_offset, chroma_cr_weight, chroma_cr_offset
  ADDR_WIDTH => LOG_ADDR_WEIGHTED_PREDICTION_PARAMETER -- for all PU in inter prediction mode in two CTU
  )
  port map(
    clk(0) => clk,
    clk(1) => clk,
    wren  => weighted_prediction_parameter_write_en,
    data_a => input_data.prediction_parameter(3*(LOG_WEIGHT+LOG_OFFSET) + 2*LOG_MOTION_VECTOR + LOG_REFERENCED_FRAME_ID + 1 downto 2*LOG_MOTION_VECTOR + LOG_REFERENCED_FRAME_ID + 2),
    address_a => weighted_prediction_parameter_write_addr,
    address_b => weighted_prediction_parameter_read_addr,
    q_b => weighted_prediction_parameter_read 
  );  
  
  
  
-- Memory containing current PredictionBlock's weighted samples used in Bidirectional Prediction 
PROC_BI_PRED_DELAY_BUFFER: process (interpolated_samples_dav_r, interpolated_samples_r, fsm2_r, pu_parameter)
begin
  if pu_parameter.inter_pred_flag = '1' and pu_parameter.bi_pred_flag = '1' and fsm2_r.bi_pred_block_ptr = '0' then 
    bi_pred_delay_write_en <= interpolated_samples_dav_r; 
  else
    bi_pred_delay_write_en <= '0';
  end if;   
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    bi_pred_delay_write_samples((i+1)*(LOG_INTERPOLATED_SAMPLE)-1 downto (i)*(LOG_INTERPOLATED_SAMPLE)) <= interpolated_samples_r(i);
  end loop;
end process;
BI_PRED_DELAY_BUFFER: RAMT generic map (
    BIT_WIDTH  => NUMBER_OF_SAMPLES_DEC * (LOG_INTERPOLATED_SAMPLE),
    ADDR_WIDTH => log2_ceil(64*64 / NUMBER_OF_SAMPLES_DEC)  -- maximum delay when size of prediction block is 64x64 
  )
  port map(
    clk(0) => clk,
    clk(1) => clk,
    wren => bi_pred_delay_write_en,                      -- write samples activated by low signal
    data_a => bi_pred_delay_write_samples,
    address_a => fsm2_r.bi_pred_delay_write_addr,
    address_b => fsm2_r.bi_pred_delay_read_addr,
    q_b => bi_pred_delay_read_samples
  ); 


--Fifo Order used to exchange information from first to second FSM  
FIFO_ORDER: fifo_dc 
  generic map(
    BIT_WIDTH => 37 + log2_ceil(NUMBER_OF_SAMPLES_DEC),
    ADDR_WIDTH  => LOG_ADDR_FIFO_ORDER,                                     
    WARN_LENGTH => FIFO_ORDER_WARN_LENGTH 
  )
  port map(   
    clk_input => clk,
    clk_output => clk,
    xrst => rst,
    datain => fifo_order_settings.data_in, 
    dataout => fifo_order_settings.data_out, 
    enable => fsm1_r.fifo_order_en,      
    full => fifo_order_settings.full,
    almost_full => fifo_order_settings.almost_full,
    ack => fsm2_r.fifo_order_ack,
    empty => fifo_order_settings.empty,
    almost_empty => fifo_order_settings.almost_empty  
  );
  

  -- -------------------------------------------------------------------------
  --                               Components                               --
  -- -------------------------------------------------------------------------  

PROC_INTERPOLATOR_OUTPUT_ASSIGNMENT: process(referenced_samples_r, interpolator_samples_out)
begin	  
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    interpolator_samples_in(i) <= referenced_samples_r(i);
    interpolated_samples_next(i) <= interpolator_samples_out(i)(21+DELTA_BIT_DEPTH downto 6+DELTA_BIT_DEPTH); -- after vertical interpolation samples are shifted to the right by DELTA_BIT_DEPTH bits, after horizontal interpolation samples are shifted to the right by 6 bits  
  end loop;
end process;  

INTERPOLATOR: interpolator_dec
  port map(
    clk => clk,
    rst_n => rst,
    samples_in_dav => fsm2_r.referenced_samples_dav,
    samples_in => interpolator_samples_in,
    start => fsm2_r.interpolator_start, 
    chroma => pu_parameter.chroma(1),
    ready => interpolator_ready,
    mv_fract_x => pu_parameter.motion_vector_fract_x,
    mv_fract_y => pu_parameter.motion_vector_fract_y,
    --chroma_format => chroma_format,     
    cu_size_x => pu_parameter.pb_size_x, 
    cu_size_y => pu_parameter.round_pb_size_y,
    samples_out_dav => interpolated_samples_dav_next,
    samples_out => open,
    samples_out_wp => interpolator_samples_out
  ); 

-- Weighted Prediction
PROC_WEIGHTED_PREDICTION_ASSIGNMENT: process(bi_pred_delay_read_samples, pu_parameter)
begin
  if pu_parameter.bi_pred_flag = '1' then
    for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
      bi_pred_delay_samples(i) <= bi_pred_delay_read_samples( (i+1)*(LOG_INTERPOLATED_SAMPLE)-1 downto i*(LOG_INTERPOLATED_SAMPLE) ); 
    end loop;	
  else
    for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
      bi_pred_delay_samples(i) <= (others=>'0');
    end loop;
  end if;
end process;
WEIGHTED_PREDICTION_MODULE: weighted_prediction
  port map(
    clk => clk,
    rst => rst,
    weight0 => pu_parameter.weight0,
    offset0 => pu_parameter.offset0,
    weight1 => pu_parameter.weight1,
    offset1 => pu_parameter.offset1,
    log_weight_denom => pu_parameter.log_weight_denom,
    weighted_pred_flag => pu_parameter.weighted_pred_flag,
    bi_pred_flag => pu_parameter.bi_pred_flag,
    input_samples0 => bi_pred_delay_samples,
    input_samples1 => interpolated_samples_r,
    output_samples => weighted_samples
  );

--Intra prediction decoder
INTRA_PRED: intra_pred_dec
  port map (
    clk => clk,
    rst_n => rst,
    bit_depth_luma => BIT_DEPTH,
    bit_depth_chroma => BIT_DEPTH,
    refs_smooth_en => REFS_SMOOTH_EN,
    cu_size => MAX_CU_SIZE,
    pu_addr_x => intra_pu_addr_x,
    pu_addr_y => intra_pu_addr_y,
    pu_avail => pu_parameter.pu_avail,
    img_comp => pu_parameter.chroma,
    pu_size_idx => pu_parameter.pu_size_idx,
    mode_idx => pu_parameter.intra_pred_mode,
    pred_params_en => fsm2_r.intra_decoder_start,
    pred_params_ack => intra_decoder_start_ack,
    pred_samples => intra_predicted_samples,
    pred_samples_en => intra_predicted_samples_dav,
    pred_samples_ack => intra_predicted_samples_ack,
    rec_addr_x => intra_pu_addr_x,
    rec_addr_y => intra_pu_addr_y,
    rec_img_comp => pu_parameter.chroma,
    rec_width => pu_parameter.pb_size_x,
    rec_height => pu_parameter.round_pb_size_y,
    rec_samples => reconstructed_samples,
    rec_samples_en => fsm2_r.intra_reconstructed_samples_dav,
    rec_samples_ack => intra_reconstructed_samples_ack
    ); 

  
  -- -------------------------------------------------------------------------
  --                         Process of FSM1                                --
  -- ------------------------------------------------------------------------- 
PROC_FSM1: process (fsm1_r, fsm3_r, start, referenced_data_port_out, fifo_order_settings, prediction_information, ctu_tree_r)
variable fsm1_v           : INTERNAL_FSM1_TYPE;
-- Support integer indicates current pingpong
variable pingpong_v       : integer range 0 to 1;
-- Variables used in WORK_BLOCK state
variable shift_counters_v : unsigned(3 downto 0);
variable counters_v       : std_logic_vector(8 downto 0); 
variable update_cnt_v     : std_logic;     

begin
  fsm1_v := fsm1_r; 
  
  -- Pingpong assignment
  if fsm1_v.pingpong = '1' then
    pingpong_v := 1;
  else
    pingpong_v := 0;
  end if;
  
  -- FSM processing
  case fsm1_r.state is
    
  when IDLE =>
  fsm1_v.pingpong   := '0';
  fsm1_v.referenced_data_port_start := '0';
  fsm1_v.fifo_order_en    := '0';
  if start = '1' then
    fsm1_v.state    := CHECK;
  end if;
 
  when CHECK =>
  fsm1_v.referenced_data_port_start := '0';
  fsm1_v.fifo_order_en := '0';
  if fsm3_r.pingpong_dav(pingpong_v) = '1' then
    fsm1_v.cnt32              := (others=>'0');  
    fsm1_v.cnt16              := (others=>'0'); 
    fsm1_v.cnt8               := (others=>'0');  
    fsm1_v.cnt4               := (others=>'0'); 
    fsm1_v.bi_pred_block_ptr  := '0';
    fsm1_v.cu_partition_ptr   := '0'; 
    fsm1_v.prediction_parameter_addr:= (others=>'0');
    fsm1_v.chroma             := "00";
    fsm1_v.state              := WORK_TREE;
  end if;
 
  when WORK_TREE =>
	
  -- Change state to WORK_BLOCK for current PredictionUnit
  fsm1_v.state := WORK_BLOCK;
  -- Reset signals
  fsm1_v.bi_pred_block_ptr := '0';	
	fsm1_v.cnt4 := "00";	
  fsm1_v.referenced_data_port_start := '0';
  fsm1_v.fifo_order_en := '0';	
	fsm1_v.intra4x4 := '0';	
	
  -- Check whether the current 64x64 CU is divided into different CU 
  if std_match(ctu_tree_r(pingpong_v).cu_64, "11--") then -- 32x32 x4
    case fsm1_r.cnt32 is
    when "00" =>      -- in top-left CU 32x32
      fsm1_v.pu_position_x  := "000000";  --0  
      fsm1_v.pu_position_y  := "000000";  --0 
    when "01" =>      -- in top-right CU 32x32
      fsm1_v.pu_position_x  := "100000";  --32  
      fsm1_v.pu_position_y  := "000000";  --0 
    when "10" =>      -- in bottom-left CU 32x32
      fsm1_v.pu_position_x  := "000000";  --0  
      fsm1_v.pu_position_y  := "100000";  --32
    when others =>    -- in bottom-right CU 32x32
      fsm1_v.pu_position_x  := "100000";  --32  
      fsm1_v.pu_position_y  := "100000";  --32
    end case;
  
    -- Check whether the current 32x32 CU is divided into different CU
    if std_match(ctu_tree_r(pingpong_v).cu_32(to_integer(unsigned(fsm1_r.cnt32))), "11--") then -- 16x16 x4
      case fsm1_r.cnt16 is
      when "01" =>      -- top-right CU 16x16 in current CU 32x32
        fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+16);  
      when "10" =>      -- bottom-left CU 16x16 in current CU 32x32 
        fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+16);
      when "11" =>      -- bottom-right CU 16x16 in current CU 32x32
        fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+16); 
        fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+16);
      when others =>
        --Position is correct
      end case;
    
      -- Check whether the current 16x16 CU is divided into different CU
      if std_match(ctu_tree_r(pingpong_v).cu_16(to_integer((unsigned(fsm1_r.cnt32)&"00") + unsigned(fsm1_r.cnt16))), "11--") then -- 8x8 x4
        case fsm1_r.cnt8 is
        when "01" =>    -- top-right CU 8x8 in current CU 16x16
          fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+8);  
        when "10" =>    -- bottom-left CU 8x8 in current CU 16x16 
          fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+8);
        when "11" =>    -- bottom-right CU 8x8 in current CU 16x16
          fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+8); 
          fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+8);
        when others =>
          --Position is correct
        end case;
    
        -- Check whether the current 8x8 CU is divided into 4x4 PU for which intra prediction is only possible
        if std_match(ctu_tree_r(pingpong_v).cu_8(to_integer((unsigned(fsm1_r.cnt32)&"0000") + (unsigned(fsm1_r.cnt16)&"00") + unsigned(fsm1_r.cnt8))), "11") then -- 4x4 x4
          fsm1_v.pu_size_x := "0001000"; --8        
          fsm1_v.pu_size_y := "0001000"; --8
					--Position is correct
					fsm1_v.intra4x4  := '1';
					--Other operations in the next state

        -- Check other options for the current 8x8 CU 
        elsif std_match(ctu_tree_r(pingpong_v).cu_8(to_integer((unsigned(fsm1_r.cnt32)&"0000") + (unsigned(fsm1_r.cnt16)&"00") + unsigned(fsm1_r.cnt8))), "00") then -- 8x8
          fsm1_v.pu_size_x := "0001000"; --8        
          fsm1_v.pu_size_y := "0001000"; --8 
          --Position is correct
        elsif std_match(ctu_tree_r(pingpong_v).cu_8(to_integer((unsigned(fsm1_r.cnt32)&"0000") + (unsigned(fsm1_r.cnt16)&"00") + unsigned(fsm1_r.cnt8))), "01") then -- 8x4 x2
          fsm1_v.pu_size_x := "0001000"; --8        
          fsm1_v.pu_size_y := "0000100"; --4
          --Position for first unit is correct
          if fsm1_v.cu_partition_ptr= '1' then
            fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+4); 
          end if;
        else                             -- 4x8 x2
          fsm1_v.pu_size_x := "0000100"; --4        
          fsm1_v.pu_size_y := "0001000"; --8
          --Position for first unit is correct
          if fsm1_v.cu_partition_ptr= '1' then
            fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+4); 
          end if;
        end if;

      -- Check other options for the current 16x16 CU 
      elsif std_match(ctu_tree_r(pingpong_v).cu_16(to_integer((unsigned(fsm1_r.cnt32)&"00") + unsigned(fsm1_r.cnt16))), "00--") then -- 16x16
        fsm1_v.pu_size_x := "0010000"; --16        
        fsm1_v.pu_size_y := "0010000"; --16
        --Position is correct 
      elsif std_match(ctu_tree_r(pingpong_v).cu_16(to_integer((unsigned(fsm1_r.cnt32)&"00") + unsigned(fsm1_r.cnt16))), "010-")then -- 16x8 x2  
        fsm1_v.pu_size_x := "0010000"; --16        
        fsm1_v.pu_size_y := "0001000"; --8
        --Position for first unit is correct
        if fsm1_v.cu_partition_ptr= '1' then
          fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+8); 
        end if;
      elsif std_match(ctu_tree_r(pingpong_v).cu_16(to_integer((unsigned(fsm1_r.cnt32)&"00") + unsigned(fsm1_r.cnt16))), "100-")then -- 8x16 x2  
        fsm1_v.pu_size_x := "0001000"; --8       
        fsm1_v.pu_size_y := "0010000"; --16
        --Position for first unit is correct
        if fsm1_v.cu_partition_ptr= '1' then
          fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+8); 
        end if;
      elsif std_match(ctu_tree_r(pingpong_v).cu_16(to_integer((unsigned(fsm1_r.cnt32)&"00") + unsigned(fsm1_r.cnt16))), "0110")then -- 16x4, 16x12  
        if fsm1_v.cu_partition_ptr= '0' then 
          fsm1_v.pu_size_x := "0010000"; --16       
          fsm1_v.pu_size_y := "0000100"; --4
          --Position is correct
        else
          fsm1_v.pu_size_x := "0010000"; --16       
          fsm1_v.pu_size_y := "0001100"; --12
          fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+4);
        end if;
      elsif std_match(ctu_tree_r(pingpong_v).cu_16(to_integer((unsigned(fsm1_r.cnt32)&"00") + unsigned(fsm1_r.cnt16))), "0111")then -- 16x12, 16x4  
        if fsm1_v.cu_partition_ptr= '0' then 
          fsm1_v.pu_size_x := "0010000"; --16       
          fsm1_v.pu_size_y := "0001100"; --12
          --Position is correct
        else
          fsm1_v.pu_size_x := "0010000"; --16       
          fsm1_v.pu_size_y := "0000100"; --4
          fsm1_v.pu_position_y  := std_logic_vector(unsigned(fsm1_v.pu_position_y)+12);
        end if;
      elsif std_match(ctu_tree_r(pingpong_v).cu_16(to_integer((unsigned(fsm1_r.cnt32)&"00") + unsigned(fsm1_r.cnt16))), "1010") then -- 4x16, 12x16 
        if fsm1_v.cu_partition_ptr= '0' then 
          fsm1_v.pu_size_x := "0000100"; --4       
          fsm1_v.pu_size_y := "0010000"; --16
          --Position is correct
        else
          fsm1_v.pu_size_x := "0001100"; --12       
          fsm1_v.pu_size_y := "0010000"; --16
          fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+4);
        end if;
      else                               -- 12x16, 4x16  
        if fsm1_v.cu_partition_ptr= '0' then 
          fsm1_v.pu_size_x := "0001100"; --12       
          fsm1_v.pu_size_y := "0010000"; --16
          --Position is correct
        else
          fsm1_v.pu_size_x := "0000100"; --4       
          fsm1_v.pu_size_y := "0010000"; --16
          fsm1_v.pu_position_x  := std_logic_vector(unsigned(fsm1_v.pu_position_x)+12);
        end if;    
      end if; 

    -- Check other options for the current 32x32 CU
    elsif std_match(ctu_tree_r(pingpong_v).cu_32(to_integer(unsigned(fsm1_r.cnt32))), "00--") then -- 32x32
      fsm1_v.pu_size_x := "0100000";         --32        
      fsm1_v.pu_size_y := "0100000";         --32
      if fsm1_r.cnt32(0) = '0' then          --top-left and bottom-left CU 32x32
        fsm1_v.pu_position_x  := "000000";   --0
      else                                   --top-right and bottom-right CU 32x32
        fsm1_v.pu_position_x  := "100000";   --32 
      end if;
      if fsm1_r.cnt32(1) = '0' then          --top-left and top-right CU 32x32 
        fsm1_v.pu_position_y  := "000000";   --0 
      else                                   --bottom-left and bottom-right CU 32x32
        fsm1_v.pu_position_y  := "100000";   --32
      end if;
    elsif std_match(ctu_tree_r(pingpong_v).cu_32(to_integer(unsigned(fsm1_r.cnt32))), "010-") then -- 32x16 x2 
      fsm1_v.pu_size_x := "0100000";         --32        
      fsm1_v.pu_size_y := "0010000";         --16
      if fsm1_r.cnt32(0) = '0' then          --unit in top-left and bottom-left CU 32x32
        fsm1_v.pu_position_x  := "000000";   --0
      else                                   --unit in top-right and bottom-right CU 32x32
        fsm1_v.pu_position_x  := "100000";   --32 
      end if;
      if fsm1_v.cu_partition_ptr= '0' then
        if fsm1_r.cnt32(1) = '0' then        --unit in top-left and top-right CU 32x32 
          fsm1_v.pu_position_y  := "000000"; --0 
        else                                 --unit in bottom-left and bottom-right CU 32x32
          fsm1_v.pu_position_y  := "100000"; --32
        end if;
      else
        if fsm1_r.cnt32(1) = '0' then        --unit in top-left and top-right CU 32x32 
          fsm1_v.pu_position_y  := "010000"; --16 
        else                                 --unit in bottom-left and bottom-right CU 32x32
          fsm1_v.pu_position_y  := "110000"; --48
        end if;  
      end if;
    elsif std_match(ctu_tree_r(pingpong_v).cu_32(to_integer(unsigned(fsm1_r.cnt32))), "100-") then -- 16x32 x2
      fsm1_v.pu_size_x := "0010000";         --16        
      fsm1_v.pu_size_y := "0100000";         --32
      if fsm1_r.cnt32(1) = '0' then          --unit in top-left and top-right CU 32x32
        fsm1_v.pu_position_y  := "000000";   --0
      else                                   --unit in bottom-left and bottom-right CU 32x32
        fsm1_v.pu_position_y  := "100000";   --32 
      end if;
      if fsm1_v.cu_partition_ptr= '0' then
        if fsm1_r.cnt32(0) = '0' then        --unit in top-left and bootom-left CU 32x32
          fsm1_v.pu_position_x  := "000000"; --0 
        else                                 --unit in top-right and bottom-right CU 32x32
          fsm1_v.pu_position_x  := "100000"; --32
        end if;
      else
        if fsm1_r.cnt32(0) = '0' then        --unit in top-left and bottom-left CU 32x32 
          fsm1_v.pu_position_x  := "010000"; --16 
        else                                 --unit in top-right and bottom-right CU 32x32
          fsm1_v.pu_position_x  := "110000"; --48
        end if;  
      end if;  
    elsif std_match(ctu_tree_r(pingpong_v).cu_32(to_integer(unsigned(fsm1_r.cnt32))), "0110") then -- 32x8, 32x24
      if fsm1_r.cnt32(0) = '0' then          --unit in top-left and bottom-left CU 32x32
        fsm1_v.pu_position_x  := "000000";   --0 
      else                                   --unit in top-right and bottom-right CU 32x32
        fsm1_v.pu_position_x  := "100000";   --32
      end if;  
      if fsm1_v.cu_partition_ptr= '0' then
        fsm1_v.pu_size_x := "0100000";       --32         
        fsm1_v.pu_size_y := "0001000";       --8
        if fsm1_r.cnt32(1) = '0' then        --unit in top-left and top-right CU 32x32
          fsm1_v.pu_position_y  := "000000"; --0 
        else                                 --unit in bottom-left and bottom-right CU 32x32
          fsm1_v.pu_position_y  := "100000"; --32
        end if; 
      else
        fsm1_v.pu_size_x := "0100000";       --32         
        fsm1_v.pu_size_y := "0011000";       --24
        if fsm1_r.cnt32(1) = '0' then        --unit in top-left and top-right CU 32x32
          fsm1_v.pu_position_y  := "001000"; --8 
        else                                 --unit in bottom-left and bottom-right CU 32x32
          fsm1_v.pu_position_y  := "101000"; --40
        end if;
      end if; 
    elsif std_match(ctu_tree_r(pingpong_v).cu_32(to_integer(unsigned(fsm1_r.cnt32))), "0111") then -- 32x24, 32x8 
      if fsm1_r.cnt32(0) = '0' then          --unit in top-left and bottom-left CU 32x32
        fsm1_v.pu_position_x  := "000000";   --0 
      else                                   --unit in top-right and bottom-right CU 32x32
        fsm1_v.pu_position_x  := "100000";   --32
      end if;  
      if fsm1_v.cu_partition_ptr= '0' then
        fsm1_v.pu_size_x := "0100000";       --32         
        fsm1_v.pu_size_y := "0011000";       --24
        if fsm1_r.cnt32(1) = '0' then        --unit in top-left and top-right CU 32x32
          fsm1_v.pu_position_y  := "000000"; --0 
        else                                 --unit in bottom-left and bottom-right CU 32x32
          fsm1_v.pu_position_y  := "100000"; --32
        end if; 
      else
        fsm1_v.pu_size_x := "0100000";       --32         
        fsm1_v.pu_size_y := "0001000";       --8
        if fsm1_r.cnt32(1) = '0' then        --unit in top-left and top-right CU 32x32
          fsm1_v.pu_position_y  := "011000"; --24 
        else                                 --unit in bottom-left and bottom-right CU 32x32
          fsm1_v.pu_position_y  := "111000"; --56
        end if;
      end if;
    elsif std_match(ctu_tree_r(pingpong_v).cu_32(to_integer(unsigned(fsm1_r.cnt32))), "1010") then -- 8x32, 24x32 
      if fsm1_r.cnt32(1) = '0' then          --unit in top-left and top-right CU 32x32
        fsm1_v.pu_position_y  := "000000";   --0 
      else                                   --unit in bottom-left and bottom-right CU 32x32
        fsm1_v.pu_position_y  := "100000";   --32
      end if;  
      if fsm1_v.cu_partition_ptr= '0' then
        fsm1_v.pu_size_x := "0001000";       --8         
        fsm1_v.pu_size_y := "0100000";       --32
        if fsm1_r.cnt32(0) = '0' then        --unit in top-left and bottom-left CU 32x32
          fsm1_v.pu_position_x  := "000000"; --0 
        else                                 --unit in top-right and bottom-right CU 32x32
          fsm1_v.pu_position_x  := "100000"; --32
        end if; 
      else
        fsm1_v.pu_size_x := "0011000";       --24         
        fsm1_v.pu_size_y := "0100000";       --32
        if fsm1_r.cnt32(0) = '0' then        --unit in top-left and bottom-left CU 32x32
          fsm1_v.pu_position_x  := "001000"; --8 
        else                                 --unit in top-right and bottom-right CU 32x32
          fsm1_v.pu_position_x  := "101000"; --40
        end if;
      end if;
    else                                                                                           -- 24x32, 8x32 
      if fsm1_r.cnt32(1) = '0' then          --unit in top-left and top-right CU 32x32
        fsm1_v.pu_position_y  := "000000";   --0 
      else                                   --unit in bottom-left and bottom-right CU 32x32
        fsm1_v.pu_position_y  := "100000";   --32
      end if;  
      if fsm1_v.cu_partition_ptr= '0' then
        fsm1_v.pu_size_x := "0011000";       --24         
        fsm1_v.pu_size_y := "0100000";       --32
        if fsm1_r.cnt32(0) = '0' then        --unit in top-left and bottom-left CU 32x32
          fsm1_v.pu_position_x  := "000000"; --0 
        else                                 --unit in top-right and bottom-right CU 32x32
          fsm1_v.pu_position_x  := "100000"; --32
        end if; 
      else
        fsm1_v.pu_size_x := "0001000";       --8         
        fsm1_v.pu_size_y := "0100000";       --32
        if fsm1_r.cnt32(0) = '0' then        --unit in top-left and bottom-left CU 32x32
          fsm1_v.pu_position_x  := "011000"; --24 
        else                                 --unit in top-right and bottom-right CU 32x32
          fsm1_v.pu_position_x  := "111000"; --56
        end if;
      end if; 
    end if;   

  -- Check other options for the current 64x64 CU
  elsif std_match(ctu_tree_r(pingpong_v).cu_64, "00--") then -- 64x64
    fsm1_v.pu_size_x  := "1000000";        
    fsm1_v.pu_size_y  := "1000000"; 
    fsm1_v.pu_position_x   := "000000";
    fsm1_v.pu_position_y   := "000000";
  elsif std_match(ctu_tree_r(pingpong_v).cu_64, "010-")then -- 64x32 x2
    fsm1_v.pu_size_x  := "1000000";       --64         
    fsm1_v.pu_size_y  := "0100000";       --32
    if fsm1_v.cu_partition_ptr= '0' then
      fsm1_v.pu_position_x := "000000";   --0
      fsm1_v.pu_position_y := "000000";   --0
    else
      fsm1_v.pu_position_x := "000000";   --0
      fsm1_v.pu_position_y := "100000";   --32
    end if;
  elsif std_match(ctu_tree_r(pingpong_v).cu_64, "100-") then-- 32x64 x2  
    fsm1_v.pu_size_x  := "0100000";       --32         
    fsm1_v.pu_size_y  := "1000000";       --64
    if fsm1_v.cu_partition_ptr= '0' then
      fsm1_v.pu_position_x := "000000";   --0
      fsm1_v.pu_position_y := "000000";   --0
    else
      fsm1_v.pu_position_x := "100000";   --32
      fsm1_v.pu_position_y := "000000";   --0
    end if;
  elsif std_match(ctu_tree_r(pingpong_v).cu_64, "0110") then -- 64x16, 64x48
    if fsm1_v.cu_partition_ptr= '0' then
      fsm1_v.pu_size_x := "1000000";     --64         
      fsm1_v.pu_size_y := "0010000";     --16
      fsm1_v.pu_position_x  := "000000"; --0
      fsm1_v.pu_position_y  := "000000"; --0
    else
      fsm1_v.pu_size_x := "1000000";     --64         
      fsm1_v.pu_size_y := "0110000";     --48
      fsm1_v.pu_position_x  := "000000"; --0
      fsm1_v.pu_position_y  := "010000"; --16
    end if;
  elsif std_match(ctu_tree_r(pingpong_v).cu_64, "0111") then -- 64x48, 64x16
    if fsm1_v.cu_partition_ptr= '0' then
      fsm1_v.pu_size_x := "1000000";     --64         
      fsm1_v.pu_size_y := "0110000";     --48
      fsm1_v.pu_position_x  := "000000"; --0
      fsm1_v.pu_position_y  := "000000"; --0
    else
      fsm1_v.pu_size_x := "1000000";     --64         
      fsm1_v.pu_size_y := "0010000";     --16
      fsm1_v.pu_position_x  := "000000"; --0
      fsm1_v.pu_position_y  := "110000"; --48
    end if;
  elsif std_match(ctu_tree_r(pingpong_v).cu_64, "1010") then -- 16x64, 48x64
    if fsm1_v.cu_partition_ptr= '0' then
      fsm1_v.pu_size_x := "0010000";     --16         
      fsm1_v.pu_size_y := "1000000";     --64
      fsm1_v.pu_position_x  := "000000"; --0
      fsm1_v.pu_position_y  := "000000"; --0
    else
      fsm1_v.pu_size_x := "0110000";     --48         
      fsm1_v.pu_size_y := "1000000";     --64
      fsm1_v.pu_position_x  := "010000"; --16
      fsm1_v.pu_position_y  := "000000"; --0
    end if;
  else                                                       -- 48x64, 16x64
    if fsm1_v.cu_partition_ptr= '0' then
      fsm1_v.pu_size_x := "0110000";     --48         
      fsm1_v.pu_size_y := "1000000";     --64
      fsm1_v.pu_position_x  := "000000"; --0
      fsm1_v.pu_position_y  := "000000"; --0
    else
      fsm1_v.pu_size_x := "0010000";     --16         
      fsm1_v.pu_size_y := "1000000";     --64
      fsm1_v.pu_position_x  := "110000"; --48
      fsm1_v.pu_position_y  := "000000"; --0
    end if;
  end if;

  when WORK_BLOCK =>
  fsm1_v.referenced_data_port_start := '0';
  fsm1_v.fifo_order_en    := '0';    

  -- Prepare supported variables
  update_cnt_v   := '0';
  if unsigned(fsm1_r.pu_size_x) > unsigned(fsm1_r.pu_size_y) then
    case fsm1_r.pu_size_x is
      when "1000000" =>
        shift_counters_v := "1000"; --8
      when "0100000" =>
        shift_counters_v := "0110"; --6
      when "0010000" =>
        shift_counters_v := "0100"; --4
      when "0001000" =>
        shift_counters_v := "0010"; --2
      when others =>
        shift_counters_v := "0000"; --0
    end case;
  else
    case fsm1_r.pu_size_y is
      when "1000000" =>
        shift_counters_v := "1000"; --8
      when "0100000" =>
        shift_counters_v := "0110"; --6
      when "0010000" =>
        shift_counters_v := "0100"; --4
      when "0001000" =>
        shift_counters_v := "0010"; --2
      when others =>
        shift_counters_v := "0000"; --0  
    end case;
  end if;

  -- Do operations for the selected type of prediction                               
  if prediction_information.inter_pred_flag = '1' then       --INTER PREDICTION	
		
    if fsm1_r.fifo_order_en = '0' then
      if referenced_data_port_out.ready='1' and fifo_order_settings.full='0'  then
         fsm1_v.referenced_data_port_start := '1';
         fsm1_v.fifo_order_en := '1';
      end if;
    else
      fsm1_v.referenced_data_port_start := '0';
      fsm1_v.fifo_order_en := '0';
      fsm1_v.prediction_parameter_addr := std_logic_vector(unsigned(fsm1_r.prediction_parameter_addr)+1);  -- read next data from MOTION_VECTOR_BUFFER
      if prediction_information.bi_pred_flag = '1' then
        if fsm1_r.bi_pred_block_ptr = '0' then
          fsm1_v.bi_pred_block_ptr := '1';
        else 
          fsm1_v.bi_pred_block_ptr := '0';
          update_cnt_v := '1';  -- go to operations updating counters and selecting next state
        end if;
      else
        update_cnt_v := '1';    -- go to operations updating counters and selecting next state
      end if;
    end if;	 
		
  else                                                       --INTRA PREDICTION 	 
		
    if fsm1_r.intra4x4 /= '1' or fsm1_r.chroma /= "00" then	
		  if fsm1_r.fifo_order_en = '0' then
      	if fifo_order_settings.full='0'  then
        	fsm1_v.fifo_order_en := '1'; 
      	end if;
    	else 
      	fsm1_v.fifo_order_en := '0';
      	fsm1_v.prediction_parameter_addr := std_logic_vector(unsigned(fsm1_r.prediction_parameter_addr)+1);  -- read next data from MOTION_VECTOR_BUFFER 
      	update_cnt_v := '1';      -- go to operations updating counters and selecting next state
				
				if fsm1_r.intra4x4 = '1' then
				  fsm1_v.prediction_parameter_addr := std_logic_vector(unsigned(fsm1_r.prediction_parameter_addr)+1);  -- read next data from MOTION_VECTOR_BUFFER
				end if;
					
    	end if;
		else 				
			if fsm1_r.cnt4 = "11" and fsm1_r.fifo_order_en = '1' then
				fsm1_v.fifo_order_en := '0';
      	fsm1_v.prediction_parameter_addr := std_logic_vector(unsigned(fsm1_r.prediction_parameter_addr)+1);  -- read next data from MOTION_VECTOR_BUFFER
      	update_cnt_v := '1';      -- go to operations updating counters and selecting next state 
			else
				if fsm1_r.fifo_order_en = '0' then
		    	if fifo_order_settings.full='0'  then
		      	fsm1_v.fifo_order_en := '1'; 
		    	end if;
				else
					fsm1_v.fifo_order_en := '0';
					fsm1_v.cnt4 := std_logic_vector(unsigned(fsm1_r.cnt4)+1);
				end if;
		  end if;	
		end if;
		
  end if;

  -- Do operations if there is need to update counters and change state
  if update_cnt_v = '1' then 
    counters_v := '0' & fsm1_r.cnt32 & fsm1_r.cnt16 & fsm1_r.cnt8 & fsm1_r.cnt4;
    if fsm1_r.pu_size_x = fsm1_r.pu_size_y then                                              -- for square unit (CodingUnit or PredictionUnit 4x4)
      -- Update counters by adding 1 in specific position (depending on size of current CU)
      counters_v := std_logic_vector(shift_right(unsigned(counters_v), to_integer(shift_counters_v)) + 1);
      counters_v := std_logic_vector(shift_left(unsigned(counters_v), to_integer(shift_counters_v)));
    else                                                                                     -- for other size unit there is need to check cu_partition_ptr
      if fsm1_r.cu_partition_ptr= '0' then
        fsm1_v.cu_partition_ptr:= '1';
      else
        -- Update counters  
        counters_v := std_logic_vector(shift_right(unsigned(counters_v), to_integer(shift_counters_v)) + 1);
        counters_v := std_logic_vector(shift_left(unsigned(counters_v), to_integer(shift_counters_v))); 
        fsm1_v.cu_partition_ptr:= '0';
      end if;
    end if;
    -- Update state and counters
    if counters_v(8) = '1' then                 -- End work for actual colour component (LUMA, CHROMA_B or CHROMA_R)
      fsm1_v.cnt32 := (others=>'0');
      fsm1_v.cnt16 := (others=>'0');
      fsm1_v.cnt8  := (others=>'0');
      fsm1_v.cnt4  := (others=>'0');
      fsm1_v.cu_partition_ptr:= '0';
      fsm1_v.prediction_parameter_addr := (others=>'0');
      case fsm1_r.chroma is 
        when "10" =>                            -- End of operation for CHROMA_B samples
          fsm1_v.chroma   := "11";
          fsm1_v.state    := WORK_TREE;         -- Go to operation for CHROMA_R samples
        when "11" =>                            -- End of operation for CHROMA_R samples
          fsm1_v.chroma   := "00";
          fsm1_v.state    := CHECK;             -- Go to next CTU
          fsm1_v.pingpong := not fsm1_r.pingpong; -- Change pingpong 
        when others =>                          -- End of operation for LUMA samples
          fsm1_v.chroma   := "10";
          fsm1_v.state    := WORK_TREE;         -- Go to operation for CHROMA_B samples
      end case;
    else                                        -- Work still for current colour component and current CTU
      fsm1_v.cnt32 := counters_v(7 downto 6);
      fsm1_v.cnt16 := counters_v(5 downto 4);
      fsm1_v.cnt8  := counters_v(3 downto 2);
      fsm1_v.cnt4  := counters_v(1 downto 0);
      fsm1_v.state := WORK_TREE;  
    end if;  
  else
    fsm1_v.state := WORK_BLOCK;
  end if;
  end case;
  
  -- Final assignment
  fsm1_next <= fsm1_v;  
end process;



  -- -------------------------------------------------------------------------
  --                         Process of FSM2                                --
  -- ------------------------------------------------------------------------- 
  
PROC_FSM2: process (fsm2_r, fsm3_r, start, fifo_order_settings, fifo_order_data, pu_parameter, interpolator_ready, interpolated_samples_dav_next, 
                    interpolated_samples_dav_r, interpolated_samples_dav_delay2_r, intra_predicted_samples_dav, predicted_samples_dav_r, predicted_samples_dav_delay1_r,
                    intra_reconstructed_samples_ack, referenced_data_port_out, chroma_format, intra_decoder_start_ack) 
                    
  variable fsm2_v                                : INTERNAL_FSM2_TYPE;
  variable referenced_data_port_sel_v            : std_logic;
  variable reconstructed_samples_read_priority_v : std_logic;
  variable max_referenced_samples_x_v            : std_logic_vector(6 downto 0); -- maximum number columns of referenced samples
  variable max_referenced_samples_y_v            : std_logic_vector(4 downto 0); -- maximum number rows of referenced samples
  variable max_y_v                               : std_logic_vector(4 downto 0); -- maximum number rows of processing samples (residuals or reconstructed samples)
  variable temp_v                                : std_logic_vector(12 downto 0);
  variable temp1_v                               : std_logic_vector(6 downto 0);
  
begin
  fsm2_v := fsm2_r;
  referenced_data_port_sel_v := '0';
  reconstructed_samples_read_priority_v := '0';

  case fsm2_r.state is  
    
  when IDLE =>
    fsm2_v.pingpong := '1';  
    fsm2_v.referenced_samples_dav := '0'; 
    fsm2_v.fifo_order_ack := '0'; 
    fsm2_v.interpolator_start := '0';
    fsm2_v.reconstructed_samples_write_en := "11"; -- write enable for reconstructed samples activated by low signal
    fsm2_v.swap_samples_flag := '0'; 
    fsm2_v.intra_decoder_start := '0'; 
    fsm2_v.intra_reconstructed_samples_dav := '0';
    fsm2_v.intra_predicted_samples_ready := '0';

    if start ='1' then
      fsm2_v.state := CHECK; 
    end if;
 
  when CHECK => 
    fsm2_v.referenced_samples_dav := '0'; 
    fsm2_v.fifo_order_ack := '0';
    fsm2_v.interpolator_start := '0';
    fsm2_v.reconstructed_samples_write_en := "11";  
    fsm2_v.swap_samples_flag := '0'; 
    fsm2_v.intra_decoder_start := '0'; 
    fsm2_v.intra_reconstructed_samples_dav := '0'; 
    fsm2_v.intra_predicted_samples_ready := '0';
    -- Prepare counters
    fsm2_v.residuals_x_cnt               := (others=>'0');
    fsm2_v.residuals_y_cnt               := (others=>'0');
    fsm2_v.reconstructed_samples_x_cnt   := (others=>'0');
    fsm2_v.reconstructed_samples_y_cnt   := (others=>'0');
    fsm2_v.referenced_samples_x_cnt      := (others=>'0');
    fsm2_v.referenced_samples_y_cnt      := (others=>'0');
    fsm2_v.intra_reconstructed_samples_x_cnt := (others=>'0');
    fsm2_v.intra_reconstructed_samples_y_cnt := (others=>'0');
    -- Prepare bidirectional prediction support signals 
    fsm2_v.bi_pred_delay_read_addr       := (others=>'0');    
    fsm2_v.bi_pred_delay_write_addr      := (others=>'0');

    if fifo_order_settings.empty = '0' then   
      -- Set pingpong
      fsm2_v.pingpong := fifo_order_data.pingpong;
      fsm2_v.chroma := fifo_order_data.chroma; 
      
	    -- Reset Weighted Prediction Parameters addres when chroma signal is changed 
	    if fifo_order_data.chroma /= fsm2_r.chroma then
        fsm2_v.weighted_prediction_parameter_addr := (others=>'0');
      end if;

      if fifo_order_data.inter_pred_flag = '1' then  
        
        if interpolator_ready = '1' then 
          -- Change state
          fsm2_v.state := INTER; 
          -- Start of work
          fsm2_v.interpolator_start := '1';
          -- Specific situation when CHROMA_FORMAT=1 and chroma="1-" for block which size_y = 2 or 6 and position_y is not multiple 4 - number of samples in related memory 
          if chroma_format = '1' then
            if fifo_order_data.chroma(1) = '1' then
              if fifo_order_data.pb_position_y(1 downto 0) = "10" then -- position_y mod 4 = 2 
                fsm2_v.swap_samples_flag:= '1';
              end if;
            end if;
          end if; 
        end if;
        
      else 
        
        -- Change state
        fsm2_v.state := INTRA;
        fsm2_v.intra_decoder_start := '1';

      end if;
    end if;
 
  when INTER =>
    fsm2_v.referenced_samples_dav := '0'; 
    fsm2_v.fifo_order_ack := '0';
    fsm2_v.interpolator_start := '0';
    fsm2_v.reconstructed_samples_write_en := "11";
    fsm2_v.intra_reconstructed_samples_dav := '0';

    -- Prepare variable used to change signals related to referenced samples
    if pu_parameter.chroma(1) = '0' then
      max_referenced_samples_x_v := std_logic_vector(unsigned(pu_parameter.pb_size_x) + 8);
      max_referenced_samples_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y) + 8, log2_ceil(NUMBER_OF_SAMPLES_DEC)), max_referenced_samples_y_v'length)); 
    else
      max_referenced_samples_x_v := std_logic_vector(unsigned(pu_parameter.pb_size_x) + 4);
      if chroma_format = '0' then
        max_referenced_samples_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y) + 8, log2_ceil(NUMBER_OF_SAMPLES_DEC)), max_referenced_samples_y_v'length));
      else
        if pu_parameter.pb_size_y(1 downto 0) = "10" then  -- Specific situation when size_y is not multiples of 4 (6 or 2) - only for 4:2:0 chroma 
          max_referenced_samples_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y) + 4, log2_ceil(NUMBER_OF_SAMPLES_DEC)) + 1, max_referenced_samples_y_v'length)); 
        else
          max_referenced_samples_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y) + 4, log2_ceil(NUMBER_OF_SAMPLES_DEC)), max_referenced_samples_y_v'length));
        end if;
      end if;
    end if;    
	
    -- Change signals related to Referenced Samples
    if fsm2_r.referenced_samples_y_cnt < max_referenced_samples_y_v then
      if referenced_data_port_out.empty = '0' then
        fsm2_v.referenced_samples_dav := '1';
        referenced_data_port_sel_v := '1';
        if unsigned(fsm2_r.referenced_samples_x_cnt) = unsigned(max_referenced_samples_x_v) - 1 then
          fsm2_v.referenced_samples_x_cnt := (others=>'0');
          fsm2_v.referenced_samples_y_cnt := std_logic_vector(unsigned(fsm2_r.referenced_samples_y_cnt) + 1);
        else
          fsm2_v.referenced_samples_x_cnt := std_logic_vector(unsigned(fsm2_r.referenced_samples_x_cnt) + 1);        
        end if;
      end if;
    end if;

    -- Change bidirectional prediction support addresses 
    if interpolated_samples_dav_next = '1' then
      fsm2_v.bi_pred_delay_read_addr := std_logic_vector(unsigned(fsm2_r.bi_pred_delay_read_addr)+1);
    end if;
    if interpolated_samples_dav_r = '1' then 
      fsm2_v.bi_pred_delay_write_addr := std_logic_vector(unsigned(fsm2_r.bi_pred_delay_write_addr)+1);
    end if;
  
    -- Prepare variable used to change address of reconstructed samples and residuals
    if pu_parameter.chroma(1) = '0' then
      max_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y), log2_ceil(NUMBER_OF_SAMPLES_DEC)), max_y_v'length)); 
    else
      if chroma_format = '0' then
        max_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y), log2_ceil(NUMBER_OF_SAMPLES_DEC)), max_y_v'length));
      else
        if pu_parameter.pb_size_y(1 downto 0) = "10" then -- Specific situation when size_y is not multiples of 4 (6 or 2) - only for 4:2:0 chroma 
          max_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y), log2_ceil(NUMBER_OF_SAMPLES_DEC)) + 1, max_y_v'length)); 
        else
          max_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y), log2_ceil(NUMBER_OF_SAMPLES_DEC)), max_y_v'length));
        end if;
      end if;
    end if;
  
    -- Change Residuals counters and read address
    if fsm2_r.residuals_y_cnt /= max_y_v then                
      if interpolated_samples_dav_delay2_r = '1' then
        -- Change counters
        if fsm2_r.residuals_x_cnt = std_logic_vector(unsigned(pu_parameter.pb_size_x) - 1) then
          fsm2_v.residuals_x_cnt := (others=>'0');
          fsm2_v.residuals_y_cnt := std_logic_vector(unsigned(fsm2_r.residuals_y_cnt)+1);
        else
          fsm2_v.residuals_x_cnt := std_logic_vector(unsigned(fsm2_r.residuals_x_cnt)+1);
        end if;
      end if;
    end if;
    
    -- Change address
    fsm2_v.residuals_addr(1)(7) := pu_parameter.chroma(1);
    if pu_parameter.chroma(1) = '0' then  
      if unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.residuals_y_cnt)&"00") >= 32 then
        fsm2_v.residuals_memory_ptr(1) := '1';
      else
        fsm2_v.residuals_memory_ptr(1) := '0';
      end if;
      if unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.residuals_x_cnt) >= 32 then  
        fsm2_v.residuals_memory_ptr(0) := '1'; 
      else
        fsm2_v.residuals_memory_ptr(0) := '0';
      end if;       
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_v.residuals_y_cnt)&"00"));
      fsm2_v.residuals_addr(1)(6 downto 0) := std_logic_vector((unsigned(temp1_v(4 downto 2))&"0000") + unsigned(pu_parameter.pb_position_x(4 downto 1)) + unsigned(fsm2_v.residuals_x_cnt(4 downto 1))); 
    else
      if chroma_format = '1' then
        if unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.residuals_y_cnt)&"00") >= 16 then
          fsm2_v.residuals_memory_ptr(1) := '1';
        else
          fsm2_v.residuals_memory_ptr(1) := '0';
        end if;
      else
        if unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.residuals_y_cnt)&"00") >= 32 then
          fsm2_v.residuals_memory_ptr(1) := '1';
        else
          fsm2_v.residuals_memory_ptr(1) := '0';
        end if;
      end if;
      if unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.residuals_x_cnt) >= 16 then
        fsm2_v.residuals_memory_ptr(0) := '1';
      else
        fsm2_v.residuals_memory_ptr(0) := '0';
      end if;
      fsm2_v.residuals_addr(1)(6) := pu_parameter.chroma(0);
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_v.residuals_y_cnt)&"00"));
      if chroma_format = '1' then
        fsm2_v.residuals_addr(1)(5 downto 0) := std_logic_vector(resize((unsigned(temp1_v(3 downto 2))&"000") + unsigned(pu_parameter.pb_position_x(3 downto 1)) + unsigned(fsm2_v.residuals_x_cnt(3 downto 1)),6)); 
      else
        fsm2_v.residuals_addr(1)(5 downto 0) := std_logic_vector(resize((unsigned(temp1_v(4 downto 2))&"000") + unsigned(pu_parameter.pb_position_x(3 downto 1)) + unsigned(fsm2_v.residuals_x_cnt(3 downto 1)),6)); 
      end if;  
    end if;
    -- Specific situation when CHROMA_FORMAT=1 and chroma="1-" for block which size_y = 6 and position_y is not multiple 4 - number of samples in related memory 
    if pu_parameter.pb_size_y = "0000110" and pu_parameter.pb_position_y(1 downto 0) = "10" then
      fsm2_v.residuals_addr(0) := std_logic_vector(unsigned(fsm2_v.residuals_addr(1)) + 8); -- 8 is number of memory cell in width for CHROMA residuals
    else
      fsm2_v.residuals_addr(0) := fsm2_v.residuals_addr(1);
    end if; 
  
    -- Change address and write enable signal for Reconstructed Samples 
    if predicted_samples_dav_r = '1' then
      -- Change counters
      if fsm2_r.reconstructed_samples_x_cnt = std_logic_vector(unsigned(pu_parameter.pb_size_x) - 1) then
        fsm2_v.reconstructed_samples_x_cnt := (others=>'0');
        fsm2_v.reconstructed_samples_y_cnt := std_logic_vector(unsigned(fsm2_r.reconstructed_samples_y_cnt)+1);
      else
        fsm2_v.reconstructed_samples_x_cnt := std_logic_vector(unsigned(fsm2_r.reconstructed_samples_x_cnt)+1);
      end if;
      -- Activate write enable
      fsm2_v.reconstructed_samples_write_en := "00"; 
      -- Specific situation when CHROMA_FORMAT=1 and chroma="1-" for block which size_y = 2 or 6 and position_y is not multiple of 4 - number of samples in related memory 
      if chroma_format = '1' and pu_parameter.chroma(1) = '1' then
        if pu_parameter.pb_size_y = "0000010" then         -- size_y equals 2
          if pu_parameter.pb_position_y(1 downto 0) = "00" then
            fsm2_v.reconstructed_samples_write_en(1) := '1';   -- deactivate 
          else
            fsm2_v.reconstructed_samples_write_en(0) := '1';   -- deactivate 
          end if; 
        elsif pu_parameter.pb_size_y = "0000110" then      -- size_y equals 6 
          if fsm2_r.reconstructed_samples_y_cnt /= "00000" then
            if pu_parameter.pb_position_y(1 downto 0) = "00" then
              fsm2_v.reconstructed_samples_write_en(1) := '1'; -- deactivate 
            else
              fsm2_v.reconstructed_samples_write_en(0) := '1'; -- deactivate 
            end if;
          end if;
        end if;
      end if;
    end if;
    
    -- Change address of writing
    fsm2_v.reconstructed_samples_write_addr(1)(10) := pu_parameter.chroma(1);
    if fifo_order_data.chroma(1) = '0' then
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.reconstructed_samples_y_cnt)&"00")); 
      fsm2_v.reconstructed_samples_write_addr(1)(9 downto 0) := std_logic_vector((unsigned(temp1_v(5 downto 2))&"000000") + unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.reconstructed_samples_x_cnt));  
    else
      fsm2_v.reconstructed_samples_write_addr(1)(9) := pu_parameter.chroma(0);
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.reconstructed_samples_y_cnt)&"00")); 
      fsm2_v.reconstructed_samples_write_addr(1)(8 downto 0) := std_logic_vector((unsigned(temp1_v(5 downto 2))&"00000") + unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.reconstructed_samples_x_cnt));  
    end if;
    -- Specific situation when CHROMA_FORMAT=1 and chroma="1-" for block which size_y = 6 and position_y is not multiple of 4 - number of samples in related memory 
    if pu_parameter.pb_size_y = "0000110" and pu_parameter.pb_position_y(1 downto 0) = "10" then
      fsm2_v.reconstructed_samples_write_addr(0) := std_logic_vector(unsigned(fsm2_v.reconstructed_samples_write_addr(1)) + 32);-- 32 is number of memory cells in width for CHROMA residuals 
    else
      fsm2_v.reconstructed_samples_write_addr(0) := fsm2_v.reconstructed_samples_write_addr(1);
    end if;  
    
    -- Operation before going to CHECK state
    if fsm2_r.fifo_order_ack = '1' then
      if pu_parameter.bi_pred_flag = '1' then
        fsm2_v.bi_pred_block_ptr :=  not fsm2_r.bi_pred_block_ptr; 
      end if;
      -- Prepare weighted prediction parameter read address
      fsm2_v.weighted_prediction_parameter_addr := std_logic_vector( unsigned(fsm2_r.weighted_prediction_parameter_addr) + 1);
    end if;
  
  when INTRA => 
    -- Control all Modules associated with intra prediction: it will be finished with INTRA implementation 
    fsm2_v.referenced_samples_dav := '0'; 
    fsm2_v.fifo_order_ack := '0';
    fsm2_v.interpolator_start := '0';
    fsm2_v.reconstructed_samples_write_en := "11"; 
    fsm2_v.intra_decoder_start := '0'; 
    fsm2_v.intra_predicted_samples_ready := '0';
    fsm2_v.intra_reconstructed_samples_dav := '0';
    
    if fsm2_r.intra_decoder_start = '1' then
      if intra_decoder_start_ack = '1' then
        fsm2_v.intra_decoder_start := '0'; 
      else
        fsm2_v.intra_decoder_start := '1';
      end if;
    end if;
  
    -- Prepare variable used to change address for reconstructed samples and residuals
    max_y_v := std_logic_vector(resize(shift_right(unsigned(pu_parameter.pb_size_y), log2_ceil(NUMBER_OF_SAMPLES_DEC)), max_y_v'length));
  
    -- Change Residuals counters and read address
    if fsm2_r.residuals_y_cnt /= max_y_v then
      -- Automat is ready to receive next predicted samples
      fsm2_v.intra_predicted_samples_ready := '1';
      
      if intra_predicted_samples_dav = '1' then
        -- Change counters
        if fsm2_r.residuals_x_cnt = std_logic_vector(unsigned(pu_parameter.pb_size_x) - 1) then
          fsm2_v.residuals_x_cnt := (others=>'0');
          fsm2_v.residuals_y_cnt := std_logic_vector(unsigned(fsm2_r.residuals_y_cnt)+1);
        else
          fsm2_v.residuals_x_cnt := std_logic_vector(unsigned(fsm2_r.residuals_x_cnt)+1);
        end if;
      end if; 
    end if;
    
    -- Change address
    fsm2_v.residuals_addr(1)(7) := pu_parameter.chroma(1);
    if pu_parameter.chroma(1) = '0' then  
      if unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.residuals_y_cnt)&"00") >= 32 then
        fsm2_v.residuals_memory_ptr(1) := '1';
      else
        fsm2_v.residuals_memory_ptr(1) := '0';
      end if;
      if unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.residuals_x_cnt) >= 32 then  
        fsm2_v.residuals_memory_ptr(0) := '1'; 
      else
        fsm2_v.residuals_memory_ptr(0) := '0';
      end if;       
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_v.residuals_y_cnt)&"00"));
      fsm2_v.residuals_addr(1)(6 downto 0) := std_logic_vector((unsigned(temp1_v(4 downto 2))&"0000") + unsigned(pu_parameter.pb_position_x(4 downto 1)) + unsigned(fsm2_v.residuals_x_cnt(4 downto 1))); 
    else
      if chroma_format = '1' then
        if unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.residuals_y_cnt)&"00") >= 16 then
          fsm2_v.residuals_memory_ptr(1) := '1';
        else
          fsm2_v.residuals_memory_ptr(1) := '0';
        end if;
      else
        if unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.residuals_y_cnt)&"00") >= 32 then
          fsm2_v.residuals_memory_ptr(1) := '1';
        else
          fsm2_v.residuals_memory_ptr(1) := '0';
        end if;
      end if;
      if unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.residuals_x_cnt) >= 16 then
        fsm2_v.residuals_memory_ptr(0) := '1';
      else
        fsm2_v.residuals_memory_ptr(0) := '0';
      end if;
      fsm2_v.residuals_addr(1)(6) := pu_parameter.chroma(0);
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_v.residuals_y_cnt)&"00"));
      if chroma_format = '1' then
        fsm2_v.residuals_addr(1)(5 downto 0) := std_logic_vector(resize((unsigned(temp1_v(3 downto 2))&"000") + unsigned(pu_parameter.pb_position_x(3 downto 1)) + unsigned(fsm2_v.residuals_x_cnt(3 downto 1)),6)); 
      else
        fsm2_v.residuals_addr(1)(5 downto 0) := std_logic_vector(resize((unsigned(temp1_v(4 downto 2))&"000") + unsigned(pu_parameter.pb_position_x(3 downto 1)) + unsigned(fsm2_v.residuals_x_cnt(3 downto 1)),6)); 
      end if;  
    end if;    
    -- In INTRA Prediction only NxN size of block is possible, so we not need swap samples like in INTER Prediction in specific situations
    fsm2_v.residuals_addr(0) := fsm2_v.residuals_addr(1);
  
  
    -- Change write address and write enable signal for Reconstructed samples  
    if predicted_samples_dav_delay1_r = '1' then
      -- Change counters
      if fsm2_r.reconstructed_samples_x_cnt = std_logic_vector(unsigned(pu_parameter.pb_size_x) - 1) then
        fsm2_v.reconstructed_samples_x_cnt := (others=>'0');
        fsm2_v.reconstructed_samples_y_cnt := std_logic_vector(unsigned(fsm2_r.reconstructed_samples_y_cnt)+1);
      else
        fsm2_v.reconstructed_samples_x_cnt := std_logic_vector(unsigned(fsm2_r.reconstructed_samples_x_cnt)+1);
      end if;
      -- Activate write enable
      fsm2_v.reconstructed_samples_write_en := "00"; 
    end if;
  
    -- Change address of writing
    fsm2_v.reconstructed_samples_write_addr(1)(10) := pu_parameter.chroma(1);
    if fifo_order_data.chroma(1) = '0' then
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.reconstructed_samples_y_cnt)&"00")); 
      fsm2_v.reconstructed_samples_write_addr(1)(9 downto 0) := std_logic_vector((unsigned(temp1_v(5 downto 2))&"000000") + unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.reconstructed_samples_x_cnt));  
    else
      fsm2_v.reconstructed_samples_write_addr(1)(9) := pu_parameter.chroma(0);
      temp1_v := std_logic_vector(unsigned(pu_parameter.pb_position_y) + (unsigned(fsm2_r.reconstructed_samples_y_cnt)&"00")); 
      fsm2_v.reconstructed_samples_write_addr(1)(8 downto 0) := std_logic_vector((unsigned(temp1_v(5 downto 2))&"00000") + unsigned(pu_parameter.pb_position_x) + unsigned(fsm2_r.reconstructed_samples_x_cnt));  
    end if;
    -- In INTRA Prediction only NxN size of block is possible, so we need change address(0) like in INTER Prediction in specific situations
    fsm2_v.reconstructed_samples_write_addr(0) := fsm2_v.reconstructed_samples_write_addr(1);
    
  end case;
  
  
  -- Common operation for INTRA and INTER state
  if fsm2_r.state = INTRA or fsm2_r.state = INTER then
    -- Change read address and read enable signal for Reconstructed samples
    if fsm2_r.intra_reconstructed_samples_y_cnt = max_y_v then  --The end of processing data for actual PredictionBlock    

      if fsm2_r.fifo_order_ack = '1' then                                
        fsm2_v.state := CHECK;
        fsm2_v.fifo_order_ack := '0';
      else
        fsm2_v.fifo_order_ack := '1';
      end if;

    else 
      
      if unsigned(fsm2_r.reconstructed_samples_y_cnt) > unsigned(fsm2_r.intra_reconstructed_samples_y_cnt) or 
        ( fsm2_r.reconstructed_samples_y_cnt = fsm2_r.intra_reconstructed_samples_y_cnt and unsigned(fsm2_r.reconstructed_samples_x_cnt) > unsigned(fsm2_r.intra_reconstructed_samples_x_cnt) ) then 
        
        if intra_reconstructed_samples_ack = '1' then
          if unsigned(fsm2_r.intra_reconstructed_samples_x_cnt) = unsigned(pu_parameter.pb_size_x) - 1 then
            fsm2_v.intra_reconstructed_samples_x_cnt := (others=>'0');
            fsm2_v.intra_reconstructed_samples_y_cnt := std_logic_vector(unsigned(fsm2_r.intra_reconstructed_samples_y_cnt)+1);
          else
            fsm2_v.intra_reconstructed_samples_x_cnt := std_logic_vector(unsigned(fsm2_r.intra_reconstructed_samples_x_cnt)+1);
          end if;
        end if;

        -- Set priority of reading from reconstructed samples buffer
        if ( unsigned(fsm2_v.intra_reconstructed_samples_x_cnt) = unsigned(pu_parameter.pb_size_x) - 1 )  or ( unsigned(fsm2_v.intra_reconstructed_samples_y_cnt) = unsigned(max_y_v) - 1 ) then 
          reconstructed_samples_read_priority_v := '1';
        end if; 
        
        -- Set dav signal of reconstructed samples
        if unsigned(fsm2_r.reconstructed_samples_y_cnt) > unsigned(fsm2_v.intra_reconstructed_samples_y_cnt) or 
          ( fsm2_r.reconstructed_samples_y_cnt = fsm2_v.intra_reconstructed_samples_y_cnt and unsigned(fsm2_r.reconstructed_samples_x_cnt) > unsigned(fsm2_v.intra_reconstructed_samples_x_cnt) ) then
          fsm2_v.intra_reconstructed_samples_dav := '1';
        end if;
        
      end if;
    end if;
    
  end if;
  
  -- Prepare pointer indicates the valid residual read from memory in which in each cell are two samples
  fsm2_v.residual_valid_ptr := fsm2_r.residuals_x_cnt(0);
  
  -- Final assignment
  fsm2_next <= fsm2_v; 
  referenced_data_port_sel <= referenced_data_port_sel_v;  
  reconstructed_samples_read_priority <= reconstructed_samples_read_priority_v;
end process;




  -- -------------------------------------------------------------------------
  --                         Process of FSM3                                --
  -- ------------------------------------------------------------------------- 
  
PROC_FSM3: process (input_data, fsm3_r, fsm2_r, start, chroma_format, ctu_tree_r, ctu_position_r, reconstructed_samples_read_priority)
  variable fsm3_v         : INTERNAL_FSM3_TYPE; 
  variable ctu_tree_v     : CTU_TREE_ARRAY; 
  variable ctu_position_v : CTU_POSITION_ARRAY;
begin
 
  fsm3_v         := fsm3_r; 
  ctu_tree_v     := ctu_tree_r; 
  ctu_position_v := ctu_position_r;  
  
  -- Update dav signal used by fsm1
  if fsm2_r.pingpong = '1' then
    fsm3_v.pingpong_dav(1) := '0';
  else
    fsm3_v.pingpong_dav(0) := '0';
  end if;
  
  -- Update other signal
  case fsm3_r.state is
  when IDLE =>
    fsm3_v.ready          := '0';
    fsm3_v.pingpong_dav   := "00";
    fsm3_v.pingpong       := '0';
    fsm3_v.residuals_write_en := (others=>'1'); 
    fsm3_v.reconstructed_samples_dav := '0';
    -- Waiting for start device to read data of current CODING_TREE_UNIT.
    if start = '1' then  
      fsm3_v.ready := '1'; -- ready for next CODING_TREE_UNIT's data
      fsm3_v.state := WORK;
      --Prepare all address
      fsm3_v.residuals_write_en := "1110"; -- top memory: "1110", bottom memory: "0111"
      fsm3_v.residuals_addr(0)  := (others=>'0');
      fsm3_v.residuals_addr(1)  := std_logic_vector(to_unsigned(15, fsm3_v.residuals_addr(1)'length));
      fsm3_v.residuals_addr(2)  := std_logic_vector(to_unsigned(112, fsm3_v.residuals_addr(2)'length));
      fsm3_v.residuals_addr(3)  := std_logic_vector(to_unsigned(127, fsm3_v.residuals_addr(3)'length)); 
      fsm3_v.prediction_parameter_addr  := (others=>'0'); 
      fsm3_v.reconstructed_samples_addr := (others=>'0');
	  fsm3_v.weighted_prediction_parameter_addr := (others=>'0'); 
    end if;   
  
  when BUSY =>
    fsm3_v.ready := '0';
    fsm3_v.residuals_write_en := (others=>'1');   
    fsm3_v.reconstructed_samples_dav := '0';
    -- Check if fsm2 finished work with previous CODING_TREE_UNIT. If finished start reading the next CODING_TREE_UNIT's data.
    if not fsm3_r.pingpong = fsm2_r.pingpong then
      fsm3_v.ready := '1'; -- ready for next CODING_TREE_UNIT's data
      fsm3_v.state := WORK;
      --Prepare all address
      fsm3_v.residuals_write_en := "1110"; -- top memory: "1110", bottom memory: "0111"
      fsm3_v.residuals_addr(0)  := (others=>'0');
      fsm3_v.residuals_addr(1)  := std_logic_vector(to_unsigned(15, fsm3_v.residuals_addr(1)'length));
      fsm3_v.residuals_addr(2)  := std_logic_vector(to_unsigned(112, fsm3_v.residuals_addr(2)'length));
      fsm3_v.residuals_addr(3)  := std_logic_vector(to_unsigned(127, fsm3_v.residuals_addr(3)'length)); 
      fsm3_v.prediction_parameter_addr  := (others=>'0'); 
      fsm3_v.reconstructed_samples_addr := (others=>'0');
	  fsm3_v.weighted_prediction_parameter_addr := (others=>'0');
    end if;

  when WORK =>
    fsm3_v.reconstructed_samples_dav := '0';
  
    -- ctu_tree and CODING_UNIT Position Information 
    if fsm3_r.pingpong = '0' then
      ctu_tree_v(0)     := input_data.ctu_tree; 
      ctu_position_v(0) := input_data.ctu_position; 
    else
      ctu_tree_v(1)     := input_data.ctu_tree; 
      ctu_position_v(1) := input_data.ctu_position; 
    end if;
    -- If input data is valid change the address of saving - Residuals
    if input_data.residuals_dav = '1' then
      fsm3_v.ready := '0'; 
      if chroma_format = '1' then -- CHROMA_FORMAT = 4:2:0
        -- Save LUMA residuals
        if unsigned(fsm3_r.residuals_addr(0)) < 127 then
          if fsm3_r.residuals_addr(1)(3 downto 0) = "0000" then 
            -- Change the enabled signal
            fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
            -- Change address
            if fsm3_r.residuals_write_en = "0111" then 
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 31);
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 31); 
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1); 
            else
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 15); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 15);  
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 15);
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 15); 
            end if;
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) - 1);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) + 1);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1); 
          end if;
        -- Check if all LUMA residuals are saved, then start save CHROMA residuals
        elsif unsigned(fsm3_r.residuals_addr(0)) = 127  then 
          -- Change the enabled signal
          fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
          if fsm3_r.residuals_write_en = "0111" then           
            fsm3_v.residuals_addr(0) := std_logic_vector(to_unsigned(128, fsm3_v.residuals_addr(3)'length));
            fsm3_v.residuals_addr(1) := std_logic_vector(to_unsigned(135, fsm3_v.residuals_addr(1)'length));
            fsm3_v.residuals_addr(2) := std_logic_vector(to_unsigned(152, fsm3_v.residuals_addr(2)'length));
            fsm3_v.residuals_addr(3) := std_logic_vector(to_unsigned(159, fsm3_v.residuals_addr(3)'length));
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 15); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 15);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 15);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 15); 
          end if;
        -- Check if all CHROMA_B residuals are saved
        elsif unsigned(fsm3_r.residuals_addr(0)) = 159  then 
          -- Change the enabled signal
          fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
          if fsm3_r.residuals_write_en = "0111" then   
            -- Change address to save CHROMA_R samples
            fsm3_v.residuals_addr(0) := std_logic_vector(to_unsigned(192, fsm3_v.residuals_addr(3)'length));
            fsm3_v.residuals_addr(1) := std_logic_vector(to_unsigned(199, fsm3_v.residuals_addr(1)'length));
            fsm3_v.residuals_addr(2) := std_logic_vector(to_unsigned(216, fsm3_v.residuals_addr(2)'length));
            fsm3_v.residuals_addr(3) := std_logic_vector(to_unsigned(223, fsm3_v.residuals_addr(3)'length)); 
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 7); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 7);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 7);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 7); 
          end if;
        -- Check if all CHROMA_R residuals are saved
        elsif unsigned(fsm3_r.residuals_addr(0)) = 223 then
          -- Change the enabled signal
          fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
          if fsm3_r.residuals_write_en = "0111" then  
            -- Change the write enabled signals
            fsm3_v.residuals_write_en  := "1111";
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 7); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 7);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 7);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 7); 
          end if;
        else 
          if fsm3_r.residuals_addr(1)(2 downto 0) = "000" then 
            -- Change the enabled signal
            fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
            -- Change address
            if fsm3_r.residuals_write_en = "0111" then            
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 15);
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 15); 
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1);
            else
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 7); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 7);  
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 7);
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 7); 
            end if;
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) - 1);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) + 1);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1); 
          end if;
        end if;
      else -- CHROMA_FORMAT = 4:2:2
        -- Save LUMA residuals
        if unsigned(fsm3_r.residuals_addr(0)) < 127 then
          if fsm3_r.residuals_addr(1)(3 downto 0) = "0000" then 
            -- Change the enabled signal
            fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
            -- Change address
            if fsm3_r.residuals_write_en = "0111" then 
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 31);
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 31); 
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1); 
            else
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 15); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 15);  
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 15);
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 15); 
            end if;
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) - 1);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) + 1);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1); 
          end if;
        -- Check if all LUMA residuals are saved, then start save CHROMA residuals
        elsif unsigned(fsm3_r.residuals_addr(0)) = 127 then 
          -- Change the enabled signal
          fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
          -- Change address
          if fsm3_r.residuals_write_en = "0111" then           
            fsm3_v.residuals_addr(0) := std_logic_vector(to_unsigned(128, fsm3_v.residuals_addr(3)'length));
            fsm3_v.residuals_addr(1) := std_logic_vector(to_unsigned(135, fsm3_v.residuals_addr(1)'length));
            fsm3_v.residuals_addr(2) := std_logic_vector(to_unsigned(184, fsm3_v.residuals_addr(2)'length));
            fsm3_v.residuals_addr(3) := std_logic_vector(to_unsigned(191, fsm3_v.residuals_addr(3)'length));
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 15); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 15);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 15);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 15); 
          end if;
        -- Check if all CHROMA_B residuals are saved
        elsif unsigned(fsm3_r.residuals_addr(0)) = 191 then 
          -- Change the enabled signal
          fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
          -- Change address to save CHROMA_R samples
          if fsm3_r.residuals_write_en = "0111" then           
            fsm3_v.residuals_addr(0) := std_logic_vector(to_unsigned(192, fsm3_v.residuals_addr(3)'length));
            fsm3_v.residuals_addr(1) := std_logic_vector(to_unsigned(199, fsm3_v.residuals_addr(1)'length));
            fsm3_v.residuals_addr(2) := std_logic_vector(to_unsigned(248, fsm3_v.residuals_addr(2)'length));
            fsm3_v.residuals_addr(3) := std_logic_vector(to_unsigned(255, fsm3_v.residuals_addr(3)'length));
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 7); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 7);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 7);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 7); 
          end if;       
        -- Check if all CHROMA_R residuals are saved
        elsif unsigned(fsm3_r.residuals_addr(0)) = 255 then
          -- Change the enabled signal
          fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
          if fsm3_r.residuals_write_en = "0111" then  
            -- Change the write enabled signals
            fsm3_v.residuals_write_en  := "1111";
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 7); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 7);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 7);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 7); 
          end if;
        else
          if fsm3_r.residuals_addr(1)(2 downto 0) = "000" then 
            -- Change the enabled signal
            fsm3_v.residuals_write_en := fsm3_r.residuals_write_en(2 downto 0) & fsm3_r.residuals_write_en(3); -- rotation 1 bit in left
            -- Change address
            if fsm3_r.residuals_write_en = "0111" then            
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 15);
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 15); 
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1);
            else
              fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) - 7); 
              fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) + 7);  
              fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) - 7);
              fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) + 7); 
            end if;
          else
            fsm3_v.residuals_addr(0) := std_logic_vector(unsigned(fsm3_r.residuals_addr(0)) + 1); 
            fsm3_v.residuals_addr(1) := std_logic_vector(unsigned(fsm3_r.residuals_addr(1)) - 1);
            fsm3_v.residuals_addr(2) := std_logic_vector(unsigned(fsm3_r.residuals_addr(2)) + 1);
            fsm3_v.residuals_addr(3) := std_logic_vector(unsigned(fsm3_r.residuals_addr(3)) - 1); 
          end if;
        end if;
      end if;  
    end if;

    -- If input data is valid change the address of saving - Motion Vectors and Weighted Prediction Parameters 
    if input_data.prediction_parameter_dav = '1' then
      fsm3_v.ready   := '0';
      fsm3_v.prediction_parameter_addr :=  std_logic_vector(unsigned(fsm3_r.prediction_parameter_addr) + 1);
	  -- When Prediction Parameter vector is related to PU reconstructed in inter mode change address of Weighted Prediction Parameter
      if input_data.prediction_parameter(0) = '1' then  -- inter_pred_flag = '1'
        fsm3_v.weighted_prediction_parameter_addr :=  std_logic_vector(unsigned(fsm3_r.weighted_prediction_parameter_addr) + 1);
      end if;	  
    end if;    
    
    -- When reading from reconstructed memory is available for FSM3
    if reconstructed_samples_read_priority = '0' then
      
      -- Prepare dav signal of reconstructed samples
      if fsm3_r.output_ctu_valid_cnt = "10" then
        fsm3_v.reconstructed_samples_dav  := '1';
      else
        if chroma_format = '1' then
          if fsm3_r.reconstructed_samples_addr = "11011111111" then -- end of all reconstructed samples
            fsm3_v.output_ctu_valid_cnt := std_logic_vector(unsigned(fsm3_r.output_ctu_valid_cnt) + 1); 
          end if;
        else
          if fsm3_r.reconstructed_samples_addr = "11111111111" then -- end of all reconstructed samples
            fsm3_v.output_ctu_valid_cnt := std_logic_vector(unsigned(fsm3_r.output_ctu_valid_cnt) + 1);   
          end if;
        end if;
      end if;
    
      -- Prepare address of reconstructed samples and after read all samples update state.
      if chroma_format = '1' then  
        if fsm3_r.reconstructed_samples_addr = "10011111111" then     -- 1279 - end of CHROMA_B reconstructed samples  
          fsm3_v.reconstructed_samples_addr := "11000000000";
        elsif fsm3_r.reconstructed_samples_addr = "11011111111" then  -- 1791 end of CHROMA_R reconstructed samples
          -- Change state
          fsm3_v.state    := BUSY;
          fsm3_v.pingpong := not fsm3_r.pingpong; 
          -- Change the status of stored data for actual pingpong
          if fsm3_r.pingpong = '0' then
            fsm3_v.pingpong_dav(0) := '1';
          else
            fsm3_v.pingpong_dav(1) := '1';
          end if;
        else
          fsm3_v.reconstructed_samples_addr := std_logic_vector(unsigned(fsm3_r.reconstructed_samples_addr) + 1); 
        end if;
      else                                            
        if fsm3_r.reconstructed_samples_addr = "11111111111" then     -- end of all reconstructed samples
          -- Change state
          fsm3_v.state := BUSY; 
          fsm3_v.pingpong := not fsm3_r.pingpong; 
          -- Change the status of stored data for actual pingpong
          if fsm3_r.pingpong = '0' then
            fsm3_v.pingpong_dav(0) := '1';
          else
            fsm3_v.pingpong_dav(1) := '1';
          end if;
        else
          fsm3_v.reconstructed_samples_addr := std_logic_vector(unsigned(fsm3_r.reconstructed_samples_addr) + 1);
        end if; 
      end if;
      
    end if;
    
  end case;

  -- Assignment 
  fsm3_next  <= fsm3_v;
  ctu_tree_next <= ctu_tree_v;
  ctu_position_next   <= ctu_position_v;
end process; 





  -- -------------------------------------------------------------------------
  --                           Other Processes                              --
  -- ------------------------------------------------------------------------- 

-- Process of output assignment 
PROC_OUTPUT: process (fsm3_r, reconstructed_samples_read)
begin
  output_data.ready  <= fsm3_r.ready; 
  output_data.reconstructed_samples_dav <= fsm3_r.reconstructed_samples_dav; 
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    output_data.reconstructed_samples(i) <= reconstructed_samples_read((8+DELTA_BIT_DEPTH)*(i+1)-1 downto (8+DELTA_BIT_DEPTH)*i);
    reconstructed_samples(i) <= reconstructed_samples_read((8+DELTA_BIT_DEPTH)*(i+1)-1 downto (8+DELTA_BIT_DEPTH)*i); 
  end loop;
end process;
  


-- Process of reading and writing internal FIFO_ORDER data
PROC_FIFO_ORDER: process(fifo_order_settings, prediction_information, fsm1_r, referenced_sample_valid_ptr, chroma_format)  
  variable fifo_data_in_v    : std_logic_vector(37+log2_ceil(NUMBER_OF_SAMPLES_DEC) -1 downto 0);
  variable fifo_order_data_v : FIFO_DATA_READ_TYPE;
  -- Support variables
  variable size_x_v          : std_logic_vector(6 downto 0); -- size of PB
  variable size_y_v          : std_logic_vector(6 downto 0); -- size of PB
  variable position_x_v      : std_logic_vector(5 downto 0); -- position of PB in CB 
  variable position_y_v      : std_logic_vector(5 downto 0); -- position of PB in CB 	
	variable mv_fract_x_v      : std_logic_vector(2 downto 0);
	variable mv_fract_y_v      : std_logic_vector(2 downto 0);  
	-- Aliases
	alias fifo_pingpong: 							      std_logic 										            				is fifo_data_in_v(0);
  alias fifo_pb_position_x: 							std_logic_vector(5 downto 0) 										  is fifo_data_in_v(6 downto 1);
	alias fifo_pb_position_y: 			        std_logic_vector(5 downto 0) 	            				is fifo_data_in_v(12 downto 7);
	alias fifo_pb_size_x: 			            std_logic_vector(6 downto 0) 	 										is fifo_data_in_v(19 downto 13);
  alias fifo_pb_size_y: 			            std_logic_vector(6 downto 0) 	 										is fifo_data_in_v(26 downto 20); 
  alias fifo_chroma: 			                std_logic_vector(1 downto 0) 	 										is fifo_data_in_v(28 downto 27);   
  alias fifo_inter_pred_flag: 			      std_logic 	 										                  is fifo_data_in_v(29); 
  alias fifo_bi_pred_flag: 			          std_logic                    	 										is fifo_data_in_v(30); 
  alias fifo_mv_fract_x: 			            std_logic_vector(2 downto 0) 	 										is fifo_data_in_v(33 downto 31);   
  alias fifo_mv_fract_y: 			            std_logic_vector(2 downto 0) 	 										is fifo_data_in_v(36 downto 34); 
  alias fifo_referenced_sample_valid_ptr: std_logic_vector(log2_ceil(NUMBER_OF_SAMPLES_DEC)-1 downto 0)	is fifo_data_in_v(36+log2_ceil(NUMBER_OF_SAMPLES_DEC) downto 37);
  alias fifo_intra_pred_mode: 			      std_logic_vector(LOG_INTRA_PRED_MODE-1 downto 0)    is fifo_data_in_v(29+LOG_INTRA_PRED_MODE downto 30);
begin 
	
  -- FIFO write
  if fsm1_r.chroma(1) = '1' then	 
    -- For chroma signal size of PB is not the same as size of PU
    size_x_v       := '0' & fsm1_r.pu_size_x(6 downto 1);
    position_x_v   := '0' & fsm1_r.pu_position_x(5 downto 1);
		mv_fract_x_v   := prediction_information.motion_vector(0)(2 downto 0);
    if chroma_format = '1' then
      size_y_v     := '0' & fsm1_r.pu_size_y(6 downto 1);
      position_y_v := '0' & fsm1_r.pu_position_y(5 downto 1);
			mv_fract_y_v := prediction_information.motion_vector(1)(2 downto 0);
    else
      size_y_v     := fsm1_r.pu_size_y;
      position_y_v := fsm1_r.pu_position_y;
			mv_fract_y_v := prediction_information.motion_vector(1)(1 downto 0)&'0';
    end if;	
	else	
		mv_fract_x_v := prediction_information.motion_vector(0)(1 downto 0)&'0';
		mv_fract_y_v := prediction_information.motion_vector(1)(1 downto 0)&'0';
		if fsm1_r.intra4x4 = '1' then	
			size_x_v := "0000100"; -- 4 
			size_y_v := "0000100"; -- 4
      position_x_v := fsm1_r.pu_position_x; 
      position_y_v := fsm1_r.pu_position_y;
		  case fsm1_r.cnt4 is
        when "01" =>  -- top-right PU 4x4 in current CU 8x8
					position_x_v := std_logic_vector(unsigned(fsm1_r.pu_position_x)+4); 
        when "10" =>  -- bottom-left PU 4x4 in current CU 8x8 
					position_y_v := std_logic_vector(unsigned(fsm1_r.pu_position_y)+4);
        when "11" =>  -- bottom-right PU 4x4 in current CU 8x8
          position_x_v := std_logic_vector(unsigned(fsm1_r.pu_position_x)+4); 
          position_y_v := std_logic_vector(unsigned(fsm1_r.pu_position_y)+4);
        when others =>
				  --Position is correct
			end case;
		else
			size_x_v := fsm1_r.pu_size_x;
			size_y_v := fsm1_r.pu_size_y;
      position_x_v := fsm1_r.pu_position_x; 
      position_y_v := fsm1_r.pu_position_y;		
		end if;		
  end if;		
 
  fifo_data_in_v := (others=>'0');
  fifo_pingpong := fsm1_r.pingpong;
  fifo_pb_position_x := position_x_v;
  fifo_pb_position_y := position_y_v;
  fifo_pb_size_x := size_x_v;
  fifo_pb_size_y := size_y_v;
  fifo_chroma := fsm1_r.chroma;
  fifo_inter_pred_flag := prediction_information.inter_pred_flag;
  if prediction_information.inter_pred_flag = '1' then
	  fifo_referenced_sample_valid_ptr := referenced_sample_valid_ptr;
    fifo_mv_fract_y := mv_fract_y_v;
    fifo_mv_fract_x := mv_fract_x_v;
    fifo_bi_pred_flag := prediction_information.bi_pred_flag;
  else
    if fsm1_r.chroma(1) = '1' then
      fifo_intra_pred_mode := prediction_information.intra_chroma_pred_mode;
    else
      case fsm1_r.cnt4 is
        when "00" =>
        fifo_intra_pred_mode := prediction_information.intra_luma_pred_mode_0;
        when "01" =>
        fifo_intra_pred_mode := prediction_information.intra_luma_pred_mode_1;
        when "10" =>
        fifo_intra_pred_mode := prediction_information.intra_luma_pred_mode_2;
        when others =>
        fifo_intra_pred_mode := prediction_information.intra_luma_pred_mode_3;    
      end case;
    end if;
  end if;
  
  -- FIFO read 
  fifo_order_data_v.pingpong                    := fifo_order_settings.data_out(0); 
  fifo_order_data_v.pb_position_x               := fifo_order_settings.data_out(6 downto 1); 
  fifo_order_data_v.pb_position_y               := fifo_order_settings.data_out(12 downto 7); 
  fifo_order_data_v.pb_size_x                   := fifo_order_settings.data_out(19 downto 13); 
  fifo_order_data_v.pb_size_y                   := fifo_order_settings.data_out(26 downto 20); 
  fifo_order_data_v.chroma                      := fifo_order_settings.data_out(28 downto 27); 
  fifo_order_data_v.inter_pred_flag             := fifo_order_settings.data_out(29); 
  -- Inter prediction interface
	fifo_order_data_v.bi_pred_flag                := fifo_order_settings.data_out(30); 
  fifo_order_data_v.motion_vector_fract_x       := fifo_order_settings.data_out(33 downto 31); 
  fifo_order_data_v.motion_vector_fract_y       := fifo_order_settings.data_out(36 downto 34);
  fifo_order_data_v.referenced_sample_valid_ptr := fifo_order_settings.data_out(37+log2_ceil(NUMBER_OF_SAMPLES_DEC)-1 downto 37);	
	-- Intra prediction interface
	fifo_order_data_v.intra_pred_mode	            := fifo_order_settings.data_out(29+LOG_INTRA_PRED_MODE downto 30);

  -- Assignments
  fifo_order_settings.data_in <= fifo_data_in_v;
  fifo_order_data <= fifo_order_data_v;
end process;



-- Process of preparing Prediction Information reading from internal memory PREDICTION_PARAMETER_BUFFER
PROC_PREDICTION_PARAMETER_READ_BUFFER: process (prediction_paramater_read)
  variable prediction_information_v               : PREDICTION_INFORMATION_TYPE;
begin 
  prediction_information_v.inter_pred_flag        := prediction_paramater_read(0); 
  prediction_information_v.bi_pred_flag           := prediction_paramater_read(1);
  prediction_information_v.motion_vector(0)       := prediction_paramater_read(LOG_MOTION_VECTOR+1 downto 2);
  prediction_information_v.motion_vector(1)       := prediction_paramater_read(LOG_MOTION_VECTOR*2+1 downto LOG_MOTION_VECTOR+2);
  prediction_information_v.referenced_frame_id    := prediction_paramater_read(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+1 downto LOG_MOTION_VECTOR*2+2); 
  prediction_information_v.intra_chroma_pred_mode := prediction_paramater_read(LOG_INTRA_CHROMA_PRED_MODE downto 1); 
  prediction_information_v.intra_luma_pred_mode_0 := prediction_paramater_read(LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE downto LOG_INTRA_CHROMA_PRED_MODE+1); 
  prediction_information_v.intra_luma_pred_mode_1 := prediction_paramater_read(2*LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE downto LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE+1);
  prediction_information_v.intra_luma_pred_mode_2 := prediction_paramater_read(3*LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE downto 2*LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE+1);
  prediction_information_v.intra_luma_pred_mode_3 := prediction_paramater_read(4*LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE downto 3*LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE+1);
  -- Assignment
  prediction_information <= prediction_information_v;
end process;



-- Process of preparing Referenced Samples reading from External Memory ports
PROC_REFERENCED_SAMPLES_SELECTING: process (referenced_data_port_out, fifo_order_data)  
  variable referenced_samples_v     : DATA_SAMPLES_DEC_TYPE;
  variable referenced_samples_ptr_v : std_logic_vector(1 downto 0);
begin
  referenced_samples_ptr_v := fifo_order_data.referenced_sample_valid_ptr;
  case referenced_samples_ptr_v is 
  when "00" =>
    for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
      referenced_samples_v(i) := referenced_data_port_out.data((8+DELTA_BIT_DEPTH)*(i+1)-1 downto (8+DELTA_BIT_DEPTH)*i);
    end loop;
  when "01" =>
    for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
      referenced_samples_v(i) := referenced_data_port_out.data((8+DELTA_BIT_DEPTH)*(i+2)-1 downto (8+DELTA_BIT_DEPTH)*(i+1));
    end loop;
  when "10" =>
    for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
      referenced_samples_v(i) := referenced_data_port_out.data((8+DELTA_BIT_DEPTH)*(i+3)-1 downto (8+DELTA_BIT_DEPTH)*(i+2));
    end loop;
  when others =>
    for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
      referenced_samples_v(i) := referenced_data_port_out.data((8+DELTA_BIT_DEPTH)*(i+4)-1 downto (8+DELTA_BIT_DEPTH)*(i+3));
    end loop;
  end case;
  -- Assignment
  referenced_samples_next <= referenced_samples_v;
end process;  


-- Process of preparing Residuals reading from internal memory RESIDUALS_BUFFER
PROC_RESIDUALS_SELECTING: process (residuals_read, fsm2_r) 
  variable residuals_v : RESIDUALS_TYPE;
begin
  if fsm2_r.residual_valid_ptr = '0' then
    case fsm2_r.residuals_memory_ptr is
    when "00" =>
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i)((9+DELTA_BIT_DEPTH)-1 downto 0);   
      end loop; 
    when "01" =>
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i+4)((9+DELTA_BIT_DEPTH)-1 downto 0);   
      end loop; 
    when "10" => 
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i+8)((9+DELTA_BIT_DEPTH)-1 downto 0);   
      end loop; 
    when others =>
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i+12)((9+DELTA_BIT_DEPTH)-1 downto 0);   
      end loop; 
    end case;
  else
    case fsm2_r.residuals_memory_ptr is
    when "00" =>
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i)((9+DELTA_BIT_DEPTH)*2-1 downto (9+DELTA_BIT_DEPTH));   
      end loop; 
    when "01" =>
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i+4)((9+DELTA_BIT_DEPTH)*2-1 downto (9+DELTA_BIT_DEPTH));   
      end loop; 
    when "10" => 
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i+8)((9+DELTA_BIT_DEPTH)*2-1 downto (9+DELTA_BIT_DEPTH));   
      end loop; 
    when others =>
      for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
        residuals_v(i) := residuals_read(i+12)((9+DELTA_BIT_DEPTH)*2-1 downto (9+DELTA_BIT_DEPTH));   
      end loop; 
    end case;
  end if;  
  -- Assignments
  residuals_next <= residuals_v;
end process;



-- Process of reconstruction samples   
PROC_RECONSTRUCTION_SAMPLES: process (weighted_samples, interpolated_samples_dav_next, interpolated_samples_dav_r, interpolated_samples_dav_delay1_r, interpolated_samples_dav_delay2_r, 
interpolated_samples_dav_delay3_r, predicted_samples_r, predicted_samples_dav_r, residuals_r, fsm2_r, pu_parameter, intra_predicted_samples, intra_predicted_samples_dav)

variable temp_v                            : signed(9+DELTA_BIT_DEPTH downto 0);       -- bit_length: 8+DELTA_BIT_DEPTH + 1 (sign) + 1 (after adding residual) 
variable predicted_samples_v               : DATA_SAMPLES_DEC_TYPE;
variable predicted_samples_dav_v           : std_logic;
variable interpolated_samples_dav_delay1_v : std_logic; 
variable reconstructed_samples_v           : DATA_SAMPLES_DEC_TYPE;

begin 
  interpolated_samples_dav_delay1_v := interpolated_samples_dav_r;
  
  if pu_parameter.inter_pred_flag = '1' then                                 -- INTER MODE Prediction 
    
    -- Prediction samples
    predicted_samples_v := weighted_samples; 
    predicted_samples_dav_v := interpolated_samples_dav_delay3_r; 
    
    -- Dav signals delay
    if pu_parameter.weighted_pred_flag = '1' then   -- Explicit Weighted Prediction
      interpolated_samples_dav_delay1_v := interpolated_samples_dav_r;	
    else											                      -- Implicit Weighted Prediction
      interpolated_samples_dav_delay1_v := interpolated_samples_dav_next;
    end if;
     
  else                                                                       -- INTRA MODE Prediction
    
    for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
      predicted_samples_v(i) := intra_predicted_samples(i);                
    end loop;
    predicted_samples_dav_v := intra_predicted_samples_dav; 
  
  end if;
  
  -- Reconstruction samples 
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    temp_v := '0'&signed('0'&predicted_samples_r(i)) + signed(residuals_r(i));  
    -- Check if there are not over-range                                                
    if temp_v > 2**(8+DELTA_BIT_DEPTH)-1 then -- over-range
      reconstructed_samples_v(i) := (others => '1');  
    elsif temp_v < 0 then
      reconstructed_samples_v(i) := (others => '0');     
    else
      reconstructed_samples_v(i) := std_logic_vector(temp_v(7+DELTA_BIT_DEPTH downto 0));
    end if; 
  end loop;    
  
  -- Assignment 
  -- Predicted samples and related dav signal
  if fsm2_r.swap_samples_flag = '1' then  -- swap sample in case we analyse block which position_y is not multiple 4, only for chroma signal
    predicted_samples_next(1 downto 0) <= predicted_samples_v(3 downto 2);
    predicted_samples_next(3 downto 2) <= predicted_samples_v(1 downto 0);
  else
    predicted_samples_next <= predicted_samples_v;
  end if;
  predicted_samples_dav_next <= predicted_samples_dav_v;
  predicted_samples_dav_delay1_next <= predicted_samples_dav_r;
  
  if intra_predicted_samples_dav = '1' and fsm2_r.intra_predicted_samples_ready = '1' then
    intra_predicted_samples_ack <= '1';
  else
    intra_predicted_samples_ack <= '0';
  end if;
  
  -- Reconstructed samples
  reconstructed_samples_next <= reconstructed_samples_v;
 
  -- Dav signals delay
  interpolated_samples_dav_delay1_next <= interpolated_samples_dav_delay1_v;
  interpolated_samples_dav_delay2_next <= interpolated_samples_dav_delay1_r;
  interpolated_samples_dav_delay3_next <= interpolated_samples_dav_delay2_r; 
end process;




-- Process of preparing External Memory support signals 
PROC_EXTERNAL_MEMORY: process (fsm1_r, fsm2_r, prediction_information, ctu_position_r, chroma_format, referenced_data_port_sel)
  variable referenced_data_port_in_v     : REF_PORT_IN_TYPE;
  variable referenced_sample_valid_ptr_v : std_logic_vector(log2_ceil(NUMBER_OF_SAMPLES_DEC)-1 downto 0);
  variable x_v                           : signed(LOG_FRAME_SIZE_X+1 downto 0);  -- one extra bit to protect against over-range and one sign bit 
  variable y_v                           : signed(LOG_FRAME_SIZE_Y+1 downto 0);  -- one extra bit to protect against over-range and one sign bit   
begin 
 
  -- Position Referenced Block in Referenced Frame
  if fsm1_r.pingpong = '1' then
    x_v := resize(signed('0'&FRAME_OVERLAP) + signed('0'&unsigned(ctu_position_r(1).position_x)) + signed('0'&unsigned(fsm1_r.pu_position_x)) + signed(prediction_information.motion_vector(0)(LOG_MOTION_VECTOR-1 downto 2)) -4, x_v'length);     
    y_v := resize(signed('0'&FRAME_OVERLAP) + signed('0'&unsigned(ctu_position_r(1).position_y)) + signed('0'&unsigned(fsm1_r.pu_position_y)) + signed(prediction_information.motion_vector(1)(LOG_MOTION_VECTOR-1 downto 2)) -4, y_v'length);
  else
    x_v := resize(signed('0'&FRAME_OVERLAP) + signed('0'&unsigned(ctu_position_r(0).position_x)) + signed('0'&unsigned(fsm1_r.pu_position_x)) + signed(prediction_information.motion_vector(0)(LOG_MOTION_VECTOR-1 downto 2)) -4, x_v'length);     
    y_v := resize(signed('0'&FRAME_OVERLAP) + signed('0'&unsigned(ctu_position_r(0).position_y)) + signed('0'&unsigned(fsm1_r.pu_position_y)) + signed(prediction_information.motion_vector(1)(LOG_MOTION_VECTOR-1 downto 2)) -4, y_v'length);
  end if;     
  -- Check if there are not over-range for x_v
  if x_v > to_signed(2**LOG_FRAME_SIZE_X, x_v'length) + signed('0'&FRAME_OVERLAP) then --over-range
    x_v := to_signed(2**LOG_FRAME_SIZE_X, x_v'length) + signed('0'&FRAME_OVERLAP);
  elsif x_v < 0 then
    x_v := (others => '0'); 
  end if;
  -- Check if there are not over-range for y_v
  if y_v > to_signed(2**LOG_FRAME_SIZE_Y, y_v'length) + signed('0'&FRAME_OVERLAP) then --over-range
    y_v := to_signed(2**LOG_FRAME_SIZE_Y, y_v'length) + signed('0'&FRAME_OVERLAP);
  elsif y_v < 0 then
    y_v := (others => '0'); 
  end if;
  
  if fsm1_r.chroma(1) = '0' then                 -- Operations for LUMA 
    -- Calculate the main variables
    referenced_data_port_in_v.addr0  := prediction_information.referenced_frame_id & fsm1_r.chroma(1) & std_logic_vector(y_v(LOG_FRAME_SIZE_Y-1 downto 2)) & std_logic_vector(x_v(LOG_FRAME_SIZE_X-1 downto 0)); 
    referenced_data_port_in_v.addr1  := prediction_information.referenced_frame_id & fsm1_r.chroma(1) & std_logic_vector(y_v(LOG_FRAME_SIZE_Y-1 downto 2) + 1) & std_logic_vector(x_v(LOG_FRAME_SIZE_X-1 downto 0));    
    referenced_data_port_in_v.width  := std_logic_vector(unsigned(fsm1_r.pu_size_x) + 8);
    referenced_data_port_in_v.height := std_logic_vector(resize(shift_right(unsigned(fsm1_r.pu_size_y) + 8,log2_ceil(NUMBER_OF_SAMPLES_DEC)), referenced_data_port_in_v.height'length));
    -- Setting jump = 2^LOG_FRAME_SIZE_X
    referenced_data_port_in_v.jump   := (others=>'0');
    referenced_data_port_in_v.jump(LOG_FRAME_SIZE_X) := '1'; 
    -- Calculate variable of first valid sample
    referenced_sample_valid_ptr_v    := std_logic_vector(y_v(1 downto 0)); 
  else                                                         
    if chroma_format = '1' then                  -- Operations for CHROMA 4:2:0 
      -- Calculate the main variables
      referenced_data_port_in_v.addr0 := prediction_information.referenced_frame_id & fsm1_r.chroma(1) & std_logic_vector('0'&y_v(LOG_FRAME_SIZE_Y-1 downto 3)) & fsm1_r.chroma(0) & std_logic_vector(x_v(LOG_FRAME_SIZE_X-1 downto 1)); 
      referenced_data_port_in_v.addr1 := prediction_information.referenced_frame_id & fsm1_r.chroma(1) & std_logic_vector('0'&y_v(LOG_FRAME_SIZE_Y-1 downto 3) + 1) & fsm1_r.chroma(0) & std_logic_vector(x_v(LOG_FRAME_SIZE_X-1 downto 1));
      referenced_data_port_in_v.width := std_logic_vector('0'&unsigned(fsm1_r.pu_size_x(6 downto 1)) + 4);
      if fsm1_r.pu_size_y(2 downto 1) = "10" then 
        referenced_data_port_in_v.height := std_logic_vector(resize('0'&shift_right(unsigned(fsm1_r.pu_size_y(6 downto 1)) + 4,log2_ceil(NUMBER_OF_SAMPLES_DEC)) + 1, referenced_data_port_in_v.height'length));
      else
        referenced_data_port_in_v.height := std_logic_vector(resize('0'&shift_right(unsigned(fsm1_r.pu_size_y(6 downto 1)) + 4,log2_ceil(NUMBER_OF_SAMPLES_DEC)), referenced_data_port_in_v.height'length));
      end if;
      -- Setting jump = 2^(LOG_FRAME_SIZE_X-1) : chroma 
      referenced_data_port_in_v.jump := (others=>'0');
      referenced_data_port_in_v.jump(LOG_FRAME_SIZE_X) := '1'; 
      -- Calculate variable of first valid sample
      referenced_sample_valid_ptr_v  := std_logic_vector(y_v(2 downto 1));
    else                                         -- Operations for CHROMA 4:2:2 
      -- Calculate the main variables
      referenced_data_port_in_v.addr0  := prediction_information.referenced_frame_id & fsm1_r.chroma(1) & std_logic_vector(y_v(LOG_FRAME_SIZE_Y-1 downto 2)) & fsm1_r.chroma(0) & std_logic_vector(x_v(LOG_FRAME_SIZE_X - 1 downto 1)); 
      referenced_data_port_in_v.addr1  := prediction_information.referenced_frame_id & fsm1_r.chroma(1) & std_logic_vector(y_v(LOG_FRAME_SIZE_Y-1 downto 2) + 1) & fsm1_r.chroma(0) & std_logic_vector(x_v(LOG_FRAME_SIZE_X - 1 downto 1));    
      referenced_data_port_in_v.width  := std_logic_vector(('0'&unsigned(fsm1_r.pu_size_x(6 downto 1))) + 4);
      referenced_data_port_in_v.height := std_logic_vector(resize(shift_right(unsigned(fsm1_r.pu_size_y) + 8,log2_ceil(NUMBER_OF_SAMPLES_DEC)), referenced_data_port_in_v.height'length));
      -- Setting jump = 2^(LOG_FRAME_SIZE_X-1) : chroma 
      referenced_data_port_in_v.jump   := (others=>'0');
      referenced_data_port_in_v.jump(LOG_FRAME_SIZE_X) := '1'; 
      -- Calculate variable of first valid sample
      referenced_sample_valid_ptr_v    := std_logic_vector(y_v(1 downto 0)); 
    end if;
  end if;
  
  -- Settings signal from Finite State Machines
  referenced_data_port_in_v.start := fsm1_r.referenced_data_port_start;
  referenced_data_port_in_v.sel   := referenced_data_port_sel;
  
  -- Assignments
  referenced_data_port_in         <= referenced_data_port_in_v;
  referenced_sample_valid_ptr     <= referenced_sample_valid_ptr_v;
end process; 

-- Process of updating prediction parameter
PROC_PREDICTION_PARAMETER_UPDATING: 
process (pred_parameter_r, pu_parameter_r)
	-- Aliases
	alias param_bi_pred_flag: 							std_logic 										            				is pred_parameter_r(0);
	alias param_motion_vector_fract_x: 			std_logic_vector(2 downto 0) 	            				is pred_parameter_r(3 downto 1);
	alias param_motion_vector_fract_y: 			std_logic_vector(2 downto 0) 	 										is pred_parameter_r(6 downto 4);
	--alias param_round_pb_size_y: 	          std_logic_vector(6 downto 0)	 										is pred_parameter_r(13 downto 7);	
	alias param_weighted_pred_flag:        	std_logic                     										is pred_parameter_r(7); 
	alias param_log_weight_denom:          	std_logic_vector(2 downto 0)   										is pred_parameter_r(10 downto 8);	 
	alias param_weight0:                   	std_logic_vector(LOG_WEIGHT-1 downto 0)   				is pred_parameter_r(10+LOG_WEIGHT downto 11);
	alias param_offset0:                   	std_logic_vector(LOG_OFFSET-1 downto 0)   				is pred_parameter_r(10+LOG_WEIGHT+LOG_OFFSET downto 11+LOG_WEIGHT);
	alias param_weight1:                   	std_logic_vector(LOG_WEIGHT-1 downto 0)   				is pred_parameter_r(10+2*LOG_WEIGHT+LOG_OFFSET downto 11+LOG_WEIGHT+LOG_OFFSET);
	alias param_offset1:                   	std_logic_vector(LOG_OFFSET-1 downto 0)   				is pred_parameter_r(10+2*LOG_WEIGHT+2*LOG_OFFSET downto 11+2*LOG_WEIGHT+LOG_OFFSET);
	alias param_intra_pred_mode:           	std_logic_vector(LOG_INTRA_PRED_MODE-1 downto 0) 	is pred_parameter_r(LOG_INTRA_PRED_MODE-1 downto 0);
	alias param_pu_avail:                  	std_logic_vector(LOG_PU_AVAIL-1 downto 0)  				is pred_parameter_r(LOG_INTRA_PRED_MODE+LOG_PU_AVAIL-1 downto LOG_INTRA_PRED_MODE);
  alias param_pu_size_idx:                std_logic_vector(1 downto 0)  				            is pred_parameter_r(LOG_INTRA_PRED_MODE+LOG_PU_AVAIL+2-1 downto LOG_INTRA_PRED_MODE+LOG_PU_AVAIL);

begin	 
  pu_parameter.chroma <= pu_parameter_r.chroma;
  pu_parameter.pb_size_x <= pu_parameter_r.pb_size_x; 
  pu_parameter.pb_size_y <=  pu_parameter_r.pb_size_y;
  pu_parameter.round_pb_size_y <= pu_parameter_r.round_pb_size_y; 
  pu_parameter.pb_position_x <= pu_parameter_r.pb_position_x;
  pu_parameter.pb_position_y <= pu_parameter_r.pb_position_y;
  pu_parameter.inter_pred_flag <= pu_parameter_r.inter_pred_flag;    
  
	pu_parameter.bi_pred_flag <= param_bi_pred_flag;   
  pu_parameter.weighted_pred_flag <= param_weighted_pred_flag; 
  pu_parameter.weight0 <= param_weight0;
  pu_parameter.offset0 <= param_offset0; 
  pu_parameter.weight1 <= param_weight1;
  pu_parameter.offset1 <= param_offset1;
  pu_parameter.log_weight_denom <= param_log_weight_denom;
  pu_parameter.motion_vector_fract_x <= param_motion_vector_fract_x; 
  pu_parameter.motion_vector_fract_y <= param_motion_vector_fract_y; 
	pu_parameter.intra_pred_mode <= param_intra_pred_mode;
	pu_parameter.pu_avail <= param_pu_avail;
  pu_parameter.pu_size_idx <= param_pu_size_idx;

end process;


-- Process of preparing prediction parameter
PROC_PREDICTION_PARAMETER_PREPARING: 
process (fifo_order_data, input_data, weight_denom, weighted_pred_flag, weighted_bipred_flag, chroma_format, weighted_prediction_parameter_read, fsm2_r)

-- Variables
variable pu_parameter_v   				: PU_PARAMETER_REGISTER_TYPE;			
variable pred_parameter_v 				: std_logic_vector(LOG_PRED_PARAMETER_REGISTER-1 downto 0);
variable pos_x                    : unsigned(LOG_FRAME_SIZE_X - 1 downto 0);
variable pos_y                    : unsigned(LOG_FRAME_SIZE_Y - 1 downto 0);

-- Aliases
alias param_bi_pred_flag: 							std_logic 										            				is pred_parameter_v(0);
alias param_motion_vector_fract_x: 			std_logic_vector(2 downto 0) 	            				is pred_parameter_v(3 downto 1);
alias param_motion_vector_fract_y: 			std_logic_vector(2 downto 0) 	 										is pred_parameter_v(6 downto 4);
--alias param_round_pb_size_y: 	          std_logic_vector(6 downto 0)	 										is pred_parameter_v(13 downto 7);	
alias param_weighted_pred_flag:        	std_logic                     										is pred_parameter_v(7); 
alias param_log_weight_denom:          	std_logic_vector(2 downto 0)   										is pred_parameter_v(10 downto 8);	 
alias param_weight0:                   	std_logic_vector(LOG_WEIGHT-1 downto 0)   				is pred_parameter_v(10+LOG_WEIGHT downto 11);
alias param_offset0:                   	std_logic_vector(LOG_OFFSET-1 downto 0)   				is pred_parameter_v(10+LOG_WEIGHT+LOG_OFFSET downto 11+LOG_WEIGHT);
alias param_weight1:                   	std_logic_vector(LOG_WEIGHT-1 downto 0)   				is pred_parameter_v(10+2*LOG_WEIGHT+LOG_OFFSET downto 11+LOG_WEIGHT+LOG_OFFSET);
alias param_offset1:                   	std_logic_vector(LOG_OFFSET-1 downto 0)   				is pred_parameter_v(10+2*LOG_WEIGHT+2*LOG_OFFSET downto 11+2*LOG_WEIGHT+LOG_OFFSET);
alias param_intra_pred_mode:           	std_logic_vector(LOG_INTRA_PRED_MODE-1 downto 0) 	is pred_parameter_v(LOG_INTRA_PRED_MODE-1 downto 0);
alias param_pu_avail:                  	std_logic_vector(LOG_PU_AVAIL-1 downto 0)  				is pred_parameter_v(LOG_INTRA_PRED_MODE+LOG_PU_AVAIL-1 downto LOG_INTRA_PRED_MODE);
alias param_pu_size_idx:                std_logic_vector(1 downto 0)  				            is pred_parameter_v(LOG_INTRA_PRED_MODE+LOG_PU_AVAIL+2-1 downto LOG_INTRA_PRED_MODE+LOG_PU_AVAIL);

begin
  pu_parameter_v   							 := pu_parameter_r;
	pred_parameter_v 							 := pred_parameter_r;
  
  -- Common parameters
  pu_parameter_v.chroma          := fifo_order_data.chroma;
  pu_parameter_v.pb_size_x       := fifo_order_data.pb_size_x;
  pu_parameter_v.pb_size_y       := fifo_order_data.pb_size_y;
  pu_parameter_v.pb_position_x   := fifo_order_data.pb_position_x;
  pu_parameter_v.pb_position_y   := fifo_order_data.pb_position_y; 
  pu_parameter_v.inter_pred_flag := fifo_order_data.inter_pred_flag;
  
	--Setting block size (multiple NUMBER_OF_SAMPLES_DEC)
  if fifo_order_data.pb_size_y(1 downto 0) = "10" then
    pu_parameter_v.round_pb_size_y := std_logic_vector(unsigned(fifo_order_data.pb_size_y) + 2);   
  else
    pu_parameter_v.round_pb_size_y := fifo_order_data.pb_size_y;
  end if; 
  
  if fsm2_r.pingpong = '1' then
    pos_x := unsigned(ctu_position_r(1).position_x) + unsigned(fifo_order_data.pb_position_x);
    pos_y := unsigned(ctu_position_r(1).position_y) + unsigned(fifo_order_data.pb_position_y(5 downto 2) & "00");
  else
    pos_x := unsigned(ctu_position_r(0).position_x) + unsigned(fifo_order_data.pb_position_x);
    pos_y := unsigned(ctu_position_r(0).position_y) + unsigned(fifo_order_data.pb_position_y(5 downto 2) & "00"); 
  end if; 

	if fifo_order_data.inter_pred_flag = '1' then																								-- Inter Prediction operations
		param_bi_pred_flag := fifo_order_data.bi_pred_flag; 
	  --Turn on/off interpolation
	  if INTERPOLATION_FLAG = true then
	    param_motion_vector_fract_x := fifo_order_data.motion_vector_fract_x;   
	    param_motion_vector_fract_y := fifo_order_data.motion_vector_fract_y;
	  else
	    param_motion_vector_fract_x := (others=>'0');   
	    param_motion_vector_fract_y := (others=>'0');
	  end if;	
	
	  -- Weighted prediction parameter
	  case input_data.slice_type is
	    when "00"   =>    -- B-Slice
	      param_weighted_pred_flag := weighted_bipred_flag;
	    when "01"   =>    -- P-Slice
	      param_weighted_pred_flag := weighted_pred_flag;
	    when others =>    -- I-Slice
	      param_weighted_pred_flag := '0';
	  end case;	  	
	  
	  case fifo_order_data.chroma is
	  when "00" =>
		  param_log_weight_denom  := weight_denom.luma_log_weight_denom;
		  if fifo_order_data.bi_pred_flag = '1' then
	      if fsm2_r.bi_pred_block_ptr = '1' then
	        param_weight1 := weighted_prediction_parameter_read(LOG_WEIGHT-1 downto 0);
	        param_offset1 := weighted_prediction_parameter_read(LOG_WEIGHT+LOG_OFFSET-1 downto LOG_WEIGHT);
	      else
	        param_weight0 := weighted_prediction_parameter_read(LOG_WEIGHT-1 downto 0);
	        param_offset0 := weighted_prediction_parameter_read(LOG_WEIGHT+LOG_OFFSET-1 downto LOG_WEIGHT);
	      end if;
	    else
	      param_weight0 := (others=>'0');
	      param_offset0 := (others=>'0'); 
	      param_weight1 := weighted_prediction_parameter_read(LOG_WEIGHT-1 downto 0);
	      param_offset1 := weighted_prediction_parameter_read(LOG_WEIGHT+LOG_OFFSET-1 downto LOG_WEIGHT);
	    end if;
		  
		when "10" =>
		  param_log_weight_denom  := weight_denom.chroma_log_weight_denom;
		  if fifo_order_data.bi_pred_flag = '1' then
	      if fsm2_r.bi_pred_block_ptr = '1' then 
	        param_weight1 := weighted_prediction_parameter_read(2*LOG_WEIGHT+LOG_OFFSET-1 downto LOG_WEIGHT+LOG_OFFSET);
	        param_offset1 := weighted_prediction_parameter_read(2*LOG_WEIGHT+2*LOG_OFFSET-1 downto 2*LOG_WEIGHT+LOG_OFFSET);
	      else
	        param_weight0 := weighted_prediction_parameter_read(2*LOG_WEIGHT+LOG_OFFSET-1 downto LOG_WEIGHT+LOG_OFFSET);
	        param_offset0 := weighted_prediction_parameter_read(2*LOG_WEIGHT+2*LOG_OFFSET-1 downto 2*LOG_WEIGHT+LOG_OFFSET);
	      end if;
	    else
	      param_weight0 := (others=>'0');
	      param_offset0 := (others=>'0'); 
	      param_weight1 := weighted_prediction_parameter_read(2*LOG_WEIGHT+LOG_OFFSET-1 downto LOG_WEIGHT+LOG_OFFSET);
	      param_offset1 := weighted_prediction_parameter_read(2*LOG_WEIGHT+2*LOG_OFFSET-1 downto 2*LOG_WEIGHT+LOG_OFFSET);
	    end if;
		  
	  when others =>
		  param_log_weight_denom  := weight_denom.chroma_log_weight_denom;
		  if fifo_order_data.bi_pred_flag = '1' then
	      if fsm2_r.bi_pred_block_ptr = '1' then 
	        param_weight1 := weighted_prediction_parameter_read(3*LOG_WEIGHT+2*LOG_OFFSET-1 downto 2*LOG_WEIGHT+2*LOG_OFFSET);
	        param_offset1 := weighted_prediction_parameter_read(3*LOG_WEIGHT+3*LOG_OFFSET-1 downto 3*LOG_WEIGHT+2*LOG_OFFSET);
	      else
	        param_weight0 := weighted_prediction_parameter_read(3*LOG_WEIGHT+2*LOG_OFFSET-1 downto 2*LOG_WEIGHT+2*LOG_OFFSET);
	        param_offset0 := weighted_prediction_parameter_read(3*LOG_WEIGHT+3*LOG_OFFSET-1 downto 3*LOG_WEIGHT+2*LOG_OFFSET);
	      end if;
	    else
	      param_weight0 := (others=>'0');
	      param_offset0 := (others=>'0'); 
	      param_weight1 := weighted_prediction_parameter_read(3*LOG_WEIGHT+2*LOG_OFFSET-1 downto 2*LOG_WEIGHT+2*LOG_OFFSET);
	      param_offset1 := weighted_prediction_parameter_read(3*LOG_WEIGHT+3*LOG_OFFSET-1 downto 3*LOG_WEIGHT+2*LOG_OFFSET);
	    end if;
	  end case;	
		
	else                                          																								-- Intra Prediction operations 
		
		param_intra_pred_mode := fifo_order_data.intra_pred_mode;
    
    case fifo_order_data.pb_size_y is
      when "0100000" => -- 32
      param_pu_size_idx := PU_32X32;
      when "0010000" => -- 16
      param_pu_size_idx := PU_16X16;
      when "0001000" => -- 8
      param_pu_size_idx := PU_8X8;
      when others => -- 4
      param_pu_size_idx := PU_4X4;
    end case;  
    
    if pos_x /= 0 then
      param_pu_avail(0) := '1';
    else
      param_pu_avail(0) := '0';
    end if;
    
    if pos_y /= 0 then
      param_pu_avail(1) := '1';
    else
      param_pu_avail(1) := '0';
    end if;
    
    param_pu_avail(3) := to_std_logic( param_pu_avail(0)='1' and param_pu_avail(1)='1' );
    
    case param_pu_size_idx is
      when PU_4X4 =>
        param_pu_avail(2) := to_std_logic( param_pu_avail(1)='1' and ( 
              ( pos_x mod 8 = 0 ) or
              ( ( pos_y mod 8 = 0 ) and ( (pos_x-4) mod 16 = 0 ) ) or
              ( ( pos_y mod 16 = 0 ) and ( (pos_x+4) mod 16 = 0 ) and not( (pos_x+4) mod 32 = 0 ) ) or
              ( ( pos_y mod 32 = 0 ) and ( (pos_x+4) mod 32 = 0 ) and not( (pos_x+4) mod 64 = 0 ) ) or
              ( ( pos_y mod 64 = 0 ) and ( (pos_x+4) mod 64 = 0 ) )
        )); 
        param_pu_avail(4) := to_std_logic( param_pu_avail(0)='1' and ( 
        			( ( pos_x mod 8 = 0 ) and ( pos_y mod 8 = 0 ) ) or
        			( ( pos_x mod 16 = 0 ) and ( (pos_y+4) mod 8 = 0 ) and not( (pos_y+4) mod 16 = 0 ) ) or
        			( ( pos_x mod 32 = 0 ) and ( (pos_y+4) mod 16 = 0 ) and not( (pos_y+4) mod 32 = 0 ) ) or
        			( ( pos_x mod 64 = 0 ) and ( (pos_y+4) mod 32 = 0 ) and not( (pos_y+4) mod 64 = 0 ) )
        ));          
      when PU_8X8 =>
        param_pu_avail(2) := to_std_logic( param_pu_avail(1)='1' and (
              ( pos_x mod 16 = 0 )  or 
  			      ( ( pos_y mod 16 = 0 ) and ( (pos_x-8) mod 32 = 0 ) ) or
        			( ( pos_y mod 32 = 0 ) and ( (pos_x+8) mod 32 = 0 ) and not( (pos_x+8) mod 64 = 0 ) ) or
        			( ( pos_y mod 64 = 0 ) and ( (pos_x+8) mod 64 = 0 ) )
  	    ));
        param_pu_avail(4) := to_std_logic( param_pu_avail(0)='1' and ( 
        			( ( pos_x mod 16 = 0 ) and ( pos_y mod 16 = 0 ) ) or
        			( ( pos_x mod 32 = 0 ) and ( (pos_y+4) mod 16 = 0 ) and not( (pos_y+4) mod 32 = 0 ) ) or
        			( ( pos_x mod 64 = 0 ) and ( (pos_y+4) mod 32 = 0 ) and not( (pos_y+4) mod 64 = 0 ) )
        ));   
      when PU_16X16 =>
        param_pu_avail(2) := to_std_logic( param_pu_avail(1)='1' and (
        			( pos_x mod 32 = 0 )  or 
        			( ( pos_y mod 32 = 0 ) and ( (pos_x-16) mod 64 = 0 ) ) or
        			( ( pos_y mod 64 = 0 ) and ( (pos_x+16) mod 64 = 0 ) ) 
  	    ));
        param_pu_avail(4) := to_std_logic( param_pu_avail(0)='1' and ( 
        			( ( pos_x mod 32 = 0 ) and ( pos_y mod 32 = 0 ) ) or
        			( ( pos_x mod 64 = 0 ) and ( (pos_y+4) mod 32 = 0 ) and not( (pos_y+4) mod 64 = 0 ) )
        ));  
      when others =>
        param_pu_avail(2) := to_std_logic( param_pu_avail(1)='1' and (
        			( pos_x mod 64 = 0 ) or ( pos_y mod 64 = 0 ) 
  	    )); 
        param_pu_avail(4) := to_std_logic( param_pu_avail(0)='1' and ( 
      			  ( pos_x mod 64 = 0 ) and ( pos_y mod 64 = 0 )
        ));  
    end case; 
    
	end if;
		
  -- Assignments
  intra_pu_addr_x     <= std_logic_vector(resize( pos_x, intra_pu_addr_x'length ));
  intra_pu_addr_y     <= std_logic_vector(resize( pos_y, intra_pu_addr_y'length ));
  pu_parameter_next   <= pu_parameter_v;
	pred_parameter_next <= pred_parameter_v;
end process; 




  -- -------------------------------------------------------------------------
  --                           Main clock process                           --
  -- ------------------------------------------------------------------------- 

PROC_CLK: process (clk, rst)
begin  
  if rst = '0' then  
  
    -- Finite State Machine 1
    fsm1_r.state                          		<= IDLE; 
    fsm1_r.pingpong                       		<= '0';
    fsm1_r.prediction_parameter_addr      		<= (others=>'0');
    fsm1_r.cnt32                          		<= (others=>'0');
    fsm1_r.cnt16                          		<= (others=>'0');
    fsm1_r.cnt8                           		<= (others=>'0');
    fsm1_r.cnt4                           		<= (others=>'0');
		fsm1_r.intra4x4                       		<= '0';					
    fsm1_r.bi_pred_block_ptr              		<= '0';
    fsm1_r.cu_partition_ptr               		<= '0';
    fsm1_r.chroma                         		<= (others=>'0');  
    fsm1_r.pu_position_x                  		<= (others=>'0');
    fsm1_r.pu_position_y                  		<= (others=>'0'); 
    fsm1_r.pu_size_x                      		<= (others=>'0');
    fsm1_r.pu_size_y                      		<= (others=>'0'); 
    fsm1_r.referenced_data_port_start     		<= '0';
    fsm1_r.fifo_order_en                  		<= '0'; 
	
    -- Finite State Machine 2
    fsm2_r.state                          		<= IDLE; 
    fsm2_r.pingpong                       		<= '1';
	  fsm2_r.chroma                         		<= (others=>'0'); 
    fsm2_r.residuals_addr(0)              		<= (others=>'0'); 
    fsm2_r.residuals_addr(1)              		<= (others=>'0');
    fsm2_r.residuals_memory_ptr           		<= (others=>'0');
    fsm2_r.swap_samples_flag              		<= '0';
    fsm2_r.residual_valid_ptr            	 		<= '0'; 
    fsm2_r.residuals_x_cnt                		<= (others=>'0'); 
    fsm2_r.residuals_y_cnt                		<= (others=>'0'); 
    fsm2_r.referenced_samples_x_cnt      		 	<= (others=>'0'); 
    fsm2_r.referenced_samples_y_cnt       		<= (others=>'0');   
    fsm2_r.reconstructed_samples_x_cnt    		<= (others=>'0'); 
    fsm2_r.reconstructed_samples_y_cnt    		<= (others=>'0'); 
    fsm2_r.interpolator_start             		<= '0';
    fsm2_r.referenced_samples_dav         		<= '0';
    fsm2_r.fifo_order_ack                 		<= '0';
    fsm2_r.weighted_prediction_parameter_addr <= (others=>'0');
    fsm2_r.bi_pred_block_ptr              		<= '0';
    fsm2_r.bi_pred_delay_read_addr        		<= (others=>'0');  
    fsm2_r.bi_pred_delay_write_addr       		<= (others=>'0');
    fsm2_r.reconstructed_samples_write_en 		<= (others=>'1');
    fsm2_r.reconstructed_samples_write_addr(0) <= (others=>'0');
    fsm2_r.reconstructed_samples_write_addr(1) <= (others=>'0'); 
    -- fsm2_r.reconstructed_samples_read_addr    <= (others=>'0');
    fsm2_r.intra_decoder_start                <= '0';
    fsm2_r.intra_predicted_samples_ready      <= '0';
    fsm2_r.intra_reconstructed_samples_x_cnt  <= (others=>'0'); 
    fsm2_r.intra_reconstructed_samples_y_cnt  <= (others=>'0'); 
    fsm2_r.intra_reconstructed_samples_dav    <= '0';	
    
    -- Finite State Machine 3
    fsm3_r.state                          		<= IDLE;
    fsm3_r.pingpong_dav                   		<= (others=>'0');          
    fsm3_r.pingpong                       		<= '0';
    fsm3_r.ready                          		<= '0';
    fsm3_r.residuals_write_en             		<= (others=>'1');  
    fsm3_r.residuals_addr(0)             			<= (others=>'0');   
    fsm3_r.residuals_addr(1)              		<= (others=>'0'); 
    fsm3_r.residuals_addr(2)              		<= (others=>'0'); 
    fsm3_r.residuals_addr(3)             			<= (others=>'0'); 
    fsm3_r.prediction_parameter_addr     			<= (others=>'0');
    fsm3_r.weighted_prediction_parameter_addr <= (others=>'0');
    fsm3_r.reconstructed_samples_addr     		<= (others=>'0'); 
    fsm3_r.reconstructed_samples_dav      		<= '0';  
    fsm3_r.output_ctu_valid_cnt           		<= (others=>'0'); 

    -- Coding Tree Unit structure
    for i in 0 to 1 loop
      ctu_tree_r(i).cu_64 <=(others=>'0');
      for j in 0 to 3 loop
        ctu_tree_r(i).cu_32(j) <=(others=>'0'); 
      end loop; 
      for j in 0 to 15 loop
        ctu_tree_r(i).cu_16(j) <=(others=>'0'); 
      end loop; 
      for j in 0 to 63 loop
        ctu_tree_r(i).cu_8(j) <=(others=>'0'); 
      end loop; 
    end loop;

    -- Coding Tree Unit position in frame
    ctu_position_r(0).position_x          <= (others=>'0');
    ctu_position_r(0).position_y          <= (others=>'0');
    ctu_position_r(1).position_x          <= (others=>'0');
    ctu_position_r(1).position_y          <= (others=>'0'); 

    -- Samples registers
    interpolated_samples_r                <= (others=>(others=>'0'));
    predicted_samples_r                   <= (others=>(others=>'0'));
    residuals_r                           <= (others=>(others=>'0'));
    reconstructed_samples_r               <= (others=>(others=>'0')); 
    referenced_samples_r                  <= (others=>(others=>'0'));
   
    -- Samples dav registers   
    interpolated_samples_dav_r            <= '0'; 
	  interpolated_samples_dav_delay1_r     <= '0';
	  interpolated_samples_dav_delay2_r     <= '0'; 
	  interpolated_samples_dav_delay3_r     <= '0'; 
    predicted_samples_dav_r               <= '0';
    predicted_samples_dav_delay1_r        <= '0';
  
    -- Prediction Parameters registers
    pu_parameter_r.chroma               	<= (others=>'0');
    pu_parameter_r.pb_size_x            	<= (others=>'0');
    pu_parameter_r.pb_size_y            	<= (others=>'0');
    pu_parameter_r.round_pb_size_y        <= (others=>'0');
    pu_parameter_r.pb_position_x        	<= (others=>'0');
    pu_parameter_r.pb_position_y        	<= (others=>'0');
    pu_parameter_r.inter_pred_flag      	<= '0';
	  pred_parameter_r 											<= (others=>'0');

  elsif rising_edge(clk) then  
  
    fsm1_r                            		<= fsm1_next;
    fsm2_r                            		<= fsm2_next;
    fsm3_r                            		<= fsm3_next;
    ctu_tree_r                        		<= ctu_tree_next;
    ctu_position_r                    		<= ctu_position_next;
    predicted_samples_r               		<= predicted_samples_next;
    predicted_samples_dav_r           		<= predicted_samples_dav_next;
    predicted_samples_dav_delay1_r        <= predicted_samples_dav_delay1_next;
    residuals_r                       		<= residuals_next;
    reconstructed_samples_r           		<= reconstructed_samples_next;
    referenced_samples_r              		<= referenced_samples_next;   
    interpolated_samples_r            		<= interpolated_samples_next;
    interpolated_samples_dav_r        		<= interpolated_samples_dav_next;   
    interpolated_samples_dav_delay1_r 		<= interpolated_samples_dav_delay1_next; 
    interpolated_samples_dav_delay2_r 		<= interpolated_samples_dav_delay2_next; 
    interpolated_samples_dav_delay3_r 		<= interpolated_samples_dav_delay3_next; 
		pu_parameter_r												<= pu_parameter_next;
    pred_parameter_r                  		<= pred_parameter_next;
	
  end if;
end process;


end motion_compensation_dec_arch;