------------------------------------------------------------------
--
-- Title       : Definitions and constants for configuration of memory controller
-- Design      : DDR2 memory controller for video compression
-- Author      : Maciej Trochimiuk
-- Company     : none
-- Start Date  : 01.03.2010
--
---------------------------------------------------------------------
-- VERSION 1.0
-----------------------------------------------------------------------------

library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all; 

package ddr2_pkg is
	-----------------------
	-- PACKAGE CONSTANTS --
	-----------------------
	-- OPTIONS 
	constant USE_ALTERA_SIMULATION_MODEL : boolean := true; -- if set use simulation model, not real altera DDR controller

	--bus widths of attached ddr2 memory
	constant MEM_NUMBER             : natural := 9;
	constant DQS_DEL_EXPORT_WIDTH   : natural := 6;
	constant MEM_ADD_WIDTH          : natural := 14;
	constant MEM_BA_WIDTH           : natural := 3;
	constant MEM_CKE_WIDTH          : natural := 1;
	constant MEM_CLK_WIDTH          : natural := 3;
	constant MEM_CS_WIDTH           : natural := 1;
	constant MEM_DM_WIDTH           : natural := MEM_NUMBER;
	constant MEM_DQ_WIDTH           : natural := 8*MEM_NUMBER;
	constant MEM_DQS_WIDTH          : natural := MEM_NUMBER;
	constant MEM_ODT_WIDTH          : natural := 1;
	constant OCT_CTL_WIDTH          : natural := 14;

	--bus widths of attached altera memory controller
	-- equals 2*external_width or 4*external_width for FULL_RATE and HALF_RATE, respectively. 
	constant ALTERA_BURST_WIDTH     : natural := 1;
	constant ALTERA_MAX_BURST       : natural := 1;
	constant ALTERA_ADDRESS_WIDTH   : natural := 24+ALTERA_BURST_WIDTH; -- determine number of address bits for the internal altera IP controler
	constant ALTERA_DATA_WIDTH      : natural := (32*(MEM_NUMBER-1))/ALTERA_BURST_WIDTH; -- internal data with from the altera IP controler
	constant ALTERA_BYTE_ENABLE     : natural := ALTERA_DATA_WIDTH / 8;
		
	--width of parameter buses in read and write ports
	constant PARAM_WIDTH            : natural := 14;
	
	--number of bidirectional ports
	constant PORTS_BIDIR            : natural := 1;
	--number of read ports
	constant PORTS_READ             : natural := 2;
	--number of write ports
	constant PORTS_WRITE            : natural := 1;
	--total number of ports
	constant PORTS_TOTAL            : natural := PORTS_BIDIR + PORTS_READ + PORTS_WRITE;

	--length of build-in FIFO queue
	type ARRAY_NATURAL_TYPE is array(7 downto 0) of natural;
	constant FIFO_LENGTH_WRITE         : ARRAY_NATURAL_TYPE := (128, 128, 128, 32, 256, 32, 256, 128); -- 0-index on the right side  
	constant FIFO_LENGTH_READ          : ARRAY_NATURAL_TYPE := (128, 128, 128, 128, 32, 64, 64, 64);
	constant ACCESS_CYCLE_LENGTH_READ  : ARRAY_NATURAL_TYPE := (32, 32, 32, 32, 2, 10, 16, 16); 
	constant ACCESS_CYCLE_LENGTH_WRITE : ARRAY_NATURAL_TYPE := (32, 32, 32, 2, 16, 2, 16, 16); 

	---------------------
	-- ARBITER DEFINES --
	---------------------
	
	type SCHEDULER_IN is record
		request_lp                  : std_logic;
		request_np                  : std_logic;
		request_hp                  : std_logic;
		done                        : std_logic;
		device_ready                : std_logic;
	end record;
	
	type SCHEDULER_OUT is record
		enable                      : std_logic;
	end record;
	
	type SCHEDULER_DEVICES_IN is array (PORTS_TOTAL-1 downto 0) of SCHEDULER_IN; 
	type SCHEDULER_DEVICES_OUT is array (PORTS_TOTAL-1 downto 0) of SCHEDULER_OUT;
	
	-------------------------------
	-- MEMORY CONTROLLER DEFINES --
	-------------------------------
	
	type ALTERA_DDR2_OUT is record
		rdata                       : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
		rdata_valid                 : std_logic;
		wdata_req                   : std_logic;
		ready                       : std_logic;
	end record;
		
	type ALTERA_DDR2_IN is record
		addr                        : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
		be                          : std_logic_vector(ALTERA_BYTE_ENABLE-1 downto 0);
		read_req                    : std_logic;
		size                        : std_logic_vector(ALTERA_BURST_WIDTH-1 downto 0);
		wdata                       : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
		write_req                   : std_logic;
	end record;
	
	type MULTIPLEXER_ALTERA_DDR2_OUT is array (PORTS_TOTAL-1 downto 0) of ALTERA_DDR2_OUT;
	type MULTIPLEXER_ALTERA_DDR2_IN is array (PORTS_TOTAL-1 downto 0) of ALTERA_DDR2_IN;
	
	-----------------------
	-- READ PORT DEFINES --
	-----------------------
	
	type READ_PORT_OUT is record
		ready                       : std_logic;
		empty                       : std_logic;
		almost_empty                : std_logic;
		half_full                   : std_logic;
		almost_full                 : std_logic;
		full                        : std_logic;
		data                        : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
	end record;
	
	type READ_PORT_IN is record
		start                       : std_logic;
		sel                         : std_logic;
		address                     : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
		wid                         : std_logic_vector(PARAM_WIDTH-1 downto 0);
		hei                         : std_logic_vector(PARAM_WIDTH-1 downto 0);
		jum                         : std_logic_vector(PARAM_WIDTH-1 downto 0);
	end record;

	type READ_PORTS_IN is array (PORTS_READ-1 downto 0) of READ_PORT_IN;
	type READ_PORTS_OUT is array (PORTS_READ-1 downto 0) of READ_PORT_OUT;
	type READ_PORTS_SCH_IN is array (PORTS_READ-1 downto 0) of SCHEDULER_OUT;
	type READ_PORTS_SCH_OUT is array (PORTS_READ-1 downto 0) of SCHEDULER_IN;
	type READ_PORTS_MEM_IN is array (PORTS_READ-1 downto 0) of ALTERA_DDR2_OUT;
	type READ_PORTS_MEM_OUT is array (PORTS_READ-1 downto 0) of ALTERA_DDR2_IN;

------------------------
-- WRITE PORT DEFINES --
------------------------  

	type WRITE_PORT_OUT is record
		ready                       : std_logic;
		empty                       : std_logic;
		almost_empty                : std_logic;
--		half_full                   : std_logic;
		almost_full                 : std_logic;
		full                        : std_logic;
	end record;

	type WRITE_PORT_IN is record
		start                       : std_logic;
		sel                         : std_logic;
		address                     : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
		wid                         : std_logic_vector(PARAM_WIDTH-1 downto 0);
		hei                         : std_logic_vector(PARAM_WIDTH-1 downto 0);
		jum                         : std_logic_vector(PARAM_WIDTH-1 downto 0);
		data                        : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
	end record;

	type WRITE_PORTS_OUT is array (PORTS_WRITE-1 downto 0) of WRITE_PORT_OUT;
	type WRITE_PORTS_IN is array (PORTS_WRITE-1 downto 0) of WRITE_PORT_IN;
	type WRITE_PORTS_SCH_IN is array (PORTS_WRITE-1 downto 0) of SCHEDULER_OUT;
	type WRITE_PORTS_SCH_OUT is array (PORTS_WRITE-1 downto 0) of SCHEDULER_IN;
	type WRITE_PORTS_MEM_IN is array (PORTS_WRITE-1 downto 0) of ALTERA_DDR2_OUT;
	type WRITE_PORTS_MEM_OUT is array (PORTS_WRITE-1 downto 0) of ALTERA_DDR2_IN;
	
	--------------------------------
	-- BIDIRECTIONAL PORT DEFINES --
	--------------------------------
	
	type BIDIR_PORT_OUT is record
		ready                       : std_logic;
		data_out                    : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
	end record;
	
	type BIDIR_PORT_IN is record
		enable                      : std_logic;
		write_req                   : std_logic;
		address                     : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
		data_in                     : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
	end record;
	
	type BIDIR_PORTS_OUT is array (PORTS_BIDIR-1 downto 0) of BIDIR_PORT_OUT;
	type BIDIR_PORTS_IN is array (PORTS_BIDIR-1 downto 0) of BIDIR_PORT_IN;
	type BIDIR_PORTS_SCH_IN is array (PORTS_BIDIR-1 downto 0) of SCHEDULER_OUT;
	type BIDIR_PORTS_SCH_OUT is array (PORTS_BIDIR-1 downto 0) of SCHEDULER_IN;
	type BIDIR_PORTS_MEM_IN is array (PORTS_BIDIR-1 downto 0) of ALTERA_DDR2_OUT;
	type BIDIR_PORTS_MEM_OUT is array (PORTS_BIDIR-1 downto 0) of ALTERA_DDR2_IN;
	
	-------------------
	-- OTHER DEFINES --
	-------------------
	
	type FIFO_IN_READ is record
		ack                           : std_logic;
	end record;
	
	type FIFO_IN_WRITE is record
		rst_n                         : std_logic;
		data_in                       : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
		write_en                      : std_logic;
	end record;
	
	type FIFO_OUT is record
		full                          : std_logic;
		almost_full                   : std_logic;
		almost_empty                  : std_logic;
		empty                         : std_logic;
		data_out                      : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
		ready                         : std_logic;
	end record;
	
end package;
