----------------------------------------------------------------------------
--
-- Title       : submodules
-- Design      : the FIFO block for JPEG 2000 forward wavelet transform
-- Author      : Grzegorz Pastuszak
-- Company     : private
-- Start Date  : 09.03.2003
--
-----------------------------------------------------------------------------
-- VERSION 2.0
-----------------------------------------------------------------------------
----  Description : the FIFO block for JPEG 2000 involvs 2 architectures,
----  based on rtl model or memory 
----------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
library work;
--use work.encoder_pkg.all;
use work.RAM_pkg.all;

entity delay is
  generic ( 
    BIT_WIDTH       : integer;
    ADDR_WIDTH      : integer);
  port (
      clk       : in  std_logic;
      rst_n     : in  std_logic;    
      en        : in  std_logic;  
      depth     : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
      din       : in  std_logic_vector(BIT_WIDTH-1 downto 0);
      dout      : out std_logic_vector(BIT_WIDTH-1 downto 0);
      dout_next : out std_logic_vector(BIT_WIDTH-1 downto 0)
    );
end;

architecture mem of delay is

signal waddress_r               : std_logic_vector (ADDR_WIDTH-1 downto 0);
signal waddress_next            : std_logic_vector (ADDR_WIDTH-1 downto 0);
signal en_n                     : std_logic;
signal ram_out                  : std_logic_vector (BIT_WIDTH-1 downto 0);

begin
  en_n <= not en;  

  COMB_PROC : process (waddress_r, waddress_next, depth)
  begin
    -- write address
    if (waddress_r >= (depth - 2)) then
      waddress_next <= (others => '0');
    else
      waddress_next <= waddress_r + 1;
    end if;     
  end process;
  -- Add Counter
  REGR_PROC : process (clk, rst_n, en)
  begin
    if (rst_n = '0') then
      waddress_r <= (others => '0');
    elsif (clk'event and clk = '1') then
      if en = '1' then
        waddress_r <= waddress_next;
      end if;
    end if;
  end process;

  REG_PROC : process (clk, en)
  begin
    if (clk'event and clk = '1') then
      if en = '1' then
        dout <= ram_out; 
      end if;
    end if;
  end process;   
  
  U_RAM : RAMT generic map (BIT_WIDTH => BIT_WIDTH, ADDR_WIDTH => ADDR_WIDTH)
  port map (clk(0) => clk, clk(1) => clk, wren => en, data_a => din, address_a => waddress_r, address_b => waddress_next, q_b => ram_out); 
  
  dout_next <= ram_out;
end;

library IEEE;
use IEEE.std_logic_1164.all;

package delay_pkg is 
component delay is
  generic ( 
    BIT_WIDTH       : integer;
    ADDR_WIDTH      : integer);
  port (
      clk       : in  std_logic;
      rst_n     : in  std_logic;    
      en        : in  std_logic;  
      depth     : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
      din       : in  std_logic_vector(BIT_WIDTH-1 downto 0);
      dout      : out std_logic_vector(BIT_WIDTH-1 downto 0);
      dout_next : out std_logic_vector(BIT_WIDTH-1 downto 0)
    );
end component;  
end;
