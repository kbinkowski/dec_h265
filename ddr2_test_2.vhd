-------------------------------------------------------------------------------
--
-- Title       : ddr2_test_2
-- Design      : motion_compensation_dec
-- Author      : 
-- Company     : 
--
-------------------------------------------------------------------------------
--
-- File        : ddr2_test_2.vhd
-- Generated   : Sun Dec 15 23:56:15 2013
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ddr2_test_2} architecture {ddr2_test_2_arch}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use std.textio.all;	
use work.ddr2_pkg.all;
use work.adapter_pkg.all;
use work.motion_compensation_dec_pkg.all;


entity ddr2_test_2 is	 
port(	
	rst 		 : in std_logic;
	clk 		 : in std_logic;	
	start        : in std_logic;	
	chroma_format: in std_logic; 
    ref_port_in  : in REF_PORT_IN_TYPE;
    ref_port_out : out REF_PORT_OUT_TYPE
	);
end ddr2_test_2;

architecture ddr2_test_2_arch of ddr2_test_2 is		

type STATE_TYPE is (IDLE, WRITE, READ);

type INTERNAL_TYPE is record
	state			: STATE_TYPE;
	w_start         : std_logic;
	w_sel			: std_logic;
	w_address       : std_logic_vector(LOG_REFERENCED_FRAME_ID + 1 + LOG_FRAME_SIZE_Y-2 + LOG_FRAME_SIZE_X - 1 downto 0);
	w_width         : std_logic_vector(LOG_FRAME_SIZE_X-1 downto 0);
	w_height        : std_logic_vector(LOG_FRAME_SIZE_Y-2-1 downto 0);
	w_jump          : std_logic_vector(LOG_FRAME_SIZE_X downto 0);
	ref_frame_id    : std_logic_vector(LOG_REFERENCED_FRAME_ID-1 downto 0);  
	r_start         : std_logic;
	r_sel			: std_logic;
	r_address       : std_logic_vector(LOG_REFERENCED_FRAME_ID + 1 + LOG_FRAME_SIZE_Y-2 + LOG_FRAME_SIZE_X - 1 downto 0);
	r_width         : std_logic_vector(LOG_FRAME_SIZE_X-1 downto 0);
	r_height        : std_logic_vector(LOG_FRAME_SIZE_Y-2-1 downto 0);
	r_jump          : std_logic_vector(LOG_FRAME_SIZE_X downto 0);
end record;	   

signal internal_r 		: INTERNAL_TYPE;
signal internal_next 	: INTERNAL_TYPE;

signal w_data_r		  : DATA_SAMPLES_DEC_TYPE; 	
signal w_data_next	  : DATA_SAMPLES_DEC_TYPE;

component ddr2_controler is
  port(
    rst_n                         : in    std_logic;

    adapted_read_out              : out ADAPTER_READ_PORTS_OUT_TYPE;
    adapted_read_in               : in  ADAPTER_READ_PORTS_IN_TYPE;
    read_clk                      : in  std_logic_vector(PORTS_READ-1 downto 0);
    adapted_write_out             : out WRITE_PORTS_OUT;  -- things going to DRAM user
    adapted_write_in              : in  ADAPTER_WRITE_PORTS_IN_TYPE;  -- things from DRAM user
    write_clk                     : in  std_logic_vector(PORTS_WRITE-1 downto 0);
		
	--ddr2 memory controller
    aux_full_rate_clk              : out   std_logic;
    aux_half_rate_clk              : out   std_logic;
    aux_scan_clk                   : out   std_logic;
    aux_scan_clk_reset_n           : out   std_logic;
    dll_reference_clk              : out   std_logic;
    dqs_delay_ctrl_export          : out   std_logic_vector (DQS_DEL_EXPORT_WIDTH-1 downto 0);
    mem_addr                       : out   std_logic_vector (MEM_ADD_WIDTH-1 downto 0);
    mem_ba                         : out   std_logic_vector (MEM_BA_WIDTH-1 downto 0);
    mem_cas_n                      : out   std_logic;
    mem_cke                        : out   std_logic_vector (MEM_CKE_WIDTH-1 downto 0);
    mem_clk                        : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_clk_n                      : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_cs_n                       : out   std_logic_vector (MEM_CS_WIDTH-1 downto 0);
    mem_dm                         : out   std_logic_vector (MEM_DM_WIDTH-1 downto 0);
    mem_dq                         : inout std_logic_vector (MEM_DQ_WIDTH-1 downto 0);
    mem_dqs                        : inout std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
    mem_odt                        : out   std_logic_vector (MEM_ODT_WIDTH-1 downto 0);
    mem_ras_n                      : out   std_logic;
    mem_we_n                       : out   std_logic;
    pll_ref_clk                    : in    std_logic;
    reset_phy_clk_n                : out   std_logic
  );
end component;	

--Support signal
signal aux_full_rate_clk              : std_logic;
signal aux_half_rate_clk              : std_logic;
signal aux_scan_clk                   : std_logic;
signal aux_scan_clk_reset_n           : std_logic;
signal dll_reference_clk              : std_logic;
signal dqs_delay_ctrl_export          : std_logic_vector (DQS_DEL_EXPORT_WIDTH-1 downto 0);
signal mem_addr                       : std_logic_vector (MEM_ADD_WIDTH-1 downto 0);
signal mem_ba                         : std_logic_vector (MEM_BA_WIDTH-1 downto 0);
signal mem_cas_n                      : std_logic;
signal mem_cke                        : std_logic_vector (MEM_CKE_WIDTH-1 downto 0);
signal mem_clk                        : std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
signal mem_clk_n                      : std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
signal mem_cs_n                       : std_logic_vector (MEM_CS_WIDTH-1 downto 0);
signal mem_dm                         : std_logic_vector (MEM_DM_WIDTH-1 downto 0);
signal mem_dq                         : std_logic_vector (MEM_DQ_WIDTH-1 downto 0);
signal mem_dqs                        : std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
signal mem_odt                        : std_logic_vector (MEM_ODT_WIDTH-1 downto 0);
signal mem_ras_n                      : std_logic;
signal mem_we_n                       : std_logic;
signal pll_ref_clk                    : std_logic;
signal reset_phy_clk_n                : std_logic;	

signal ports_read_out : ADAPTER_READ_PORTS_OUT_TYPE;
signal ports_read_in  : ADAPTER_READ_PORTS_IN_TYPE;

signal ports_write_out : WRITE_PORTS_OUT;
signal ports_write_in  : ADAPTER_WRITE_PORTS_IN_TYPE;

signal read_clk		: std_logic_vector(PORTS_READ-1 downto 0);
signal write_clk 	: std_logic_vector(PORTS_WRITE-1 downto 0);	

signal read_test_data : DATA_SAMPLES_DEC_TYPE;  

begin  

read_clk  <= (others=>clk);
write_clk <= (others=>clk);
DDR2_MEMORY: ddr2_controler
port map(
    rst_n 						=> rst,

    adapted_read_out            => ports_read_out,
    adapted_read_in             => ports_read_in,
    read_clk 					=> read_clk,
    adapted_write_out           => ports_write_out,
    adapted_write_in            => ports_write_in,
    write_clk 					=> write_clk,
		
	--ddr2 memory controller
    aux_full_rate_clk 			=> aux_full_rate_clk,
    aux_half_rate_clk 			=> aux_half_rate_clk,
    aux_scan_clk                => aux_scan_clk, 
    aux_scan_clk_reset_n        => aux_scan_clk_reset_n,
    dll_reference_clk           => dll_reference_clk,
    dqs_delay_ctrl_export       => dqs_delay_ctrl_export,
    mem_addr                    => mem_addr,
    mem_ba                      => mem_ba,
    mem_cas_n                   => mem_cas_n,
    mem_cke                     => mem_cke,
    mem_clk                     => mem_clk,
    mem_clk_n                   => mem_clk_n,
    mem_cs_n                    => mem_cs_n,
    mem_dm                      => mem_dm,
    mem_dq                      => mem_dq,
    mem_dqs                     => mem_dqs,
    mem_odt                     => mem_odt,
    mem_ras_n                   => mem_ras_n,
    mem_we_n                    => mem_we_n,
    pll_ref_clk                 => pll_ref_clk,
    reset_phy_clk_n             => reset_phy_clk_n
); 

--Process ddr2 ports output support 
PROC_READ_OUT: process (ports_read_out) 
variable ref_port_out_v   : REF_PORT_OUT_TYPE;
variable read_test_data_v : DATA_SAMPLES_DEC_TYPE; 
begin
  -- READ_INTERFACE
  ref_port_out_v.ready := ports_read_out(0).ready and ports_read_out(1).ready;  
  ref_port_out_v.empty := ports_read_out(0).empty or ports_read_out(1).empty;
  ref_port_out_v.data(ports_read_out(0).data'length-1 downto 0) := ports_read_out(0).data;
  ref_port_out_v.data(ref_port_out_v.data'length-1 downto ports_read_out(0).data'length) := ports_read_out(1).data((8+DELTA_BIT_DEPTH)*(NUMBER_OF_SAMPLES_DEC-1)-1 downto 0); 
  
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    read_test_data_v(i) := ports_read_out(2).data((8+DELTA_BIT_DEPTH)*(i+1)-1 downto (8+DELTA_BIT_DEPTH)*i);
  end loop;
  --Assigment 
  ref_port_out   <= ref_port_out_v;	   
  read_test_data <= read_test_data_v;
end process; 

--Process ddr2 ports input support 
PROC_READ_IN: process (ref_port_in, internal_r) 
variable ports_read_in_v  : ADAPTER_READ_PORTS_IN_TYPE;
begin
  -- READ_INTERFACE 
  ports_read_in_v(0).start    := ref_port_in.start;
  ports_read_in_v(0).sel      := ref_port_in.sel; 
  ports_read_in_v(0).address  := (others=>'0');
  ports_read_in_v(0).wid      := (others=>'0');
  ports_read_in_v(0).hei      := (others=>'0');
  ports_read_in_v(0).jum      := (others=>'0');
  ports_read_in_v(0).address(ref_port_in.addr0'length-1 downto 0)   := ref_port_in.addr0;
  ports_read_in_v(0).wid(ref_port_in.width'length-1 downto 0)       := ref_port_in.width; 
  ports_read_in_v(0).hei(ref_port_in.height'length-1 downto 0)      := ref_port_in.height;
  ports_read_in_v(0).jum(ref_port_in.jump'length-1 downto 0)        := ref_port_in.jump; 
  ports_read_in_v(1).start    := ref_port_in.start;
  ports_read_in_v(1).sel      := ref_port_in.sel; 
  ports_read_in_v(1).address  := (others=>'0');
  ports_read_in_v(1).wid      := (others=>'0');
  ports_read_in_v(1).hei      := (others=>'0');
  ports_read_in_v(1).jum      := (others=>'0');
  ports_read_in_v(1).address(ref_port_in.addr1'length-1 downto 0)   := ref_port_in.addr1;
  ports_read_in_v(1).wid(ref_port_in.width'length-1 downto 0)    	:= ref_port_in.width; 
  ports_read_in_v(1).hei(ref_port_in.height'length-1 downto 0)      := ref_port_in.height;
  ports_read_in_v(1).jum(ref_port_in.jump'length-1 downto 0)        := ref_port_in.jump; 
  
  --READ_TEST INTERFACE
  ports_read_in_v(2).start    := internal_r.r_start;
  ports_read_in_v(2).sel      := internal_r.r_sel; 
  ports_read_in_v(2).address  := (others=>'0');
  ports_read_in_v(2).wid      := (others=>'0');
  ports_read_in_v(2).hei      := (others=>'0');
  ports_read_in_v(2).jum      := (others=>'0');
  ports_read_in_v(2).address(internal_r.r_address'length-1 downto 0)  := internal_r.r_address;
  ports_read_in_v(2).wid(internal_r.r_width'length-1 downto 0)    	:= internal_r.r_width; 
  ports_read_in_v(2).hei(internal_r.r_height'length-1 downto 0)        := internal_r.r_height;
  ports_read_in_v(2).jum(internal_r.r_jump'length-1 downto 0)       := internal_r.r_jump; 
  --Assigment 
  ports_read_in  <= ports_read_in_v;
end process;

--Process ddr2 ports input support 
PROC_WRITE_IN: process (internal_r, w_data_r) 
variable ports_write_in_v : ADAPTER_WRITE_PORTS_IN_TYPE; 
begin
  -- WRITE INTERFACE
  ports_write_in_v(0).start    := internal_r.w_start;
  ports_write_in_v(0).sel      := internal_r.w_sel; 
  ports_write_in_v(0).address  := (others=>'0');
  ports_write_in_v(0).wid      := (others=>'0');
  ports_write_in_v(0).hei      := (others=>'0');
  ports_write_in_v(0).jum      := (others=>'0');  
  ports_write_in_v(0).address(internal_r.w_address'length-1 downto 0)  := internal_r.w_address; 
  ports_write_in_v(0).wid(internal_r.w_width'length-1 downto 0)        := internal_r.w_width; 
  ports_write_in_v(0).hei(internal_r.w_height'length-1 downto 0)       := internal_r.w_height;
  ports_write_in_v(0).jum(internal_r.w_jump'length-1 downto 0)         := internal_r.w_jump; 
  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop
    ports_write_in_v(0).data((8+DELTA_BIT_DEPTH)*(i+1)-1 downto (8+DELTA_BIT_DEPTH)*i) := w_data_r(i);
  end loop; 
  --Assigment 
  ports_write_in <= ports_write_in_v;
end process;

-- FSM Process
PROC_REG: process(internal_r, start, chroma_format, ports_write_out, ports_read_out, w_data_r)
variable internal_v          : INTERNAL_TYPE;  
variable w_data_v            : DATA_SAMPLES_DEC_TYPE;
begin
    internal_v := internal_r;  
	w_data_v   := w_data_r;
	
    case internal_r.state is
      when IDLE =>
        internal_v.w_start  := '0';
        internal_v.w_sel    := '0';
        internal_v.r_start  := '0';
        internal_v.r_sel    := '0';
           
        --Start signal received, prepare device for interpolation
        if start = '1' and ports_write_out(0).ready = '1' then
          --Reset internal counters
          internal_v.state    := WRITE;
		  internal_v.w_start  := '1';
		  internal_v.w_address := (others=>'0');
	      internal_v.w_address(internal_v.w_address'length-1 downto internal_v.w_address'length-LOG_REFERENCED_FRAME_ID-1) := internal_r.ref_frame_id & '0';
	      internal_v.w_width  := std_logic_vector(to_unsigned(16, internal_v.w_width'length));
	      internal_v.w_height := std_logic_vector(to_unsigned(16 / NUMBER_OF_SAMPLES_DEC, internal_v.w_height'length));
		  -- Setting jump 
		  internal_v.w_jump   := (others=>'0');
	      internal_v.w_jump(LOG_FRAME_SIZE_X) := '1';
        end if;

      when WRITE =>
        internal_v.w_start  := '0';
        internal_v.w_sel    := '0';
        internal_v.r_start  := '0';
        internal_v.r_sel    := '0'; 
		  
	    if (unsigned(w_data_r(0)) = 64) then
		  if (ports_read_out(2).ready = '1') then 
			internal_v.state     := READ;	
		    internal_v.r_start   := '1';
		    internal_v.r_address := (others=>'0');
		    internal_v.r_address(internal_v.w_address'length-1 downto internal_v.w_address'length-LOG_REFERENCED_FRAME_ID-1) := internal_r.ref_frame_id & '0';
		    internal_v.r_width := std_logic_vector(to_unsigned((16), internal_v.w_width'length));
			-- Setting jump 
		    internal_v.r_jump   := (others=>'0');
	        internal_v.r_jump(LOG_FRAME_SIZE_X) := '1';			  
			internal_v.r_height := std_logic_vector(to_unsigned((16)/NUMBER_OF_SAMPLES_DEC, internal_v.w_height'length));	  
		   end if;
		else
		  if (ports_write_out(0).almost_full = '0') then
		    internal_v.w_sel := '1'; 
			w_data_v(0) := std_logic_vector(unsigned(w_data_r(0))+1);
		  else
		    internal_v.w_sel := '0'; 
		  end if;
		end if;   
		 
      when READ =>
        internal_v.w_start  := '0';
        internal_v.w_sel    := '0';
        internal_v.r_start  := '0';
        internal_v.r_sel    := '0'; 	 
	  
		if (ports_read_out(2).empty = '0') then
		  internal_v.r_sel     := '1'; 
		end if;	
		
	end case;
	
	w_data_next   <= w_data_v;
	internal_next <= internal_v;
end process;

	
--Main clk process.
PROC_CLK: process (clk, rst)
begin  
	if (rst = '0') then	
		internal_r.state        <= IDLE;		
		internal_r.w_start      <= '0';
		internal_r.w_sel        <= '0';	
		internal_r.w_address    <= (others=>'0');
		internal_r.w_width      <= (others=>'0');	
		internal_r.w_height     <= (others=>'0');
		internal_r.w_jump       <= (others=>'0');	  
		internal_r.ref_frame_id <= (others=>'0');
		w_data_r		        <= (others=>(others=>'0'));
		internal_r.r_start      <= '0';
		internal_r.r_sel        <= '0';	
		internal_r.r_address    <= (others=>'0');
		internal_r.r_width      <= (others=>'0');	
		internal_r.r_height     <= (others=>'0');
		internal_r.r_jump       <= (others=>'0');
	elsif rising_edge(clk) then	
		internal_r <= internal_next;
		w_data_r   <= w_data_next;
	end if;
end process;
		
end ddr2_test_2_arch;
