-------------------------------------------------------------------------------
-- Title      : Module transforming data from smaller bit width into larger.
-- Project    : 
-------------------------------------------------------------------------------
-- File       : ddr2_read_adapter.vhd
-- Author     :   <Roszkowski@STORK>
-- Company    : 
-- Created    : 2010-06-11
-- Last update: 2010-06-23
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: Module that multiplexes output of one bit width into outputs
--              of smaller bit widths. It also transforms block widths, heights
--              and addresses appropriately (in most simple cases).
-------------------------------------------------------------------------------
-- Copyright (c) 2010 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2010-06-11  1.0      Roszkowski	Created
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

use work.ddr2_pkg.all;
use work.adapter_pkg.all;
use work.motion_compensation_dec_pkg.all;

entity ddr2_read_adapter is
  port (
    clk              : in  std_logic;  -- clock
    rst_n            : in  std_logic;  -- reset (active low)
    dram_read_in     : in  READ_PORT_OUT;  -- things coming from dram controller
    dram_read_out    : out READ_PORT_IN;  -- things going to dram controller
    adapted_read_out : out READ_PORT_OUT_ADAPTER;  -- things going to DRAM user
    adapted_read_in  : in  READ_PORT_IN_ADAPTER  -- things from DRAM user
    );
end ddr2_read_adapter;

architecture rtl of ddr2_read_adapter is

  constant MULTIPLY_FACTOR : natural := (ALTERA_DATA_WIDTH / DRAM_MEMORY_DATA_WIDTH);
  --type RAM_OUTPUT_ARRAY_TYPE is array(MULTIPLY_FACTOR-2 downto 0) of std_logic_vector(DRAM_MEMORY_DATA_WIDTH-1 downto 0);

  type RAM_OUTPUT_CONTROLLER_TYPE is record
    --data : RAM_OUTPUT_ARRAY_TYPE;        -- data to be stored into memory
    --first_select : std_logic;
    max_width           : std_logic_vector(PARAM_WIDTH-1 downto 0);
    init_offset         : std_logic_vector(log2_ceil(MULTIPLY_FACTOR)-1 downto 0);
    pos_x               : std_logic_vector(PARAM_WIDTH-1 downto 0);
    num_parts_valid     : std_logic_vector(log2_ceil(MULTIPLY_FACTOR)-1 downto 0);  -- how many parts of data in data are valid
    last_sample_in_line : std_logic;
  end record;

  signal ram_output_r    : RAM_OUTPUT_CONTROLLER_TYPE;
  signal ram_output_next : RAM_OUTPUT_CONTROLLER_TYPE;
begin  -- rtl

  COMB_PROC: process (adapted_read_in, dram_read_in, ram_output_r)
    type OUTPUT_DATA_ARRAY_TYPE is array(MULTIPLY_FACTOR-1 downto 0) of std_logic_vector(DRAM_MEMORY_DATA_WIDTH-1 downto 0);

    variable ram_output_next_v          : RAM_OUTPUT_CONTROLLER_TYPE;
    variable dram_read_out_v            : READ_PORT_IN;
    variable adapted_read_out_v         : READ_PORT_OUT_ADAPTER;
    variable width_padding_v            : std_logic_vector(log2_ceil(MULTIPLY_FACTOR)-1 downto 0);
    variable output_samples_sel_array_v : OUTPUT_DATA_ARRAY_TYPE;
    variable last_sample_in_line_v      : std_logic;
    variable inc_pos_x_v                : std_logic;
    variable width_v                    : std_logic_vector(PARAM_WIDTH-1 downto 0);
  begin  -- process COMB_PROC
    -- calculate width from the beggining of the real memory cell that has to be fetched
    -- it is equal to width + address offset from the beggining of the cell
    width_v := fill_MSB_with_zeroes(adapted_read_in.wid,width_v'length) + fill_MSB_with_zeroes(adapted_read_in.address(ram_output_next_v.init_offset'high downto 0),width_v'length);
    inc_pos_x_v := '0';
    ram_output_next_v := ram_output_r;
    width_padding_v := (others => '0');
    -- initialise memory read
    dram_read_out_v.start := adapted_read_in.start;
    if adapted_read_in.start = '1' then
      -- set num parts valid to the offset specified by the address bits that will be cut
      ram_output_next_v.num_parts_valid :=  adapted_read_in.address(ram_output_next_v.num_parts_valid'high downto 0);
      ram_output_next_v.max_width := adapted_read_in.wid - 1;
      ram_output_next_v.pos_x := (others => '0');
      ram_output_next_v.last_sample_in_line := '0';
      ram_output_next_v.init_offset := adapted_read_in.address(ram_output_next_v.init_offset'high downto 0);
    end if;
    dram_read_out_v.hei   := adapted_read_in.hei;
    -- all bit depth difference will be compensated using width - so cut it appropriately
    dram_read_out_v.wid   := width_padding_v & width_v(adapted_read_in.wid'high downto log2_ceil(MULTIPLY_FACTOR));--adapted_read_in.wid(adapted_read_in.wid'high downto log2_ceil(MULTIPLY_FACTOR));
    if width_v(log2_ceil(MULTIPLY_FACTOR)-1 downto 0) /= 0 then--adapted_read_in.wid(log2_ceil(MULTIPLY_FACTOR)-1 downto 0) /= 0 then
      -- if block size to be fetched has width that is not multiple of 4, use width += 1
      dram_read_out_v.wid := dram_read_out_v.wid + 1;
    end if;
    dram_read_out_v.jum   := width_padding_v & adapted_read_in.jum(adapted_read_in.jum'high downto log2_ceil(MULTIPLY_FACTOR));
    -- after cutting width related things it is also necessary to cut address
    dram_read_out_v.address := adapted_read_in.address(ALTERA_ADDRESS_WIDTH+ALTERA_ADDRESS_CORRECTION-1 downto ALTERA_ADDRESS_CORRECTION);
    -- determine whether it is last sample in line
    if ram_output_r.pos_x = ram_output_r.max_width then
      last_sample_in_line_v := '1';
    else
      last_sample_in_line_v := '0';
    end if;
    
    -- select signal has to be passed only when local buffer is empty or it is last sample in the block line we are interested in
    if ram_output_r.num_parts_valid = conv_std_logic_vector(MULTIPLY_FACTOR-1,ram_output_next_v.num_parts_valid'length)
--       or last_sample_in_line_v ='1'  then
       or ram_output_r.last_sample_in_line ='1'  then
      -- last sample selected - pass select to FIFO
      dram_read_out_v.sel := adapted_read_in.sel;      
    else
      dram_read_out_v.sel := '0';
    end if;

    if dram_read_out_v.sel = '1' then
--      if last_sample_in_line_v = '1' then
      if ram_output_r.last_sample_in_line = '1' then
        -- move to next logical line - we need to start to read samples from init offset, and zero position X in line
        ram_output_next_v.num_parts_valid := ram_output_r.init_offset;
        ram_output_next_v.pos_x := (others => '0');
        ram_output_next_v.last_sample_in_line := '0';
      else
        inc_pos_x_v := '1';
        ram_output_next_v.num_parts_valid := (others => '0');
      end if;
    elsif adapted_read_in.sel = '1' then
      ram_output_next_v.num_parts_valid := ram_output_r.num_parts_valid + 1;
      inc_pos_x_v := '1';
    end if;

    if inc_pos_x_v = '1' then
      ram_output_next_v.pos_x := ram_output_r.pos_x + 1;
      if ram_output_next_v.pos_x = ram_output_r.max_width then
        ram_output_next_v.last_sample_in_line := '1';
      end if;
    end if;
    
    if dram_read_in.empty = '1' then
      adapted_read_out_v.empty := '1';
    else
      adapted_read_out_v.empty := '0';
    end if;
    adapted_read_out_v.ready := dram_read_in.ready;-- and adapted_read_out_v.empty;

    
    adapted_read_out_v.almost_empty := dram_read_in.almost_empty;
    adapted_read_out_v.almost_full := dram_read_in.almost_full;
    adapted_read_out_v.full := dram_read_in.full;
    for i in 0 to output_samples_sel_array_v'length-1 loop
      output_samples_sel_array_v(i) := dram_read_in.data((i+1)*DRAM_MEMORY_DATA_WIDTH-1 downto I*DRAM_MEMORY_DATA_WIDTH);
    end loop;
--    adapted_read_out_v.data := output_samples_sel_array_v(conv_integer(ram_output_r.num_parts_valid)); -- GP
    adapted_read_out_v.data := (others => '0');
    for i in 0 to DRAM_MEMORY_DATA_WIDTH/8-1 loop -- GP 
      adapted_read_out_v.data(7+i*(8+DELTA_BIT_DEPTH) downto i*(8+DELTA_BIT_DEPTH)) := output_samples_sel_array_v(conv_integer(ram_output_r.num_parts_valid))((i+1)*8-1 downto i*8);
    end loop;
    
    ram_output_next <= ram_output_next_v;
    adapted_read_out <= adapted_read_out_v;
    dram_read_out <= dram_read_out_v;
  end process COMB_PROC;
  
  REG_PROC : process (clk, rst_n)
  begin  -- process REG_PROC 
    if rst_n = '0' then                 -- asynchronous reset (active low)
      --for I in 0 to ram_output_r.data'length-1 loop
      --  ram_output_r.data(I) <= (others => '0');
      --end loop;
      ram_output_r.num_parts_valid <= (others => '0');
      ram_output_r.pos_x <= (others => '0');
      ram_output_r.max_width <= (others => '0');
      ram_output_r.init_offset <= (others => '0');
      --ram_output_r.first_select <= '0';
      ram_output_r.last_sample_in_line <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      
      ram_output_r <= ram_output_next;
    end if;
  end process REG_PROC ;

end rtl;
