library ieee; 
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ext_vhdl_pkg.all;

package intra_dec_pkg is
  
  constant LUMA                                                      : std_logic_vector := "00";
  constant CHROMA_CB                                                 : std_logic_vector := "10";
  constant CHROMA_CR                                                 : std_logic_vector := "11";
  
  constant PU_4X4                                                    : std_logic_vector := "00";
  constant PU_8X8                                                    : std_logic_vector := "01";
  constant PU_16X16                                                  : std_logic_vector := "10";
  constant PU_32X32                                                  : std_logic_vector := "11";
  
  constant BOTTOM_LEFT_AVAIL                                         : integer := 4;
  constant LEFT_AVAIL                                                : integer := 0;
  constant TOP_LEFT_AVAIL                                            : integer := 3;
  constant TOP_AVAIL                                                 : integer := 1;
  constant TOP_RIGHT_AVAIL                                           : integer := 2;
  
  constant LEFT_NEIGHBOR                                             : std_logic_vector(1 downto 0) := "00";
  constant TOP_NEIGHBOR                                              : std_logic_vector(1 downto 0) := "01";
  constant CORNER_NEIGHBOR                                           : std_logic_vector(1 downto 0) := "10";
  constant NOT_REQUIRED                                              : std_logic_vector(1 downto 0) := "11";
  
  constant REFS_AVAIL                                                : std_logic_vector(2 downto 0) := "000";
  constant SUBS_BY_CORNER                                            : std_logic_vector(2 downto 0) := "001";
  constant SUBS_BY_FIRST_REF                                         : std_logic_vector(2 downto 0) := "010";
  constant SUBS_BY_LAST_REF                                          : std_logic_vector(2 downto 0) := "011";
  constant SUBS_BY_DEF_VAL                                           : std_logic_vector(2 downto 0) := "100";
  
  constant MAX_PIC_WIDTH                                             : integer := 1920;
  constant MAX_PIC_WIDTH_LOG2                                        : integer := log2(MAX_PIC_WIDTH);
  constant MAX_PIC_HEIGHT                                            : integer := 1080;
  constant MAX_PIC_HEIGHT_LOG2                                       : integer := log2(MAX_PIC_HEIGHT);
  
  constant DELTA_BIT_DEPTH                                           : integer := 0;
  
  constant REFS_SMOOTH_AVAIL                                         : std_logic := '1';
  
  constant REFS_MEM_DATA_WIDTH                                       : integer := 4 * (8 + DELTA_BIT_DEPTH);
  constant REFS_MEM_ADDR_WIDTH                                       : integer := log2(2 * (MAX_PIC_WIDTH/4 + 64/4 + 64/16));
  
  type REFS_POINTER_TYPE is record
    neighbor                                                         : std_logic_vector(1 downto 0);
    position                                                         : std_logic_vector(4 downto 0);
  end record;
  
  type SET_OF_2_SAMPLES_TYPE is array(1 downto 0) of std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
  type SET_OF_4_SAMPLES_TYPE is array(3 downto 0) of std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
  type SET_OF_8_SAMPLES_TYPE is array(7 downto 0) of std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
  type SET_OF_64_SAMPLES_TYPE is array(63 downto 0) of std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
  
  function get_def_val(img_comp : std_logic_vector; bit_depth_luma : std_logic_vector; bit_depth_chroma : std_logic_vector) return std_logic_vector;
  
  function get_pu_idx(pu_col : std_logic_vector; pu_row : std_logic_vector) return std_logic_vector;
  
  function get_pu_col(pu_idx : std_logic_vector) return std_logic_vector;
  function get_pu_row(pu_idx : std_logic_vector) return std_logic_vector;
  
  function get_pu_col(pu_addr_x : std_logic_vector; cu_size : std_logic_vector) return std_logic_vector;
  function get_pu_row(pu_addr_y : std_logic_vector; cu_size : std_logic_vector) return std_logic_vector;
  function get_pu_size(pu_size_idx : std_logic_vector) return std_logic_vector;
  function get_log2_pu_size(pu_size_idx : std_logic_vector) return std_logic_vector;
  
  function get_refs_prep_req(img_comp : std_logic_vector; pu_size_idx : std_logic_vector; mode_idx : std_logic_vector) return std_logic;
  
  function get_smooth_term(bit_depth_luma : std_logic_vector; corner : std_logic_vector; bound_ref : std_logic_vector; pu_bound_ref : std_logic_vector) return std_logic;
  
  function get_filtered_sample(sample : std_logic_vector; left_sample : std_logic_vector; right_sample : std_logic_vector) return std_logic_vector;
  
  function get_smoothed_planar_corner(corner : std_logic_vector; bound_ref : std_logic_vector) return std_logic_vector;
  function get_smoothed_sample_offset(refs_pos : std_logic_vector; corner : std_logic_vector; bound_ref : std_logic_vector) return std_logic_vector;
  function get_smoothed_sample(ref_pos : std_logic_vector; offset : std_logic_vector; corner : std_logic_vector; bound_ref : std_logic_vector) return std_logic_vector;
  
  function get_pred_angle(mode_idx : std_logic_vector) return std_logic_vector;
  function get_ang_ref_pos(mode_idx : std_logic_vector; ref_idx : std_logic_vector) return std_logic_vector;
  
  function get_dc_corner_pred(filt_req : std_logic; dc_val : std_logic_vector; left_ref : std_logic_vector; right_ref : std_logic_vector) return std_logic_vector;
  function get_dc_edge_pred(filt_req : std_logic; dc_val : std_logic_vector; side_ref : std_logic_vector) return std_logic_vector;
  function get_linear_edge_pred(filt_req : std_logic; base_ref : std_logic_vector; corner : std_logic_vector; side_ref : std_logic_vector;
                                max_pred_val : std_logic_vector) return std_logic_vector;
  
  procedure adjust_pointer_to_pu_avail(pu_avail : in std_logic_vector; pu_row : in std_logic_vector; pu_size_m1 : in std_logic_vector;
                                       refs_pointer : inout REFS_POINTER_TYPE; adjust_type : out std_logic_vector);
  
end package intra_dec_pkg;

package body intra_dec_pkg is
  
  function get_def_val(img_comp : std_logic_vector; bit_depth_luma : std_logic_vector; bit_depth_chroma : std_logic_vector) return std_logic_vector is
    variable bit_depth_v                                             : std_logic_vector(bit_depth_luma'range);
    variable def_val_v                                               : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
  begin
    
    if (img_comp /= LUMA) then
      bit_depth_v := bit_depth_chroma;
    else
      bit_depth_v := bit_depth_luma;
    end if;
    
    case conv_integer(bit_depth_v(2 downto 0)) is
      when 0 =>
        def_val_v := conv_std_logic_vector(128, def_val_v'length);
      when 1 =>
        def_val_v := conv_std_logic_vector(256, def_val_v'length);
      when 2 =>
        def_val_v := conv_std_logic_vector(512, def_val_v'length);
      when 3 =>
        def_val_v := conv_std_logic_vector(1024, def_val_v'length);
      when 4 =>
        def_val_v := conv_std_logic_vector(2048, def_val_v'length);
      when 5 =>
        def_val_v := conv_std_logic_vector(4096, def_val_v'length);
      when others =>
        def_val_v := conv_std_logic_vector(9192, def_val_v'length);
    end case;
    
    return def_val_v;
  end function get_def_val;
  
  function get_pu_idx(pu_col : std_logic_vector; pu_row : std_logic_vector) return std_logic_vector is
    variable pu_idx_v                                                : std_logic_vector(7 downto 0);
  begin
    for i in 0 to 3 loop
      pu_idx_v(2 * i + 1) := pu_row(i);
      pu_idx_v(2 * i) := pu_col(i);
    end loop;
    return pu_idx_v;
  end function get_pu_idx;
  
  function get_pu_col(pu_idx : std_logic_vector) return std_logic_vector is
  begin
    return pu_idx(6) & pu_idx(4) & pu_idx(2) & pu_idx(0);
  end function get_pu_col;
  
  function get_pu_row(pu_idx : std_logic_vector) return std_logic_vector is
  begin
    return pu_idx(7) & pu_idx(5) & pu_idx(3) & pu_idx(1);
  end function get_pu_row;
  
  function get_pu_col(pu_addr_x : std_logic_vector; cu_size : std_logic_vector) return std_logic_vector is
    variable pu_col_v                                                : std_logic_vector(3 downto 0);
  begin
    pu_col_v := (others => '0');
    case conv_integer(cu_size(6 downto 0)) is
      when 16 =>
        pu_col_v(1 downto 0) := pu_addr_x(3 downto 2);
      when 32 =>
        pu_col_v(2 downto 0) := pu_addr_x(4 downto 2);
      when others =>
        pu_col_v(3 downto 0) := pu_addr_x(5 downto 2);
    end case;
    return pu_col_v;
  end function get_pu_col;
  
  function get_pu_row(pu_addr_y : std_logic_vector; cu_size : std_logic_vector) return std_logic_vector is
    variable pu_row_v                                                : std_logic_vector(3 downto 0);
  begin
    pu_row_v := (others => '0');
    case conv_integer(cu_size(6 downto 0)) is
      when 16 =>
        pu_row_v(1 downto 0) := pu_addr_y(3 downto 2);
      when 32 =>
        pu_row_v(2 downto 0) := pu_addr_y(4 downto 2);
      when others =>
        pu_row_v(3 downto 0) := pu_addr_y(5 downto 2);
    end case;
    return pu_row_v;
  end function get_pu_row;
  
  function get_pu_size(pu_size_idx : std_logic_vector) return std_logic_vector is
    variable pu_size_v                                               : std_logic_vector(5 downto 0);
  begin
    case pu_size_idx(1 downto 0) is
      when PU_4X4 =>
        pu_size_v := conv_std_logic_vector(4, pu_size_v'length);
      when PU_8X8 =>
        pu_size_v := conv_std_logic_vector(8, pu_size_v'length);
      when PU_16X16 =>
        pu_size_v := conv_std_logic_vector(16, pu_size_v'length);
      when others =>
        pu_size_v := conv_std_logic_vector(32, pu_size_v'length);
    end case;
    return pu_size_v;
  end function get_pu_size;
  
  function get_log2_pu_size(pu_size_idx : std_logic_vector) return std_logic_vector is
    variable pu_size_log2_v                                          : std_logic_vector(2 downto 0);
  begin
    case pu_size_idx(1 downto 0) is
      when PU_4X4 =>
        pu_size_log2_v := conv_std_logic_vector(2, pu_size_log2_v'length);
      when PU_8X8 =>
        pu_size_log2_v := conv_std_logic_vector(3, pu_size_log2_v'length);
      when PU_16X16 =>
        pu_size_log2_v := conv_std_logic_vector(4, pu_size_log2_v'length);
      when others =>
        pu_size_log2_v := conv_std_logic_vector(5, pu_size_log2_v'length);
    end case;
    return pu_size_log2_v;
  end function get_log2_pu_size;
  
  function get_refs_prep_req(img_comp : std_logic_vector; pu_size_idx : std_logic_vector; mode_idx : std_logic_vector) return std_logic is
    variable refs_prep_req_v                                         : std_logic;
  begin
    refs_prep_req_v := '0';
    
    if (img_comp = LUMA) then
      case pu_size_idx(1 downto 0) is
        when PU_8X8 =>
          if (mode_idx = 0) or (mode_idx = 2) or (mode_idx = 18) or (mode_idx = 34) then
            refs_prep_req_v := '1';
          end if;
        when PU_16X16 =>
          if (mode_idx /= 1) and (mode_idx /= 9) and (mode_idx /= 10) and (mode_idx /= 11) and
             (mode_idx /= 25) and (mode_idx /= 26) and (mode_idx /= 27) then
            refs_prep_req_v := '1';
          end if;
        when PU_32X32 =>
          if (mode_idx /= 1) and (mode_idx /= 10) and (mode_idx /= 26) then
            refs_prep_req_v := '1';
          end if;
        when others =>
          null;
      end case;
    end if;
    
    return refs_prep_req_v;
  end function get_refs_prep_req;
  
  function get_smooth_term(bit_depth_luma : std_logic_vector; corner : std_logic_vector; bound_ref : std_logic_vector; pu_bound_ref : std_logic_vector) return std_logic is
    variable limit_v                                                 : std_logic_vector(corner'high + 2 downto 0);
    variable subtrahend_v                                            : std_logic_vector(corner'high + 1 downto 0);
    variable bound_sum_v                                             : std_logic_vector(corner'high + 2 downto 0);
    variable smooth_term_v                                           : std_logic;
  begin
    
    limit_v := (others => '0');
    limit_v(conv_integer(bit_depth_luma(2 downto 0)) + 3) := '1';
    
    subtrahend_v := pu_bound_ref & '0';
    bound_sum_v := "00" & corner;
    bound_sum_v := bound_sum_v + bound_ref;
    
    bound_sum_v := bound_sum_v - subtrahend_v;
    if (bound_sum_v(bound_sum_v'high) = '1') then     -- negative value
      bound_sum_v := not bound_sum_v + 1;
    end if;
    
    smooth_term_v := '0';
    if (bound_sum_v < limit_v) then
      smooth_term_v := '1';
    end if;
    
    return smooth_term_v;
  end function get_smooth_term;
  
  function get_filtered_sample(sample : std_logic_vector; left_sample : std_logic_vector; right_sample : std_logic_vector) return std_logic_vector is
    variable sample_ext_v                                            : std_logic_vector(sample'high + 2 downto 0);
    variable sample_filt_v                                           : std_logic_vector(sample'range);
  begin
    sample_ext_v := '0' & sample & '0';
    sample_ext_v := sample_ext_v + left_sample + right_sample + 2;
    sample_filt_v := sample_ext_v(sample_ext_v'high downto 2);
    return sample_filt_v;
  end function get_filtered_sample;
  
  function get_smoothed_planar_corner(corner : std_logic_vector; bound_ref : std_logic_vector) return std_logic_vector is
    variable ref_diff_v                                              : std_logic_vector(corner'high + 7 downto corner'low);
    variable ref_sum_v                                               : std_logic_vector(corner'high + 7 downto corner'low);
    variable planar_corner_v                                         : std_logic_vector(corner'range);
  begin
    ref_diff_v := (others => '0');
    ref_diff_v(bound_ref'range) := bound_ref;
    ref_diff_v := ref_diff_v - corner;
    
    ref_sum_v := (others => '0');
    ref_sum_v(corner'range) := corner;
    ref_sum_v := ref_sum_v + bound_ref + 1;
    ref_sum_v := ref_sum_v(ref_sum_v'high - 5 downto 0) & "00000";
    ref_sum_v := ref_sum_v + ref_diff_v;
    
    planar_corner_v := ref_sum_v(planar_corner_v'high + 6 downto planar_corner_v'low + 6);
    return planar_corner_v;
  end function get_smoothed_planar_corner;
  
  function get_smoothed_sample_offset(refs_pos : std_logic_vector; corner : std_logic_vector; bound_ref : std_logic_vector) return std_logic_vector is
    variable offset_incr_v                                           : std_logic_vector(refs_pos'high + bound_ref'high downto bound_ref'low);
    variable offset_v                                                : std_logic_vector(offset_incr_v'high + refs_pos'high + 1 downto offset_incr_v'low);
  begin
    offset_v := (others => '0');
    for mult_idx in refs_pos'low to refs_pos'high loop
      offset_incr_v := (others => '0');
      if (refs_pos(mult_idx) = '0') then
        offset_incr_v(corner'high + mult_idx downto corner'low + mult_idx) := corner;
      else
        offset_incr_v(bound_ref'high + mult_idx downto bound_ref'low + mult_idx) := bound_ref;
      end if;
      offset_v := offset_v + offset_incr_v;
    end loop;
    offset_v := offset_v(offset_v'high - 2 downto offset_v'low) & "00";
    offset_v := offset_v + bound_ref + 32;
    return offset_v;
  end function get_smoothed_sample_offset;
  
  function get_smoothed_sample(ref_pos : std_logic_vector; offset : std_logic_vector; corner : std_logic_vector; bound_ref : std_logic_vector) return std_logic_vector is
    variable ref_incr_v                                              : std_logic_vector(bound_ref'high + 2 downto bound_ref'low);
    variable ref_unclip_v                                            : std_logic_vector(offset'high + 1 downto offset'low);
    variable ref_smooth_v                                            : std_logic_vector(corner'range);
  begin
    ref_unclip_v := (others => '0');
    for mult_idx in ref_pos'low to ref_pos'high loop
      ref_incr_v := (others => '0');
      if (ref_pos(mult_idx) = '0') then
        ref_incr_v(corner'high + mult_idx downto corner'low + mult_idx) := corner;
      else
        ref_incr_v(bound_ref'high + mult_idx downto bound_ref'low + mult_idx) := bound_ref;
      end if;
      ref_unclip_v := ref_unclip_v + ref_incr_v;
    end loop;
    ref_unclip_v := ref_unclip_v + offset;
    ref_smooth_v := ref_unclip_v(ref_smooth_v'high + 6 downto ref_smooth_v'low + 6);
    return ref_smooth_v;
  end function get_smoothed_sample;
  
  function get_pred_angle(mode_idx : std_logic_vector) return std_logic_vector is
    variable pred_angle_v                                            : std_logic_vector(6 downto 0);
  begin
    case conv_integer(mode_idx(5 downto 0)) is
      when 2| 34 =>
        pred_angle_v := conv_std_logic_vector(32, pred_angle_v'length);
      when 3| 33 =>
        pred_angle_v := conv_std_logic_vector(26, pred_angle_v'length);
      when 4| 32 =>
        pred_angle_v := conv_std_logic_vector(21, pred_angle_v'length);
      when 5| 31 =>
        pred_angle_v := conv_std_logic_vector(17, pred_angle_v'length);
      when 6| 30 =>
        pred_angle_v := conv_std_logic_vector(13, pred_angle_v'length);
      when 7| 29 =>
        pred_angle_v := conv_std_logic_vector(9, pred_angle_v'length);
      when 8| 28 =>
        pred_angle_v := conv_std_logic_vector(5, pred_angle_v'length);
      when 9| 27 =>
        pred_angle_v := conv_std_logic_vector(2, pred_angle_v'length);
      when 11| 25 =>
        pred_angle_v := conv_std_logic_vector(-2, pred_angle_v'length);
      when 12| 24 =>
        pred_angle_v := conv_std_logic_vector(-5, pred_angle_v'length);
      when 13| 23 =>
        pred_angle_v := conv_std_logic_vector(-9, pred_angle_v'length);
      when 14| 22 =>
        pred_angle_v := conv_std_logic_vector(-13, pred_angle_v'length);
      when 15| 21 =>
        pred_angle_v := conv_std_logic_vector(-17, pred_angle_v'length);
      when 16| 20 =>
        pred_angle_v := conv_std_logic_vector(-21, pred_angle_v'length);
      when 17| 19 =>
        pred_angle_v := conv_std_logic_vector(-26, pred_angle_v'length);
      when 18 =>
        pred_angle_v := conv_std_logic_vector(-32, pred_angle_v'length);
      when others =>
        pred_angle_v := (others => '0');
    end case;
    return pred_angle_v;
  end function get_pred_angle;
  
  function get_ang_ref_pos(mode_idx : std_logic_vector; ref_idx : std_logic_vector) return std_logic_vector is
    constant REF_IDX_MAX                                               : std_logic_vector(ref_idx'range) := (others => '1');
    variable ref_pos_v                                                 : std_logic_vector(ref_idx'range);
  begin
    ref_pos_v := ref_idx;
    case conv_integer(mode_idx(5 downto 0)) is
      when 11| 25 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(16, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when 12| 24 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(6, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(13, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 2) then
          ref_pos_v := conv_std_logic_vector(19, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 3) then
          ref_pos_v := conv_std_logic_vector(26, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 4) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when 13| 23 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(4, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(7, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 2) then
          ref_pos_v := conv_std_logic_vector(11, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 3) then
          ref_pos_v := conv_std_logic_vector(14, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 4) then
          ref_pos_v := conv_std_logic_vector(18, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 5) then
          ref_pos_v := conv_std_logic_vector(21, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 6) then
          ref_pos_v := conv_std_logic_vector(25, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 7) then
          ref_pos_v := conv_std_logic_vector(28, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 8) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when 14| 22 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(2, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(5, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 2) then
          ref_pos_v := conv_std_logic_vector(7, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 3) then
          ref_pos_v := conv_std_logic_vector(10, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 4) then
          ref_pos_v := conv_std_logic_vector(12, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 5) then
          ref_pos_v := conv_std_logic_vector(15, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 6) then
          ref_pos_v := conv_std_logic_vector(17, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 7) then
          ref_pos_v := conv_std_logic_vector(20, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 8) then
          ref_pos_v := conv_std_logic_vector(22, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 9) then
          ref_pos_v := conv_std_logic_vector(25, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 10) then
          ref_pos_v := conv_std_logic_vector(27, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 11) then
          ref_pos_v := conv_std_logic_vector(30, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 12) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when 15| 21 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(2, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(4, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 2) then
          ref_pos_v := conv_std_logic_vector(6, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 3) then
          ref_pos_v := conv_std_logic_vector(8, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 4) then
          ref_pos_v := conv_std_logic_vector(9, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 5) then
          ref_pos_v := conv_std_logic_vector(11, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 6) then
          ref_pos_v := conv_std_logic_vector(13, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 7) then
          ref_pos_v := conv_std_logic_vector(15, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 8) then
          ref_pos_v := conv_std_logic_vector(17, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 9) then
          ref_pos_v := conv_std_logic_vector(19, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 10) then
          ref_pos_v := conv_std_logic_vector(21, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 11) then
          ref_pos_v := conv_std_logic_vector(23, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 12) then
          ref_pos_v := conv_std_logic_vector(24, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 13) then
          ref_pos_v := conv_std_logic_vector(26, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 14) then
          ref_pos_v := conv_std_logic_vector(28, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 15) then
          ref_pos_v := conv_std_logic_vector(30, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 16) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when 16| 20 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(2, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(3, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 2) then
          ref_pos_v := conv_std_logic_vector(5, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 3) then
          ref_pos_v := conv_std_logic_vector(6, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 4) then
          ref_pos_v := conv_std_logic_vector(8, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 5) then
          ref_pos_v := conv_std_logic_vector(9, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 6) then
          ref_pos_v := conv_std_logic_vector(11, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 7) then
          ref_pos_v := conv_std_logic_vector(12, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 8) then
          ref_pos_v := conv_std_logic_vector(14, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 9) then
          ref_pos_v := conv_std_logic_vector(15, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 10) then
          ref_pos_v := conv_std_logic_vector(17, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 11) then
          ref_pos_v := conv_std_logic_vector(18, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 12) then
          ref_pos_v := conv_std_logic_vector(20, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 13) then
          ref_pos_v := conv_std_logic_vector(21, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 14) then
          ref_pos_v := conv_std_logic_vector(23, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 15) then
          ref_pos_v := conv_std_logic_vector(24, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 16) then
          ref_pos_v := conv_std_logic_vector(26, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 17) then
          ref_pos_v := conv_std_logic_vector(27, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 18) then
          ref_pos_v := conv_std_logic_vector(29, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 19) then
          ref_pos_v := conv_std_logic_vector(30, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 20) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when 17| 19 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(1, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(2, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 2) then
          ref_pos_v := conv_std_logic_vector(4, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 3) then
          ref_pos_v := conv_std_logic_vector(5, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 4) then
          ref_pos_v := conv_std_logic_vector(6, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 5) then
          ref_pos_v := conv_std_logic_vector(7, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 6) then
          ref_pos_v := conv_std_logic_vector(9, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 7) then
          ref_pos_v := conv_std_logic_vector(10, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 8) then
          ref_pos_v := conv_std_logic_vector(11, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 9) then
          ref_pos_v := conv_std_logic_vector(12, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 10) then
          ref_pos_v := conv_std_logic_vector(14, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 11) then
          ref_pos_v := conv_std_logic_vector(15, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 12) then
          ref_pos_v := conv_std_logic_vector(16, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 13) then
          ref_pos_v := conv_std_logic_vector(17, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 14) then
          ref_pos_v := conv_std_logic_vector(18, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 15) then
          ref_pos_v := conv_std_logic_vector(20, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 16) then
          ref_pos_v := conv_std_logic_vector(21, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 17) then
          ref_pos_v := conv_std_logic_vector(22, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 18) then
          ref_pos_v := conv_std_logic_vector(23, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 19) then
          ref_pos_v := conv_std_logic_vector(25, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 20) then
          ref_pos_v := conv_std_logic_vector(26, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 21) then
          ref_pos_v := conv_std_logic_vector(27, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 22) then
          ref_pos_v := conv_std_logic_vector(28, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 23) then
          ref_pos_v := conv_std_logic_vector(30, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 24) then
          ref_pos_v := conv_std_logic_vector(31, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 25) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when 18 =>
        if (ref_idx = REF_IDX_MAX) then
          ref_pos_v := conv_std_logic_vector(1, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 1) then
          ref_pos_v := conv_std_logic_vector(2, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 2) then
          ref_pos_v := conv_std_logic_vector(3, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 3) then
          ref_pos_v := conv_std_logic_vector(4, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 4) then
          ref_pos_v := conv_std_logic_vector(5, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 5) then
          ref_pos_v := conv_std_logic_vector(6, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 6) then
          ref_pos_v := conv_std_logic_vector(7, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 7) then
          ref_pos_v := conv_std_logic_vector(8, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 8) then
          ref_pos_v := conv_std_logic_vector(9, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 9) then
          ref_pos_v := conv_std_logic_vector(10, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 10) then
          ref_pos_v := conv_std_logic_vector(11, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 11) then
          ref_pos_v := conv_std_logic_vector(12, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 12) then
          ref_pos_v := conv_std_logic_vector(13, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 13) then
          ref_pos_v := conv_std_logic_vector(14, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 14) then
          ref_pos_v := conv_std_logic_vector(15, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 15) then
          ref_pos_v := conv_std_logic_vector(16, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 16) then
          ref_pos_v := conv_std_logic_vector(17, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 17) then
          ref_pos_v := conv_std_logic_vector(18, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 18) then
          ref_pos_v := conv_std_logic_vector(19, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 19) then
          ref_pos_v := conv_std_logic_vector(20, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 20) then
          ref_pos_v := conv_std_logic_vector(21, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 21) then
          ref_pos_v := conv_std_logic_vector(22, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 22) then
          ref_pos_v := conv_std_logic_vector(23, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 23) then
          ref_pos_v := conv_std_logic_vector(24, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 24) then
          ref_pos_v := conv_std_logic_vector(25, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 25) then
          ref_pos_v := conv_std_logic_vector(26, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 26) then
          ref_pos_v := conv_std_logic_vector(27, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 27) then
          ref_pos_v := conv_std_logic_vector(28, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 28) then
          ref_pos_v := conv_std_logic_vector(29, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 29) then
          ref_pos_v := conv_std_logic_vector(30, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 30) then
          ref_pos_v := conv_std_logic_vector(31, ref_pos_v'length);
        elsif (ref_idx = REF_IDX_MAX - 31) then
          ref_pos_v := conv_std_logic_vector(32, ref_pos_v'length);
        end if;
      when others =>
        null;
    end case;
    return ref_pos_v;
  end function get_ang_ref_pos;
  
  function get_dc_corner_pred(filt_req : std_logic; dc_val : std_logic_vector; left_ref : std_logic_vector; right_ref : std_logic_vector) return std_logic_vector is
    variable trans_filt_val_v                                        : std_logic_vector(dc_val'high + 2 downto 0);
    variable sample_filt_v                                           : std_logic_vector(dc_val'range);
  begin
    sample_filt_v := dc_val;
    trans_filt_val_v := '0' & dc_val & '0';
    trans_filt_val_v := trans_filt_val_v + left_ref + right_ref + 2;
    if (filt_req = '1') then
      sample_filt_v := trans_filt_val_v(dc_val'high + 2 downto 2);
    end if;
    return sample_filt_v;
  end function get_dc_corner_pred;
  
  function get_dc_edge_pred(filt_req : std_logic; dc_val : std_logic_vector; side_ref : std_logic_vector) return std_logic_vector is
    variable trans_filt_val_v                                        : std_logic_vector(dc_val'high + 2 downto 0);
    variable sample_filt_v                                           : std_logic_vector(dc_val'range);
  begin
    sample_filt_v := dc_val;
    trans_filt_val_v := '0' & dc_val & '0';
    trans_filt_val_v := trans_filt_val_v + dc_val + side_ref + 2;
    if (filt_req = '1') then
      sample_filt_v := trans_filt_val_v(dc_val'high + 2 downto 2);
    end if;
    return sample_filt_v;
  end function get_dc_edge_pred;
  
  function get_linear_edge_pred(filt_req : std_logic; base_ref : std_logic_vector; corner : std_logic_vector; side_ref : std_logic_vector;
                                max_pred_val : std_logic_vector) return std_logic_vector is
    variable trans_filt_val_v                                        : std_logic_vector(corner'high + 2 downto 0);
    variable sample_filt_v                                           : std_logic_vector(corner'range);
  begin
    trans_filt_val_v := "00" & side_ref;
    trans_filt_val_v := trans_filt_val_v - corner;
    trans_filt_val_v := trans_filt_val_v(trans_filt_val_v'high) & trans_filt_val_v(trans_filt_val_v'high downto 1);
    trans_filt_val_v := trans_filt_val_v + base_ref;
    if (filt_req = '0') then
      sample_filt_v := base_ref;
    elsif (trans_filt_val_v(trans_filt_val_v'high) = '1') then     -- negative value
      sample_filt_v := (others => '0');
    elsif (trans_filt_val_v > max_pred_val) then     -- value bigger than the allowed maximum value
      sample_filt_v := max_pred_val;
    else
      sample_filt_v := trans_filt_val_v(sample_filt_v'range);
    end if;
    return sample_filt_v;
  end function get_linear_edge_pred;
  
  procedure adjust_pointer_to_pu_avail(pu_avail : in std_logic_vector; pu_row : in std_logic_vector; pu_size_m1 : in std_logic_vector;
                                       refs_pointer : inout REFS_POINTER_TYPE; adjust_type : out std_logic_vector) is
    variable req_dir_avail_v                                         : std_logic_vector(4 downto 0);
  begin
    
    req_dir_avail_v(LEFT_AVAIL) := '1';
    if (refs_pointer.neighbor = LEFT_NEIGHBOR) then
      req_dir_avail_v(LEFT_AVAIL) := pu_avail(LEFT_AVAIL);
    end if;
    
    req_dir_avail_v(TOP_AVAIL) := '1';
    if (refs_pointer.neighbor = TOP_NEIGHBOR) then
      req_dir_avail_v(TOP_AVAIL) := pu_avail(TOP_AVAIL);
    end if;
    
    req_dir_avail_v(TOP_LEFT_AVAIL) := '1';
    if (refs_pointer.neighbor = CORNER_NEIGHBOR) then
      req_dir_avail_v(TOP_AVAIL) := pu_avail(TOP_LEFT_AVAIL);
    end if;
    
    req_dir_avail_v(TOP_RIGHT_AVAIL) := '1';
    if (refs_pointer.neighbor = TOP_NEIGHBOR) and (refs_pointer.position > pu_size_m1(5 downto 2)) then
      req_dir_avail_v(TOP_RIGHT_AVAIL) := pu_avail(TOP_RIGHT_AVAIL);
    end if;
    
    req_dir_avail_v(BOTTOM_LEFT_AVAIL) := '1';
    if (refs_pointer.neighbor = LEFT_NEIGHBOR) and (refs_pointer.position > pu_size_m1(5 downto 2)) then
      req_dir_avail_v(BOTTOM_LEFT_AVAIL) := pu_avail(BOTTOM_LEFT_AVAIL);
    end if;
    
    adjust_type := REFS_AVAIL;
    if (req_dir_avail_v(TOP_LEFT_AVAIL) = '0') then
      
      adjust_type := SUBS_BY_FIRST_REF;
      if (pu_avail(LEFT_AVAIL) = '0') and (pu_avail(TOP_AVAIL) = '0') then
        adjust_type := SUBS_BY_DEF_VAL;
      end if;
      
      refs_pointer.neighbor := LEFT_NEIGHBOR;
      refs_pointer.position := (others => '0');
      if (pu_avail(TOP_AVAIL) = '1') then
        refs_pointer.neighbor := TOP_NEIGHBOR;
      end if;
      
    elsif (req_dir_avail_v(LEFT_AVAIL) = '0') or (req_dir_avail_v(TOP_AVAIL) = '0') then
      
      adjust_type := SUBS_BY_FIRST_REF;
      if (pu_avail(TOP_LEFT_AVAIL) = '1') then
        adjust_type := SUBS_BY_CORNER;
      elsif (pu_avail(LEFT_AVAIL) = '0') and (pu_avail(TOP_AVAIL) = '0') then
        adjust_type := SUBS_BY_DEF_VAL;
      end if;
      
      refs_pointer.neighbor := TOP_NEIGHBOR;
      refs_pointer.position := (others => '0');
      if (pu_avail(TOP_LEFT_AVAIL) = '1') then
        refs_pointer.neighbor := CORNER_NEIGHBOR;
        refs_pointer.position(1 downto 0) := pu_row(3 downto 2);
      elsif (pu_avail(LEFT_AVAIL) = '1') then
        refs_pointer.neighbor := LEFT_NEIGHBOR;
      end if;
      
    elsif (req_dir_avail_v(BOTTOM_LEFT_AVAIL) = '0') or (req_dir_avail_v(TOP_RIGHT_AVAIL) = '0') then
      
      adjust_type := SUBS_BY_LAST_REF;
      
      refs_pointer.position := '0' & pu_size_m1(5 downto 2);
      
    end if;
    
  end procedure adjust_pointer_to_pu_avail;
  
end package body intra_dec_pkg;