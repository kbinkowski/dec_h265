------------------------------------------------------------------
--
-- Title       : Entropy Subblocks
-- Design      : subblocks for JPEG 2000 entropy coder
-- Author      : Grzegorz Pastuszak
-- Company     : private property
-- Start Date  : 28.07.2003
--
---------------------------------------------------------------------
-- VERSION 1.0
-----------------------------------------------------------------------------
----  Description : the ram modules are employed in JPEG 2000 encoder 
----  There are fifo modules, ram modules. All subblocks can instancionated
----  as fpga ALTERA modules or asic modules.
----------------------------------------------------------------------------- 

-- same as RAMB but with double CLOCK
library IEEE;
use IEEE.std_logic_1164.all;
LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;

entity ramt is
   generic (
    BIT_WIDTH  : integer :=32;                --Bit Width of word.
    ADDR_WIDTH : integer :=8;                 --Address Width. 
    OUT_REG    : boolean := false
    );
  port(
  clk              : in   std_logic_vector(1 downto 0);
  clk_en           : in   std_logic_vector(1 downto 0) := "11";
  wren             : in   std_logic;   
  rden             : in   std_logic := '1';
  data_a           : in   std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
  address_a        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  address_b        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  q_b              : out  std_logic_vector(BIT_WIDTH-1 downto 0)
  );
end;  

architecture ram of ramt is

function outreg(str : string; reg: boolean) return string is
begin
  if reg then
    return str;
  else
    return "UNREGISTERED";
  end if;
end;

begin    

  ALTRAM: altsyncram generic map(
--    intended_device_family => "Arria II GX",
    operation_mode => "DUAL_PORT",
    width_a => BIT_WIDTH,
    widthad_a => ADDR_WIDTH,
    numwords_a => 2**ADDR_WIDTH,
    width_b => BIT_WIDTH,
    widthad_b => ADDR_WIDTH,
    numwords_b => 2**ADDR_WIDTH,
    lpm_type => "altsyncram",
    width_byteena_a => 1,
    outdata_reg_b => outreg("CLOCK1", OUT_REG),
    indata_aclr_a => "NONE",
    wrcontrol_aclr_a => "NONE",
    address_aclr_a => "NONE",
    address_reg_b => "CLOCK1",
    address_aclr_b => "NONE",
    outdata_aclr_b => "NONE",
    ram_block_type => "AUTO"
    )
  port map (clocken0 => clk_en(0), clocken1 => clk_en(1), clock0 => clk(0), clock1 => clk(1),
    wren_a => wren, rden_b => rden, address_a => address_a, address_b => address_b,
    data_a => data_a, q_b => q_b);
end;

library IEEE;
use IEEE.std_logic_1164.all;
LIBRARY altera_mf;
USE altera_mf.altera_mf_components.all;

entity ramd is
   generic (
    BIT_WIDTH  : integer := 16;                --Bit Width of word.
    ADDR_WIDTH : integer := 7;                 --Address Width.  
    OUT_REG    : boolean := false
    );
  port(
  clk              : in   std_logic_vector(1 downto 0);
  clk_en           : in   std_logic_vector(1 downto 0) := "11";
  wren             : in   std_logic_vector(1 downto 0) := "00";   
  rden             : in   std_logic_vector(1 downto 0) := "11";
  data_a           : in   std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
  data_b           : in   std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
  address_a        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  address_b        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  q_a              : out  std_logic_vector(BIT_WIDTH-1 downto 0);
  q_b              : out  std_logic_vector(BIT_WIDTH-1 downto 0)
  );
end;  

architecture ram of ramd is
function outreg(str : string; reg: boolean) return string is
begin
  if reg then
    return str;
  else
    return "UNREGISTERED";
  end if;
end;

begin    

--FPGA: altsyncram generic map(
--  intended_device_family => "Arria II GX",
--  operation_mode => "DUAL_PORT",
--  width_a => BIT_WIDTH,
--  widthad_a => ADDR_WIDTH,
--  numwords_a => 2**ADDR_WIDTH,
--  width_b => BIT_WIDTH,
--  widthad_b => ADDR_WIDTH,
--  numwords_b => 2**ADDR_WIDTH,
--  lpm_type => "altsyncram",
--  width_byteena_a => 1,
--  outdata_reg_b => "UNREGISTERED",
--  indata_aclr_a => "NONE",
--  wrcontrol_aclr_a => "NONE",
--  address_aclr_a => "NONE",
--  address_reg_b => "CLOCK1",
--  rdcontrol_reg_b => "CLOCK1",
--  address_aclr_b => "NONE",
--  outdata_aclr_b => "NONE",
--  ram_block_type => "AUTO"
--  )
--  port map (clocken0 => logic1, clocken1 => xihrb, clock0 => clk, clock1 => clk,
--  wren_a => xwei, rden_b => rei, address_a => ia, address_b => rb,
--  data_a => i, q_b => b);
--end;

FPGA : altsyncram
	generic map(
--		intended_device_family => "Arria II GX",
		operation_mode => "BIDIR_DUAL_PORT",
		address_reg_b => "CLOCK1",
		clock_enable_input_a => "NORMAL",
		clock_enable_input_b => "NORMAL",
		clock_enable_output_a => "NORMAL",
		clock_enable_output_b => "NORMAL",
		indata_reg_b => "CLOCK1",
		lpm_type => "altsyncram",
		outdata_aclr_a => "NONE",
		outdata_aclr_b => "NONE",
		outdata_reg_a => outreg("CLOCK0", OUT_REG),--"CLOCK0",
		outdata_reg_b => outreg("CLOCK1", OUT_REG),--"CLOCK1",
		power_up_uninitialized => "FALSE",
--		read_during_write_mode_port_a => "NEW_DATA_WITH_NBE_READ",
--		read_during_write_mode_port_b => "NEW_DATA_WITH_NBE_READ",
		numwords_a => 2**ADDR_WIDTH,
		numwords_b => 2**ADDR_WIDTH,
		widthad_a => ADDR_WIDTH,
		widthad_b => ADDR_WIDTH,
		width_a => BIT_WIDTH,
		width_b => BIT_WIDTH,
		width_byteena_a => 1,
		width_byteena_b => 1,
		wrcontrol_wraddress_reg_b => "CLOCK1"
	)
	port map(
		clocken0 => clk_en(0),
		clocken1 => clk_en(1),
		wren_a => wren(0),
		clock0 => clk(0),
		wren_b => wren(1),
		clock1 => clk(1),
		address_a => address_a,
		address_b => address_b,
		rden_a => rden(0),
		rden_b => rden(1),
		data_a => data_a,
		data_b => data_b,
		q_a => q_a,
		q_b => q_b
	);

end;


library IEEE;
use IEEE.std_logic_1164.all;

package  RAM_pkg is
component ramt is
   generic (
    BIT_WIDTH  : integer :=32;                --Bit Width of word.
    ADDR_WIDTH : integer :=8;                 --Address Width.  
    OUT_REG    : boolean := false
    );
  port(
  clk              : in   std_logic_vector(1 downto 0);
  clk_en           : in   std_logic_vector(1 downto 0) := "11";
  wren             : in   std_logic;   
  rden             : in   std_logic := '1';
  data_a           : in   std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
  address_a        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  address_b        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  q_b              : out  std_logic_vector(BIT_WIDTH-1 downto 0)
  );
end component;  

component ramd is
   generic (
    BIT_WIDTH  : integer := 16;                --Bit Width of word.
    ADDR_WIDTH : integer := 7;                 --Address Width.  
    OUT_REG    : boolean := false
    );
  port(
  clk              : in   std_logic_vector(1 downto 0);
  clk_en           : in   std_logic_vector(1 downto 0) := "11";
  wren             : in   std_logic_vector(1 downto 0) := "00";   
  rden             : in   std_logic_vector(1 downto 0) := "11";
  data_a           : in   std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
  data_b           : in   std_logic_vector(BIT_WIDTH-1 downto 0) := (others => '0');
  address_a        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  address_b        : in   std_logic_vector(ADDR_WIDTH-1 downto 0);
  q_a              : out  std_logic_vector(BIT_WIDTH-1 downto 0);
  q_b              : out  std_logic_vector(BIT_WIDTH-1 downto 0)
  );
end component;  

end;
  


