library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;
use ieee.numeric_std.std_match;
use work.interpolator_pkg.all; 
use work.motion_compensation_dec_pkg.DELTA_BIT_DEPTH;

entity interpolator_pe is
  port (
    rst_n            : in  std_logic;
    clk_pe           : in  std_logic;
    input_data       : in  PE_INPUT_DATA_TYPE;
    stage1_valid     : in  std_logic;
    stage2_valid     : in  std_logic;
    output_round     : out std_logic_vector(7+DELTA_BIT_DEPTH downto 0);
    output_16lsb     : out std_logic_vector(15+DELTA_BIT_DEPTH downto 0);
    output_full      : out std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_en        : in  std_logic;
    mode             : in  std_logic_vector(2 downto 0);
    sample_half      : in  std_logic;
    sample_adjacent  : in  std_logic;
    sample_invert    : in  std_logic;
    sample_zero      : in  std_logic
  );
end entity;

architecture comb of interpolator_pe is

  type    ADDITION_1_FILTER_TYPE  is array(3 downto 0) of std_logic_vector(21+DELTA_BIT_DEPTH downto 0); 
  type    ADDITION_2_FILTER_TYPE  is array(1 downto 0) of std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
  type    FILTER_TAPS_TYPE        is array(7 downto 0) of std_logic_vector(7 downto 0);
  type    FILTER_TAPS_TYPE_CHROMA is array(4 downto 0, 3 downto 0) of std_logic_vector(7 downto 0); 
  type    FILTER_TAPS_TYPE_LUMA   is array(2 downto 0, 7 downto 0) of std_logic_vector(7 downto 0); 
  type    MULTIPLICATION_TYPE     is array(7 downto 0) of std_logic_vector(21+DELTA_BIT_DEPTH downto 0); 
  
  type FILTER_REG_TYPE is record  
    input_samples       : PE_INPUT_DATA_TYPE;
    input_taps          : FILTER_TAPS_TYPE;
    multiplication      : MULTIPLICATION_TYPE;
    addition_stage_1    : ADDITION_1_FILTER_TYPE;
    addition_stage_2    : ADDITION_2_FILTER_TYPE;
    addition_stage_3    : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    addition_stage_4    : std_logic_vector(10+DELTA_BIT_DEPTH downto 0);
    addition_stage_5    : std_logic_vector(7+DELTA_BIT_DEPTH downto 0);
    chroma              : std_logic;
    zero                : std_logic;
    chroma_12_plus      : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_14_minus     : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_18_minus     : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_38_minus     : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_38_plus      : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_half_minus   : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_half_plus    : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_minus        : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_plus         : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    chroma_quar_minus   : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_1                : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_2                : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_common           : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_half_minus       : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_half_plus        : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_minus            : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_quar             : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    dc_sum              : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    lc                  : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    luma_12_minus       : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    luma_12_plus        : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    luma_14_minus       : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    luma_14_plus        : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    luma_common_plus    : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    luma_minus          : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
    luma_plus           : std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
  end record;
  
  constant FILTER_TAPS_LUMA : FILTER_TAPS_TYPE_LUMA := (
    ( conv_std_logic_vector(-1, 8), conv_std_logic_vector(4, 8), conv_std_logic_vector(-11, 8), conv_std_logic_vector(40, 8), conv_std_logic_vector(40, 8), conv_std_logic_vector(-11, 8), conv_std_logic_vector(4, 8), conv_std_logic_vector(-1, 8) ),
    ( (others => '0'), conv_std_logic_vector(1, 8), conv_std_logic_vector(-5, 8), conv_std_logic_vector(17, 8), conv_std_logic_vector(58, 8), conv_std_logic_vector(-10, 8), conv_std_logic_vector(4, 8), conv_std_logic_vector(-1, 8) ),
    ( (others => '0'), (others => '0'), (others => '0'), (others => '0'), conv_std_logic_vector(64, 8), (others => '0'), (others => '0'), (others => '0') )
  );
  
  constant FILTER_TAPS_CHROMA : FILTER_TAPS_TYPE_CHROMA := (
    ( conv_std_logic_vector(-4, 8), conv_std_logic_vector(36, 8), conv_std_logic_vector(36, 8), conv_std_logic_vector(-4, 8) ),
    ( conv_std_logic_vector(-4, 8), conv_std_logic_vector(28, 8), conv_std_logic_vector(46, 8), conv_std_logic_vector(-6, 8) ),
    ( conv_std_logic_vector(-2, 8), conv_std_logic_vector(16, 8), conv_std_logic_vector(54, 8), conv_std_logic_vector(-4, 8) ),
    ( conv_std_logic_vector(-2, 8), conv_std_logic_vector(10, 8), conv_std_logic_vector(58, 8), conv_std_logic_vector(-2, 8) ),
    ( (others => '0'), (others => '0'), conv_std_logic_vector(64, 8), (others => '0') )
  );

  signal pe_next, pe_r   : FILTER_REG_TYPE;
  
begin

  output_round <= pe_r.addition_stage_5;
  output_16lsb <= pe_r.addition_stage_3(15+DELTA_BIT_DEPTH downto 0);
  output_full  <= pe_r.addition_stage_3;
  
  --praca - sekwencyjnie
  PROC_INTERPOLATION: process(chroma_en, input_data, mode, pe_r, sample_adjacent, sample_half, sample_invert )
    variable inverted_input_v     : MULTIPLICATION_TYPE;
    variable not_inverted_input_v : MULTIPLICATION_TYPE;
    variable pe_v                 : FILTER_REG_TYPE;
  begin
  
    --stan początkowy zmiennych
    pe_v := pe_r;
    
    --uzupełnienie wstępne danych
    for i in 7 downto 0 loop
      pe_v.input_samples(i) := input_data(i);
      pe_v.input_taps(i) := (others => '0');
      pe_v.multiplication(i) := (others => '0');
    end loop;
    
    if FPGA = true then     
      if chroma_en = '0' then
        if std_match(mode, "00-") then  --całkowita luma
          for i in 7 downto 0 loop
            pe_v.input_taps(i) := FILTER_TAPS_LUMA(0, i);
          end loop;
        elsif std_match(mode, "01-") then --luma 1/4
          for i in 7 downto 0 loop
            pe_v.input_taps(i) := FILTER_TAPS_LUMA(1, i);
          end loop;
        elsif std_match(mode, "10-") then --luma 1/2
          for i in 7 downto 0 loop
            pe_v.input_taps(i) := FILTER_TAPS_LUMA(2, i);
          end loop;
        elsif std_match(mode, "11-") then --luma 3/4
          for i in 7 downto 0 loop
            pe_v.input_taps(i) := FILTER_TAPS_LUMA(1, 7-i);
          end loop;
        end if;
      else
        if std_match(mode, "000") then --całkowita chroma
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(0, i);
          end loop;
        elsif std_match(mode, "001") then --chroma 1/8
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(1, i);
          end loop;
        elsif std_match(mode, "010") then --chroma 1/4
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(2, i);
          end loop;
        elsif std_match(mode, "011") then --chroma 3/8
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(3, i);
          end loop;
        elsif std_match(mode, "100") then --chroma 1/2
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(4, i);
          end loop;
        elsif std_match(mode, "101") then --chroma 5/8
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(3, 3-i);
          end loop;
        elsif std_match(mode, "110") then --chroma 3/4
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(2, 3-i);
          end loop;
        elsif std_match(mode, "111") then --chroma 7/8
          for i in 3 downto 0 loop
            pe_v.input_taps(2+i) := FILTER_TAPS_CHROMA(1, 3-i);
          end loop;
        end if;
      end if;
      
      for i in 0 to 7 loop
        pe_v.multiplication(i) := signed(pe_v.input_taps(i)(5 downto 0)) * signed(pe_v.input_samples(i));
      end loop;
      
      for k in 0 to 3 loop -- for each pair of taps  
        pe_v.addition_stage_1(k) := pe_r.multiplication(2*k) + pe_r.multiplication(2*k+1);
      end loop;
      pe_v.addition_stage_2(0) := pe_r.addition_stage_1(0) + pe_r.addition_stage_1(1);
      pe_v.addition_stage_2(1) := pe_r.addition_stage_1(2) + pe_r.addition_stage_1(3);
   
      pe_v.addition_stage_3 := pe_r.addition_stage_2(0) + pe_r.addition_stage_2(1);
      
    else
    
      if sample_invert = '1' then
        for i in 7 downto 0 loop
          inverted_input_v(i) := conv_std_logic_vector(signed(input_data(7-i)),22+DELTA_BIT_DEPTH);
          not_inverted_input_v(i) := conv_std_logic_vector(signed(input_data(i)),22+DELTA_BIT_DEPTH);
        end loop;
      else
        for i in 7 downto 0 loop
          inverted_input_v(i) := conv_std_logic_vector(signed(input_data(i)),22+DELTA_BIT_DEPTH);
          not_inverted_input_v(i) := conv_std_logic_vector(signed(input_data(i)),22+DELTA_BIT_DEPTH);
        end loop;
      end if;
      
      --Sekcja luma/chroma
      pe_v.chroma_12_plus := not_inverted_input_v(3) + not_inverted_input_v(4);
      pe_v.chroma_38_plus := (inverted_input_v(3)(18+DELTA_BIT_DEPTH downto 0) & "000") - (inverted_input_v(3));
      pe_v.chroma_38_minus := (inverted_input_v(2)) + (inverted_input_v(4)(20+DELTA_BIT_DEPTH downto 0) & "0");
      pe_v.chroma_14_minus := (inverted_input_v(2)) + (inverted_input_v(3)(20+DELTA_BIT_DEPTH downto 0) & "0");
      pe_v.chroma_18_minus := (inverted_input_v(4)) + (inverted_input_v(4)(20+DELTA_BIT_DEPTH downto 0) & "0");
      pe_v.luma_12_plus :=  (not_inverted_input_v(2)) + (not_inverted_input_v(6)(19+DELTA_BIT_DEPTH downto 0) & "00");
      pe_v.luma_12_minus := (not_inverted_input_v(5)(18+DELTA_BIT_DEPTH downto 0) & "000") + (not_inverted_input_v(7));
      pe_v.luma_14_plus := (inverted_input_v(4)) + (inverted_input_v(6));
      pe_v.luma_14_minus := (inverted_input_v(3)(20+DELTA_BIT_DEPTH downto 0) & "0") + (inverted_input_v(5)(19+DELTA_BIT_DEPTH downto 0) & "00");
      pe_v.luma_common_plus := (inverted_input_v(1)(19+DELTA_BIT_DEPTH downto 0) & "00") + (inverted_input_v(5));
      
      --Sekcja pół/ćwierć/zero
      pe_v.dc_common := inverted_input_v(3);
      pe_v.dc_minus := (not_inverted_input_v(2)) + (not_inverted_input_v(5));
      pe_v.dc_quar := (inverted_input_v(4)(19+DELTA_BIT_DEPTH downto 0) & "00") - (inverted_input_v(3));
      pe_v.dc_half_plus := inverted_input_v(4);
      if chroma_en = '0' then
        pe_v.dc_half_minus := (inverted_input_v(0)) + (inverted_input_v(2)(18+DELTA_BIT_DEPTH downto 0) & "000");
      else
        pe_v.dc_half_minus := (others => '0');
      end if;
      
      --Multipleksery Chromy 1/4-1/8 i 1/2-3/8
      if sample_adjacent = '1' then
        pe_v.chroma_half_plus  := pe_v.chroma_38_plus;
        pe_v.chroma_half_minus := pe_v.chroma_38_minus;
        pe_v.chroma_quar_minus := pe_v.chroma_18_minus + pe_v.dc_common;
      else
        pe_v.chroma_half_plus  := (pe_v.chroma_12_plus(20+DELTA_BIT_DEPTH downto 0) & "0");
        pe_v.chroma_half_minus := (others => '0');
        pe_v.chroma_quar_minus := pe_v.chroma_14_minus + pe_v.dc_common;
      end if;
    
      --Multipleksery 1/4-1/2
      if sample_half = '1' then
        pe_v.chroma_plus := pe_v.chroma_half_plus;
        pe_v.chroma_minus := pe_v.chroma_half_minus;
        pe_v.luma_plus := pe_v.luma_12_plus + pe_v.luma_common_plus;
        pe_v.luma_minus := pe_v.luma_12_minus - (pe_v.chroma_12_plus(18+DELTA_BIT_DEPTH downto 0) & "000");
      else
        pe_v.chroma_plus := (others => '0');
        pe_v.chroma_minus := pe_v.chroma_quar_minus;
        pe_v.luma_plus := pe_v.luma_14_plus + pe_v.luma_common_plus;
        pe_v.luma_minus := pe_v.luma_14_minus;
      end if;
      
      --Multiplekser Luma/Chroma
      pe_v.chroma := chroma_en;
      pe_v.zero := sample_zero;
      if pe_r.chroma = '0' then
        pe_v.lc := pe_r.luma_plus - pe_r.luma_minus;
      else
        pe_v.lc := (pe_r.chroma_plus(20+DELTA_BIT_DEPTH downto 0) & "0") - (pe_r.chroma_minus(20+DELTA_BIT_DEPTH downto 0) & "0");
      end if;
      
      --Multipleksery 1/4-1/2
      if sample_half = '1' then
        pe_v.dc_1 := (pe_v.dc_common(17+DELTA_BIT_DEPTH downto 0) & "0000") - (pe_v.dc_minus(20+DELTA_BIT_DEPTH downto 0) & "0");
        pe_v.dc_2 := (pe_v.dc_half_plus(16+DELTA_BIT_DEPTH downto 0) & "00000") - pe_v.dc_half_minus;
      else
        pe_v.dc_1 := (pe_v.dc_common(16+DELTA_BIT_DEPTH downto 0) & "00000") - pe_v.dc_minus;
        pe_v.dc_2 := (pe_v.dc_quar(19+DELTA_BIT_DEPTH downto 0) & "00") - pe_v.dc_half_minus;
      end if;
      
      pe_v.dc_sum := (pe_r.dc_1(20+DELTA_BIT_DEPTH downto 0) & "0") + pe_r.dc_2;
      
      --Multiplekser wyjściowy
      if pe_r.zero = '0' then
        pe_v.addition_stage_3 := pe_v.dc_sum + pe_v.lc;      
      else
        pe_v.addition_stage_3 := (pe_r.dc_common(15+DELTA_BIT_DEPTH downto 0) & "000000");
      end if;
    end if;
    
    pe_v.addition_stage_4 := pe_v.addition_stage_3(21+DELTA_BIT_DEPTH downto 11) + 1;
            
    if pe_v.addition_stage_4(10+DELTA_BIT_DEPTH) = '1' then
      pe_v.addition_stage_5 := (others => '0');
    elsif pe_v.addition_stage_4(9+DELTA_BIT_DEPTH) = '1'  then
      pe_v.addition_stage_5 := (others => '1');
    else
      pe_v.addition_stage_5 := pe_v.addition_stage_4(8+DELTA_BIT_DEPTH downto 1);
    end if;
    
    pe_next <= pe_v;
    
  end process;
  
  PROC_CLK1: process(clk_pe, rst_n, pe_next, stage1_valid)
  begin
  
    if rst_n = '0' then
      pe_r.chroma <= '0';
      pe_r.chroma_minus <= (others => '0');
      pe_r.chroma_plus <= (others => '0');
      pe_r.dc_1 <= (others => '0');
      pe_r.dc_2 <= (others => '0');
      pe_r.dc_common <= (others => '0');
      pe_r.luma_minus <= (others => '0');
      pe_r.luma_plus <= (others => '0');
      pe_r.zero <= '0';
    elsif rising_edge(clk_pe) and (stage1_valid = '1' or FPGA) then
      pe_r.chroma <= pe_next.chroma;
      pe_r.chroma_minus <= pe_next.chroma_minus;
      pe_r.chroma_plus <= pe_next.chroma_plus;
      pe_r.dc_1 <= pe_next.dc_1;
      pe_r.dc_2 <= pe_next.dc_2;
      pe_r.dc_common <= pe_next.dc_common;
      pe_r.luma_minus <= pe_next.luma_minus;
      pe_r.luma_plus <= pe_next.luma_plus;
      pe_r.zero <= pe_next.zero;
    end if;  
  end process;
     
  PROC_CLK2: process(clk_pe, rst_n, pe_next, stage2_valid)
  begin
    
    if rst_n = '0' then
      pe_r.addition_stage_3 <= (others => '0');
      pe_r.addition_stage_4 <= (others => '0');
      pe_r.addition_stage_5 <= (others => '0');
    elsif rising_edge(clk_pe) and (stage2_valid = '1' or FPGA) then
      pe_r.addition_stage_3 <= pe_next.addition_stage_3;
      pe_r.addition_stage_4 <= pe_next.addition_stage_4;
      pe_r.addition_stage_5 <= pe_next.addition_stage_5;
    end if;
  end process;
  
end architecture;