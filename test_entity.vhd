-------------------------------------------------------------------------------
--                                                                           --
-- Title       : MOTION COMPENSATION DECODER - H.265/HEVC                    --
-- Design      : the H.265/HEVC decoder                                      --
-- Author      : Kamil Binkowski, University of Technology in Warsaw         --
-- Start Date  : 02.03.2013                                                  --
--                                                                           --
-------------------------------------------------------------------------------  
--                                                                           --
-- File        : test_entity.vhd                                             --
--                                                                           --
-------------------------------------------------------------------------------
-- Description: The top_level entity for testing module MOTION COMPENSATION. --
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.motion_compensation_dec_pkg.all;
use ieee.numeric_std.all;


entity test_entity is
end test_entity;


architecture test_arch of test_entity is 

component motion_compensation_dec is  
  port(
  clk                      : in  std_logic;
  rst                      : in  std_logic;
  start                    : in  std_logic;
  input_data               : in  INPUT_DATA_TYPE; 
  chroma_format            : in  std_logic;	
  weight_denom             : in  WEIGHT_DENOM_TYPE;
  weighted_pred_flag       : in  std_logic;   
  weighted_bipred_flag     : in  std_logic;
  output_data              : out OUTPUT_DATA_TYPE;                       
  referenced_data_port_in  : out REF_PORT_IN_TYPE;
  referenced_data_port_out : in  REF_PORT_OUT_TYPE
  );
end component;  

component read_test is  
  port(
  rst                  : in std_logic;
  clk                  : in std_logic;
  start                : in std_logic;
  ready                : in std_logic;   
  read_info            : out INPUT_DATA_TYPE;
  chroma_format        : out std_logic;
  weight_denom         : out WEIGHT_DENOM_TYPE;
  weighted_pred_flag   : out std_logic;  
  weighted_bipred_flag : out std_logic
  );
end component;  

component checker_test is  
  port(   
  clk         : in std_logic;
  samples     : in DATA_SAMPLES_DEC_TYPE;
  samples_dav : in std_logic :='0'
  );
end component;

component ddr2_test is  
  port(   
  rst                      : in std_logic;
  clk                      : in std_logic;  
  start                    : in std_logic;  
  chroma_format            : in std_logic; 
  referenced_data_port_in  : in REF_PORT_IN_TYPE;
  referenced_data_port_out : out REF_PORT_OUT_TYPE
  );
end component;

signal clk_gen                      : std_logic :='0';
signal rst_gen                      : std_logic;
signal start_gen                    : std_logic;
signal output_data_gen              : OUTPUT_DATA_TYPE;
signal input_data_gen               : INPUT_DATA_TYPE;
signal referenced_data_port_in_gen  : REF_PORT_IN_TYPE;
signal referenced_data_port_out_gen : REF_PORT_OUT_TYPE;
signal ddr2_start_gen               : std_logic;  
signal chroma_format_gen            : std_logic;	
signal weight_denom_gen             : WEIGHT_DENOM_TYPE;
signal weighted_pred_flag_gen       : std_logic;  
signal weighted_bipred_flag_gen     : std_logic;


begin 

MOTION_COMPENSATION: motion_compensation_dec  
  port map(
  clk => clk_gen,
  rst => rst_gen,
  start => start_gen,
  input_data => input_data_gen,
  chroma_format => chroma_format_gen, 
  weight_denom => weight_denom_gen,
  weighted_pred_flag => weighted_pred_flag_gen,	   
  weighted_bipred_flag => weighted_bipred_flag_gen,
  output_data => output_data_gen,                   
  referenced_data_port_in => referenced_data_port_in_gen,
  referenced_data_port_out => referenced_data_port_out_gen
); 

READ: read_test  
  port map(
  clk => clk_gen,
  rst => rst_gen,
  start => start_gen,
  ready => output_data_gen.ready,
  read_info => input_data_gen, 
  chroma_format => chroma_format_gen,
  weight_denom => weight_denom_gen,
  weighted_pred_flag => weighted_pred_flag_gen, 
  weighted_bipred_flag => weighted_bipred_flag_gen
); 

CHECK: checker_test  
port map(   
  clk => clk_gen,
  samples => output_data_gen.reconstructed_samples,
  samples_dav => output_data_gen.reconstructed_samples_dav
);

DDR2: ddr2_test
port map(
  rst => rst_gen,
  clk => clk_gen,  
  start => ddr2_start_gen,  
  chroma_format => chroma_format_gen, 
  referenced_data_port_in => referenced_data_port_in_gen,
  referenced_data_port_out => referenced_data_port_out_gen
);


CLK_GENERATOR: process
begin
  wait for 5 ns;
  clk_gen <= not clk_gen;
end process;  


STIMULUS_PROC: process
begin
  rst_gen <= '1';
  ddr2_start_gen <='0';	 
  start_gen <='0'; 
  wait for 10 ns;
  rst_gen <= '0';
  wait for 10 ns;
  rst_gen <= '1'; 
  wait for 45 ns;
  ddr2_start_gen <='1';
  wait for 10 ns;
  ddr2_start_gen <='0';
  wait for 800 us;  -- Delay to start module Motion_Compensation after start external memory controler and module which writes samples to this memory 
  start_gen <= '1';
  wait for 10 ns;
  start_gen <= '0';
  wait for 5000 ms;
  assert FALSE
  report "END OF SIMULATION"
  severity FAILURE;
end process;


end test_arch;
