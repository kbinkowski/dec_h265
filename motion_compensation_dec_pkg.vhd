-------------------------------------------------------------------------------
--
-- Title       : MOTION COMPENSATION DECODER - H.265/HEVC
-- Design      : the H.265/HEVC decoder
-- Author      : Kamil Binkowski, University of Technology in Warsaw
-- Start Date  : 02.03.2013
--
-------------------------------------------------------------------------------  
--
-- File        : motion_compensation_dec_pkg.vhd
-- Generated   : Sun Mar 10 00:10:58 2013
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
-- Description: MOTION COMPENSATION package  
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all; 
use IEEE.numeric_std.all;

package motion_compensation_dec_pkg is   
  
  --Function returns the maximum of arguments. 
  function MAX (LEFT, RIGHT : natural) return natural;
  --Function used to calculate the Logarithm in base 2 of natural argument
  function log2_ceil (arg : natural) return natural;
  --Function convert boolean value to std_logic
  function to_std_logic(arg : boolean) return std_logic;

  constant DELTA_BIT_DEPTH                  : natural := 0;
  constant BIT_DEPTH                        : std_logic_vector(2 downto 0) := "000";  
  constant MAX_CU_SIZE                      : std_logic_vector(6 downto 0) := "1000000";  
  
  -- Inter prediction parameters
  constant INTERPOLATION_FLAG               : boolean := true;
  -- Intra prediction parameters
  constant REFS_SMOOTH_EN                   : std_logic := '1';
  
  -- Frame parameters
	constant FRAME_SIZE_X 										: natural := 704;
	constant FRAME_SIZE_Y											: natural := 576;
  constant LOG_FRAME_SIZE_X                 : natural := log2_ceil(FRAME_SIZE_X);
  constant LOG_FRAME_SIZE_Y                 : natural := log2_ceil(FRAME_SIZE_Y); 
  constant FRAME_OVERLAP                    : unsigned := "1001000"; --64 + 4 + 4  
  
  constant NUMBER_OF_SAMPLES                : natural := 8;
  constant NUMBER_OF_SAMPLES_DEC            : natural := 4;
  
  constant LOG_MOTION_VECTOR                : natural := 9;
  constant LOG_REFERENCED_FRAME_ID          : natural := 4;
  constant LOG_OFFSET                       : natural := 8;
  constant LOG_WEIGHT                       : natural := 9;
  constant LOG_INTRA_LUMA_PRED_MODE         : natural := 6;
	constant LOG_INTRA_CHROMA_PRED_MODE       : natural := 6;
	constant LOG_INTRA_PRED_MODE						  : natural := MAX(LOG_INTRA_LUMA_PRED_MODE, LOG_INTRA_CHROMA_PRED_MODE);
	constant LOG_PU_AVAIL                     : natural := 5;
  constant LOG_PREDICTION_PARAMETER         : natural := MAX(2+2*LOG_MOTION_VECTOR+LOG_REFERENCED_FRAME_ID, 1+4*LOG_INTRA_LUMA_PRED_MODE+LOG_INTRA_CHROMA_PRED_MODE);                                         
  constant LOG_ADDR_PREDICTION_PARAMETER    : natural := 9; -- (128 cells for one CTU) x2 for pingpong   
  constant LOG_ADDR_WEIGHTED_PREDICTION_PARAMETER : natural := 8;
  constant LOG_ADDR_RESIDUAL                : natural := 9;   
  constant LOG_ADDR_RECONSTRUCTED_SAMPLE    : natural := 12;
  constant LOG_ADDR_FIFO_ORDER              : natural := 2; 
  constant LOG_DATA_FIFO_ORDER              : natural := MAX(37+log2_ceil(NUMBER_OF_SAMPLES_DEC), 30+LOG_INTRA_PRED_MODE);
  constant FIFO_ORDER_WARN_LENGTH           : natural := 2;  
  constant LOG_INTERPOLATED_SAMPLE          : natural := 16; -- signed sample
  constant LOG_PRED_PARAMETER_REGISTER      : natural := MAX(2*LOG_WEIGHT+2*LOG_OFFSET+11, LOG_INTRA_PRED_MODE+LOG_PU_AVAIL+2);  
  
  -- Description CTU structure
  type CU_32_ARRAY_TYPE is array (3 downto 0)  of std_logic_vector(3 downto 0);
  type CU_16_ARRAY_TYPE is array (15 downto 0) of std_logic_vector(3 downto 0);
  type CU_8_ARRAY_TYPE  is array (63 downto 0) of std_logic_vector(1 downto 0);
  type CTU_TREE_TYPE is record
    cu_64  : std_logic_vector(3 downto 0);
    cu_32  : CU_32_ARRAY_TYPE;
    cu_16  : CU_16_ARRAY_TYPE;
    cu_8   : CU_8_ARRAY_TYPE;
  end record;   
  
  --Position of CTU in Frame
  type CTU_POSITION_TYPE is record
    position_x  : std_logic_vector(LOG_FRAME_SIZE_X-1 downto 0);
    position_y  : std_logic_vector(LOG_FRAME_SIZE_Y-1 downto 0);
  end record;
  
  --Denominator of all weighted factors
  type WEIGHT_DENOM_TYPE is record
    luma_log_weight_denom        : std_logic_vector(2 downto 0); --denominator of all weighted factor for luma samples
    chroma_log_weight_denom      : std_logic_vector(2 downto 0); --denominator of all weighted factor for chroma samples
  end record;
  
  --Processing samples types
  type DATA_SAMPLES_TYPE         is array (NUMBER_OF_SAMPLES-1 downto 0)     of std_logic_vector(7+DELTA_BIT_DEPTH downto 0);        -- 8+DELTA_BIT_DEPTH bit precision
  type DATA_SAMPLES_DEC_TYPE     is array (NUMBER_OF_SAMPLES_DEC-1 downto 0) of std_logic_vector(7+DELTA_BIT_DEPTH downto 0);         -- 8+DELTA_BIT_DEPTH bit precision
  type RESIDUALS_TYPE            is array (NUMBER_OF_SAMPLES_DEC-1 downto 0) of std_logic_vector(8+DELTA_BIT_DEPTH downto 0);         -- 8+DELTA_BIT_DEPTH bit precision + 1 bit of sign 
  type RESIDUALS_INPUT_TYPE      is array (NUMBER_OF_SAMPLES-1 downto 0)     of std_logic_vector(8+DELTA_BIT_DEPTH downto 0);       -- 8+DELTA_BIT_DEPTH bit precision + 1 bit of sign
  type INTERPOLATED_SAMPLES_TYPE is array (NUMBER_OF_SAMPLES_DEC-1 downto 0) of std_logic_vector(LOG_INTERPOLATED_SAMPLE-1 downto 0); -- 15-bit precision + 1 bit of sign 
  
  --Motion Vector type
  type MOTION_VECTOR_TYPE        is array (1 downto 0) of std_logic_vector(LOG_MOTION_VECTOR-1 downto 0);
  
  -- Input data 
  type INPUT_DATA_TYPE is record 
    -- Slice type
    slice_type                   : std_logic_vector(1 downto 0); -- 2 I-Slice, 1 P-Slice, 0 B-Slice
    -- CodingTreeUnit position
    ctu_position                 : CTU_POSITION_TYPE;
    -- CodingTreeUnit structure description
    ctu_tree                     : CTU_TREE_TYPE;
    -- Original Residuals
    residuals_dav                : std_logic;
    residuals                    : RESIDUALS_INPUT_TYPE;
    -- Motion Vector and other prediction parameters
    prediction_parameter_dav     : std_logic;  
  prediction_parameter         : std_logic_vector(LOG_MOTION_VECTOR*2+LOG_REFERENCED_FRAME_ID+3*(LOG_WEIGHT+LOG_OFFSET)+2-1 downto 0);
  end record; 
  
  -- Output data
  type OUTPUT_DATA_TYPE is record 
    -- Output flag informs that module is ready to read next CTU
    ready                        : std_logic;
    -- Reconstructed samples
    reconstructed_samples        : DATA_SAMPLES_DEC_TYPE;
    reconstructed_samples_dav    : std_logic;
  end record;
  
  -- Referenced Data Ports type - used to read referenced samples
  type REF_PORT_OUT_TYPE is record
    ready  : std_logic;
    empty  : std_logic;
    data   : std_logic_vector((2*NUMBER_OF_SAMPLES_DEC - 1)*(8+DELTA_BIT_DEPTH)-1 downto 0); -- whether NUMBER_OF_SAMPLES_DEC equals 4 we need only 7 referenced samples, from which we choose 4 valid samples   
  end record;

  -- Referenced Data Ports type - used to read referenced samples
  type REF_PORT_IN_TYPE is record
    start  : std_logic;
    sel    : std_logic;
    addr0  : std_logic_vector(LOG_REFERENCED_FRAME_ID+1+LOG_FRAME_SIZE_Y-2+LOG_FRAME_SIZE_X-1 downto 0);  -- LOG_REFERENCED_FRAME_ID + 1 bit (chroma) + (LOG_FRAME_SIZE_Y - 2) (cell's y position) + LOG_FRAME_SIZE_X ( cell's x position)  
    addr1  : std_logic_vector(LOG_REFERENCED_FRAME_ID+1+LOG_FRAME_SIZE_Y-2+LOG_FRAME_SIZE_X-1 downto 0);  -- LOG_REFERENCED_FRAME_ID + 1 bit (chroma) + (LOG_FRAME_SIZE_Y - 2) (cell's y position) + LOG_FRAME_SIZE_X ( cell's x position)
    width  : std_logic_vector(6 downto 0);                                                                -- Width Referenced Block, maximum: 64 + 2x4 (overlaps)
    height : std_logic_vector(4 downto 0);                                                                -- Height Referenced Block in memory cells, maximum: (64 + 2x4)/NUMBER_OF_SAMPLES_DEC 
    jump   : std_logic_vector(LOG_FRAME_SIZE_X downto 0);
  end record;
   
   
  -- LUT used to calculate the power of 2
  type INT_ARRAY_TYPE is array(0 to 14) of integer;
  constant POWER2 : INT_ARRAY_TYPE := (1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384);
  
  -- LUT used in weighted prediction to calculate offset adding to sample before rounding
  type OFFSET_WD_ARRAY_TYPE is array(0 to 13) of integer;
  constant OFFSET_WD : OFFSET_WD_ARRAY_TYPE := (0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096);
   
end; 

  
package body motion_compensation_dec_pkg is 
  

  function to_std_logic(arg : boolean) return std_logic is
  begin
    if arg then
      return '1';
    else
      return '0';
    end if;
  end function;
  
  function log2_ceil (arg : natural) return natural is
  begin
    if arg <= 1 then
      return 0;
    elsif arg <= 2 then
      return 1;
    elsif arg <= 4 then
      return 2;
    elsif arg <= 8 then
      return 3;
    elsif arg <= 16 then
      return 4;
    elsif arg <= 32 then
      return 5;
    elsif arg <= 64 then
      return 6;
    elsif arg <= 128 then
      return 7;
    elsif arg <= 256 then
      return 8;
    elsif arg <= 512 then
      return 9;
    elsif arg <= 1024 then
      return 10;
    elsif arg <= 2048 then
      return 11;	
    elsif arg <= 4096 then
      return 12;
    elsif arg <= 8192 then
      return 13;
    else
      return 0;
    end if;
  end function;  
  
  function MAX (LEFT, RIGHT : natural) return natural is
  begin
    if LEFT > RIGHT then 
      return LEFT;
    else 
      return RIGHT;
    end if;
  end MAX;    
  
end package body;

