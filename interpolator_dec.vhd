library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.std_match;
use work.delay_pkg.all;
use work.interpolator_pkg.all;
use work.motion_compensation_dec_pkg.log2_ceil;	 
use work.motion_compensation_dec_pkg.DELTA_BIT_DEPTH;

entity interpolator_dec is
  port (                                                          
    clk             : in  std_logic;
    rst_n           : in  std_logic;
    samples_in      : in  INPUT_OUTPUT_ROW_TYPE;
    samples_in_dav  : in  std_logic;
    start           : in  std_logic;
    chroma          : in  std_logic;
    mv_fract_x      : in  std_logic_vector(2 downto 0); 
    mv_fract_y      : in  std_logic_vector(2 downto 0);
    cu_size_x       : in  std_logic_vector(log2_ceil(CODING_UNIT_SIZE) downto 0);
    cu_size_y       : in  std_logic_vector(log2_ceil(CODING_UNIT_SIZE) downto 0);
    samples_out_dav : out std_logic;
    samples_out     : out INPUT_OUTPUT_ROW_TYPE;
    samples_out_wp  : out OUTPUT_ROW_TYPE;
    ready           : out std_logic
  );
end entity;

architecture rtl of interpolator_dec is

  component delay is
    generic ( 
      BIT_WIDTH       : integer;
      ADDR_WIDTH      : integer);
    port (
        clk       : in  std_logic;
        rst_n     : in  std_logic;    
        en        : in  std_logic;  
        depth     : in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        din       : in  std_logic_vector(BIT_WIDTH-1 downto 0);
        dout      : out std_logic_vector(BIT_WIDTH-1 downto 0);
        dout_next : out std_logic_vector(BIT_WIDTH-1 downto 0)
      );
  end component;  
  
  component interpolator_pe is 
    port (
      rst_n            : in  std_logic;
      clk_pe           : in  std_logic;
      input_data       : in  PE_INPUT_DATA_TYPE;
      stage1_valid     : in  std_logic;
      stage2_valid     : in  std_logic;
      output_round     : out std_logic_vector(7+DELTA_BIT_DEPTH downto 0);
      output_16lsb     : out std_logic_vector(15+DELTA_BIT_DEPTH downto 0);
      output_full      : out std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
      chroma_en        : in  std_logic;
      mode             : in  std_logic_vector(2 downto 0);
      sample_half      : in  std_logic;
      sample_adjacent  : in  std_logic;
      sample_invert    : in  std_logic;
      sample_zero      : in  std_logic
    );
  end component;

  type STATE_TYPE is (WAITING, WORKING);
  type VERTICAL_INPUT_LINE_TYPE       is array(INPUT_BLOCK_SIZE+FILTER_BLOCK_SIZE-1 downto -2) of std_logic_vector(DELTA_BIT_DEPTH+7 downto 0);
  type VERTICAL_HORIZONTAL_INPUT_TYPE is array(INPUT_BLOCK_SIZE-1 downto 0) of PE_INPUT_DATA_TYPE;
  type VERTICAL_OUTPUT_TYPE           is array(INPUT_BLOCK_SIZE-1 downto 0) of std_logic_vector(DELTA_BIT_DEPTH+15 downto 0);
  type HORIZONTAL_OUTPUT_TYPE         is array(INPUT_BLOCK_SIZE-1 downto 0) of std_logic_vector(DELTA_BIT_DEPTH+7 downto 0);    
  type HORIZONTAL_OUTPUT_WP_TYPE      is array(INPUT_BLOCK_SIZE-1 downto 0) of std_logic_vector(DELTA_BIT_DEPTH+21 downto 0);    
  
  type REGISTERS is record
    chroma                       : std_logic;
    delay_en                     : std_logic;
    horizontal_adjacent          : std_logic;
    horizontal_half              : std_logic;
    horizontal_invert            : std_logic;
    horizontal_zero              : std_logic;
    horizontal_mode              : std_logic_vector(2 downto 0);
    horizontal_counter           : std_logic_vector(log2_ceil(CODING_UNIT_SIZE+8) downto 0);
    horizontal_input             : VERTICAL_HORIZONTAL_INPUT_TYPE;
    horizontal_input_valid       : std_logic;
    horizontal_input_valid_delay : std_logic;
    horizontal_pixels            : std_logic_vector(log2_ceil(CODING_UNIT_SIZE+8) downto 0);
    output_dav                   : std_logic;
    queue_depth_1                : std_logic_vector(1+log2_ceil(CODING_UNIT_SIZE+8) downto 0);
    queue_depth_2                : std_logic_vector(1+log2_ceil(CODING_UNIT_SIZE+8) downto 0);
    ready                        : std_logic;
    state                        : STATE_TYPE;
    vertical_adjacent            : std_logic;
    vertical_half                : std_logic;
    vertical_invert              : std_logic;
    vertical_zero                : std_logic;
    vertical_mode                : std_logic_vector(2 downto 0);
    vertical_counter             : std_logic_vector(log2_ceil((CODING_UNIT_SIZE+8)/INPUT_BLOCK_SIZE) downto 0);
    vertical_input_valid         : std_logic;
    vertical_input_valid_delay   : std_logic;
    vertical_output_valid        : std_logic;
    vertical_pixels              : std_logic_vector(log2_ceil((CODING_UNIT_SIZE+8)/INPUT_BLOCK_SIZE) downto 0);
  end record;
  
  signal delay_enable               : std_logic;
  signal vertical_input_pins        : VERTICAL_INPUT_LINE_TYPE;
  signal vertical_input_lines       : VERTICAL_HORIZONTAL_INPUT_TYPE;
  signal vertical_output_pins       : VERTICAL_OUTPUT_TYPE;
  signal horizontal_output_pins     : HORIZONTAL_OUTPUT_TYPE;
  signal horizontal_output_wp_pins  : HORIZONTAL_OUTPUT_WP_TYPE;
  signal register_next              : REGISTERS;
  signal register_r                 : REGISTERS;
  
begin

  LINES: for i in INPUT_BLOCK_SIZE-1 downto 0 generate
    vertical_input_pins(i+2*INPUT_BLOCK_SIZE) <= samples_in(i);
    
    DELAY_MEM_1: delay generic map (
      BIT_WIDTH => 8+DELTA_BIT_DEPTH,
      ADDR_WIDTH => 2+log2_ceil(CODING_UNIT_SIZE+8)
    ) port map (
      clk => clk,
      rst_n => rst_n,
      en => delay_enable,
      depth => register_r.queue_depth_1,
      din => samples_in(i),
      dout => vertical_input_pins(i),
      dout_next => open
    );
    
    DELAY_MEM_2: delay generic map (
      BIT_WIDTH => 8+DELTA_BIT_DEPTH,
      ADDR_WIDTH => 2+log2_ceil(CODING_UNIT_SIZE+8)
    ) port map (
      clk => clk,
      rst_n => rst_n,
      en => delay_enable,
      depth => register_r.queue_depth_2,
      din => samples_in(i),
      dout => vertical_input_pins(i+INPUT_BLOCK_SIZE),
      dout_next => open
    );
    
    VERTICAL_PE: interpolator_pe port map (
      rst_n => rst_n,
      clk_pe => clk,
      input_data => vertical_input_lines(i),
      stage1_valid => register_r.vertical_input_valid,
      stage2_valid => register_r.vertical_input_valid_delay,
      output_round => open,
      output_16lsb => vertical_output_pins(i),
      output_full => open,
      chroma_en => register_r.chroma,
      mode => register_r.vertical_mode,
      sample_half => register_r.vertical_half,
      sample_adjacent => register_r.vertical_adjacent,
      sample_invert => register_r.vertical_invert,
      sample_zero => register_r.vertical_zero
    );
    
    HORIZONTAL_PE: interpolator_pe port map (
      rst_n => rst_n,
      clk_pe => clk,
      input_data => register_r.horizontal_input(i),
      stage1_valid => register_r.horizontal_input_valid,
      stage2_valid => register_r.horizontal_input_valid_delay,
      output_round => horizontal_output_pins(i),
      output_16lsb => open,
      output_full => horizontal_output_wp_pins(i),
      chroma_en => register_r.chroma,
      mode => register_r.horizontal_mode,
      sample_half => register_r.horizontal_half,
      sample_adjacent => register_r.horizontal_adjacent,
      sample_invert => register_r.horizontal_invert,
      sample_zero => register_r.horizontal_zero
    );
    
    samples_out(i) <= horizontal_output_pins(i) when register_r.output_dav = '1' else
                      (others => '0');
                      
    samples_out_wp(i) <= horizontal_output_wp_pins(i) when register_r.output_dav = '1' else
                         (others => '0');
    
    VERTICAL_LI: for j in FILTER_BLOCK_SIZE-1 downto 0 generate
      vertical_input_lines(i)(j) <= "00000000" & vertical_input_pins(j+i-1) when chroma = '1' or register_r.chroma = '1' else
                                    "00000000" & vertical_input_pins(j+i+1);
    end generate;
  end generate;
    
  vertical_input_pins(-1) <= (others => '0');
  vertical_input_pins(-2) <= (others => '0');
  ready <= register_r.ready;
  samples_out_dav <= register_r.output_dav;
  delay_enable <= samples_in_dav or register_r.delay_en;
                
  PROC_CLK: process(clk, rst_n)
  begin 
    if rst_n = '0' then --zerujemy
      register_r.chroma <= '0';
      register_r.delay_en <= '0';
      register_r.horizontal_adjacent <= '0';
      register_r.horizontal_counter <= (others => '0');
      register_r.horizontal_half <= '0';
      register_r.horizontal_input_valid <= '0';
      register_r.horizontal_input_valid_delay <= '0';
      register_r.horizontal_invert <= '0';
      register_r.horizontal_mode <= (others => '0');
      register_r.horizontal_pixels <= (others => '0');
      register_r.horizontal_zero <= '0';
      register_r.output_dav <= '0';
      register_r.queue_depth_1 <= (others => '0');
      register_r.queue_depth_2 <= (others => '0');
      register_r.ready <= '0';
      register_r.state <= WAITING;
      register_r.vertical_adjacent <= '0';
      register_r.vertical_counter <= (others => '0');
      register_r.vertical_half <= '0';
      register_r.vertical_input_valid <= '0';
      register_r.vertical_input_valid_delay <= '0';
      register_r.vertical_output_valid <= '0';
      register_r.vertical_invert <= '0';
      register_r.vertical_mode <= (others => '0');
      register_r.vertical_pixels <= (others => '0');
      register_r.vertical_zero <= '0';
      
      for i in INPUT_BLOCK_SIZE-1 downto 0 loop
        for j in FILTER_BLOCK_SIZE-1 downto 0 loop
          register_r.horizontal_input(i)(j) <= (others => '0');
        end loop;
      end loop;
    elsif rising_edge(clk) then
      register_r <= register_next;
    end if;
  end process;    

  PROC_STATE: process(samples_in, samples_in_dav, start, chroma, delay_enable, mv_fract_x, mv_fract_y, cu_size_x, cu_size_y, register_r, vertical_input_pins, vertical_input_lines, vertical_output_pins, horizontal_output_pins)
    variable register_v         : REGISTERS;
  begin
    register_v := register_r;
    register_v.horizontal_input_valid := '0';
    register_v.horizontal_input_valid_delay := register_r.horizontal_input_valid;
    register_v.output_dav := register_r.horizontal_input_valid_delay;
    register_v.vertical_input_valid := '0';
    register_v.vertical_input_valid_delay := register_r.vertical_input_valid;
    register_v.vertical_output_valid := register_r.vertical_input_valid_delay;
    
    case register_r.state is
      when WAITING =>
        register_v.ready := '1';
        register_v.delay_en := '0';
        
        if start = '1' then
          register_v.state := WORKING;
          register_v.ready := '0';
          register_v.output_dav := '0';
          register_v.chroma := chroma;
          if chroma = '1' then
            register_v.horizontal_pixels := conv_std_logic_vector(conv_integer(cu_size_x) + 4, log2_ceil(CODING_UNIT_SIZE+8)+1);
            register_v.queue_depth_1 := conv_std_logic_vector(2 * (conv_integer(cu_size_x) + 4), log2_ceil(CODING_UNIT_SIZE+8)+2);
            register_v.queue_depth_2 := conv_std_logic_vector(conv_integer(cu_size_x) + 4, log2_ceil(CODING_UNIT_SIZE+8)+2);
            register_v.vertical_counter := (others => '0');
            register_v.vertical_pixels := conv_std_logic_vector((conv_integer(cu_size_y) + 4) / INPUT_BLOCK_SIZE, log2_ceil((CODING_UNIT_SIZE+8)/INPUT_BLOCK_SIZE)+1);
          else
            register_v.horizontal_pixels := conv_std_logic_vector(conv_integer(cu_size_x) + 8, log2_ceil(CODING_UNIT_SIZE+8)+1);
            register_v.queue_depth_1 := conv_std_logic_vector(2 * (conv_integer(cu_size_x) + 8), log2_ceil(CODING_UNIT_SIZE+8)+2);
            register_v.queue_depth_2 := conv_std_logic_vector(conv_integer(cu_size_x) + 8, log2_ceil(CODING_UNIT_SIZE+8)+2);
            register_v.vertical_counter := (others => '0');
            register_v.vertical_pixels := conv_std_logic_vector((conv_integer(cu_size_y) + 8) / INPUT_BLOCK_SIZE, log2_ceil((CODING_UNIT_SIZE+8)/INPUT_BLOCK_SIZE)+1);
          end if;
          
          if samples_in_dav = '1' then
            register_v.horizontal_counter := conv_std_logic_vector(1, log2_ceil(CODING_UNIT_SIZE+8)+1);
          else
            register_v.horizontal_counter := (others => '0');
          end if;
          
          register_v.vertical_mode := mv_fract_y;
          register_v.horizontal_mode := mv_fract_x;
          
          if std_match(mv_fract_y, "10-") or (chroma = '1' and std_match(mv_fract_y, "011")) then
            register_v.vertical_half := '1';
          else
            register_v.vertical_half := '0';
          end if;
          
          if std_match(mv_fract_x, "10-") or (chroma = '1' and std_match(mv_fract_x, "011")) then
            register_v.horizontal_half := '1';
          else
            register_v.horizontal_half := '0';
          end if;
          
          if std_match(mv_fract_y, "--1") then
            register_v.vertical_adjacent := '1';
          else
            register_v.vertical_adjacent := '0';
          end if;
          
          if std_match(mv_fract_x, "--1") then
            register_v.horizontal_adjacent := '1';
          else
            register_v.horizontal_adjacent := '0';
          end if;
          
          if std_match(mv_fract_y, "11-") or (chroma = '1' and std_match(mv_fract_y, "101")) then
            register_v.vertical_invert := '1';
          else
            register_v.vertical_invert := '0';
          end if;
          
          if std_match(mv_fract_x, "11-") or (chroma = '1' and std_match(mv_fract_x, "101")) then
            register_v.horizontal_invert := '1';
          else
            register_v.horizontal_invert := '0';
          end if;
           
          if (chroma = '0' and std_match(mv_fract_y, "00-")) or (chroma = '1' and std_match(mv_fract_y, "000")) then
            register_v.vertical_zero :=  '1';
          else
            register_v.vertical_zero :=  '0';
          end if;
          
          if (chroma = '0' and std_match(mv_fract_x, "00-")) or (chroma = '1' and std_match(mv_fract_x, "000")) then
            register_v.horizontal_zero :=  '1';
          else
            register_v.horizontal_zero :=  '0';
          end if;
        end if;
        
      when WORKING =>
        if delay_enable = '1' then
          register_v.horizontal_counter := register_r.horizontal_counter + 1;
          if register_v.horizontal_counter = register_r.horizontal_pixels then
            register_v.horizontal_counter :=  (others => '0');
            register_v.vertical_counter := register_r.vertical_counter + 1;
            if register_v.vertical_counter = register_r.vertical_pixels then
              register_v.delay_en := '1';
            end if;
          end if;
          
          if register_r.vertical_counter > 1 then
            register_v.vertical_input_valid := '1';
          elsif register_r.vertical_counter = 1 and register_r.horizontal_counter = register_r.horizontal_pixels-1 then
            register_v.vertical_input_valid := '1';
          end if;
          
          if register_r.chroma = '0' then
            if register_r.horizontal_counter > 9 and register_r.vertical_counter > 1 then
              register_v.horizontal_input_valid := '1';
            elsif register_r.horizontal_counter < 2 and register_r.vertical_counter > 2 then
              register_v.horizontal_input_valid := '1';
            end if;
            
            if register_r.horizontal_counter = 4 and register_r.vertical_counter = register_r.vertical_pixels then
              register_v.state := WAITING;
            end if;
          else 
			-- Warunek wprowadzony przez K.B, tak aby uk�ad by� kompatybilny z blokami chrominancji o szeroko�ci mniejszej ni� 4
			if register_r.horizontal_pixels < 7 then
              if register_r.horizontal_counter > 1 and register_r.horizontal_counter < 4 and register_r.vertical_counter > 2 then
                register_v.horizontal_input_valid := '1';
			  end if;
			  if register_r.vertical_counter = register_r.vertical_pixels+2 then
                register_v.state := WAITING;
              end if;
			else
              if register_r.horizontal_counter > 7 and register_r.vertical_counter > 1 then
                register_v.horizontal_input_valid := '1';	
              elsif register_r.horizontal_counter < 4 and register_r.vertical_counter > 2 then
                register_v.horizontal_input_valid := '1';
              end if;
			  if register_r.horizontal_counter = 6 and register_r.vertical_counter = register_r.vertical_pixels+1 then
                register_v.state := WAITING;
              end if;
            end if;
            
          end if;
          
          if register_v.state = WAITING then
            register_v.delay_en := '0';
            register_v.horizontal_counter := (others => '0');
            register_v.horizontal_pixels := (others => '0');
            register_v.vertical_counter := (others => '0');
            register_v.vertical_pixels := (others => '0');
            register_v.horizontal_input_valid := '0';
            register_v.horizontal_input_valid_delay := '0';
            register_v.output_dav := '0';
            register_v.vertical_input_valid := '0';
            register_v.vertical_input_valid_delay := '0';
            register_v.vertical_output_valid := '0';
          end if;
        end if;
		
        if register_r.vertical_output_valid = '1' then
          for i in INPUT_BLOCK_SIZE-1 downto 0 loop
            register_v.horizontal_input(i)(7) := vertical_output_pins(i);
            for j in 7 downto 1 loop
            register_v.horizontal_input(i)(j-1) := register_r.horizontal_input(i)(j);
            end loop;
          end loop;
        end if;
    end case;
    
    register_next <= register_v;
  end process;  
  
end architecture;