library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ddr2_pkg.all;
use work.adapter_pkg.all;

entity ddr2_controler is
  port(
    --ddr2 memory controller
--    clk                            : in    std_logic;
--	clk_pixels                     : in    std_logic;
    rst_n                          : in    std_logic;

    adapted_read_out              : out ADAPTER_READ_PORTS_OUT_TYPE;
    adapted_read_in               : in  ADAPTER_READ_PORTS_IN_TYPE;
    read_clk                      : in  std_logic_vector(PORTS_READ-1 downto 0);
    adapted_write_out             : out WRITE_PORTS_OUT;  -- things going to DRAM user
    adapted_write_in              : in  ADAPTER_WRITE_PORTS_IN_TYPE;  -- things from DRAM user
    write_clk                     : in  std_logic_vector(PORTS_WRITE-1 downto 0);
		
	--ddr2 memory controller
    aux_full_rate_clk              : out   std_logic;
    aux_half_rate_clk              : out   std_logic;
    aux_scan_clk                   : out   std_logic;
    aux_scan_clk_reset_n           : out   std_logic;
    dll_reference_clk              : out   std_logic;
    dqs_delay_ctrl_export          : out   std_logic_vector (DQS_DEL_EXPORT_WIDTH-1 downto 0);
    mem_addr                       : out   std_logic_vector (MEM_ADD_WIDTH-1 downto 0);
    mem_ba                         : out   std_logic_vector (MEM_BA_WIDTH-1 downto 0);
    mem_cas_n                      : out   std_logic;
    mem_cke                        : out   std_logic_vector (MEM_CKE_WIDTH-1 downto 0);
    mem_clk                        : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_clk_n                      : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_cs_n                       : out   std_logic_vector (MEM_CS_WIDTH-1 downto 0);
    mem_dm                         : out   std_logic_vector (MEM_DM_WIDTH-1 downto 0);
    mem_dq                         : inout std_logic_vector (MEM_DQ_WIDTH-1 downto 0);
    mem_dqs                        : inout std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
--    mem_dqsn              : inout std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
    mem_odt                        : out   std_logic_vector (MEM_ODT_WIDTH-1 downto 0);
    mem_ras_n                      : out   std_logic;
    mem_we_n                       : out   std_logic;
--    local_ready           : out   std_logic;
--    oct_ctl_rs_value               : in    std_logic_vector (OCT_CTL_WIDTH-1 downto 0);
--    oct_ctl_rt_value               : in    std_logic_vector (OCT_CTL_WIDTH-1 downto 0);
    pll_ref_clk                    : in    std_logic;
    reset_phy_clk_n                : out   std_logic
  );
end entity;

architecture struct_arch of ddr2_controler is

component ddr2_toplevel is
  port(
    --ddr2 memory controller
    clk_port              : out   std_logic;
	ready                 : out   std_logic;
    rst_n                 : in    std_logic;
    --read ports
    read_clk              : in    std_logic_vector(PORTS_READ-1 downto 0);
    read_in		          : in    READ_PORTS_IN;
    read_out		      : out   READ_PORTS_OUT;
    --bidir ports
    bidir_in              : in    BIDIR_PORTS_IN;
    bidir_out		      : out   BIDIR_PORTS_OUT;
    --write ports
    write_clk             : in    std_logic_vector(PORTS_WRITE-1 downto 0);
    write_in		      : in    WRITE_PORTS_IN;
    write_out		      : out   WRITE_PORTS_OUT;
    --ddr2 memory controller
    aux_full_rate_clk     : out   std_logic;
    aux_half_rate_clk     : out   std_logic;
    aux_scan_clk          : out   std_logic;
    aux_scan_clk_reset_n  : out   std_logic;
    dll_reference_clk     : out   std_logic;
    dqs_delay_ctrl_export : out   std_logic_vector (DQS_DEL_EXPORT_WIDTH-1 downto 0);
    mem_addr              : out   std_logic_vector (MEM_ADD_WIDTH-1 downto 0);
    mem_ba                : out   std_logic_vector (MEM_BA_WIDTH-1 downto 0);
    mem_cas_n             : out   std_logic;
    mem_cke               : out   std_logic_vector (MEM_CKE_WIDTH-1 downto 0);
    mem_clk               : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_clk_n             : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_cs_n              : out   std_logic_vector (MEM_CS_WIDTH-1 downto 0);
    mem_dm                : out   std_logic_vector (MEM_DM_WIDTH-1 downto 0);
    mem_dq                : inout std_logic_vector (MEM_DQ_WIDTH-1 downto 0);
    mem_dqs               : inout std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
--    mem_dqsn              : inout std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
    mem_odt               : out   std_logic_vector (MEM_ODT_WIDTH-1 downto 0);
    mem_ras_n             : out   std_logic;
    mem_we_n              : out   std_logic;
--    local_ready           : out   std_logic;
--    oct_ctl_rs_value      : in    std_logic_vector (OCT_CTL_WIDTH-1 downto 0);
--    oct_ctl_rt_value      : in    std_logic_vector (OCT_CTL_WIDTH-1 downto 0);
    pll_ref_clk           : in    std_logic;
    reset_phy_clk_n       : out   std_logic
  );
end component;

component ddr2_ram_write_adapter is
  port (
    clk               : in  std_logic;  -- clock
    rst_n             : in  std_logic;  -- reset (active low)
    dram_write_in     : in  WRITE_PORT_OUT;  -- things coming from dram controller
    dram_write_out    : out WRITE_PORT_IN;  -- things going to dram controller
    adapted_write_out : out WRITE_PORT_OUT;  -- things going to DRAM user
    adapted_write_in  : in  WRITE_PORT_IN_ADAPTER  -- things from DRAM user
    );
end component;

component ddr2_read_adapter is
  port (
    clk               : in  std_logic;  -- clock
    rst_n             : in  std_logic;  -- reset (active low)
    dram_read_in      : in  READ_PORT_OUT;  -- things coming from dram controller
    dram_read_out     : out READ_PORT_IN;  -- things going to dram controller
    adapted_read_out  : out READ_PORT_OUT_ADAPTER;  -- things going to DRAM user
    adapted_read_in   : in  READ_PORT_IN_ADAPTER  -- things from DRAM user
    );
end component;

  signal read_out          : READ_PORTS_OUT;
  signal read_in           : READ_PORTS_IN;
  
  signal bidir_out         : BIDIR_PORTS_OUT;
  signal bidir_in          : BIDIR_PORTS_IN;
  
  signal write_out         : WRITE_PORTS_OUT;
  signal write_in          : WRITE_PORTS_IN;
  
 
begin
	----------------
	-- COMPONENTS --
	----------------

    U_TOP: ddr2_toplevel port map( 
        rst_n => rst_n,
		mem_cke => mem_cke,
		--mem_dqsn => mem_dqsn,
		mem_addr => mem_addr,
		mem_dm => mem_dm,
		mem_dq => mem_dq,
		mem_ras_n => mem_ras_n,
		mem_cs_n => mem_cs_n,
		mem_ba => mem_ba,
		mem_odt => mem_odt,
		mem_cas_n => mem_cas_n,
		mem_we_n => mem_we_n,
		mem_dqs => mem_dqs,
		mem_clk_n => mem_clk_n,
		mem_clk => mem_clk,
--		aux_scan_clk_reset_n => aux_scan_clk_reset_n,
		aux_full_rate_clk => aux_full_rate_clk,
--		aux_scan_clk => aux_scan_clk,
		aux_half_rate_clk => aux_half_rate_clk,	 
		pll_ref_clk => pll_ref_clk,
		dqs_delay_ctrl_export => dqs_delay_ctrl_export,
		dll_reference_clk => dll_reference_clk,
--		oct_ctl_rt_value => oct_ctl_rt_value,
--		oct_ctl_rs_value => oct_ctl_rs_value,
		reset_phy_clk_n => reset_phy_clk_n,
		
		write_in => write_in,
		write_out => write_out,
		write_clk => write_clk, 
		read_in => read_in,
		read_out => read_out,
		read_clk => read_clk, 
		bidir_out => bidir_out,
		bidir_in => bidir_in		
	);
	
 
	READ_GEN: for i in PORTS_READ-1 downto 0 generate
		ADAPT : ddr2_read_adapter port map(
		clk => read_clk(i),
		rst_n => rst_n,
		dram_read_in => read_out(i),
		dram_read_out => read_in(i),
		adapted_read_in => adapted_read_in(i),
		adapted_read_out => adapted_read_out(i)
		);
	end generate;
  

	WRITE_GEN: for i in PORTS_WRITE-1 downto 0 generate
		ADAPT : ddr2_ram_write_adapter port map (
		clk => write_clk(i),
		rst_n => rst_n,
		dram_write_in => write_out(i),
		dram_write_out => write_in(i),
		adapted_write_in => adapted_write_in(i),
		adapted_write_out => adapted_write_out(i)
		);
	end generate;
	


bidir_in(0).enable <= '0';
bidir_in(0).write_req <= '0';
bidir_in(0).address <= (others => '0');
bidir_in(0).data_in <= (others => '0');

end struct_arch;
