library IEEE;
use IEEE.std_logic_1164.all; 


package fifo_dc_pkg is
component fifo_dc is
  generic 
  (
    BIT_WIDTH        : integer := 8;
    ADDR_WIDTH       : integer := 10;
    WARN_LENGTH      : integer := 10
  );
  port
  (   
    clk_input        : in   std_logic;
    clk_output       : in   std_logic;
    xrst             : in   std_logic;
    
    datain           : in   std_logic_vector (BIT_WIDTH-1 downto 0);
    dataout          : out  std_logic_vector (BIT_WIDTH-1 downto 0);
    enable           : in   std_logic;
    full             : out  std_logic;
    almost_full      : out  std_logic;
    ack              : in   std_logic;
    empty            : out  std_logic;
    almost_empty     : out  std_logic  
  );
end component;
end;