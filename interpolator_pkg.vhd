library ieee;
use ieee.std_logic_1164.all;
use work.motion_compensation_dec_pkg.DELTA_BIT_DEPTH;

package interpolator_pkg is
  
  constant FPGA                   : boolean := FALSE;
  
  constant CODING_UNIT_SIZE       : natural := 64;
  constant FILTER_BLOCK_SIZE      : natural := 8;
  constant INPUT_BLOCK_SIZE       : natural := 4;
  
  type PE_INPUT_DATA_TYPE         is array(FILTER_BLOCK_SIZE-1 downto 0) of std_logic_vector(15+DELTA_BIT_DEPTH downto 0);
  type INPUT_OUTPUT_ROW_TYPE      is array(INPUT_BLOCK_SIZE-1 downto 0) of std_logic_vector(7+DELTA_BIT_DEPTH downto 0);
  type OUTPUT_ROW_TYPE            is array(INPUT_BLOCK_SIZE-1 downto 0) of std_logic_vector(21+DELTA_BIT_DEPTH downto 0);
  
end package;

package body interpolator_pkg is
  
end package body;