library altera;
use altera.altera_europa_support_lib.all;
library altera_mf;
use altera_mf.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ddr2_pkg.all;
use work.ddr2_read;
use work.ddr2_write;
use work.ddr2_bidir;
use work.ddr2_arbiter; 
use work.motion_compensation_dec_pkg.all;

entity ddr2_toplevel is
  port(
    --ddr2 memory controller
    clk_port              : out   std_logic;
    ready                 : out   std_logic;
    rst_n                 : in    std_logic;
        
    --read ports
    read_clk              : in    std_logic_vector(PORTS_READ-1 downto 0);
    read_in               : in    READ_PORTS_IN;
    read_out              : out   READ_PORTS_OUT;

    --bidir ports
    bidir_in              : in    BIDIR_PORTS_IN;
    bidir_out             : out   BIDIR_PORTS_OUT;

    --write ports
    write_clk             : in    std_logic_vector(PORTS_WRITE-1 downto 0);
    write_in              : in    WRITE_PORTS_IN;
    write_out             : out   WRITE_PORTS_OUT;

    --ddr2 memory controller
    aux_full_rate_clk     : out   std_logic;
    aux_half_rate_clk     : out   std_logic;
    aux_scan_clk          : out   std_logic;
    aux_scan_clk_reset_n  : out   std_logic;
    dll_reference_clk     : out   std_logic;
    dqs_delay_ctrl_export : out   std_logic_vector (DQS_DEL_EXPORT_WIDTH-1 downto 0);
    mem_addr              : out   std_logic_vector (MEM_ADD_WIDTH-1 downto 0);
    mem_ba                : out   std_logic_vector (MEM_BA_WIDTH-1 downto 0);
    mem_cas_n             : out   std_logic;
    mem_cke               : out   std_logic_vector (MEM_CKE_WIDTH-1 downto 0);
    mem_clk               : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_clk_n             : inout std_logic_vector (MEM_CLK_WIDTH-1 downto 0);
    mem_cs_n              : out   std_logic_vector (MEM_CS_WIDTH-1 downto 0);
    mem_dm                : out   std_logic_vector (MEM_DM_WIDTH-1 downto 0);
    mem_dq                : inout std_logic_vector (MEM_DQ_WIDTH-1 downto 0);
    mem_dqs               : inout std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
    mem_dqsn              : inout std_logic_vector (MEM_DQS_WIDTH-1 downto 0);
    mem_odt               : out   std_logic_vector (MEM_ODT_WIDTH-1 downto 0);
    mem_ras_n             : out   std_logic;
    mem_we_n              : out   std_logic;
    pll_ref_clk           : in    std_logic;
    reset_phy_clk_n       : out   std_logic
  );
end entity;

architecture struct of ddr2_toplevel is
	
  component ddr2_v is
	  port (
		signal mem_cke               : OUT STD_LOGIC_VECTOR (MEM_CKE_WIDTH-1 DOWNTO 0);
		--signal mem_dqsn              : INOUT STD_LOGIC_VECTOR (MEM_DQS_WIDTH-1 DOWNTO 0);
		signal mem_addr              : OUT STD_LOGIC_VECTOR (MEM_ADD_WIDTH-1 DOWNTO 0);
		signal aux_scan_clk_reset_n  : OUT STD_LOGIC;
		signal mem_dm                : OUT STD_LOGIC_VECTOR (MEM_DM_WIDTH-1 DOWNTO 0);
		signal aux_full_rate_clk     : OUT STD_LOGIC;
		signal local_init_done       : OUT STD_LOGIC;
		signal mem_dq                : INOUT STD_LOGIC_VECTOR (MEM_DQ_WIDTH-1 DOWNTO 0);
		signal mem_ras_n             : OUT STD_LOGIC;
		signal mem_cs_n              : OUT STD_LOGIC_VECTOR (MEM_CS_WIDTH-1 DOWNTO 0);
		signal phy_clk               : OUT STD_LOGIC;
		signal mem_ba                : OUT STD_LOGIC_VECTOR (MEM_BA_WIDTH-1 DOWNTO 0);
		signal dqs_delay_ctrl_export : OUT STD_LOGIC_VECTOR (DQS_DEL_EXPORT_WIDTH-1 DOWNTO 0);
		signal aux_scan_clk          : OUT STD_LOGIC;
		signal aux_half_rate_clk     : OUT STD_LOGIC;
		signal local_rdata           : OUT STD_LOGIC_VECTOR (ALTERA_DATA_WIDTH-1 DOWNTO 0);
		signal local_rdata_valid     : OUT STD_LOGIC;
		signal mem_we_n              : OUT STD_LOGIC;
		signal reset_phy_clk_n       : OUT STD_LOGIC;
		signal local_ready           : OUT STD_LOGIC;
		signal mem_odt               : OUT STD_LOGIC_VECTOR (MEM_ODT_WIDTH-1 DOWNTO 0);
		signal mem_cas_n             : OUT STD_LOGIC;
		signal dll_reference_clk     : OUT STD_LOGIC;
		signal mem_clk               : INOUT STD_LOGIC_VECTOR (MEM_CLK_WIDTH-1 DOWNTO 0);
		signal local_refresh_ack     : OUT STD_LOGIC;
		signal local_wdata_req       : OUT STD_LOGIC;
		signal reset_request_n       : OUT STD_LOGIC;
		signal mem_dqs               : INOUT STD_LOGIC_VECTOR (MEM_DQS_WIDTH-1 DOWNTO 0);
		signal mem_clk_n             : INOUT STD_LOGIC_VECTOR (MEM_CLK_WIDTH-1 DOWNTO 0);
		signal local_size            : IN STD_LOGIC; -- 1 bit for HALF_RATE, 2 bits for FULL_RATE !!!!!!!!
		signal oct_ctl_rt_value      : IN STD_LOGIC_VECTOR (13 DOWNTO 0);
		signal local_wdata           : IN STD_LOGIC_VECTOR (ALTERA_DATA_WIDTH-1 DOWNTO 0);
		signal soft_reset_n          : IN STD_LOGIC;
		signal global_reset_n        : IN STD_LOGIC;
		signal local_write_req       : IN STD_LOGIC;
		signal local_address         : IN STD_LOGIC_VECTOR (ALTERA_ADDRESS_WIDTH-1 DOWNTO 0);
		signal oct_ctl_rs_value      : IN STD_LOGIC_VECTOR (13 DOWNTO 0);
		signal pll_ref_clk           : IN STD_LOGIC;
		signal local_be              : IN STD_LOGIC_VECTOR (ALTERA_BYTE_ENABLE-1 DOWNTO 0);
		signal local_read_req        : IN STD_LOGIC
	  );
  end component;

  component altera_controller_model
    generic(
      CLOCK_PERIOD               : time    := 5 ns;
      QUEUE_DEPTH                : integer := 6;     -- how many commands can be accepted
      EXTRA_WRITE_QUEUE_DEPTH    : integer := 4;          -- simulation indicates that queue for write seems to be emptied faster - which is modeled by adding smth to its depth
      INIT_DELAY                 : integer := 1000;  -- how many clock cycles should be waited before controller is ready to work
      INTER_BURST_WRITE_DELAY    : integer := 4;    -- delay between each DDR write burst
      INTER_BURST_READ_DELAY     : integer := 4;     -- delay between each DDR read burst
      INTER_READ_LOCAL_DELAY     : integer := 3;     -- delay between read local ready
      INTER_WRITE_LOCAL_DELAY    : integer := 1;     -- delay between local write ready and local wdata_req
      WRITE_TO_READ_SWITCH_DELAY : integer := 4;    -- delay when switching from write to read command
      READ_INIT_DELAY            : integer := 4;    -- delay between read request and first data on memory output
      MEM_BURST_WIDTH            : integer := 4      -- amount of data in one burst
      );
    port (
      signal mem_cke               : OUT   STD_LOGIC_VECTOR (MEM_CKE_WIDTH-1 DOWNTO 0);
      signal mem_addr              : OUT   STD_LOGIC_VECTOR (MEM_ADD_WIDTH-1 DOWNTO 0);
      signal aux_scan_clk_reset_n  : OUT   STD_LOGIC;
      signal mem_dm                : OUT   STD_LOGIC_VECTOR (MEM_DM_WIDTH-1 DOWNTO 0);
      signal aux_full_rate_clk     : OUT   STD_LOGIC;
      signal local_init_done       : OUT   STD_LOGIC;
      signal mem_dq                : INOUT STD_LOGIC_VECTOR (MEM_DQ_WIDTH-1 DOWNTO 0);
      signal mem_ras_n             : OUT   STD_LOGIC;
      signal mem_cs_n              : OUT   STD_LOGIC_VECTOR (MEM_CS_WIDTH-1 DOWNTO 0);
      signal phy_clk               : OUT   STD_LOGIC;
      signal mem_ba                : OUT   STD_LOGIC_VECTOR (MEM_BA_WIDTH-1 DOWNTO 0);
      signal dqs_delay_ctrl_export : OUT   STD_LOGIC_VECTOR (DQS_DEL_EXPORT_WIDTH-1 DOWNTO 0);
      signal aux_scan_clk          : OUT   STD_LOGIC;
      signal aux_half_rate_clk     : OUT   STD_LOGIC;
      signal local_rdata           : OUT   STD_LOGIC_VECTOR (ALTERA_DATA_WIDTH-1 DOWNTO 0);
      signal local_rdata_valid     : OUT   STD_LOGIC;
      signal mem_we_n              : OUT   STD_LOGIC;
      signal reset_phy_clk_n       : OUT   STD_LOGIC;
      signal local_ready           : OUT   STD_LOGIC;
      signal mem_odt               : OUT   STD_LOGIC_VECTOR (MEM_ODT_WIDTH-1 DOWNTO 0);
      signal mem_cas_n             : OUT   STD_LOGIC;
      signal dll_reference_clk     : OUT   STD_LOGIC;
      signal mem_clk               : INOUT STD_LOGIC_VECTOR (MEM_CLK_WIDTH-1 DOWNTO 0);
      signal local_refresh_ack     : OUT   STD_LOGIC;
      signal local_wdata_req       : OUT   STD_LOGIC;
      signal reset_request_n       : OUT   STD_LOGIC;
      signal mem_dqs               : INOUT STD_LOGIC_VECTOR (MEM_DQS_WIDTH-1 DOWNTO 0);
      signal mem_clk_n             : INOUT STD_LOGIC_VECTOR (MEM_CLK_WIDTH-1 DOWNTO 0);
      signal local_wdata           : IN    STD_LOGIC_VECTOR (ALTERA_DATA_WIDTH-1 DOWNTO 0);
      signal soft_reset_n          : IN    STD_LOGIC;
      signal global_reset_n        : IN    STD_LOGIC;
      signal local_write_req       : IN    STD_LOGIC;
      signal local_address         : IN    STD_LOGIC_VECTOR (ALTERA_ADDRESS_WIDTH-1 DOWNTO 0);
      signal pll_ref_clk           : IN    STD_LOGIC;
      signal local_be              : IN    STD_LOGIC_VECTOR (ALTERA_BYTE_ENABLE-1 DOWNTO 0);
      signal local_read_req        : IN    STD_LOGIC);
  end component;

  component ddr2_arbiter is
	port (
		clk                   : in std_logic;
		rst_n                 : in std_logic;
		mem_ready             : out std_logic;
		devices_i             : in SCHEDULER_DEVICES_IN;
		devices_o             : out SCHEDULER_DEVICES_OUT;
		devices_mem_i         : out MULTIPLEXER_ALTERA_DDR2_OUT;
		devices_mem_o         : in  MULTIPLEXER_ALTERA_DDR2_IN;
		local_init_done       : in  std_logic;    
		local_rdata           : in  std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
		local_rdata_valid     : in  std_logic;
		local_ready           : in  std_logic;
		local_refresh_ack     : in  std_logic;
		local_wdata_req       : in  std_logic;
		local_addr            : out std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
		local_be              : out std_logic_vector(ALTERA_BYTE_ENABLE-1 downto 0);
		local_size            : out std_logic_vector(ALTERA_BURST_WIDTH-1 downto 0);
		local_wdata           : out std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
		local_read_req        : out std_logic;
		local_write_req       : out std_logic
	);
	end component;

	component ddr2_bidir is
	port (
		clk_altera            : in std_logic;
		rst_n                 : in std_logic;
		port_i                : in BIDIR_PORT_IN;
		port_o                : out BIDIR_PORT_OUT;
		sch_i                 : in SCHEDULER_OUT;
		sch_o                 : out SCHEDULER_IN;
		ddr2_i                : in ALTERA_DDR2_OUT;
		ddr2_o                : out ALTERA_DDR2_IN
	);
	end component;
	
	component ddr2_read is
	generic (
		FIFO_LENGTH          : natural := 128;
		ACCESS_CYCLE_LENGTH  : natural := 64
	);  port (
		clk_altera            : in std_logic;
		clk_port              : in std_logic;
		rst_n                 : in std_logic;
 		local_init_done       : in  std_logic;    
		enc_i                 : in READ_PORT_IN;
		enc_o                 : out READ_PORT_OUT;
		sch_i                 : in SCHEDULER_OUT;
		sch_o                 : out SCHEDULER_IN;
		ddr2_i                : in ALTERA_DDR2_OUT;
		ddr2_o                : out ALTERA_DDR2_IN
	  );
	end component;
	
	component ddr2_write is
	generic (
		FIFO_LENGTH          : natural := 128;
		ACCESS_CYCLE_LENGTH  : natural := 64;
		WARN_LENGTH          : natural := 16 
	);  port (
		clk_altera            : in std_logic;
		clk_port              : in std_logic;
		rst_n                 : in std_logic;
		enc_i                 : in WRITE_PORT_IN;
		enc_o                 : out WRITE_PORT_OUT;
		sch_i                 : in SCHEDULER_OUT;
		sch_o                 : out SCHEDULER_IN;
		ddr2_i                : in ALTERA_DDR2_OUT;
		ddr2_o                : out ALTERA_DDR2_IN
	);
	end component;

  signal clk              : std_logic;	
	
  signal sch_dev_in       : SCHEDULER_DEVICES_IN;
  signal sch_dev_out      : SCHEDULER_DEVICES_OUT;
  signal alt_mem_in       : MULTIPLEXER_ALTERA_DDR2_IN;
  signal alt_mem_out      : MULTIPLEXER_ALTERA_DDR2_OUT;
  
  signal read_enc_o       : READ_PORTS_OUT;
  signal read_enc_i       : READ_PORTS_IN;
  signal read_sch_o       : READ_PORTS_SCH_OUT;
  signal read_sch_i       : READ_PORTS_SCH_IN;
  signal read_ddr2_o      : READ_PORTS_MEM_OUT;
  signal read_ddr2_i      : READ_PORTS_MEM_IN;
  
  signal bidir_port_o     : BIDIR_PORTS_OUT;
  signal bidir_port_i     : BIDIR_PORTS_IN;
  signal bidir_sch_o      : BIDIR_PORTS_SCH_OUT;
  signal bidir_sch_i      : BIDIR_PORTS_SCH_IN;
  signal bidir_ddr2_o     : BIDIR_PORTS_MEM_OUT;
  signal bidir_ddr2_i     : BIDIR_PORTS_MEM_IN;
  
  signal write_enc_o      : WRITE_PORTS_OUT;
  signal write_enc_i      : WRITE_PORTS_IN;
  signal write_sch_o      : WRITE_PORTS_SCH_OUT;
  signal write_sch_i      : WRITE_PORTS_SCH_IN;
  signal write_ddr2_o     : WRITE_PORTS_MEM_OUT;
  signal write_ddr2_i     : WRITE_PORTS_MEM_IN;
  
  signal memory_init_done   : std_logic;    
  signal memory_rdata       : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
  signal memory_rdata_valid : std_logic;
  signal memory_ready       : std_logic;
  signal memory_refresh_ack : std_logic;
  signal memory_wdata_req   : std_logic;
  signal memory_addr        : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);
  signal memory_be          : std_logic_vector(ALTERA_BYTE_ENABLE-1 downto 0);
  signal memory_size        : std_logic_vector(ALTERA_BURST_WIDTH-1 downto 0);
  signal memory_wdata       : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
  signal memory_read_req    : std_logic;
  signal memory_write_req   : std_logic;
  
  signal tie_high           : std_logic;
  signal mem_ready          : std_logic;
  
  signal oct_ctl_rs_value   : std_logic_vector (13 downto 0);
  signal oct_ctl_rt_value   : std_logic_vector (13 downto 0);
    
  
begin
	
	--clock of internal components
	clk_port <= clk;
	ready <= mem_ready;
	tie_high <= std_logic'('1');
	oct_ctl_rs_value <= (others => '0');
	oct_ctl_rt_value <= (others => '0');
			
	----------------
	-- COMPONENTS --
	----------------
	
  GEN_ALTERA_CONTR_FULL: if not USE_ALTERA_SIMULATION_MODEL generate
	ALTERA_CONTROLLER: ddr2_v port map(
		mem_cke => mem_cke,
		--mem_dqsn => mem_dqsn,
		mem_addr => mem_addr,
		aux_scan_clk_reset_n => aux_scan_clk_reset_n,
		mem_dm => mem_dm,
		aux_full_rate_clk => aux_full_rate_clk,
		local_init_done => memory_init_done,
		mem_dq => mem_dq,
		mem_ras_n => mem_ras_n,
		mem_cs_n => mem_cs_n,
		phy_clk => clk,
		mem_ba => mem_ba,
		dqs_delay_ctrl_export => dqs_delay_ctrl_export,
		aux_scan_clk => aux_scan_clk,
		aux_half_rate_clk => aux_half_rate_clk,
		local_rdata => memory_rdata,
		local_rdata_valid => memory_rdata_valid,
		mem_we_n => mem_we_n,
		reset_phy_clk_n => reset_phy_clk_n,
		local_ready => memory_ready,
		mem_odt => mem_odt,
		mem_cas_n => mem_cas_n,
		dll_reference_clk => dll_reference_clk,
		mem_clk => mem_clk,
		local_refresh_ack => memory_refresh_ack,
		local_wdata_req => memory_wdata_req,
		reset_request_n => open,
		mem_dqs => mem_dqs,
		mem_clk_n => mem_clk_n,
		local_size => memory_size(0),
		oct_ctl_rt_value => oct_ctl_rt_value,
		local_wdata => memory_wdata,
		soft_reset_n => tie_high,
		global_reset_n => rst_n,
		local_write_req => memory_write_req,
		local_address => memory_addr,
		oct_ctl_rs_value => oct_ctl_rs_value,
		pll_ref_clk => pll_ref_clk,
		local_be => memory_be,
		local_read_req => memory_read_req
	);	
   end generate;
	
   GEN_ALTERA_CONTR_SIM_MODEL: if USE_ALTERA_SIMULATION_MODEL generate
        ALTERA_CONTROLLER_1: altera_controller_model port map(
		mem_cke => mem_cke,
		--mem_dqsn => mem_dqsn,
		mem_addr => mem_addr,
		aux_scan_clk_reset_n => aux_scan_clk_reset_n,
		mem_dm => mem_dm,
		aux_full_rate_clk => aux_full_rate_clk,
		local_init_done => memory_init_done,
		mem_dq => mem_dq,
		mem_ras_n => mem_ras_n,
		mem_cs_n => mem_cs_n,
		phy_clk => clk,
		mem_ba => mem_ba,
		dqs_delay_ctrl_export => dqs_delay_ctrl_export,
		aux_scan_clk => aux_scan_clk,
		aux_half_rate_clk => aux_half_rate_clk,
		local_rdata => memory_rdata,
		local_rdata_valid => memory_rdata_valid,
		mem_we_n => mem_we_n,
		reset_phy_clk_n => reset_phy_clk_n,
		local_ready => memory_ready,
		mem_odt => mem_odt,
		mem_cas_n => mem_cas_n,
		dll_reference_clk => dll_reference_clk,
		mem_clk => mem_clk,
		local_refresh_ack => memory_refresh_ack,
		local_wdata_req => memory_wdata_req,
		reset_request_n => open,
		mem_dqs => mem_dqs,
		mem_clk_n => mem_clk_n,
		local_wdata => memory_wdata,
		soft_reset_n => tie_high,
		global_reset_n => rst_n,
		local_write_req => memory_write_req,
		local_address => memory_addr,
		pll_ref_clk => pll_ref_clk,
		local_be => memory_be,
		local_read_req => memory_read_req
	);
   end generate;  
		
	SCHEDULER: ddr2_arbiter port map(
		clk => clk, 
		rst_n => rst_n,
		mem_ready => mem_ready,
		devices_i => sch_dev_in, 
		devices_o => sch_dev_out, 
		devices_mem_i => alt_mem_out, 
		devices_mem_o => alt_mem_in, 
		local_init_done => memory_init_done, 
		local_rdata => memory_rdata, 
		local_rdata_valid => memory_rdata_valid, 
		local_ready => memory_ready, 
		local_refresh_ack => memory_refresh_ack,
		local_wdata_req => memory_wdata_req, 
		local_addr => memory_addr, 
		local_be => memory_be, 
		local_size => memory_size, 
		local_wdata => memory_wdata, 
		local_read_req => memory_read_req, 
		local_write_req => memory_write_req
	);
  
	READ_GEN: for i in PORTS_READ-1 downto 0 generate
	begin
		sch_dev_in(i) <= read_sch_o(i);
		read_sch_i(i) <= sch_dev_out(i);
		alt_mem_in(i) <= read_ddr2_o(i);
		read_ddr2_i(i) <= alt_mem_out(i);
		read_out(i) <= read_enc_o(i);
		read_enc_i(i) <= read_in(i);
		PORT_READ : ddr2_read 
		generic map (
			FIFO_LENGTH => FIFO_LENGTH_READ(i),
			ACCESS_CYCLE_LENGTH => ACCESS_CYCLE_LENGTH_READ(i))
		port map(
			clk_altera => clk,
			clk_port => read_clk(i),
			rst_n => rst_n,
			local_init_done => memory_init_done,
			enc_i => read_enc_i(i),
			enc_o => read_enc_o(i),
			sch_i => read_sch_i(i),
			sch_o => read_sch_o(i),
			ddr2_i => read_ddr2_i(i),
			ddr2_o => read_ddr2_o(i)
		);
	end generate;
  
  	BIDIR_GEN: for i in PORTS_BIDIR-1 downto 0 generate
		sch_dev_in(i+PORTS_READ) <= bidir_sch_o(i);
		bidir_sch_i(i) <= sch_dev_out(i+PORTS_READ);
		alt_mem_in(i+PORTS_READ) <= bidir_ddr2_o(i);
		bidir_ddr2_i(i) <= alt_mem_out(i+PORTS_READ);
		bidir_out(i) <= bidir_port_o(i);
		bidir_port_i(i) <= bidir_in(i);
		PORT_BIDIR : ddr2_bidir port map(
			clk_altera => clk,
			rst_n => rst_n,
			port_i => bidir_port_i(i),
			port_o => bidir_port_o(i),
			sch_i => bidir_sch_i(i),
			sch_o => bidir_sch_o(i),
			ddr2_i => bidir_ddr2_i(i),
			ddr2_o => bidir_ddr2_o(i)
		);
	end generate;


	WRITE_GEN: for i in PORTS_WRITE-1 downto 0 generate
		sch_dev_in(i+PORTS_READ+PORTS_BIDIR) <= write_sch_o(i);
		write_sch_i(i) <= sch_dev_out(i+PORTS_READ+PORTS_BIDIR);
		alt_mem_in(i+PORTS_READ+PORTS_BIDIR) <= write_ddr2_o(i);
		write_ddr2_i(i) <= alt_mem_out(i+PORTS_READ+PORTS_BIDIR);
		write_out(i) <= write_enc_o(i);
		write_enc_i(i) <= write_in(i);
		PORT_WRITE : ddr2_write 
		generic map (
			FIFO_LENGTH => FIFO_LENGTH_WRITE(i),
			ACCESS_CYCLE_LENGTH => ACCESS_CYCLE_LENGTH_WRITE(i),
			WARN_LENGTH => 2*log2_ceil(FIFO_LENGTH_WRITE(i)))
		port map (
			clk_altera => clk,
			clk_port => write_clk(i),
			rst_n => rst_n,
			enc_i => write_enc_i(i),
			enc_o => write_enc_o(i),
			sch_i => write_sch_i(i),
			sch_o => write_sch_o(i),
			ddr2_i => write_ddr2_i(i),
			ddr2_o => write_ddr2_o(i)
		);
	end generate;
	
end struct;