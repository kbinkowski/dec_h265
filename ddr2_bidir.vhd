library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ddr2_pkg.all;

entity ddr2_bidir is
  port (
    clk_altera      : in std_logic;
    rst_n           : in std_logic;
    
    port_i          : in BIDIR_PORT_IN;
    port_o          : out BIDIR_PORT_OUT;
    
    sch_i           : in SCHEDULER_OUT;
    sch_o           : out SCHEDULER_IN;
    
    ddr2_i          : in ALTERA_DDR2_OUT;
    ddr2_o          : out ALTERA_DDR2_IN
  );
end entity;

architecture rtl of ddr2_bidir is
  
  type STATE_TYPE_ALTERA is (INIT, READY, QUEUED, BUSY, FINISH);
  type REQUEST_FLAG_TYPE is (F_READ, F_WRITE, WDATA, RDATA);

  signal flag_r          : REQUEST_FLAG_TYPE;
  signal flag_next       : REQUEST_FLAG_TYPE;
  signal scheduler_r     : SCHEDULER_IN;
  signal scheduler_next  : SCHEDULER_IN;
  signal port_r          : BIDIR_PORT_OUT;
  signal port_next       : BIDIR_PORT_OUT;
  signal altera_r        : ALTERA_DDR2_IN;
  signal altera_next     : ALTERA_DDR2_IN;
  signal state_r         : STATE_TYPE_ALTERA;
  signal state_next      : STATE_TYPE_ALTERA;
  
begin

  port_o <= port_r;
  sch_o <= scheduler_r;
  ddr2_o <= altera_r;

  PROC_CLK_RST_ALTERA: process(clk_altera, rst_n)
  begin
    if rst_n = '0' then
		state_r <= INIT;
		port_r.ready <= '0';
    elsif rising_edge(clk_altera) then
		state_r <= state_next;
		scheduler_r <= scheduler_next;
		port_r <= port_next;
		altera_r <= altera_next;
		flag_r <= flag_next;
    end if;
  end process;
  
  PROC_ALTERA: process(state_r, port_i, port_r, scheduler_r, altera_r, flag_r, sch_i, ddr2_i)
    variable state_v        : STATE_TYPE_ALTERA;
    variable scheduler_v    : SCHEDULER_IN;
    variable port_v         : BIDIR_PORT_OUT;
    variable altera_v       : ALTERA_DDR2_IN;
    variable flag_v         : REQUEST_FLAG_TYPE;
    
  begin
    state_v := state_r;
    scheduler_v := scheduler_r;
    port_v := port_r;
    altera_v := altera_r;
    flag_v := flag_r;
    
    case state_v is
      when INIT =>
        state_v := READY;
        scheduler_v.request_hp := '0';
        scheduler_v.request_np := '0';
        scheduler_v.request_lp := '0';
        scheduler_v.done := '0';
        scheduler_v.device_ready := '0';
        port_v.ready := '0';
        port_v.data_out := (others => '0');
        altera_v.addr := (others => '0');
        altera_v.be := (others => '1');
        altera_v.size := (others => '0');
        altera_v.wdata := (others => '0');
        altera_v.read_req := '0';
        altera_v.write_req := '0';
        
      when READY =>
        scheduler_v.device_ready := '1';
        scheduler_v.request_hp := '0';
        scheduler_v.request_np := '0';
        scheduler_v.request_lp := '0';
        scheduler_v.done := '0';
		port_v.ready := '1';
        if port_i.enable = '1' then
		  port_v.ready := '0';
		  state_v := QUEUED;
          if port_i.write_req = '1' then
            altera_v.wdata := port_i.data_in;
            flag_v := F_WRITE;
            altera_v.addr := port_i.address;
          else
			altera_v.wdata := (others => '0');
            flag_v := F_READ;
            altera_v.addr := port_i.address;
          end if;
        end if;
        
      when QUEUED =>
		port_v.ready := '0';
        scheduler_v.device_ready := '1';
        scheduler_v.request_hp := '1';
        scheduler_v.request_np := '0';
        scheduler_v.request_lp := '0';
        if sch_i.enable = '1' and ddr2_i.ready = '1' then
          state_v := BUSY;
        end if;
        
      when BUSY =>
		    port_v.ready := '0';
        scheduler_v.request_hp := '1';
        scheduler_v.request_np := '0';
        scheduler_v.request_lp := '0';
        --just a single request, unset write or read lag
        if altera_v.write_req = '1' then
          altera_v.write_req := '0';
          flag_v := WDATA;
        elsif altera_v.read_req = '1' then
          altera_v.read_req := '0';
          flag_v := RDATA;
        end if;
        
        if ddr2_i.ready = '1' then
          --send request
          if flag_v = F_WRITE then
            --write request
            altera_v.write_req := '1';
            altera_v.size := conv_std_logic_vector(1, ALTERA_BURST_WIDTH);
          elsif flag_v = F_READ then
            --read request
            altera_v.read_req := '1';
            altera_v.size := conv_std_logic_vector(1, ALTERA_BURST_WIDTH);
          end if;
        end if;
        
        if flag_v = WDATA and ddr2_i.wdata_req = '1' then
          --write cycle about to finish
          port_v.ready := '0';
          state_v := FINISH;
        end if;
        
        if flag_v = RDATA and ddr2_i.rdata_valid = '1' then
          --read cycle finished
          port_v.data_out := ddr2_i.rdata;
          port_v.ready := '1';
          state_v := READY;
          scheduler_v.done := '1';
        end if;
		
	  when FINISH =>
	    --write cycle finished
		  scheduler_v.done := '1';
		  state_v := READY;
    end case;
    
    state_next <= state_v;
    scheduler_next <= scheduler_v;
    port_next <= port_v;
    altera_next <= altera_v;
    flag_next <= flag_v;
  end process;
end rtl;