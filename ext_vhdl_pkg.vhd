library ieee; 
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

package ext_vhdl_pkg is
  
  function log2(n : integer) return integer;
  
  function get_std_logic(number : integer) return std_logic;
  
  function conv_char(bit_val : std_logic) return character;
  
  function conv_string(bit_val : std_logic) return string;
  function conv_string(vector : std_logic_vector) return string;
  
  function add_vector_and_bit(vector : std_logic_vector; bit_val : std_logic) return std_logic_vector;
  
  function mult_factor_by_weight(factor : std_logic_vector; weight : std_logic_vector) return std_logic_vector;
  function mult_signed_factor_by_weight(factor : std_logic_vector; weight : std_logic_vector) return std_logic_vector;
  
  function truncate_vector(vector : std_logic_vector; req_length : integer) return std_logic_vector;
  
  function extend_signed_vector(vector : std_logic_vector; req_length : integer) return std_logic_vector;
  function extend_unsigned_vector(vector : std_logic_vector; req_length : integer) return std_logic_vector;
  
  function shift_left_vector(vector : std_logic_vector; shift : natural := 1) return std_logic_vector;
  function shift_right_unsigned_vector(vector : std_logic_vector; shift : natural := 1) return std_logic_vector;
  function shift_right_signed_vector(vector : std_logic_vector; shift : natural := 1) return std_logic_vector;
  
  function find_lead_one_pos(vector : std_logic_vector) return integer;
  function find_lead_zero_pos(vector : std_logic_vector) return integer;
  
  function find_last_one_pos(vector : std_logic_vector) return integer;
  function find_last_zero_pos(vector : std_logic_vector) return integer;
  
end package ext_vhdl_pkg;

package body ext_vhdl_pkg is
  
  function log2(n : integer) return integer is
  begin
    if (n <= 2) then
      return 1;
    else
      if (n mod 2 = 0) then
        return 1 + log2(n/2);
      else
        return 1 + log2((n + 1)/2);
      end if;
    end if;
  end function log2;
  
  function get_std_logic(number : integer) return std_logic is
    variable bit_v                                                   : std_logic;
  begin
    bit_v := '0';
    if (number /= 0) then
      bit_v := '1';
    end if;
    return bit_v;
  end function get_std_logic;
  
  function conv_char(bit_val : std_logic) return character is
    variable char_v                                                  : character;
  begin
    if (bit_val = '1') then
      char_v := '1';
    else
      char_v := '0';
    end if;
    return char_v;
  end function conv_char;
  
  function conv_string(bit_val : std_logic) return string is
    variable string_buffer_v                                         : string(1 to 1);
  begin
    string_buffer_v(1) := conv_char(bit_val);
    return string_buffer_v;
  end function conv_string;
  
  function conv_string(vector : std_logic_vector) return string is
    variable string_buffer_v                                         : string(vector'low + 1 to vector'high + 3);
  begin
    string_buffer_v(string_buffer_v'low) := '[';
    string_buffer_v(string_buffer_v'high) := ']';
    
    for i in vector'high downto vector'low loop
      string_buffer_v(vector'high + vector'low + 1 - i) := conv_char(vector(i));
    end loop;
    
    return string_buffer_v;
  end function conv_string;
  
  function add_vector_and_bit(vector : std_logic_vector; bit_val : std_logic) return std_logic_vector is
    variable bit_vector_v                                            : std_logic_vector(0 downto 0);
    variable sum_v                                                   : std_logic_vector(vector'range);
  begin
    bit_vector_v(0) := bit_val;
    sum_v := vector + bit_vector_v;
    return sum_v;
  end function add_vector_and_bit;
  
  function mult_factor_by_weight(factor : std_logic_vector; weight : std_logic_vector) return std_logic_vector is
    variable mult_factor_v                                           : std_logic_vector(factor'length + weight'length - 1 downto 0);
    variable mult_result_v                                           : std_logic_vector(factor'length + weight'length - 1 downto 0);
  begin
    mult_result_v := (others => '0');
    for i in 0 to weight'high loop
      mult_factor_v := (others => '0');
      if (weight(i) = '1') then
        mult_factor_v(factor'high + i downto i) := factor;
      end if;
      mult_result_v := mult_result_v + mult_factor_v;
    end loop;
    return mult_result_v;
  end function mult_factor_by_weight;
  
  function mult_signed_factor_by_weight(factor : std_logic_vector; weight : std_logic_vector) return std_logic_vector is
    variable factor_sign_v                                           : std_logic;
    variable mult_factor_v                                           : std_logic_vector(factor'length + weight'length downto 0);
    variable mult_result_v                                           : std_logic_vector(factor'length + weight'length downto 0);
  begin
    factor_sign_v := factor(factor'high);
    mult_result_v := (others => '0');
    for i in 0 to weight'high loop
      if (weight(i) = '1') then
        mult_factor_v := (others => factor_sign_v);
        mult_factor_v(factor'high + i downto i) := factor;
        if (i > 0) then
          mult_factor_v(i - 1 downto 0) := (others => '0');
        end if;
      else
        mult_factor_v := (others => '0');
      end if;
      mult_result_v := mult_result_v + mult_factor_v;
    end loop;
    return mult_result_v;
  end function mult_signed_factor_by_weight;
  
  function truncate_vector(vector : std_logic_vector; req_length : integer) return std_logic_vector is
  begin
    return vector(req_length - 1 downto 0);
  end function truncate_vector;
  
  function extend_signed_vector(vector : std_logic_vector; req_length : integer) return std_logic_vector is
    variable ext_vector_v                                            : std_logic_vector(req_length - 1 downto 0);
  begin
    if (vector'length > req_length) then
      return vector(req_length - 1 downto 0);
    end if;
    ext_vector_v := (others => vector(vector'high));
    ext_vector_v(vector'length - 1 downto 0) := vector;
    return ext_vector_v;
  end function extend_signed_vector;
  
  function extend_unsigned_vector(vector : std_logic_vector; req_length : integer) return std_logic_vector is
    variable ext_vector_v                                            : std_logic_vector(req_length - 1 downto 0);
  begin
    if (vector'length > req_length) then
      return vector(req_length - 1 downto 0);
    end if;
    ext_vector_v := (others => '0');
    ext_vector_v(vector'length - 1 downto 0) := vector;
    return ext_vector_v;
  end function extend_unsigned_vector;
  
  function shift_left_vector(vector : std_logic_vector; shift : natural := 1) return std_logic_vector is
    variable shift_vector_v                                          : std_logic_vector(vector'range);
  begin
    shift_vector_v := (others => '0');
    for bit_idx in vector'low to vector'high loop
      if (bit_idx >= vector'low + shift) then
        shift_vector_v(bit_idx) := vector(bit_idx - shift);
      end if;
    end loop;
    return shift_vector_v;
  end function shift_left_vector;
  
  function shift_right_unsigned_vector(vector : std_logic_vector; shift : natural := 1) return std_logic_vector is
    variable shift_vector_v                                          : std_logic_vector(vector'range);
  begin
    shift_vector_v := (others => '0');
    for bit_idx in vector'low to vector'high loop
      if (bit_idx <= vector'high - shift) then
        shift_vector_v(bit_idx) := vector(bit_idx + shift);
      end if;
    end loop;
    return shift_vector_v;
  end function shift_right_unsigned_vector;
  
  function shift_right_signed_vector(vector : std_logic_vector; shift : natural := 1) return std_logic_vector is
    variable shift_vector_v                                          : std_logic_vector(vector'range);
  begin
    shift_vector_v := (others => vector(vector'high));
    for bit_idx in vector'low to vector'high loop
      if (bit_idx <= vector'high - shift) then
        shift_vector_v(bit_idx) := vector(bit_idx + shift);
      end if;
    end loop;
    return shift_vector_v;
  end function shift_right_signed_vector;
  
  function find_lead_one_pos(vector : std_logic_vector) return integer is
    variable lead_one_pos_v                                          : integer;
  begin
    lead_one_pos_v := -1;
    for bit_pos in vector'high downto vector'low loop
      if (vector(bit_pos) = '1') then
        lead_one_pos_v := bit_pos;
        exit;
      end if;
    end loop;
    return lead_one_pos_v;
  end function find_lead_one_pos;
  
  function find_lead_zero_pos(vector : std_logic_vector) return integer is
    variable lead_zero_pos_v                                         : integer;
  begin
    lead_zero_pos_v := -1;
    for bit_pos in vector'high downto vector'low loop
      if (vector(bit_pos) = '0') then
        lead_zero_pos_v := bit_pos;
        exit;
      end if;
    end loop;
    return lead_zero_pos_v;
  end function find_lead_zero_pos;
  
  function find_last_one_pos(vector : std_logic_vector) return integer is
    variable last_one_pos_v                                          : integer;
  begin
    last_one_pos_v := -1;
    for bit_pos in vector'low to vector'high loop
      if (vector(bit_pos) = '0') then
        last_one_pos_v := bit_pos;
        exit;
      end if;
    end loop;
    return last_one_pos_v;
  end function find_last_one_pos;
  
  function find_last_zero_pos(vector : std_logic_vector) return integer is
    variable last_zero_pos_v                                         : integer;
  begin
    last_zero_pos_v := -1;
    for bit_pos in vector'low to vector'high loop
      if (vector(bit_pos) = '0') then
        last_zero_pos_v := bit_pos;
        exit;
      end if;
    end loop;
    return last_zero_pos_v;
  end function find_last_zero_pos;
  
end package body ext_vhdl_pkg;

