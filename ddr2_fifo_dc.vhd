library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;
use work.ram_pkg.all;
--use work.ddr2_pkg.all;

entity ddr2_fifo_dc is
  generic 
  (
    BIT_WIDTH        : integer := 8;
    ADDR_WIDTH       : integer := 10;
    LP_FULLNESS      : integer := 32;
    WARN_LENGTH      : integer := 20
  );
  port
  (   
    rst_n            : in   std_logic;
    
    w_clk            : in   std_logic;
    w_data           : in   std_logic_vector (BIT_WIDTH-1 downto 0);
    w_enable         : in   std_logic;
    r_clk            : in   std_logic;
    r_data           : out  std_logic_vector (BIT_WIDTH-1 downto 0);
    r_ack            : in   std_logic;
    empty            : out  std_logic;
    almost_empty     : out  std_logic;  
    half_full        : out  std_logic;  
    almost_full      : out  std_logic;
    full             : out  std_logic
  );
end;

architecture flow of ddr2_fifo_dc is

--  constant WARN_LENGTH         : integer := ADDR_WIDTH * 2;
  type QUEUE_STATE is record
  empty                      : std_logic;
  almost_empty               : std_logic;
  half_full                  : std_logic;
  almost_full                : std_logic;
  full                       : std_logic;
  end record;
  
  
  signal write_address_r         : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_address_r_next    : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_address_r          : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_address_r_next     : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_ptr_input_r       : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_ptr_input_r_next  : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_ptr_output_r      : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_ptr_output_next   : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_ptr_input_r        : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_ptr_input_next     : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_ptr_output_r       : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_ptr_output_r_next  : std_logic_vector(ADDR_WIDTH-1 downto 0);       
  signal wei                     : std_logic;
  signal fifo_state              : QUEUE_STATE;
  signal empty_r                 : std_logic;
  
begin

  empty <= empty_r;--fifo_state.empty;
  almost_empty <= fifo_state.almost_empty;
  half_full <= fifo_state.half_full;
  almost_full <= fifo_state.almost_full;
  full <= fifo_state.full;
    

  COMB: process (write_address_r, read_address_r, r_ack, empty_r, w_enable, write_ptr_input_r, write_ptr_output_r, read_ptr_input_r, read_ptr_output_r)
  
    variable q_state_v              : QUEUE_STATE;
    variable write_v                : std_logic; 
    variable write_address_v        : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable read_address_v         : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable read_address_incr_v    : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable write_ptr_input_v      : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable write_ptr_output_v     : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable read_ptr_input_v       : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable read_ptr_output_v      : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable address_diff_input_v   : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable address_diff_output_v  : std_logic_vector(ADDR_WIDTH-1 downto 0); 
    
  begin
    
    -- gray/bns input
    
    write_ptr_input_v := write_address_r xor ('0' & (write_address_r(ADDR_WIDTH-1 downto 1)));
    
    read_ptr_input_v(ADDR_WIDTH - 1) := read_ptr_input_r(ADDR_WIDTH - 1);
    for i in 2 to ADDR_WIDTH loop
      read_ptr_input_v(ADDR_WIDTH - i) := read_ptr_input_r(ADDR_WIDTH - i) xor read_ptr_input_v((ADDR_WIDTH - i) + 1); 
    end loop;
    
    -- input stage
    
    q_state_v.empty := '0';
    q_state_v.almost_empty := '0';
    q_state_v.half_full := '0';
    q_state_v.almost_full := '0';
    q_state_v.full := '0';
    write_v := '1';
    write_address_v := write_address_r;
    
    if (write_address_r + 1) = read_ptr_input_v then
      q_state_v.full := '1';
    elsif w_enable = '1' then
      write_address_v := write_address_r + 1;
      write_v := '0';
    end if;
    
    address_diff_input_v := read_ptr_input_v - write_address_r;
    if address_diff_input_v <= WARN_LENGTH and address_diff_input_v /= 0 then
      q_state_v.almost_full := '1';
    end if;
    
    -- gray/bns output
    
    read_ptr_output_v := read_address_r xor ('0' & (read_address_r(ADDR_WIDTH-1 downto 1)));
    
    write_ptr_output_v(ADDR_WIDTH - 1) := write_ptr_output_r(ADDR_WIDTH - 1);
    for i in 2 to ADDR_WIDTH loop
      write_ptr_output_v(ADDR_WIDTH - i) := write_ptr_output_r(ADDR_WIDTH - i) xor write_ptr_output_v((ADDR_WIDTH - i) + 1); 
    end loop;
    
    -- output stage
    
    read_address_v := read_address_r;
    read_address_incr_v := read_address_r + 1;
    
--    if write_ptr_output_v = read_address_r then
--      q_state_v.empty := '1';
--    elsif r_ack = '1' then
--      read_address_v := read_address_incr_v;--read_address_r + 1;
--    end if;
    if write_ptr_output_v = read_address_r then
      q_state_v.empty := '1';
    end if;
    if r_ack = '1' and empty_r = '0' then
      read_address_v := read_address_incr_v;--read_address_r + 1;
      if write_ptr_output_v = read_address_incr_v then
        q_state_v.empty := '1';
      end if;
    end if;
    
    address_diff_output_v := write_ptr_output_v - read_address_r;
    if address_diff_output_v <= WARN_LENGTH then
      q_state_v.almost_empty := '1';
    end if; 
    if address_diff_output_v >= LP_FULLNESS then
      q_state_v.half_full := '1';
    end if; 
  
    -- signal and register assignments
    
    fifo_state <= q_state_v;
    wei <= not write_v;     
  
--  if rst_n = '1' then
      write_address_r_next <= write_address_v;
      read_address_r_next <= read_address_v;
      write_ptr_input_r_next <= write_ptr_input_v;
      read_ptr_output_r_next <= read_ptr_output_v;
--  else
--      write_address_r_next <= (others => '0');
--      read_address_r_next <= (others => '0');
--      write_ptr_input_r_next <= (others => '0');
--      read_ptr_output_r_next <= (others => '0');
--  end if;

  end process;
  
  read_ptr_input_next <= read_ptr_output_r when rst_n = '1' else (others => '0');
  write_ptr_output_next <= write_ptr_input_r when rst_n = '1' else (others => '0'); 
  
  reg_input : process (w_clk, rst_n)
  
  begin
    
    if rst_n = '0' then
      write_address_r <= (others => '0');
      write_ptr_input_r <= (others => '0');
      read_ptr_input_r <= (others => '0');
    elsif rising_edge(w_clk) then
      write_address_r <= write_address_r_next;
      write_ptr_input_r <= write_ptr_input_r_next;
      read_ptr_input_r <= read_ptr_input_next;--read_ptr_output_r;
    end if;
    
  end process;
  
  reg_output : process (r_clk, rst_n)
  
  begin
    
    if rst_n = '0' then
      read_address_r <= (others => '0');
      write_ptr_output_r <= (others => '0');
      read_ptr_output_r <= (others => '0');
      empty_r <= '1';
    elsif rising_edge(r_clk) then
      read_address_r <= read_address_r_next;
      write_ptr_output_r <= write_ptr_output_next;--write_ptr_input_r;
      read_ptr_output_r <= read_ptr_output_r_next;
      empty_r <= fifo_state.empty;
    end if;
    
  end process;

  asic_ram : RAMT
    generic map
    (
      BIT_WIDTH => BIT_WIDTH,
      ADDR_WIDTH => ADDR_WIDTH
    )
    port map
    (
      clk(0) => w_clk,
      clk(1) => r_clk,
      wren => wei,
      data_a => w_data,
      address_a => write_address_r,
      address_b => read_address_r_next,
      q_b => r_data
     );
     
end flow;

library IEEE;
use IEEE.std_logic_1164.all;

package ddr2_fifo_dc_pkg is
component ddr2_fifo_dc is
  generic 
  (
    BIT_WIDTH        : integer := 8;
    ADDR_WIDTH       : integer := 10;
    LP_FULLNESS      : integer := 32;
    WARN_LENGTH      : integer := 20
  );
  port
  (   
    rst_n            : in   std_logic;
    
    w_clk            : in   std_logic;
    w_data           : in   std_logic_vector (BIT_WIDTH-1 downto 0);
    w_enable         : in   std_logic;
  
    r_clk            : in   std_logic;
    r_data           : out  std_logic_vector (BIT_WIDTH-1 downto 0);
    r_ack            : in   std_logic;
    
    empty            : out  std_logic;
    almost_empty     : out  std_logic;  
    half_full        : out  std_logic;
    almost_full      : out  std_logic;
    full             : out  std_logic
  );
end component;
end;