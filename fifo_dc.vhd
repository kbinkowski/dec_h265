library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;
use work.ram_pkg.all; 

entity fifo_dc is
  generic 
  (
    BIT_WIDTH        : integer := 8;
    ADDR_WIDTH       : integer := 10;
    WARN_LENGTH      : integer := 10
  );
  port
  (   
    clk_input        : in   std_logic;
    clk_output       : in   std_logic;
    xrst             : in   std_logic;
    
    datain           : in   std_logic_vector (BIT_WIDTH-1 downto 0);
    dataout          : out  std_logic_vector (BIT_WIDTH-1 downto 0);
    enable           : in   std_logic;
    full             : out  std_logic;
    almost_full      : out  std_logic;
    ack              : in   std_logic;
    empty            : out  std_logic;
    almost_empty     : out  std_logic  
  );
end;

architecture flow of fifo_dc is

  signal write_address_r         : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_address_r_next    : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_address_r          : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_address_r_next     : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_ptr_input_r       : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_ptr_input_r_next  : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal write_ptr_output_r      : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_ptr_input_r        : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_ptr_output_r       : std_logic_vector(ADDR_WIDTH-1 downto 0);
  signal read_ptr_output_r_next  : std_logic_vector(ADDR_WIDTH-1 downto 0);       
  signal wei                     : std_logic;

begin

  comb : process (write_address_r, read_address_r, ack, enable, write_ptr_input_r,
                  write_ptr_output_r, read_ptr_input_r, read_ptr_output_r)
  
    variable full_v                 : std_logic;
    variable almost_full_v          : std_logic;
    variable empty_v                : std_logic;
    variable almost_empty_v         : std_logic; 
    variable write_v                : std_logic; 
    variable write_address_v        : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable read_address_v         : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable write_ptr_input_v      : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable write_ptr_output_v     : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable read_ptr_input_v       : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable read_ptr_output_v      : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable address_diff_input_v   : std_logic_vector(ADDR_WIDTH-1 downto 0);
    variable address_diff_output_v  : std_logic_vector(ADDR_WIDTH-1 downto 0); 
    
  begin
    
    -- gray/bns input
    
    write_ptr_input_v := write_address_r xor ('0' & (write_address_r(ADDR_WIDTH-1 downto 1)));
    
    read_ptr_input_v(ADDR_WIDTH - 1) := read_ptr_input_r(ADDR_WIDTH - 1);
    for i in 2 to ADDR_WIDTH loop
      read_ptr_input_v(ADDR_WIDTH - i) := read_ptr_input_r(ADDR_WIDTH - i) xor read_ptr_input_v((ADDR_WIDTH - i) + 1); 
    end loop;
    
    -- input stage
    
    full_v := '0';
    write_v := '1';
    write_address_v := write_address_r;
    
    if (write_address_r + 1) = read_ptr_input_v then
      full_v := '1';
    elsif enable = '1' then
      write_address_v := write_address_r + 1;
      write_v := '0';
    end if;
    
    almost_full_v := '0';
    
    address_diff_input_v := read_ptr_input_v - write_address_r;
    if address_diff_input_v = 0 then
      almost_full_v := '0';
    elsif address_diff_input_v = WARN_LENGTH then				
      almost_full_v := '1';
    end if;
    
    -- gray/bns output
    
    read_ptr_output_v := read_address_r xor ('0' & (read_address_r(ADDR_WIDTH-1 downto 1)));
    
    write_ptr_output_v(ADDR_WIDTH - 1) := write_ptr_output_r(ADDR_WIDTH - 1);
    for i in 2 to ADDR_WIDTH loop
      write_ptr_output_v(ADDR_WIDTH - i) := write_ptr_output_r(ADDR_WIDTH - i) xor write_ptr_output_v((ADDR_WIDTH - i) + 1); 
    end loop;
    
    -- output stage
    
    empty_v := '0';
    read_address_v := read_address_r;
    
    if write_ptr_output_v = read_address_r then
      empty_v := '1';
    elsif ack = '1' then
      read_address_v := read_address_r + 1;
    end if;
    
    almost_empty_v := '0';
    
    address_diff_output_v := write_ptr_output_v - read_address_r;
    if address_diff_output_v = WARN_LENGTH then
      almost_empty_v := '1';
    end if; 
    
    -- signal and register assignments
    
    full <= full_v;
    almost_full <= almost_full_v;
    empty <= empty_v;
    almost_empty <= almost_empty_v;
    wei <= not write_v;
    write_address_r_next <= write_address_v;
    read_address_r_next <= read_address_v;
    write_ptr_input_r_next <= write_ptr_input_v;
    read_ptr_output_r_next <= read_ptr_output_v;
    
  end process;
  
  reg_input : process (clk_input, xrst)
  
  begin
    
    if xrst = '0' then
      write_address_r <= (others => '0');
      write_ptr_input_r <= (others => '0');
      read_ptr_input_r <= (others => '0');
    elsif rising_edge(clk_input) then
      write_address_r <= write_address_r_next;
      write_ptr_input_r <= write_ptr_input_r_next;
      read_ptr_input_r <= read_ptr_output_r;
    end if;
    
  end process;
  
  reg_output : process (clk_output, xrst)
  
  begin
    
    if xrst = '0' then
      read_address_r <= (others => '0');
      write_ptr_output_r <= (others => '0');
      read_ptr_output_r <= (others => '0');
    elsif rising_edge(clk_output) then
      read_address_r <= read_address_r_next;
      write_ptr_output_r <= write_ptr_input_r;
      read_ptr_output_r <= read_ptr_output_r_next;
    end if;
    
  end process;
  
  
  
 asic_ram : RAMT 
   generic map(
	    BIT_WIDTH => BIT_WIDTH,
      ADDR_WIDTH => ADDR_WIDTH
		)
	port map(
	    clk(0) => clk_input,
      clk(1) => clk_output,
      wren => wei,
      data_a => datain,
      address_a => write_address_r,
      address_b => read_address_r_next,
      q_b => dataout
	);
  
end flow;
