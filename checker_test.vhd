-------------------------------------------------------------------------------
--                                                                           --
-- Title       : MOTION COMPENSATION DECODER - H.265/HEVC                    --
-- Design      : the H.265/HEVC decoder                                      --
-- Author      : Kamil Binkowski, University of Technology in Warsaw         --
-- Start Date  : 02.03.2013                                                  --
--                                                                           --
-------------------------------------------------------------------------------  
--                                                                           --
-- File        : checker_test.vhd                                            --
--                                                                           --
-------------------------------------------------------------------------------
-- Description: Result verification module used in test.                     --
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.motion_compensation_dec_pkg.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;   


entity checker_test is
port(   
   clk         : in std_logic;
   samples     : in DATA_SAMPLES_DEC_TYPE;
   samples_dav : in std_logic :='0'
   );
end checker_test;


architecture checker_test_arch of checker_test is 

file rec_file          : text open read_mode is  "/test/rec_file.txt";   
file dif_file          : text open write_mode is "/test/dif_file.txt";
file out_file          : text open write_mode is "/test/out_file.txt";  
file error_file        : text open write_mode is "/test/error_file.txt";

type DIF_SAMPLES_DEC_TYPE  is array (NUMBER_OF_SAMPLES_DEC-1 downto 0) of std_logic_vector(8+DELTA_BIT_DEPTH downto 0);  
signal dif_samples : DIF_SAMPLES_DEC_TYPE; 
signal ctu_cnt     : std_logic_vector(9 downto 0) := "0000000000"; -- Counter CTU, used to identify where is ERROR
signal line_cnt    : std_logic_vector(10 downto 0) := "00000000000"; -- Counter line of samples to identicicate changing of CTU 

begin

CHECK_PROC: process(clk, samples, samples_dav)
variable rec_samples  : DATA_SAMPLES_DEC_TYPE;
variable read_line_v  : line; 
variable out_write_line_v   : line; 
variable dif_write_line_v   : line; 
variable error_write_line_v : line;
variable integer_v    : integer;
variable temp_v       : integer;
variable error_v      : std_logic;
variable ctu_cnt_v    :	std_logic_vector(9 downto 0); 
variable line_cnt_v   : std_logic_vector(10 downto 0);

begin
   error_v := '0';
   ctu_cnt_v := ctu_cnt; 
   line_cnt_v := line_cnt;
   
   if rising_edge(clk) then
      if (samples_dav = '1') then
        if not endfile(rec_file) then 
         readline(rec_file, read_line_v);
         line_cnt_v :=std_logic_vector(unsigned(line_cnt) + 1);
         for i in 0 to 3 loop
           read(read_line_v, integer_v); 
           rec_samples(i) := std_logic_vector(to_unsigned(integer_v, rec_samples(i)'length)); 
         end loop; 
         
         for i in 0 to 3 loop
           integer_v := to_integer(signed('0'&samples(i)) - signed('0'&rec_samples(i))); 
           dif_samples(i) <= std_logic_vector(to_signed(integer_v, dif_samples(i)'length)); 
           write(dif_write_line_v, integer_v);
           write(dif_write_line_v, " ");
           
           if integer_v /= 0 then
            write(error_write_line_v, "ERROR in CTU number: ");
            integer_v := to_integer(unsigned(ctu_cnt)+1);
            write(error_write_line_v, integer_v);
            
            if unsigned(line_cnt_v) <= 1024 then
              write(error_write_line_v, ". Sample Y on position: ");
              temp_v := (to_integer(unsigned(line_cnt_v)) / 64)*4 + i;
              write(error_write_line_v, temp_v); 
              write(error_write_line_v, " x ");
              temp_v := to_integer(unsigned(line_cnt_v)) mod 64;
              write(error_write_line_v, temp_v); 
              write(error_write_line_v, ". Expected "); 
              write(error_write_line_v, to_integer(signed('0'&rec_samples(i)))); 
              write(error_write_line_v, ", was "); 
              write(error_write_line_v, to_integer(signed('0'&samples(i))));
              writeline(error_file, error_write_line_v);
            elsif unsigned(line_cnt_v) <= 1280 then
              write(error_write_line_v, ". Sample U on position: ");
              temp_v := ((to_integer(unsigned(line_cnt_v)) - 1024) / 32)*4 + i;
              write(error_write_line_v, temp_v); 
              write(error_write_line_v, " x ");
              temp_v := (to_integer(unsigned(line_cnt_v)) - 1024) mod 32; 
              write(error_write_line_v, temp_v); 
              write(error_write_line_v, ". Expected "); 
              write(error_write_line_v, to_integer(signed('0'&rec_samples(i)))); 
              write(error_write_line_v, ", was "); 
              write(error_write_line_v, to_integer(signed('0'&samples(i))));
              writeline(error_file, error_write_line_v); 
            else
              write(error_write_line_v, ". Sample V on position: ");
              temp_v := ((to_integer(unsigned(line_cnt_v)) - 1280) / 32)*4 + i;
              write(error_write_line_v, temp_v); 
              write(error_write_line_v, " x ");
              temp_v := (to_integer(unsigned(line_cnt_v)) - 1280) mod 32;
              write(error_write_line_v, temp_v); 
              write(error_write_line_v, ". Expected "); 
              write(error_write_line_v, to_integer(signed('0'&rec_samples(i)))); 
              write(error_write_line_v, ", was "); 
              write(error_write_line_v, to_integer(signed('0'&samples(i))));
              writeline(error_file, error_write_line_v); 
            end if; 
          end if; 
           
          integer_v := to_integer(signed('0'&samples(i))); 
          write(out_write_line_v, integer_v);
          write(out_write_line_v, " ");
        end loop;
         
         writeline(dif_file, dif_write_line_v); 
         writeline(out_file, out_write_line_v);  
        else
          assert FALSE report "END OF SIMULATION, NOTHING TO DO" severity failure;
        end if;
      end if;   
   end if;
   
	if unsigned(line_cnt_v) = 1536 then
	  ctu_cnt_v := std_logic_vector(unsigned(ctu_cnt) + 1); 
    line_cnt_v := "00000000000";
	end if;	
	
	ctu_cnt <= ctu_cnt_v;  
  line_cnt <= line_cnt_v;
	
end process;
         
end checker_test_arch;
