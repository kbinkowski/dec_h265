library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all; 
use work.motion_compensation_dec_pkg.all;

use work.ddr2_pkg.all;

package adapter_pkg is

  constant DRAM_MEMORY_DATA_WIDTH : natural := 32;
  constant ALTERA_JUMP_CORRECTION : natural := (ALTERA_DATA_WIDTH / DRAM_MEMORY_DATA_WIDTH);
  constant ALTERA_ADDRESS_CORRECTION : natural := log2_ceil(ALTERA_DATA_WIDTH / DRAM_MEMORY_DATA_WIDTH);

  
  type WRITE_PORT_IN_ADAPTER is record
    start   : std_logic;
    sel     : std_logic;
    address : std_logic_vector(ALTERA_ADDRESS_WIDTH-1+ALTERA_ADDRESS_CORRECTION downto 0);
    wid     : std_logic_vector(PARAM_WIDTH-1 downto 0);
    hei     : std_logic_vector(PARAM_WIDTH-1 downto 0);
    jum     : std_logic_vector(PARAM_WIDTH-1 downto 0);
    data    : std_logic_vector(DRAM_MEMORY_DATA_WIDTH+4*DELTA_BIT_DEPTH-1 downto 0);
  end record;

  type READ_PORT_OUT_ADAPTER is record
    ready        : std_logic;
    empty        : std_logic;
    almost_empty : std_logic;
    almost_full  : std_logic;
    full         : std_logic;
    data         : std_logic_vector(DRAM_MEMORY_DATA_WIDTH+4*DELTA_BIT_DEPTH-1 downto 0);
  end record;

  type READ_PORT_IN_ADAPTER is record
    start   : std_logic;
    sel     : std_logic;
    address : std_logic_vector(ALTERA_ADDRESS_WIDTH-1+ALTERA_ADDRESS_CORRECTION downto 0);
    wid     : std_logic_vector(PARAM_WIDTH-1 downto 0);
    hei     : std_logic_vector(PARAM_WIDTH-1 downto 0);
    jum     : std_logic_vector(PARAM_WIDTH-1 downto 0);
  end record;
  
  type ADAPTER_WRITE_PORTS_IN_TYPE is array(PORTS_WRITE-1 downto 0) of WRITE_PORT_IN_ADAPTER;
  type ADAPTER_READ_PORTS_OUT_TYPE is array(PORTS_READ-1 downto 0) of READ_PORT_OUT_ADAPTER;
  type ADAPTER_READ_PORTS_IN_TYPE is array(PORTS_READ-1 downto 0) of READ_PORT_IN_ADAPTER;

  function change_std_logic_vector_length (
    input_vector : std_logic_vector;
    output_length : natural
    )
    return std_logic_vector;

  function fill_MSB_with_zeroes (
    sig          : std_logic_vector; -- signal to be extended
    FINAL_LENGTH : integer)           
    return std_logic_vector;
  
end adapter_pkg;

package body adapter_pkg is

  function change_std_logic_vector_length(
    input_vector : std_logic_vector;
    output_length : natural
    )
    return std_logic_vector is

    variable result_v :std_logic_vector(output_length-1 downto 0);
  begin  -- change_std_logic_vector_length
    if output_length > input_vector'length then
      -- make vector longer
      result_v := (others => '0');
      result_v(input_vector'length-1 downto 0) := input_vector;
    elsif output_length = input_vector'length then
      -- do nothing
      result_v := input_vector;
    else
      -- make vector shorter - cut most significant bits
      result_v := input_vector(output_length-1 downto 0);
    end if;
    return result_v;
  end change_std_logic_vector_length;

  function fill_MSB_with_zeroes (
    sig          : std_logic_vector; -- signal to be extended
    FINAL_LENGTH : integer)           
    return std_logic_vector is
      
    variable result_v : std_logic_vector(final_length-1 downto 0);
  begin  -- fill_MSB
    result_v :=  (others => '0' );
    result_v(sig'length-1 downto 0) := sig;
    return result_v;
  end fill_MSB_with_zeroes;
  
end adapter_pkg;
