library ieee; 
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ram_pkg.all;
use work.intra_dec_pkg.all;
use work.ext_vhdl_pkg.all;

entity intra_pred_dec is
  port (clk                                                          : in  std_logic;
        rst_n                                                        : in  std_logic;
        -- sequence, picture and slice parameters
        bit_depth_luma                                               : in  std_logic_vector(2 downto 0);
        bit_depth_chroma                                             : in  std_logic_vector(2 downto 0);
        refs_smooth_en                                               : in  std_logic;
        cu_size                                                      : in  std_logic_vector(6 downto 0);
        -- position of the current PU within picture
        pu_addr_x                                                    : in  std_logic_vector(MAX_PIC_WIDTH_LOG2 - 1 downto 0);
        pu_addr_y                                                    : in  std_logic_vector(MAX_PIC_HEIGHT_LOG2 - 1 downto 0);
        pu_avail                                                     : in  std_logic_vector(4 downto 0);
        -- intra prediction parameters
        img_comp                                                     : in  std_logic_vector(1 downto 0);
        pu_size_idx                                                  : in  std_logic_vector(1 downto 0);
        mode_idx                                                     : in  std_logic_vector(5 downto 0);
        pred_params_en                                               : in  std_logic;
        pred_params_ack                                              : out std_logic;
        -- intra prediction
        pred_samples                                                 : out SET_OF_4_SAMPLES_TYPE;
        pred_samples_en                                              : out std_logic;
        pred_samples_ack                                             : in  std_logic;
        -- reconstruction interface
        rec_addr_x                                                   : in  std_logic_vector(MAX_PIC_WIDTH_LOG2 - 1 downto 0);
        rec_addr_y                                                   : in  std_logic_vector(MAX_PIC_HEIGHT_LOG2 - 1 downto 0);
        rec_img_comp                                                 : in  std_logic_vector(1 downto 0);
        rec_width                                                    : in  std_logic_vector(6 downto 0);
        rec_height                                                   : in  std_logic_vector(6 downto 0);
        rec_samples                                                  : in  SET_OF_4_SAMPLES_TYPE;
        rec_samples_en                                               : in  std_logic;
        rec_samples_ack                                              : out std_logic);
end entity intra_pred_dec;

architecture rtl of intra_pred_dec is
  
  constant LEFT_DIR                                                  : std_logic := '0';
  constant TOP_DIR                                                   : std_logic := '1';
  
  type SET_OF_8_REFS_SOURCE_TYPE is array(7 downto 0) of std_logic_vector(5 downto 0);
  type SET_OF_4_REFS_IDX_TYPE is array(3 downto 0) of std_logic_vector(6 downto 0);
  type SET_OF_4_WEIGHTS_TYPE is array(3 downto 0) of std_logic_vector(4 downto 0);
  type SET_OF_4_EXT_SAMPLES_TYPE is array(3 downto 0) of std_logic_vector(8 + DELTA_BIT_DEPTH downto 0);
  type SET_OF_8_EXT_SAMPLES_TYPE is array(7 downto 0) of std_logic_vector(8 + DELTA_BIT_DEPTH downto 0);
  type SET_OF_8_MULT_WEIGHTS_TYPE is array(7 downto 0) of std_logic_vector(5 downto 0);
  
  type PRED_CONTROL_STATE_TYPE is (IDLE, FETCH_CORNER, CHECK_SMOOTH, FETCH_PLANAR, CALC_DC_VAL, FETCH_REFS, START_PRED, CALC_PRED, STORE_CORNERS, STORE_REFS,
                                   STORE_LAST_REFS_SET);
  
  type PRED_CONTROL_TYPE is record
    state                                                            : PRED_CONTROL_STATE_TYPE;
    skip_pred                                                        : std_logic;
    col_idx                                                          : std_logic_vector(6 downto 0);
    row_idx                                                          : std_logic_vector(4 downto 0);
    reading_idx                                                      : std_logic_vector(4 downto 0);
    refs_adjust_type                                                 : std_logic_vector(2 downto 0);
    refs_read_dir                                                    : std_logic;
    refs_read_pos                                                    : std_logic_vector(3 downto 0);
    corner_top_left                                                  : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    corner_bottom_left                                               : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    corner_top_right                                                 : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    refs_buffer                                                      : SET_OF_64_SAMPLES_TYPE;
    dc_val                                                           : std_logic_vector(13 + DELTA_BIT_DEPTH downto 0);
    last_left_ref                                                    : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    last_top_ref                                                     : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    angle_accum                                                      : std_logic_vector(11 downto 0);
    ang_weight                                                       : std_logic_vector(4 downto 0);
    refs_idxs                                                        : SET_OF_4_REFS_IDX_TYPE;
    refs_corner                                                      : std_logic_vector(7 downto 0);
    refs_source                                                      : SET_OF_8_REFS_SOURCE_TYPE;
    col_idx_dly                                                      : std_logic_vector(6 downto 0);
    row_idx_dly                                                      : std_logic_vector(4 downto 0);
    pred_buffer                                                      : SET_OF_8_EXT_SAMPLES_TYPE;
    pred_weights                                                     : SET_OF_8_MULT_WEIGHTS_TYPE;
    pred_offsets                                                     : SET_OF_4_EXT_SAMPLES_TYPE;
    pred_samples_en                                                  : std_logic;
  end record;
  
  type CORNER_CONTROL_STATE_TYPE is (IDLE, READ_TOP_CORNER, STORE_TOP_CORNER, READ_LEFT_CORNER, STORE_LEFT_CORNER, FINISHED);
  
  type CORNER_CONTROL_TYPE is record
    state                                                            : CORNER_CONTROL_STATE_TYPE;
    avail                                                            : std_logic;
    corner                                                           : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
  end record;
  
  type SMOOTH_CONTROL_TYPE is record
    smooth_terms                                                     : std_logic_vector(1 downto 0);
  end record;
  
  signal pred_control_r                                              : PRED_CONTROL_TYPE;
  signal pred_control_next                                           : PRED_CONTROL_TYPE;
  
  signal corner_control_r                                            : CORNER_CONTROL_TYPE;
  signal corner_control_next                                         : CORNER_CONTROL_TYPE;
  
  signal smooth_control_r                                            : SMOOTH_CONTROL_TYPE;
  signal smooth_control_next                                         : SMOOTH_CONTROL_TYPE;
  
  signal refs_data_in                                                : std_logic_vector(REFS_MEM_DATA_WIDTH - 1 downto 0);
  signal refs_addr_in                                                : std_logic_vector(REFS_MEM_ADDR_WIDTH - 1 downto 0);
  signal refs_data_en                                                : std_logic;
  
  signal refs_data_out                                               : std_logic_vector(REFS_MEM_DATA_WIDTH - 1 downto 0);
  signal refs_addr_out                                               : std_logic_vector(REFS_MEM_ADDR_WIDTH - 1 downto 0);
  
begin
  
  CONTROL_PROCESS: process(bit_depth_luma, bit_depth_chroma, refs_smooth_en, cu_size, pu_addr_x, pu_addr_y, pu_avail, img_comp, pu_size_idx, mode_idx,
                           pred_params_en, pred_samples_ack, rec_addr_x, rec_addr_y, rec_img_comp, rec_width, rec_height, rec_samples, rec_samples_en,
                           refs_data_out, pred_control_r, corner_control_r, smooth_control_r)
    
    type SET_OF_4_ANGLE_INCR_TYPE is array(3 downto 0) of std_logic_vector(pred_control_r.angle_accum'range);
    type SET_OF_4_MULT_RESULTS_TYPE is array(3 downto 0) of std_logic_vector(15 + DELTA_BIT_DEPTH downto 0);
    
    variable pred_control_v                                          : PRED_CONTROL_TYPE;
    variable corner_control_v                                        : CORNER_CONTROL_TYPE;
    variable smooth_control_v                                        : SMOOTH_CONTROL_TYPE;
    
    variable pred_params_ack_v                                       : std_logic;
    
    variable pred_samples_v                                          : SET_OF_4_SAMPLES_TYPE;
    
    variable rec_samples_ack_v                                       : std_logic;
    
    variable refs_in_v                                               : SET_OF_4_SAMPLES_TYPE;
    
    variable refs_data_in_v                                          : std_logic_vector(REFS_MEM_DATA_WIDTH - 1 downto 0);
    variable refs_addr_in_v                                          : std_logic_vector(REFS_MEM_ADDR_WIDTH - 1 downto 0);
    variable refs_data_en_v                                          : std_logic;
    
    variable refs_pointer_v                                          : REFS_POINTER_TYPE;
    
    variable refs_addr_out_v                                         : std_logic_vector(REFS_MEM_ADDR_WIDTH - 1 downto 0);
    
    variable corner_top_right_sgn_v                                  : std_logic_vector(8 + DELTA_BIT_DEPTH downto 0);
    variable corner_bottom_left_sgn_v                                : std_logic_vector(8 + DELTA_BIT_DEPTH downto 0);
    
    variable refs_out_v                                              : SET_OF_4_SAMPLES_TYPE;
    variable refs_read_v                                             : SET_OF_4_SAMPLES_TYPE;
    
    variable sample_idx_v                                            : std_logic_vector(1 downto 0);
    
    variable base_img_comp_v                                         : std_logic_vector(1 downto 0);
    variable base_addr_x_v                                           : std_logic_vector(MAX_PIC_WIDTH_LOG2 - 1 downto 0);
    variable base_addr_y_v                                           : std_logic_vector(MAX_PIC_HEIGHT_LOG2 - 1 downto 0);
    
    variable base_addr_corner_v                                      : std_logic_vector(REFS_MEM_ADDR_WIDTH - 1 downto 0);
    variable base_addr_left_v                                        : std_logic_vector(REFS_MEM_ADDR_WIDTH - 1 downto 0);
    variable base_addr_top_v                                         : std_logic_vector(REFS_MEM_ADDR_WIDTH - 1 downto 0);
    
    variable def_val_v                                               : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    
    variable pu_col_v                                                : std_logic_vector(3 downto 0);
    variable pu_row_v                                                : std_logic_vector(3 downto 0);
    
    variable pu_size_v                                               : std_logic_vector(5 downto 0);
    variable pu_size_m1_v                                            : std_logic_vector(5 downto 0);
    variable pu_size_log2_v                                          : std_logic_vector(2 downto 0);
    
    variable refs_prep_req_v                                         : std_logic;
    
    variable smooth_check_req_v                                      : std_logic;
    variable smooth_req_v                                            : std_logic;
    
    variable filt_req_v                                              : std_logic;
    
    variable bound_ref_v                                             : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    variable smooth_term_v                                           : std_logic;
    
    variable corner_planar_left_v                                    : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    variable corner_planar_filt_v                                    : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    
    variable corner_bottom_left_smooth_v                             : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    variable corner_top_right_smooth_v                               : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    
    variable pred_angle_v                                            : std_logic_vector(6 downto 0);
    
    variable ang_hor_v                                               : std_logic;
    
    variable bidir_pred_v                                            : std_logic;
    
    variable readings_limit_v                                        : std_logic_vector(4 downto 0);
    variable readings_dir_limit_v                                    : std_logic_vector(4 downto 0);
    
    variable reading_idx_planar_v                                    : std_logic_vector(4 downto 0);
    variable reading_idx_v                                           : std_logic_vector(4 downto 0);
    
    variable angle_mult2_v                                           : std_logic_vector(pred_angle_v'high + 1 downto pred_angle_v'low);
    variable angle_mult4_v                                           : std_logic_vector(pred_angle_v'high + 2 downto pred_angle_v'low);
    variable angle_hor_incr_v                                        : std_logic_vector(pred_control_r.angle_accum'range);
    variable angle_ver_incr_v                                        : std_logic_vector(pred_control_r.angle_accum'range);
    
    variable rec_height_limit_v                                      : std_logic_vector(4 downto 0);
    variable rec_width_limit_v                                       : std_logic_vector(6 downto 0);
    
    variable smooth_check_finished_v                                 : std_logic;
    
    variable planar_fetch_finished_v                                 : std_logic;
    variable refs_fetch_finished_v                                   : std_logic;
    
    variable reading_smooth_req_v                                    : std_logic;
    variable reading_planar_corner_req_v                             : std_logic;
    variable reading_req_v                                           : std_logic;
    
    variable refs_smooth_offset_v                                    : std_logic_vector(14 + DELTA_BIT_DEPTH downto 0);
    variable refs_smooth_v                                           : SET_OF_4_SAMPLES_TYPE;
    
    variable ref_second_to_last_v                                    : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    variable ref_last_v                                              : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    variable refs_filt_v                                             : SET_OF_4_SAMPLES_TYPE;
    
    variable storage_pos_v                                           : std_logic_vector(5 downto 0);
    variable storage_req_v                                           : std_logic;
    
    variable accum_upd_req_v                                         : std_logic;
    
    variable rec_left_storage_req_v                                  : std_logic;
    variable rec_top_storage_req_v                                   : std_logic;
    
    variable reading_corner_req_v                                    : std_logic;
    
    variable refs_left_start_num_v                                   : std_logic_vector(1 downto 0);
    
    variable rec_width_m1_v                                          : std_logic_vector(6 downto 0);
    variable rec_height_m1_v                                         : std_logic_vector(6 downto 0);
    
    variable corner_left_storage_req_v                               : std_logic;
    variable corner_left_pos_v                                       : std_logic_vector(4 downto 0);
    
    variable corner_top_pos_v                                        : std_logic_vector(4 downto 0);
    
    variable corner_base_pos_v                                       : std_logic_vector(3 downto 0);
    variable corner_ext_pos_v                                        : std_logic_vector(4 downto 0);
    
    variable corner_idx_v                                            : std_logic_vector(3 downto 0);
    variable corner_upd_req_v                                        : std_logic;
    variable corner_storage_req_v                                    : std_logic;
    
    variable refs_to_be_stored_v                                     : SET_OF_4_SAMPLES_TYPE;
    variable ref_selected_v                                          : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    
    variable col_idx_next_v                                          : std_logic_vector(6 downto 0);
    variable row_idx_next_v                                          : std_logic_vector(4 downto 0);
    
    variable refs_corner_v                                           : std_logic_vector(7 downto 0);
    variable refs_source_v                                           : SET_OF_8_REFS_SOURCE_TYPE;
    
    variable angle_incr_v                                            : SET_OF_4_ANGLE_INCR_TYPE;
    variable angle_accum_v                                           : std_logic_vector(pred_control_r.angle_accum'range);
    variable ang_pos_offset_v                                        : std_logic_vector(6 downto 0);
    
    variable ang_pos_left_v                                          : std_logic_vector(6 downto 0);
    variable ang_pos_top_v                                           : std_logic_vector(6 downto 0);
    
    variable ang_ref_idx_v                                           : std_logic_vector(6 downto 0);
    
    variable ang_refs_corner_v                                       : std_logic_vector(7 downto 0);
    variable ang_refs_source_v                                       : SET_OF_8_REFS_SOURCE_TYPE;
    
    variable max_pred_val_v                                          : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    
    variable process_corner_v                                        : std_logic;
    variable process_left_edge_v                                     : std_logic;
    variable process_top_edge_v                                      : std_logic;
    
    variable pred_buffer_v                                           : SET_OF_8_SAMPLES_TYPE;
    
    variable dc_corner_pred_v                                        : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    variable dc_top_edge_pred_v                                      : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    variable dc_left_edge_pred_v                                     : std_logic_vector(7 + DELTA_BIT_DEPTH downto 0);
    
    variable dc_pred_v                                               : SET_OF_4_SAMPLES_TYPE;
    
    variable hor_pred_v                                              : SET_OF_4_SAMPLES_TYPE;
    
    variable ver_pred_v                                              : SET_OF_4_SAMPLES_TYPE;
    
    variable planar_weights_v                                        : SET_OF_8_MULT_WEIGHTS_TYPE;
    variable ang_weights_v                                           : SET_OF_8_MULT_WEIGHTS_TYPE;
    
    variable pred_offsets_v                                          : SET_OF_4_MULT_RESULTS_TYPE;
    variable mult_results_v                                          : SET_OF_4_MULT_RESULTS_TYPE;
    
    variable planar_pred_v                                           : SET_OF_4_SAMPLES_TYPE;
    
    variable ang_pred_v                                              : SET_OF_4_SAMPLES_TYPE;
    
  begin
    pred_control_v := pred_control_r;
    pred_control_v.ang_weight := pred_control_r.angle_accum(4 downto 0);
    pred_control_v.col_idx_dly := pred_control_r.col_idx;
    pred_control_v.row_idx_dly := pred_control_r.row_idx;
    pred_control_v.pred_samples_en := '0';
    
    corner_control_v := corner_control_r;
    
    smooth_control_v := smooth_control_r;
    
    pred_params_ack_v := '0';
    
    for sample_idx in pred_samples_v'low to pred_samples_v'high loop
      pred_samples_v(sample_idx) := (others => '0');
    end loop;
    
    rec_samples_ack_v := '0';
    
    for sample_idx in refs_out_v'low to refs_out_v'high loop
      refs_read_v(sample_idx) := refs_data_out((sample_idx + 1) * (8 + DELTA_BIT_DEPTH) - 1 downto sample_idx * (8 + DELTA_BIT_DEPTH));
    end loop;
    
    refs_in_v := refs_read_v;
    refs_data_in_v := (others => '0');
    refs_addr_in_v := (others => '0');
    refs_data_en_v := '1';
    
    refs_pointer_v.neighbor := NOT_REQUIRED;
    refs_pointer_v.position := (others => '0');
    
    if (pred_control_r.state = STORE_REFS) or (pred_control_r.skip_pred = '1') then
      base_img_comp_v := rec_img_comp;
      base_addr_x_v := rec_addr_x;
      base_addr_y_v := rec_addr_y;
    else
      base_img_comp_v := img_comp;
      base_addr_x_v := pu_addr_x;
      base_addr_y_v := pu_addr_y;
    end if;
    
    base_addr_corner_v(REFS_MEM_ADDR_WIDTH - 1) := '0';
    base_addr_corner_v(REFS_MEM_ADDR_WIDTH - 2 downto 0) := (others => '1');
    base_addr_corner_v := base_addr_corner_v - 64/16 + 1 + base_addr_y_v(5 downto 4);
    if (base_img_comp_v = CHROMA_CB) then
      base_addr_corner_v(REFS_MEM_ADDR_WIDTH - 1 downto REFS_MEM_ADDR_WIDTH - 2) := "10";
    elsif (base_img_comp_v = CHROMA_CR) then
      base_addr_corner_v(REFS_MEM_ADDR_WIDTH - 1 downto REFS_MEM_ADDR_WIDTH - 2) := "11";
    end if;
    if (base_img_comp_v /= LUMA) then
      base_addr_corner_v(REFS_MEM_ADDR_WIDTH - 3 downto 0) := (others => '1');
      base_addr_corner_v := base_addr_corner_v - 64/32 + 1 + base_addr_y_v(4);
    end if;
    
    base_addr_left_v(REFS_MEM_ADDR_WIDTH - 1) := '0';
    base_addr_left_v(REFS_MEM_ADDR_WIDTH - 2 downto 0) := (others => '1');
    base_addr_left_v := base_addr_left_v - 64/4 - 64/16 + 1 + base_addr_y_v(5 downto 2);
    if (base_img_comp_v = CHROMA_CB) then
      base_addr_left_v(REFS_MEM_ADDR_WIDTH - 1 downto REFS_MEM_ADDR_WIDTH - 2) := "10";
    elsif (base_img_comp_v = CHROMA_CR) then
      base_addr_left_v(REFS_MEM_ADDR_WIDTH - 1 downto REFS_MEM_ADDR_WIDTH - 2) := "11";
    end if;
    if (base_img_comp_v /= LUMA) then
      base_addr_left_v(REFS_MEM_ADDR_WIDTH - 3 downto 0) := (others => '1');
      base_addr_left_v := base_addr_left_v - 64/8 - 64/32 + 1 + base_addr_y_v(4 downto 2);
    end if;
    
    base_addr_top_v := (others => '0');
    base_addr_top_v(MAX_PIC_WIDTH_LOG2 - 3 downto 0) := base_addr_x_v(MAX_PIC_WIDTH_LOG2 - 1 downto 2);
    if (base_img_comp_v = CHROMA_CB) then
      base_addr_top_v(REFS_MEM_ADDR_WIDTH - 1 downto REFS_MEM_ADDR_WIDTH - 2) := "10";
    elsif (base_img_comp_v = CHROMA_CR) then
      base_addr_top_v(REFS_MEM_ADDR_WIDTH - 1 downto REFS_MEM_ADDR_WIDTH - 2) := "11";
    end if;
    if (base_img_comp_v /= LUMA) then
      base_addr_top_v(MAX_PIC_WIDTH_LOG2 - 4 downto 0) := base_addr_x_v(MAX_PIC_WIDTH_LOG2 - 2 downto 2);
    end if;
    
    def_val_v := get_def_val(img_comp, bit_depth_luma, bit_depth_chroma);
    
    pu_row_v := get_pu_row(pu_addr_y, cu_size);
    
    pu_size_v := get_pu_size(pu_size_idx);
    pu_size_m1_v := pu_size_v - 1;
    pu_size_log2_v := get_log2_pu_size(pu_size_idx);
    
    refs_out_v := refs_read_v;
    for sample_idx in refs_out_v'low to refs_out_v'high loop
      if (pred_control_r.refs_adjust_type = SUBS_BY_CORNER) then
        refs_out_v(sample_idx) := refs_out_v(conv_integer(pu_row_v(1 downto 0)));
      elsif (pred_control_r.refs_adjust_type = SUBS_BY_FIRST_REF) then
        refs_out_v(sample_idx) := refs_out_v(refs_out_v'low);
      elsif (pred_control_r.refs_adjust_type = SUBS_BY_LAST_REF) then
        refs_out_v(sample_idx) := refs_out_v(refs_out_v'high);
      elsif (pred_control_r.refs_adjust_type = SUBS_BY_DEF_VAL) then
        refs_out_v(sample_idx) := def_val_v;
      end if;
    end loop;
    
    refs_prep_req_v := get_refs_prep_req(img_comp, pu_size_idx, mode_idx);
    
    smooth_check_req_v := '0';
    if (pu_size_idx = PU_32X32) then
      smooth_check_req_v := REFS_SMOOTH_AVAIL and refs_smooth_en and refs_prep_req_v;
    end if;
    
    smooth_req_v := smooth_check_req_v and smooth_control_r.smooth_terms(0) and smooth_control_r.smooth_terms(1);
    
    filt_req_v := refs_prep_req_v and not smooth_req_v;
    
    bound_ref_v := pred_control_r.last_left_ref;
    if (pred_control_r.reading_idx = 4) then
      bound_ref_v := pred_control_r.last_top_ref;
    end if;
    
    smooth_term_v := get_smooth_term(bit_depth_luma, pred_control_r.corner_top_left, refs_out_v(3), bound_ref_v);
    
    corner_planar_left_v := pred_control_r.corner_bottom_left;
    if (pred_control_r.reading_idx = 4) then
      corner_planar_left_v := pred_control_r.corner_top_right;
    end if;
    
    corner_planar_filt_v := get_filtered_sample(refs_out_v(0),  corner_planar_left_v, refs_out_v(1));
    
    corner_bottom_left_smooth_v := get_smoothed_planar_corner(pred_control_r.corner_top_left, pred_control_r.last_left_ref);
    corner_top_right_smooth_v := get_smoothed_planar_corner(pred_control_r.corner_top_left, pred_control_r.last_top_ref);
    
    pred_angle_v := get_pred_angle(mode_idx);
    
    if (mode_idx > 1) and (mode_idx /= 10) and (mode_idx < 18) then
      ang_hor_v := '1';
    else
      ang_hor_v := '0';
    end if;
    
    bidir_pred_v := '0';
    if ((mode_idx = 0) or ((mode_idx > 10) and (mode_idx < 26))) then
      bidir_pred_v := '1';
    end if;
    
    case pu_size_idx is
      when PU_4X4 =>
        readings_limit_v := conv_std_logic_vector(1, readings_limit_v'length);
        readings_dir_limit_v := conv_std_logic_vector(1, readings_dir_limit_v'length);
      when PU_8X8 =>
        readings_limit_v := conv_std_logic_vector(3, readings_limit_v'length);
        readings_dir_limit_v := conv_std_logic_vector(2, readings_dir_limit_v'length);
      when PU_16X16 =>
        readings_limit_v := conv_std_logic_vector(7, readings_limit_v'length);
        readings_dir_limit_v := conv_std_logic_vector(4, readings_dir_limit_v'length);
      when others =>
        readings_limit_v := conv_std_logic_vector(15, readings_limit_v'length);
        readings_dir_limit_v := conv_std_logic_vector(8, readings_dir_limit_v'length);
    end case;
    
    if (bidir_pred_v = '1') and (filt_req_v = '1')  then
      readings_limit_v := readings_limit_v + 2;
      readings_dir_limit_v := readings_dir_limit_v + 1;
    end if;
    
    reading_idx_planar_v := (others => '0');
    reading_idx_v := (others => '0');
    
    angle_mult2_v := pred_angle_v & '0';
    angle_mult4_v := pred_angle_v & "00";
    if (ang_hor_v = '1') then
      angle_hor_incr_v := pred_control_r.angle_accum + extend_signed_vector(pred_angle_v, angle_hor_incr_v'length);
      angle_ver_incr_v := extend_signed_vector(pred_angle_v, angle_ver_incr_v'length);
    else
      angle_hor_incr_v := pred_control_r.angle_accum;
      angle_ver_incr_v := pred_control_r.angle_accum + extend_signed_vector(angle_mult4_v, angle_hor_incr_v'length);
    end if;
    
    rec_width_limit_v := rec_width - 1;
    rec_height_limit_v := rec_height(6 downto 2) - 1;
    
    smooth_check_finished_v := '0';
    if (pred_control_r.reading_idx = 4) then
      smooth_check_finished_v := '1';
    end if;
    
    planar_fetch_finished_v := '0';
    if (smooth_req_v = '1') or ((pred_control_r.reading_idx = 2) and (refs_prep_req_v = '0')) or (pred_control_r.reading_idx = 4) then
      planar_fetch_finished_v := '1';
    end if;
    
    refs_fetch_finished_v := '0';
    if ((pred_control_r.reading_idx = 2) and (pu_size_idx = PU_4X4)) or (pred_control_r.reading_idx = 3) then
      refs_fetch_finished_v := '1';
    end if;
    
    reading_smooth_req_v := '0';
    reading_planar_corner_req_v := '0';
    reading_req_v := '0';
    
    storage_req_v := '0';
    
    accum_upd_req_v := '0';
    
    rec_left_storage_req_v := '0';
    rec_top_storage_req_v := '0';
    
    case pred_control_r.state is
      when IDLE =>
        
        pred_control_v.skip_pred := rec_samples_en;
        
        pred_control_v.col_idx := (others => '0');
        pred_control_v.row_idx := (others => '0');
        
        pred_control_v.reading_idx := (others => '0');
        pred_control_v.refs_adjust_type := (others => '0');
        pred_control_v.refs_read_dir := LEFT_DIR;
        pred_control_v.refs_read_pos := (others => '0');
        
        pred_control_v.corner_bottom_left := (others => '0');
        pred_control_v.corner_top_left := (others => '0');
        pred_control_v.corner_top_right := (others => '0');
        
        pred_control_v.dc_val := (others => '0');
        pred_control_v.dc_val(5 downto 0) := pu_size_v;
        
        pred_control_v.angle_accum := (others => '0');
        pred_control_v.angle_accum := extend_signed_vector(pred_angle_v, pred_control_r.angle_accum'length);
        
        smooth_control_v.smooth_terms := (others => '0');
        
        refs_pointer_v.neighbor := CORNER_NEIGHBOR;
        refs_pointer_v.position := (others => '0');
        
        if (rec_samples_en = '1') then
          pred_control_v.state := STORE_CORNERS;
        elsif (pred_params_en = '1') then
          pred_control_v.state := FETCH_CORNER;
        end if;
        
      when FETCH_CORNER =>
        
        pred_control_v.reading_idx := pred_control_r.reading_idx + 1;
        pred_control_v.corner_top_left := refs_out_v(conv_integer(pu_row_v(1 downto 0)));
        
        pred_control_v.last_left_ref := pred_control_v.corner_top_left;
        pred_control_v.last_top_ref := pred_control_v.corner_top_left;
        
        if (smooth_check_req_v = '1') then
          pred_control_v.state := CHECK_SMOOTH;
          reading_smooth_req_v := '1';
        elsif (mode_idx = 0) then
          pred_control_v.state := FETCH_PLANAR;
          reading_planar_corner_req_v := '1';
        elsif (mode_idx = 1) then
          pred_control_v.state := CALC_DC_VAL;
          reading_req_v := '1';
        else
          pred_control_v.state := FETCH_REFS;
          reading_req_v := '1';
        end if;
        
      when CHECK_SMOOTH =>
        
        pred_control_v.reading_idx := pred_control_r.reading_idx + 1;
        reading_smooth_req_v := not smooth_check_finished_v;
        
        case conv_integer(pred_control_r.reading_idx) is
          when 1| 2 =>
            smooth_control_v.smooth_terms(0) := smooth_term_v;
            pred_control_v.last_left_ref := refs_out_v(3);
          when others =>
            smooth_control_v.smooth_terms(1) := smooth_term_v;
            pred_control_v.last_top_ref := refs_out_v(3);
        end case;
        
        if (smooth_check_finished_v = '1') then
          pred_control_v.reading_idx := "00001";
          if (mode_idx = 0) then
            pred_control_v.state := FETCH_PLANAR;
            reading_planar_corner_req_v := '1';
          else
            pred_control_v.state := FETCH_REFS;
            reading_req_v := '1';
          end if;
        end if;
        
      when FETCH_PLANAR =>
        
        pred_control_v.reading_idx := pred_control_r.reading_idx + 1;
        reading_idx_planar_v := pred_control_r.reading_idx;
        reading_planar_corner_req_v := '1';
        
        case conv_integer(pred_control_r.reading_idx) is
          when 1 =>
            pred_control_v.corner_bottom_left := refs_out_v(0);
            if (refs_prep_req_v = '1') then
              pred_control_v.corner_bottom_left := refs_out_v(3);
            end if;
          when 2 =>
            pred_control_v.corner_top_right := refs_out_v(0);
            if (refs_prep_req_v = '1') then
              pred_control_v.corner_bottom_left := corner_planar_filt_v;
            end if;
          when 3 =>
            pred_control_v.corner_top_right := refs_out_v(3);
          when others =>
            pred_control_v.corner_top_right := corner_planar_filt_v;
        end case;
        
        if (smooth_req_v = '1') then
          pred_control_v.corner_bottom_left := corner_bottom_left_smooth_v;
          pred_control_v.corner_top_right := corner_top_right_smooth_v;
        end if;
        
        if (planar_fetch_finished_v = '1') then
          pred_control_v.state := FETCH_REFS;
          pred_control_v.reading_idx := "00001";
          reading_req_v := '1';
        end if;
        
      when CALC_DC_VAL =>
        
        reading_req_v := '1';
        storage_req_v := '1';
        
        reading_idx_v := pred_control_r.reading_idx;
        pred_control_v.reading_idx := pred_control_r.reading_idx + 1;
        
        pred_control_v.dc_val := pred_control_r.dc_val + refs_out_v(0) + refs_out_v(1) + refs_out_v(2) + refs_out_v(3);
        
        if (pred_control_r.reading_idx = readings_limit_v + 1) then
          pred_control_v.state := START_PRED;
        end if;
        
      when FETCH_REFS =>
        
        reading_req_v := '1';
        storage_req_v := '1';
        
        reading_idx_v := pred_control_r.reading_idx;
        pred_control_v.reading_idx := pred_control_r.reading_idx + 1;
        
        if (filt_req_v = '1')  then
          if (pred_control_r.reading_idx = 1) then
            pred_control_v.dc_val := (others => '0');
            pred_control_v.dc_val(8 + DELTA_BIT_DEPTH downto 1) := pred_control_r.corner_top_left;
            pred_control_v.dc_val := pred_control_v.dc_val + refs_out_v(0);
          elsif (refs_fetch_finished_v = '1') then
            pred_control_v.dc_val := pred_control_r.dc_val + refs_out_v(0) + 2;
            pred_control_v.corner_top_left := pred_control_v.dc_val(9 + DELTA_BIT_DEPTH downto 2);
          end if;
        end if;
        
        if (refs_fetch_finished_v = '1') then
          pred_control_v.state := START_PRED;
          pred_control_v.angle_accum := angle_hor_incr_v;
        end if;
        
      when START_PRED =>
        
        pred_control_v.state := CALC_PRED;
        pred_control_v.dc_val := shift_right_unsigned_vector(pred_control_v.dc_val, conv_integer(pu_size_log2_v + 1));
        
        pred_control_v.pred_samples_en := '1';
        
        accum_upd_req_v := '1';
        
        reading_idx_v := pred_control_r.reading_idx;
        
        if (pred_control_r.reading_idx /= readings_limit_v + 2) then
          reading_req_v := '1';
        end if;
        
        if (pred_control_r.reading_idx /= readings_limit_v + 2) then
          storage_req_v := '1';
        end if;
        
        if (pred_control_r.reading_idx /= readings_limit_v + 2) then
          pred_control_v.reading_idx := pred_control_r.reading_idx + 1;
        end if;
        
        pred_control_v.col_idx := pred_control_r.col_idx + 1;
        if (pred_control_r.col_idx = pu_size_m1_v) then
          pred_control_v.col_idx := (others => '0');
          pred_control_v.row_idx := pred_control_r.row_idx + 1;
        end if;
        
      when CALC_PRED =>
        
        pred_control_v.pred_samples_en := '1';
        
        accum_upd_req_v := pred_samples_ack;
        
        reading_idx_v := pred_control_r.reading_idx;
        
        if (pred_control_r.reading_idx /= readings_limit_v + 2) then
          reading_req_v := '1';
        end if;
        
        if (pred_control_r.reading_idx /= readings_limit_v + 2) then
          storage_req_v := '1';
        end if;
        
        if (pred_control_r.reading_idx /= readings_limit_v + 2) then
          pred_control_v.reading_idx := pred_control_r.reading_idx + 1;
        end if;
        
        if (pred_samples_ack = '1') then
          pred_control_v.col_idx := pred_control_r.col_idx + 1;
          if (pred_control_r.col_idx = pu_size_m1_v) then
            pred_control_v.col_idx := (others => '0');
            pred_control_v.row_idx := pred_control_r.row_idx + 1;
            if (pred_control_r.row_idx = pu_size_m1_v(5 downto 2)) then
              pred_control_v.state := STORE_REFS;
              if (corner_control_r.state /= FINISHED) then
                pred_control_v.state := STORE_CORNERS;
              end if;
              pred_control_v.row_idx := (others => '0');
            end if;
          end if;
        end if;
        
        if (pred_control_r.col_idx = pu_size_m1_v) and (pred_control_r.row_idx = pu_size_m1_v(5 downto 2)) then
          pred_params_ack_v := '1';
        end if;
        
      when STORE_CORNERS =>
        
        if (corner_control_r.state = FINISHED) then
          pred_control_v.state := STORE_REFS;
        end if;
        
      when STORE_REFS =>
        
        if (pred_control_r.col_idx /= rec_width_limit_v) or (pred_control_r.row_idx /= rec_height_limit_v) then
          rec_samples_ack_v := rec_samples_en;
        end if;
        
        if (pred_control_r.row_idx = rec_height_limit_v) and (pred_control_r.col_idx(1 downto 0) = 3) then
          rec_top_storage_req_v := rec_samples_en;
        elsif (pred_control_r.col_idx = rec_width_limit_v) then
          rec_left_storage_req_v := rec_samples_en;
        end if;
        
        if (rec_samples_en = '1') then
          pred_control_v.col_idx := pred_control_r.col_idx + 1;
          if (pred_control_r.col_idx = rec_width_limit_v) then
            pred_control_v.col_idx := (others => '0');
            pred_control_v.row_idx := pred_control_r.row_idx + 1;
            if (pred_control_r.row_idx = rec_height_limit_v) then
              pred_control_v.state := STORE_LAST_REFS_SET;
              pred_control_v.col_idx := pred_control_r.col_idx;
              pred_control_v.row_idx := pred_control_r.row_idx;
            end if;
          end if;
        end if;
        
        case conv_integer(pred_control_r.col_idx(1 downto 0)) is
          when 0 =>
           pred_control_v.corner_top_left := rec_samples(3);
          when 1 =>
            pred_control_v.corner_top_right := rec_samples(3);
          when 2 =>
            pred_control_v.corner_bottom_left := rec_samples(3);
          when others =>
            null;
        end case;
        
      when STORE_LAST_REFS_SET =>
        
        rec_left_storage_req_v := '1';
        
        rec_samples_ack_v := '1';
        pred_control_v.state := IDLE;
        pred_control_v.col_idx := (others => '0');
        pred_control_v.row_idx := (others => '0');
        
      when others =>
        null;
    end case;
    
    if (reading_smooth_req_v = '1') then
      case conv_integer(pred_control_r.reading_idx) is
        when 0 =>
          refs_pointer_v.neighbor := LEFT_NEIGHBOR;
          refs_pointer_v.position := "00111";
        when 1 =>
          refs_pointer_v.neighbor := LEFT_NEIGHBOR;
          refs_pointer_v.position := "01111";
        when 2 =>
          refs_pointer_v.neighbor := TOP_NEIGHBOR;
          refs_pointer_v.position := "00111";
        when others =>
          refs_pointer_v.neighbor := TOP_NEIGHBOR;
          refs_pointer_v.position := "01111";
      end case;
    end if;
    
    if (reading_planar_corner_req_v = '1') then
      case conv_integer(reading_idx_planar_v) is
        when 0 =>
          refs_pointer_v.neighbor := LEFT_NEIGHBOR;
          refs_pointer_v.position := '0' & pu_size_v(5 downto 2);
          if (filt_req_v = '1') then
            refs_pointer_v.position := '0' & pu_size_m1_v(5 downto 2);
          end if;
        when 1 =>
          refs_pointer_v.neighbor := TOP_NEIGHBOR;
          if (filt_req_v = '1') then
            refs_pointer_v.neighbor := LEFT_NEIGHBOR;
          end if;
          refs_pointer_v.position := '0' & pu_size_v(5 downto 2);
        when 2 =>
          refs_pointer_v.neighbor := TOP_NEIGHBOR;
          refs_pointer_v.position := '0' & pu_size_m1_v(5 downto 2);
        when others =>
          refs_pointer_v.neighbor := TOP_NEIGHBOR;
          refs_pointer_v.position := '0' & pu_size_v(5 downto 2);
      end case;
    end if;
    
    if (pu_size_idx = PU_4X4) then
      refs_left_start_num_v := conv_std_logic_vector(1, refs_left_start_num_v'length);
    else
      refs_left_start_num_v := conv_std_logic_vector(2, refs_left_start_num_v'length);
    end if;
    
    if (reading_req_v = '1') and (reading_idx_v <= readings_limit_v) then
      if (mode_idx > 1) and (mode_idx < 10) then
        refs_pointer_v.neighbor := LEFT_NEIGHBOR;
        refs_pointer_v.position := '0' & reading_idx_v(3 downto 0);
      elsif (mode_idx > 26) then
        refs_pointer_v.neighbor := TOP_NEIGHBOR;
        refs_pointer_v.position := '0' & reading_idx_v(3 downto 0);
      elsif (reading_idx_v < refs_left_start_num_v) then
        refs_pointer_v.neighbor := LEFT_NEIGHBOR;
        refs_pointer_v.position := reading_idx_v(refs_pointer_v.position'range);
      elsif (reading_idx_v - refs_left_start_num_v < readings_dir_limit_v) then
        refs_pointer_v.neighbor := TOP_NEIGHBOR;
        refs_pointer_v.position := extend_unsigned_vector(reading_idx_v(3 downto 0), refs_pointer_v.position'length) - refs_left_start_num_v;
      else
        refs_pointer_v.neighbor := LEFT_NEIGHBOR;
        refs_pointer_v.position := (others => '0');
        refs_pointer_v.position(3 downto 0) := reading_idx_v(3 downto 0) - readings_dir_limit_v(3 downto 0);
      end if;
    end if;
    
    if (accum_upd_req_v = '1') then
      pred_control_v.angle_accum := angle_hor_incr_v;
      if (pred_control_r.col_idx + 1 = pu_size_m1_v) then
        pred_control_v.angle_accum := angle_ver_incr_v;
      end if;
    end if;
    
    if (rec_left_storage_req_v = '1') then
      refs_in_v := rec_samples;
      refs_addr_in_v := base_addr_left_v + pred_control_r.row_idx;
      refs_data_en_v := '0';
    end if;
    
    if (rec_top_storage_req_v = '1') then
      refs_in_v(0) := pred_control_r.corner_top_left;
      refs_in_v(1) := pred_control_r.corner_top_right;
      refs_in_v(2) := pred_control_r.corner_bottom_left;
      refs_in_v(3) := rec_samples(3);
      refs_addr_in_v := base_addr_top_v + pred_control_r.col_idx(pred_control_r.col_idx'high downto 2);
      refs_data_en_v := '0'; 
    end if;
    
    pred_control_v.refs_read_dir := LEFT_DIR;
    if (refs_pointer_v.neighbor = TOP_NEIGHBOR) then
      pred_control_v.refs_read_dir := TOP_DIR;
    end if;
    pred_control_v.refs_read_pos := refs_pointer_v.position(3 downto 0);
    
    adjust_pointer_to_pu_avail(pu_avail, pu_row_v, pu_size_m1_v, refs_pointer_v, pred_control_v.refs_adjust_type);
    
    reading_corner_req_v := '0';
    if (pred_control_r.state /= IDLE) and (corner_control_r.state /= FINISHED) then
      reading_corner_req_v := not (reading_smooth_req_v or reading_planar_corner_req_v or reading_req_v);
    end if;
    
    rec_width_m1_v := rec_width - 1;
    rec_height_m1_v := rec_height - 1;
    
    corner_left_storage_req_v := pu_avail(BOTTOM_LEFT_AVAIL) or pred_control_r.skip_pred;
    
    corner_left_pos_v := '0' & pu_size_m1_v(5 downto 2);
    if (pred_control_r.skip_pred = '1') then
      corner_left_pos_v := rec_height_m1_v(6 downto 2);
    end if;
    
    corner_top_pos_v := '0' & pu_size_m1_v(5 downto 2);
    if (pred_control_r.skip_pred = '1') then
      corner_top_pos_v := rec_width_m1_v(6 downto 2);
    end if;
    
    corner_base_pos_v := pu_row_v;
    if (pred_control_r.skip_pred = '1') then
      corner_base_pos_v := get_pu_row(rec_addr_y, cu_size);
    end if;
    
    corner_ext_pos_v := '0' & pu_size_v(5 downto 2);
    if (pred_control_r.skip_pred = '1') then
      corner_ext_pos_v := rec_height(6 downto 2);
    end if;
    
    corner_idx_v := "00" & corner_base_pos_v(1 downto 0);
    
    corner_upd_req_v := '0';
    corner_storage_req_v := '0';
    case corner_control_r.state is
      when IDLE =>
        
        corner_control_v.avail := '1';
        if (reading_corner_req_v = '1') then
          corner_control_v.state := READ_TOP_CORNER;
          refs_pointer_v.neighbor := TOP_NEIGHBOR;
          refs_pointer_v.position := corner_top_pos_v;
        end if;
        
      when READ_TOP_CORNER =>
        
        corner_upd_req_v := corner_control_r.avail;
        corner_control_v.avail := reading_corner_req_v;
        if (reading_corner_req_v = '1') then
          corner_control_v.state := STORE_TOP_CORNER;
          refs_pointer_v.neighbor := CORNER_NEIGHBOR;
          refs_pointer_v.position := (others => '0');
        end if;
        
      when STORE_TOP_CORNER =>
        
        corner_storage_req_v := corner_control_r.avail;
        
        corner_control_v.avail := reading_corner_req_v;
        if (corner_left_storage_req_v = '0') then
          corner_control_v.state := FINISHED;
        elsif (reading_corner_req_v = '1') then
          corner_control_v.state := READ_LEFT_CORNER;
          refs_pointer_v.neighbor := LEFT_NEIGHBOR;
          refs_pointer_v.position := corner_left_pos_v;
        end if;
        
      when READ_LEFT_CORNER =>
        
        corner_upd_req_v := corner_control_r.avail;
        corner_idx_v := corner_idx_v + corner_ext_pos_v(3 downto 0);
        
        corner_control_v.avail := reading_corner_req_v;
        if (reading_corner_req_v = '1') then
          corner_control_v.state := STORE_LEFT_CORNER;
          refs_pointer_v.neighbor := CORNER_NEIGHBOR;
          refs_pointer_v.position := "000" & corner_idx_v(3 downto 2);
        end if;
        
      when STORE_LEFT_CORNER =>
        
        corner_storage_req_v := corner_control_r.avail;
        corner_idx_v := corner_idx_v + corner_ext_pos_v(3 downto 0);
        
        corner_control_v.state := FINISHED;
        corner_control_v.avail := '0';
        
      when FINISHED =>
        
        if (pred_control_v.state = IDLE) then
          corner_control_v.state := IDLE;
        end if;
        
      when others =>
        null;
    end case;
    
    if (corner_upd_req_v = '1') then
      corner_control_v.corner := refs_out_v(3);
    end if;
    
    if (corner_storage_req_v = '1') then
      refs_in_v(conv_integer(corner_idx_v(1 downto 0))) := corner_control_r.corner;
      refs_addr_in_v := base_addr_corner_v + corner_idx_v(3 downto 2);
      refs_data_en_v := '0';
    end if;
    
    bound_ref_v := pred_control_r.last_left_ref;
    if (pred_control_r.refs_read_dir = TOP_DIR) then
      bound_ref_v := pred_control_r.last_top_ref;
    end if;
    
    refs_smooth_offset_v := get_smoothed_sample_offset(pred_control_r.refs_read_pos, pred_control_r.corner_top_left, bound_ref_v);
    for sample_idx in 0 to 3 loop
      sample_idx_v := conv_std_logic_vector(sample_idx, 2);
      refs_smooth_v(sample_idx) := get_smoothed_sample(sample_idx_v, refs_smooth_offset_v, pred_control_r.corner_top_left, bound_ref_v);
    end loop;
    
    storage_pos_v := pred_control_r.refs_read_pos & "00";
    storage_pos_v := storage_pos_v - 1;
    if (pred_control_r.refs_read_dir = TOP_DIR) then
      storage_pos_v := not storage_pos_v;
    end if;
    
    ref_last_v := pred_control_r.refs_buffer(conv_integer(storage_pos_v));
    if (pred_control_r.refs_read_pos = 0) then
      ref_last_v := pred_control_r.corner_top_left;
    end if;
    
    refs_filt_v(0) := get_filtered_sample(refs_out_v(0), ref_last_v, refs_out_v(1));
    refs_filt_v(1) := get_filtered_sample(refs_out_v(1), refs_out_v(0), refs_out_v(2));
    refs_filt_v(2) := get_filtered_sample(refs_out_v(2), refs_out_v(1), refs_out_v(3));
    refs_filt_v(3) := refs_out_v(3);
    
    ref_second_to_last_v := pred_control_r.last_left_ref;
    if (pred_control_r.refs_read_dir = TOP_DIR) then
      ref_second_to_last_v := pred_control_r.last_top_ref;
    end if;
    
    ref_selected_v := get_filtered_sample(ref_last_v, ref_second_to_last_v, refs_out_v(0));
    if (storage_req_v = '1') and (filt_req_v = '1') and (pred_control_r.refs_read_pos /= 0) then
      pred_control_v.refs_buffer(conv_integer(storage_pos_v)) := ref_selected_v;
    end if;
    
    if (bidir_pred_v = '1') and (filt_req_v = '1') and ((pred_control_r.reading_idx = readings_dir_limit_v + 2) or
       (pred_control_r.reading_idx = readings_limit_v + 1)) then
      storage_req_v := '0';
    end if;
    
    if (smooth_req_v = '1') then
      refs_to_be_stored_v := refs_smooth_v;
    elsif (filt_req_v = '1') then
      refs_to_be_stored_v := refs_filt_v;
    else
      refs_to_be_stored_v := refs_out_v;
    end if;
    
    for sample_idx in 0 to 3 loop
      
      storage_pos_v := pred_control_r.refs_read_pos & conv_std_logic_vector(sample_idx, 2);
      if (pred_control_r.refs_read_dir = TOP_DIR) then
        storage_pos_v := not storage_pos_v;
      end if;
      
      if (storage_req_v = '1') then
        pred_control_v.refs_buffer(conv_integer(storage_pos_v)) := refs_to_be_stored_v(sample_idx);
      end if;
      
    end loop;
    
    if (storage_req_v = '1') and (filt_req_v = '1') then
      if (pred_control_r.refs_read_dir = LEFT_DIR) then
        pred_control_v.last_left_ref := refs_out_v(2);
      else
        pred_control_v.last_top_ref := refs_out_v(2);
      end if;
    end if;
    
    col_idx_next_v := (others => '0');
    row_idx_next_v := (others => '0');
    if (pred_control_r.state = START_PRED) or (pred_control_r.state = CALC_PRED) then
      col_idx_next_v := pred_control_r.col_idx + 1;
      row_idx_next_v := pred_control_r.row_idx;
      if (pred_control_r.col_idx = pu_size_m1_v) then
        col_idx_next_v := (others => '0');
        row_idx_next_v := pred_control_r.row_idx + 1;
      end if;
    end if;
    
    refs_corner_v := (others => '0');
    for sample_idx in 0 to 3 loop
      sample_idx_v := conv_std_logic_vector(sample_idx, 2);
      refs_source_v(2 * sample_idx) := '0' & row_idx_next_v(2 downto 0) & sample_idx_v;
      refs_source_v(2 * sample_idx + 1) := '0' & col_idx_next_v(4 downto 0);
      refs_source_v(2 * sample_idx + 1) := not (refs_source_v(2 * sample_idx + 1));
    end loop;
    
    angle_incr_v(0) := (others => '0');
    angle_incr_v(1) := extend_signed_vector(pred_angle_v, angle_incr_v(1)'length);
    angle_incr_v(2) := extend_signed_vector(angle_mult2_v, angle_incr_v(2)'length);
    angle_incr_v(3) := extend_signed_vector(angle_mult2_v, angle_incr_v(3)'length) + extend_signed_vector(pred_angle_v, angle_incr_v(3)'length);
    
    for sample_idx in 0 to 3 loop
      
      sample_idx_v := conv_std_logic_vector(sample_idx, 2);
      
      angle_accum_v := pred_control_v.angle_accum;
      if (ang_hor_v = '0') then
        angle_accum_v := pred_control_v.angle_accum + angle_incr_v(sample_idx);
      end if;
      
      pred_control_v.refs_idxs(sample_idx) := angle_accum_v(11 downto 5);
      pred_control_v.refs_idxs(sample_idx) := pred_control_v.refs_idxs(sample_idx) + 1;
      
    end loop;
    
    ang_refs_corner_v := (others => '0');
    for sample_idx in 0 to 3 loop
      
      sample_idx_v := conv_std_logic_vector(sample_idx, 2);
      
      for ref_idx in 0 to 1 loop
        
        ang_ref_idx_v := pred_control_r.refs_idxs(sample_idx) + ref_idx;
        if (ang_hor_v = '1') then
          ang_ref_idx_v := ang_ref_idx_v + (row_idx_next_v(2 downto 0) & sample_idx_v);
        else
          ang_ref_idx_v := ang_ref_idx_v + col_idx_next_v(4 downto 0);
        end if;
        
        ang_pos_offset_v := get_ang_ref_pos(mode_idx, ang_ref_idx_v);
        
        ang_pos_left_v := ang_pos_offset_v - 1;
        
        ang_pos_top_v := (others => '0');
        ang_pos_top_v := ang_pos_top_v - ang_pos_offset_v;
        
        if (ang_ref_idx_v = 0) then
          ang_refs_corner_v(2 * sample_idx + ref_idx) := '1';
        end if;
        
        if ((mode_idx > 10) and (ang_ref_idx_v(ang_ref_idx_v'high) = ang_hor_v)) or (mode_idx > 26) then
          ang_refs_source_v(2 * sample_idx + ref_idx) := ang_pos_top_v(5 downto 0);
        else
          ang_refs_source_v(2 * sample_idx + ref_idx) := ang_pos_left_v(5 downto 0);
        end if;
        
      end loop;
    end loop;
    
    if (mode_idx = 0) or (mode_idx = 1) or (mode_idx = 10) or (mode_idx = 26) then
      pred_control_v.refs_corner := refs_corner_v;
      pred_control_v.refs_source := refs_source_v;
    else
      pred_control_v.refs_corner := ang_refs_corner_v;
      pred_control_v.refs_source := ang_refs_source_v;
    end if;
    
    for sample_idx in refs_in_v'low to refs_in_v'high loop
      refs_data_in_v((sample_idx + 1) * (8 + DELTA_BIT_DEPTH) - 1 downto sample_idx * (8 + DELTA_BIT_DEPTH)) := refs_in_v(sample_idx);
    end loop;
    
    refs_addr_out_v := (others => '0');
    if (refs_pointer_v.neighbor = CORNER_NEIGHBOR) then
      refs_addr_out_v := base_addr_corner_v;
    elsif (refs_pointer_v.neighbor = LEFT_NEIGHBOR) then
      refs_addr_out_v := base_addr_left_v;
    elsif (refs_pointer_v.neighbor = TOP_NEIGHBOR) then
      refs_addr_out_v := base_addr_top_v;
    end if;
    refs_addr_out_v := refs_addr_out_v + refs_pointer_v.position;
    
    for sample_idx in 0 to 7 loop
      pred_control_v.pred_buffer(sample_idx) := '0' & pred_control_r.refs_buffer(conv_integer(pred_control_r.refs_source(sample_idx)));
      if (pred_control_r.refs_corner(sample_idx) = '1') then
        pred_control_v.pred_buffer(sample_idx) := '0' & pred_control_r.corner_top_left;
      end if;
    end loop;
    
    for sample_idx in 0 to 3 loop
      pred_control_v.pred_offsets(sample_idx) := conv_std_logic_vector(1, pred_control_v.pred_offsets(sample_idx)'length);
      pred_control_v.pred_offsets(sample_idx) := pred_control_v.pred_offsets(sample_idx) + pred_control_v.pred_buffer(2 * sample_idx) +
                                                 pred_control_v.pred_buffer(2 * sample_idx + 1);
    end loop;
    
    corner_top_right_sgn_v := '0' & pred_control_r.corner_top_right;
    corner_bottom_left_sgn_v := '0' & pred_control_r.corner_bottom_left;
    
    if (mode_idx = 0) then
      for sample_idx in 0 to 3 loop
        pred_control_v.pred_buffer(2 * sample_idx) := corner_top_right_sgn_v - pred_control_v.pred_buffer(2 * sample_idx);
        pred_control_v.pred_buffer(2 * sample_idx + 1) := corner_bottom_left_sgn_v - pred_control_v.pred_buffer(2 * sample_idx + 1);
      end loop;
    end if;
    
    max_pred_val_v := (others => '1');
    
    for sample_idx in 0 to 7 loop
      pred_buffer_v(sample_idx) := pred_control_r.pred_buffer(sample_idx)(7 + DELTA_BIT_DEPTH downto 0);
    end loop;
    
    process_corner_v := '0';
    if (img_comp = LUMA) and (pu_size_idx /= PU_32X32) and (pred_control_r.col_idx_dly = 0) and (pred_control_r.row_idx_dly = 0) then
      process_corner_v := '1';
    end if;
    
    process_left_edge_v := '0';
    if (img_comp = LUMA) and (pu_size_idx /= PU_32X32) and (pred_control_r.col_idx_dly = 0) then
      process_left_edge_v := '1';
    end if;
    
    process_top_edge_v := '0';
    if (img_comp = LUMA) and (pu_size_idx /= PU_32X32) and (pred_control_r.row_idx_dly = 0) then
      process_top_edge_v := '1';
    end if;
    
    dc_corner_pred_v := get_dc_corner_pred(process_corner_v, pred_control_r.dc_val(7 + DELTA_BIT_DEPTH downto 0), pred_buffer_v(0), pred_buffer_v(1));
    dc_top_edge_pred_v := get_dc_edge_pred(process_top_edge_v, pred_control_r.dc_val(7 + DELTA_BIT_DEPTH downto 0), pred_buffer_v(1));
    dc_left_edge_pred_v := get_dc_edge_pred(process_left_edge_v, pred_control_r.dc_val(7 + DELTA_BIT_DEPTH downto 0), pred_buffer_v(0));
    
    if (pred_control_r.col_idx_dly = 0) and (pred_control_r.row_idx_dly = 0) then
      dc_pred_v(0) := dc_corner_pred_v;
    elsif (pred_control_r.col_idx_dly = 0) then
      dc_pred_v(0) := dc_left_edge_pred_v;
    elsif (pred_control_r.row_idx_dly = 0) then
      dc_pred_v(0) := dc_top_edge_pred_v;
    else
      dc_pred_v(0) := pred_control_r.dc_val(7 + DELTA_BIT_DEPTH downto 0);
    end if;
    
    for sample_idx in 1 to 3 loop
      dc_left_edge_pred_v := get_dc_edge_pred(process_left_edge_v, pred_control_r.dc_val(7 + DELTA_BIT_DEPTH downto 0), pred_buffer_v(2 * sample_idx));
      if (pred_control_r.col_idx_dly = 0) then
        dc_pred_v(sample_idx) := dc_left_edge_pred_v;
      else
        dc_pred_v(sample_idx) := pred_control_r.dc_val(7 + DELTA_BIT_DEPTH downto 0);
      end if;
    end loop;
    
    if (pred_control_r.row_idx_dly = 0) then
      hor_pred_v(0) := get_linear_edge_pred(process_top_edge_v, pred_buffer_v(0), pred_control_r.corner_top_left, pred_buffer_v(1), max_pred_val_v);
    else
      hor_pred_v(0) := pred_buffer_v(0);
    end if;
    
    for sample_idx in 1 to 3 loop
      hor_pred_v(sample_idx) := pred_buffer_v(2 * sample_idx);
    end loop;
    
    for sample_idx in 0 to 3 loop
      if (pred_control_r.col_idx_dly = 0) then
        ver_pred_v(sample_idx) := get_linear_edge_pred(process_left_edge_v, pred_buffer_v(2 * sample_idx + 1), pred_control_r.corner_top_left,
                                                       pred_buffer_v(2 * sample_idx), max_pred_val_v);
      else
        ver_pred_v(sample_idx) := pred_buffer_v(2 * sample_idx + 1);
      end if;
    end loop;
    
    for weight_idx in 0 to 3 loop
      planar_weights_v(2 * weight_idx) := '0' & pred_control_r.col_idx(4 downto 0);
      planar_weights_v(2 * weight_idx) := planar_weights_v(2 * weight_idx) + 1;
      planar_weights_v(2 * weight_idx + 1) := pred_control_r.row_idx(3 downto 0) & "01";
      planar_weights_v(2 * weight_idx + 1) := planar_weights_v(2 * weight_idx + 1) + weight_idx;
    end loop;
    
    for weight_idx in 0 to 3 loop
      ang_weights_v(2 * weight_idx + 1) := '0' & pred_control_r.ang_weight;
    end loop;
    
    if (ang_hor_v = '0') then
      ang_weights_v(3)(4 downto 0) := pred_control_r.ang_weight + pred_angle_v(4 downto 0);
      ang_weights_v(5)(4 downto 0) := pred_control_r.ang_weight + shift_left_vector(pred_angle_v(4 downto 0), 1);
      ang_weights_v(7)(4 downto 0) := pred_control_r.ang_weight + pred_angle_v(4 downto 0) + shift_left_vector(pred_angle_v(4 downto 0), 1);
    end if;
    
    for weight_idx in 0 to 3 loop
      ang_weights_v(2 * weight_idx) := conv_std_logic_vector(32, ang_weights_v(2 * weight_idx)'length);
      ang_weights_v(2 * weight_idx) := ang_weights_v(2 * weight_idx) - ang_weights_v(2 * weight_idx + 1);
    end loop;
    
    if (mode_idx = 0) then
      pred_control_v.pred_weights := planar_weights_v;
    else
      pred_control_v.pred_weights := ang_weights_v;
    end if;
    
    for sample_idx in 0 to 3 loop
      if (mode_idx = 0) then
        pred_offsets_v(sample_idx) := (others => '0');
        pred_offsets_v(sample_idx)(8 + DELTA_BIT_DEPTH downto 0) := pred_control_r.pred_offsets(sample_idx);
        pred_offsets_v(sample_idx) := shift_left_vector(pred_offsets_v(sample_idx), conv_integer(pu_size_log2_v));
      else
        pred_offsets_v(sample_idx) := conv_std_logic_vector(16, pred_offsets_v(sample_idx)'length);
      end if;
    end loop;
    
    for sample_idx in 0 to 3 loop
      mult_results_v(sample_idx) := mult_signed_factor_by_weight(pred_control_r.pred_buffer(2 * sample_idx), pred_control_r.pred_weights(2 * sample_idx)) +
                                    mult_signed_factor_by_weight(pred_control_r.pred_buffer(2 * sample_idx + 1), pred_control_r.pred_weights(2 * sample_idx + 1)) +
                                    pred_offsets_v(sample_idx);
    end loop;
    
    for sample_idx in 0 to 3 loop
      planar_pred_v(sample_idx) := shift_right_unsigned_vector(mult_results_v(sample_idx), conv_integer(pu_size_log2_v + 1))(7 + DELTA_BIT_DEPTH downto 0);
    end loop;
    
    for sample_idx in 0 to 3 loop
      ang_pred_v(sample_idx) := mult_results_v(sample_idx)(12 + DELTA_BIT_DEPTH downto 5);
    end loop;
    
    case conv_integer(mode_idx) is
      when 0 =>
        pred_samples_v := planar_pred_v;
      when 1 =>
        pred_samples_v := dc_pred_v;
      when 10 =>
        pred_samples_v := hor_pred_v;
      when 26 =>
        pred_samples_v := ver_pred_v;
      when others =>
        pred_samples_v := ang_pred_v;
    end case;
    
    refs_data_in <= refs_data_in_v;
    refs_addr_in <= refs_addr_in_v;
    refs_data_en <= not refs_data_en_v;
    
    refs_addr_out <= refs_addr_out_v;
    
    rec_samples_ack <= rec_samples_ack_v;
    
    pred_samples <= pred_samples_v;
    
    pred_params_ack <= pred_params_ack_v;
    
    smooth_control_next <= smooth_control_v;
    corner_control_next <= corner_control_v;
    pred_control_next <= pred_control_v;
  end process;
  
  pred_samples_en <= pred_control_r.pred_samples_en;
  
  REGISTER_PROCESS: process(rst_n, clk)
  begin
    if (rst_n = '0') then
      
      pred_control_r.state <= IDLE;
      pred_control_r.skip_pred <= '0';
      pred_control_r.col_idx <= (others => '0');
      pred_control_r.row_idx <= (others => '0');
      pred_control_r.reading_idx <= (others => '0');
      pred_control_r.refs_adjust_type <= (others => '0');
      pred_control_r.refs_read_dir <= '0';
      pred_control_r.refs_read_pos <= (others => '0');
      pred_control_r.corner_top_left <= (others => '0');
      pred_control_r.corner_bottom_left <= (others => '0');
      pred_control_r.corner_top_right <= (others => '0');
      pred_control_r.dc_val <= (others => '0');
      pred_control_r.last_left_ref <= (others => '0');
      pred_control_r.last_top_ref <= (others => '0');
      pred_control_r.angle_accum <= (others => '0');
      pred_control_r.ang_weight <= (others => '0');
      pred_control_r.refs_corner <= (others => '0');
      pred_control_r.col_idx_dly <= (others => '0');
      pred_control_r.row_idx_dly <= (others => '0');
      
      for i in 0 to 63 loop
        pred_control_r.refs_buffer(i) <= (others => '0');
      end loop;
      
      for i in 0 to 3 loop
        pred_control_r.refs_idxs(i) <= (others => '0');
      end loop;
      
      for i in 0 to 7 loop
        pred_control_r.refs_source(i) <= (others => '0');
        pred_control_r.pred_buffer(i) <= (others => '0');
        pred_control_r.pred_weights(i) <= (others => '0');
      end loop;
      
      corner_control_r.state <= IDLE;
      corner_control_r.avail <= '0';
      corner_control_r.corner <= (others => '0');
      
      if (REFS_SMOOTH_AVAIL = '1') then
        smooth_control_r.smooth_terms <= (others => '0');
      end if;
      
    elsif (clk = '1') and clk'event then
      pred_control_r <= pred_control_next;
      corner_control_r <= corner_control_next;
      if (REFS_SMOOTH_AVAIL = '1') then
        smooth_control_r <= smooth_control_next;
      end if;
    end if;
  end process;
  
  REFS_MEM: RAMT
    generic map (BIT_WIDTH => REFS_MEM_DATA_WIDTH,
                 ADDR_WIDTH => REFS_MEM_ADDR_WIDTH)
    port map    (clk(0) => clk, 
                 clk(1) => clk,
                 data_a => refs_data_in,
                 address_a => refs_addr_in,
                 wren => refs_data_en,
                 q_b => refs_data_out,
                 address_b => refs_addr_out);
  
end architecture rtl;