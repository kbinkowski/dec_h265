library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.motion_compensation_dec_pkg.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;  


entity weighted_prediction_test is
end weighted_prediction_test;


architecture test_arch of weighted_prediction_test is 

file wp_in  : text open read_mode is "/test/wp_in.txt";   
file wp_out : text open write_mode is "/test/wp_out.txt";

component weighted_prediction is  
  port(
  clk                  : in  std_logic;
  rst                  : in  std_logic;  --Reset, active LOW 
  
  weight0              : in  std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset0              : in  std_logic_vector(LOG_OFFSET-1 downto 0);
  weight1              : in  std_logic_vector(LOG_WEIGHT-1 downto 0);
  offset1              : in  std_logic_vector(LOG_OFFSET-1 downto 0);
  
  log_weight_denom     : in  std_logic_vector(2 downto 0);
  weighted_pred_flag   : in  std_logic;  
  bi_pred_flag         : in  std_logic;
  
  input_samples0       : in  INTERPOLATED_SAMPLES_TYPE;
  input_samples1       : in  INTERPOLATED_SAMPLES_TYPE;
  
  output_samples       : out DATA_SAMPLES_DEC_TYPE
  );
end component;

signal clk_gen         : std_logic :='0';
signal rst_gen         : std_logic; 
signal start_gen       : std_logic :='0';

signal interpolated_samples0_r    : INTERPOLATED_SAMPLES_TYPE;
signal interpolated_samples0_next : INTERPOLATED_SAMPLES_TYPE;
signal interpolated_samples1_r    : INTERPOLATED_SAMPLES_TYPE;
signal interpolated_samples1_next : INTERPOLATED_SAMPLES_TYPE;
signal weighted_pred_flag_r       : std_logic;
signal weighted_pred_flag_next    : std_logic;  
signal bi_pred_flag_r             : std_logic;
signal bi_pred_flag_next          : std_logic;
signal weight0_r                  : std_logic_vector(LOG_WEIGHT-1 downto 0);
signal weight0_next               : std_logic_vector(LOG_WEIGHT-1 downto 0);
signal offset0_r                  : std_logic_vector(LOG_OFFSET-1 downto 0); 
signal offset0_next               : std_logic_vector(LOG_OFFSET-1 downto 0); 
signal weight1_r                  : std_logic_vector(LOG_WEIGHT-1 downto 0);
signal weight1_next               : std_logic_vector(LOG_WEIGHT-1 downto 0);
signal offset1_r                  : std_logic_vector(LOG_OFFSET-1 downto 0); 
signal offset1_next               : std_logic_vector(LOG_OFFSET-1 downto 0); 
signal log_weight_denom_r         : std_logic_vector(2 downto 0);
signal log_weight_denom_next      : std_logic_vector(2 downto 0); 
signal pred_samples_gen           : DATA_SAMPLES_DEC_TYPE; 

signal input_dav_r                : std_logic;
signal input_dav_next             : std_logic; 
signal input_dav_delay1_r         : std_logic;
signal input_dav_delay1_next      : std_logic;
signal input_dav_delay2_r         : std_logic;
signal input_dav_delay2_next      : std_logic;
signal output_dav_r               : std_logic; 
signal output_dav_next            : std_logic;

signal counter_r                  : std_logic_vector(1 downto 0);
signal counter_next               : std_logic_vector(1 downto 0);

begin 

WP_MODULE: weighted_prediction  
  port map(
  clk => clk_gen,
  rst => rst_gen,   
  weight0 => weight0_r,
  offset0 => offset0_r,
  weight1 => weight1_r,
  offset1 => offset1_r,
  
  log_weight_denom => log_weight_denom_r,
  weighted_pred_flag => weighted_pred_flag_r,  
  bi_pred_flag => bi_pred_flag_r,
  
  input_samples0 => interpolated_samples0_r,
  input_samples1 => interpolated_samples1_r,
  
  output_samples => pred_samples_gen
); 	

CLK_GENERATOR: process
begin
  wait for 2 ns;
  clk_gen <= not clk_gen;
end process;  


STIMULUS_PROC: process
begin
  rst_gen <= '1';
  wait for 4 ns;
  rst_gen <= '0';
  wait for 4 ns;
  rst_gen <= '1'; 
  start_gen <= '1';
  wait for 1 ms;
  assert FALSE
  report "END OF SIMULATION"
  severity FAILURE;
end process;  	 

PROC_DAV: process (input_dav_r, input_dav_delay1_r, input_dav_delay2_r, weighted_pred_flag_r)
begin	   
  if weighted_pred_flag_r = '1' then
    input_dav_delay1_next <= input_dav_r;
	input_dav_delay2_next <= input_dav_delay1_r;
	output_dav_next       <= input_dav_delay2_r;
  else
    input_dav_delay1_next <= input_dav_r;
	input_dav_delay2_next <= input_dav_delay1_r;
	output_dav_next       <= input_dav_delay1_r;
  end if;
end process;

--Process of read data from files
PROC_READ: process(clk_gen) 
variable interpolated_samples0_v    : INTERPOLATED_SAMPLES_TYPE;
variable interpolated_samples1_v    : INTERPOLATED_SAMPLES_TYPE;
variable weighted_pred_flag_v       : std_logic;
variable bi_pred_flag_v             : std_logic;
variable weight0_v                  : std_logic_vector(LOG_WEIGHT-1 downto 0);
variable offset0_v                  : std_logic_vector(LOG_OFFSET-1 downto 0);
variable weight1_v                  : std_logic_vector(LOG_WEIGHT-1 downto 0);
variable offset1_v                  : std_logic_vector(LOG_OFFSET-1 downto 0); 
variable log_weight_denom_v         : std_logic_vector(2 downto 0); 
variable integer_v                  : integer;
variable bit_v                      : std_logic;
variable line_v                     : line;	 
variable input_dav_v                : std_logic; 
variable counter_v                  : std_logic_vector(1 downto 0);

begin                
  interpolated_samples0_v := interpolated_samples0_r; 
  interpolated_samples1_v := interpolated_samples1_r;
  weighted_pred_flag_v := weighted_pred_flag_r; 
  bi_pred_flag_v := bi_pred_flag_r; 
  weight0_v := weight0_r;
  offset0_v := offset0_r;	
  weight1_v := weight1_r;
  offset1_v := offset1_r;
  log_weight_denom_v := log_weight_denom_r;
  input_dav_v := input_dav_r;
  counter_v := counter_r;
  
  if falling_edge(clk_gen) and start_gen = '1' then  
	  
	  counter_v   := std_logic_vector( unsigned(counter_r) + 1 );
	  input_dav_v := '0'; 
	  
	  if counter_r = "00" then
        
        if not endfile(wp_in) then 
		  -- INPUT
    	  input_dav_v := '1';
          readline(wp_in, line_v);  
          read(line_v, bit_v);
          weighted_pred_flag_v := bit_v;
		  read(line_v, bit_v);
          bi_pred_flag_v := bit_v;
          read(line_v, integer_v);   
          weight0_v := std_logic_vector(to_signed(integer_v, weight0_v'length));
          read(line_v, integer_v);   
          weight1_v := std_logic_vector(to_signed(integer_v, weight1_v'length));    
          read(line_v, integer_v);   
          offset0_v := std_logic_vector(to_signed(integer_v, offset0_v'length));     
          read(line_v, integer_v);   
          offset1_v := std_logic_vector(to_signed(integer_v, offset1_v'length));
          read(line_v, integer_v);   
          log_weight_denom_v := std_logic_vector(to_unsigned(integer_v, log_weight_denom_v'length));  
          for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop 
            read(line_v, integer_v);
            interpolated_samples0_v(i) := std_logic_vector(to_unsigned(integer_v, interpolated_samples0_v(i)'length));    
          end loop;
          for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop 
            read(line_v, integer_v);
            interpolated_samples1_v(i) := std_logic_vector(to_unsigned(integer_v, interpolated_samples1_v(i)'length));    
          end loop;	
      end if;
	  
	end if;
	
	
	-- OUTPUT
	if output_dav_r = '1' then
	  for i in 0 to NUMBER_OF_SAMPLES_DEC-1 loop 
	    integer_v := to_integer(unsigned(pred_samples_gen(i)));
	    write(line_v, integer_v); 
	    writeline(wp_out, line_v);
	  end loop;
    end if;	
		
  end if;
  
interpolated_samples0_next <= interpolated_samples0_v; 
interpolated_samples1_next <= interpolated_samples1_v; 
weighted_pred_flag_next <= weighted_pred_flag_v;  
bi_pred_flag_next <= bi_pred_flag_v;
weight0_next <= weight0_v;
offset0_next <= offset0_v; 
weight1_next <= weight1_v;
offset1_next <= offset1_v;
log_weight_denom_next <= log_weight_denom_v;
input_dav_next <= input_dav_v;
counter_next <= counter_v;

end process;

--Main clock process.
PROC_CLK: process (clk_gen, rst_gen)
begin  
  if rst_gen = '0' then 
    interpolated_samples0_r <= (others=>(others=>'0'));	
	interpolated_samples1_r <= (others=>(others=>'0'));
    log_weight_denom_r <= (others=>'0'); 
	weight0_r <= (others=>'0'); 
	offset0_r <= (others=>'0');  
	weight1_r <= (others=>'0'); 
	offset1_r <= (others=>'0'); 
    weighted_pred_flag_r <= '0';
	bi_pred_flag_r <= '0';	 
	input_dav_r <= '0';	
	input_dav_delay1_r <= '0';
	output_dav_r <= '0';	
	counter_r <= (others=>'0');
  elsif rising_edge(clk_gen) then
    interpolated_samples0_r <= interpolated_samples0_next;	
	interpolated_samples1_r <= interpolated_samples1_next;
    log_weight_denom_r <= log_weight_denom_next; 
	weight0_r <= weight0_next; 
	offset0_r <= offset0_next;   
    weight1_r <= weight1_next; 
	offset1_r <= offset1_next; 
    weighted_pred_flag_r <= weighted_pred_flag_next;
	bi_pred_flag_r <= bi_pred_flag_next; 
	input_dav_r <= input_dav_next; 
	input_dav_delay1_r <= input_dav_delay1_next;
	input_dav_delay2_r <= input_dav_delay2_next;
	output_dav_r <= output_dav_next;
	counter_r <= counter_next;
  end if;
end process;

end test_arch;