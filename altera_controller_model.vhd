-------------------------------------------------------------------------------
-- Title      : Simulation model of the altera controller.
-- Project    : 
-------------------------------------------------------------------------------
-- File       : altera_controller_model.vhd
-- Author     :   <Roszkowski@STORK>
-- Company    : 
-- Created    : 2010-06-24
-- Last update: 2010-06-29
-- Platform   : 
-- Standard   : VHDL'87
-------------------------------------------------------------------------------
-- Description: Simulation only module attempting to model the behaviour of the
--              altera DDR SDRAM controller as closely as possible. However, some
--              simplifications are made, regarding command queueing.  In this 
--              module read and write commands are queued independently, and
--              simulation of interleaved short write/read commands is far from
--              the original altera module.
-------------------------------------------------------------------------------
-- Copyright (c) 2010 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2010-06-24  1.0      Roszkowski	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.ddr2_pkg.all;
use work.ddr2_read;
use work.ddr2_write;
use work.ddr2_bidir;
use work.ddr2_arbiter;

entity altera_controller_model is
  generic (
    CLOCK_PERIOD : time := 5 ns;
    QUEUE_DEPTH : integer := 6;         -- how many commands can be accepted
    EXTRA_WRITE_QUEUE_DEPTH : integer := 4;  -- simulation indicates that queue for write seems to be emptied faster - which is modeled by adding smth to its depth
    INIT_DELAY : integer := 1000;       -- how many clock cycles should be waited before controller is ready to work
    INTER_BURST_WRITE_DELAY : integer := 12;  -- delay between each DDR write burst
    INTER_BURST_READ_DELAY  : integer := 8;  -- delay between each DDR read burst
    INTER_READ_LOCAL_DELAY  : integer := 3;  -- delay between read local ready
    INTER_WRITE_LOCAL_DELAY : integer := 1;  -- delay between local write ready and local wdata_req
    WRITE_TO_READ_SWITCH_DELAY : integer := 22;  -- delay when switching from write to read command
    READ_INIT_DELAY : integer := 28;    -- delay between read request and first data on memory output
    MEM_BURST_WIDTH : integer := 4      -- amount of data in one burst
    );
  port (
    signal mem_cke               : OUT   STD_LOGIC_VECTOR (MEM_CKE_WIDTH-1 DOWNTO 0);
    signal mem_addr              : OUT   STD_LOGIC_VECTOR (MEM_ADD_WIDTH-1 DOWNTO 0);
    signal aux_scan_clk_reset_n  : OUT   STD_LOGIC;
    signal mem_dm                : OUT   STD_LOGIC_VECTOR (MEM_DM_WIDTH-1 DOWNTO 0);
    signal aux_full_rate_clk     : OUT   STD_LOGIC;
    signal local_init_done       : OUT   STD_LOGIC;
    signal mem_dq                : INOUT STD_LOGIC_VECTOR (MEM_DQ_WIDTH-1 DOWNTO 0);
    signal mem_ras_n             : OUT   STD_LOGIC;
    signal mem_cs_n              : OUT   STD_LOGIC_VECTOR (MEM_CS_WIDTH-1 DOWNTO 0);
    signal phy_clk               : OUT   STD_LOGIC;
    signal mem_ba                : OUT   STD_LOGIC_VECTOR (MEM_BA_WIDTH-1 DOWNTO 0);
    signal dqs_delay_ctrl_export : OUT   STD_LOGIC_VECTOR (DQS_DEL_EXPORT_WIDTH-1 DOWNTO 0);
    signal aux_scan_clk          : OUT   STD_LOGIC;
    signal aux_half_rate_clk     : OUT   STD_LOGIC;
    signal local_rdata           : OUT   STD_LOGIC_VECTOR (ALTERA_DATA_WIDTH-1 DOWNTO 0);
    signal local_rdata_valid     : OUT   STD_LOGIC;
    signal mem_we_n              : OUT   STD_LOGIC;
    signal reset_phy_clk_n       : OUT   STD_LOGIC;
    signal local_ready           : OUT   STD_LOGIC;
    signal mem_odt               : OUT   STD_LOGIC_VECTOR (MEM_ODT_WIDTH-1 DOWNTO 0);
    signal mem_cas_n             : OUT   STD_LOGIC;
    signal dll_reference_clk     : OUT   STD_LOGIC;
    signal mem_clk               : INOUT STD_LOGIC_VECTOR (MEM_CLK_WIDTH-1 DOWNTO 0);
    signal local_refresh_ack     : OUT   STD_LOGIC;
    signal local_wdata_req       : OUT   STD_LOGIC;
    signal reset_request_n       : OUT   STD_LOGIC;
    signal mem_dqs               : INOUT STD_LOGIC_VECTOR (MEM_DQS_WIDTH-1 DOWNTO 0);
    signal mem_clk_n             : INOUT STD_LOGIC_VECTOR (MEM_CLK_WIDTH-1 DOWNTO 0);
    signal local_wdata           : IN    STD_LOGIC_VECTOR (ALTERA_DATA_WIDTH-1 DOWNTO 0);
    signal soft_reset_n          : IN    STD_LOGIC;
    signal global_reset_n        : IN    STD_LOGIC;
    signal local_write_req       : IN    STD_LOGIC;
    signal local_address         : IN    STD_LOGIC_VECTOR (ALTERA_ADDRESS_WIDTH-1 DOWNTO 0);
    signal pll_ref_clk           : IN    STD_LOGIC;
    signal local_be              : IN    STD_LOGIC_VECTOR (ALTERA_BYTE_ENABLE-1 DOWNTO 0);
    signal local_read_req        : IN    STD_LOGIC);

end altera_controller_model;

architecture test of altera_controller_model is

  -- constants
  constant READ_DEPTH : integer := 1024;
  
  -- memory storage related typed
  type CELL_TYPE is array(0 downto 0) of std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
  type CELL_ACCESS_TYPE is access CELL_TYPE;
  type MEMORY_TYPE is array(2**ALTERA_ADDRESS_WIDTH-1 downto 0) of CELL_ACCESS_TYPE;
  type MEMORY_ACCESS_TYPE is access MEMORY_TYPE;

  -- simualation related types
--  type COMMAND_TYPE is record
--    rd_command : std_logic;             -- '1' if read command, '0' otherwise
--    address    : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);  -- address to be read / written
--  end type;

  type COMMAND_ARRAY_TYPE is array(READ_DEPTH-1 downto 0) of std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);--COMMAND_TYPE;
  
  type COMMAND_QUEUE_TYPE is record
    commands      : COMMAND_ARRAY_TYPE;  -- essentially addresses to be read
    read_address  : integer;
    write_address : integer;
  end record;

  type PORT_CONTROLLER_STATE_TYPE is (INIT,WAIT_REQUEST,WAIT_WRITE_TO_READ_DELAY,WAIT_DELAY,WAIT_MEM_READY);
  type PORT_CONTROLLER_TYPE is record
    state             : PORT_CONTROLLER_STATE_TYPE;
    write_req_count   : integer;        -- count write requests
    read_req_count    : integer;        -- count read requests
    read_burst        : boolean;        -- true if in the middle of read requests burst, false if in the middle of write requests burst
    delay             : integer;        -- delay between active local readys when queue full
    inter_burst_delay : integer;        -- delay between last written sample and first read sample
    wdata_req         : std_logic;      -- send wdata request
    wdata_req_dly     : std_logic;      -- delayed wdata request - on this clock cycle we should store data provided by the user
    address           : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);  -- write address connected with wdata_req
    address_dly       : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);  -- write address connected with wdata_req_dly
  end record;

  type READ_CONTROLLER_STATE_TYPE is (IDLE,WAIT_DELAY,READ_DATA);
  type READ_CONTROLLER_TYPE is record
    state             : READ_CONTROLLER_STATE_TYPE;
    prev_read_address : std_logic_vector(ALTERA_ADDRESS_WIDTH-1 downto 0);  -- previous address that has been read
    delay             : integer;
  end record;
  
  -- signals
  signal clk : std_logic;
  signal command_queue_r : COMMAND_QUEUE_TYPE;
  signal command_queue_next : COMMAND_QUEUE_TYPE;
  signal port_controller_r : PORT_CONTROLLER_TYPE;
  signal port_controller_next : PORT_CONTROLLER_TYPE;
  signal read_controller_r : READ_CONTROLLER_TYPE;
  signal read_controller_next : READ_CONTROLLER_TYPE;
  
begin  -- test

  -- This module is not trying to simulate external memory access, but alll external memory ports
  -- appear in its interface for the sake of making switching between model and real controller easier.
  -- All the unnecessary ports are zeroed here.
  mem_cke               <= (others => '0');
  mem_addr              <= (others => '0');
  aux_scan_clk_reset_n  <= '0';
  mem_dm                <= (others => '0');
  aux_full_rate_clk     <= '0';
  mem_dq                <= (others => '0');
  mem_ras_n             <= '1';
  mem_cs_n              <= (others => '1');
  mem_ba                <= (others => '0');
  dqs_delay_ctrl_export <= (others => '0');
  aux_scan_clk          <= '0';
  aux_half_rate_clk     <= '0';
  mem_we_n              <= '1';
  reset_phy_clk_n       <= '1';
  mem_odt               <= (others => '0');
  mem_cas_n             <= '1';
  dll_reference_clk     <= '0';
  mem_clk               <= (others => '0');
  reset_request_n       <= '1';
  mem_dqs               <= (others => '0');
  mem_clk_n             <= (others => '0');
  

  -- local refresh is an unsupported operation
  local_refresh_ack <= '0';
  
  -- assign here the most rarely used, but still important, output signals
  phy_clk <= clk;
  local_init_done <= '1' when port_controller_r.state /= INIT else
                     '0';
  local_wdata_req <= port_controller_r.wdata_req;
  
  -- generate local clock
  CLK_GEN: process
  begin
    clk <= '0';
    wait for CLOCK_PERIOD/2;
    clk <= '1';
    wait for CLOCK_PERIOD/2;
  end process;


  
  
  CONTROLLER_PROC : process (command_queue_r, local_address, local_read_req, local_write_req, port_controller_r, read_controller_r)

    -- controllers variables
    variable command_queue_next_v   : COMMAND_QUEUE_TYPE;
    variable port_controller_next_v : PORT_CONTROLLER_TYPE;
    variable read_controller_next_v : READ_CONTROLLER_TYPE;
    -- local memory bus variables
    variable local_rdata_v : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
variable local_coarse0_v : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
variable local_coarse1_v : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
variable local_coarse2_v : std_logic_vector(ALTERA_DATA_WIDTH-1 downto 0);
    variable local_rdata_valid_v : std_logic;
    variable local_ready_v       : std_logic;
    variable temp_int_v          : integer;
    variable memory_v            : MEMORY_ACCESS_TYPE;
  begin
    --local memory bus inits
    local_rdata_valid_v := '0';
    local_ready_v := '0';
    --controllers variables inits
    read_controller_next_v := read_controller_r;
    port_controller_next_v := port_controller_r;
    command_queue_next_v   := command_queue_r;

    -- calculate inter burst delay
    if port_controller_r.inter_burst_delay /= 0 then      
      port_controller_next_v.inter_burst_delay := port_controller_r.inter_burst_delay - 1;
    end if;

    -- perform memory write
    if port_controller_r.wdata_req_dly = '1' then
      -- wdata request was sent in previous clock cycle, now we can gather data from local bus
      if memory_v = null then
        memory_v := new MEMORY_TYPE;
      end if;
      if memory_v(conv_integer(port_controller_r.address_dly)) = null then
        memory_v(conv_integer(port_controller_r.address_dly)) := new CELL_TYPE;
      end if;
      memory_v(conv_integer(port_controller_r.address_dly))(0) := local_wdata;      
    end if;
    
    -- specify correct delayed wdata request
    port_controller_next_v.wdata_req := '0';
    port_controller_next_v.wdata_req_dly := port_controller_r.wdata_req;
    port_controller_next_v.address_dly := port_controller_r.address;
    -- calculate inter burst delay all the time
    -- port controller FSM
    case port_controller_r.state is
      when INIT =>
        -- wait until initial delay expires, before accepting any requests
        port_controller_next_v.delay := port_controller_r.delay - 1;
        if port_controller_r.delay = 0 then
          port_controller_next_v.state := WAIT_REQUEST;
        end if;
        
      when WAIT_REQUEST =>
        -- assert local ready, as we are ready to accept requests
        local_ready_v := '1';
        if local_read_req = '1' then
          -- we receive request to read from memory
          port_controller_next_v.inter_burst_delay     := WRITE_TO_READ_SWITCH_DELAY;
          port_controller_next_v.read_burst            := true;
          if port_controller_r.read_burst and port_controller_r.inter_burst_delay /= 0 then
            -- read burst is being continued
            port_controller_next_v.read_req_count        := port_controller_r.read_req_count + 1;
          else
            -- first read in a burst
            port_controller_next_v.read_req_count := 1;
          end if;
          -- queue read in the command queue
          command_queue_next_v.commands(command_queue_r.write_address) := local_address;
          command_queue_next_v.write_address           := command_queue_r.write_address +1;
          if command_queue_next_v.write_address = READ_DEPTH then
            command_queue_next_v.write_address := 0;
          end if;
          if not port_controller_r.read_burst and port_controller_r.inter_burst_delay /= 0 then
            -- if switched to read from write burst wait certain delay,
            -- which simulates storing samples into DDR memory
            port_controller_next_v.state := WAIT_WRITE_TO_READ_DELAY;            
          elsif port_controller_next_v.read_req_count >= QUEUE_DEPTH then
            -- when input queue is full accept read commands with certain delay
            port_controller_next_v.state := WAIT_DELAY;
            port_controller_next_v.delay := INTER_READ_LOCAL_DELAY;
          end if;
          
        elsif local_write_req = '1' then
          port_controller_next_v.inter_burst_delay     := WRITE_TO_READ_SWITCH_DELAY;
          port_controller_next_v.read_burst := false;
          port_controller_next_v.address := local_address;
          if not port_controller_r.read_burst and port_controller_r.inter_burst_delay /= 0 then
            -- write burst is being continued
            port_controller_next_v.write_req_count := port_controller_r.write_req_count + 1;
          else
            -- first write in a burst
            port_controller_next_v.write_req_count := 1;
          end if;
          
          if read_controller_r.state /= IDLE then
            -- previous burst was a read burst
            -- wait for all the data from the external memory to be read before continuing
            port_controller_next_v.state := WAIT_MEM_READY;
          elsif port_controller_next_v.write_req_count >= QUEUE_DEPTH + EXTRA_WRITE_QUEUE_DEPTH then
            -- above all acceptable depths -> store data in  bursts of four samples
            temp_int_v := port_controller_next_v.write_req_count - (QUEUE_DEPTH + EXTRA_WRITE_QUEUE_DEPTH);
            temp_int_v := (temp_int_v-1) mod MEM_BURST_WIDTH;
            if temp_int_v = 0 then
              -- delay between memory bursts
              port_controller_next_v.state := WAIT_DELAY;
              port_controller_next_v.delay := INTER_BURST_WRITE_DELAY;
            else
              -- delay within memory burst
              port_controller_next_v.state := WAIT_DELAY;
              port_controller_next_v.delay := 0;
              port_controller_next_v.wdata_req := '1';
            end if;
          elsif port_controller_next_v.write_req_count > QUEUE_DEPTH then
            -- in between QUEUE_DEPTH and QUEUE_DEPTH + EXTRA_WRITE_QUEUE_DEPTH
            port_controller_next_v.state := WAIT_DELAY;
            port_controller_next_v.delay := INTER_WRITE_LOCAL_DELAY;
          else
            -- below QUEUE_DEPTH
            port_controller_next_v.state := WAIT_DELAY;
            port_controller_next_v.delay := 0;
            port_controller_next_v.wdata_req := '1';
          end if;          
        end if;
        
      when WAIT_WRITE_TO_READ_DELAY =>        
        if port_controller_r.inter_burst_delay = 0 then                      
          port_controller_next_v.state := WAIT_REQUEST;
        end if;
        
      when WAIT_DELAY => 
        port_controller_next_v.delay := port_controller_r.delay - 1;
        if port_controller_r.delay = 1 and not port_controller_r.read_burst then
          -- write burst -> send write req
          port_controller_next_v.wdata_req := '1';
        end if;
        if port_controller_r.delay = 0 then
          port_controller_next_v.state := WAIT_REQUEST;
        end if;
        
      when WAIT_MEM_READY =>
        if read_controller_r.state = IDLE then
          port_controller_next_v.state := WAIT_REQUEST;
        end if;
      when others => null;
    end case;

    -- read controller FSM
    case read_controller_r.state is
      when IDLE =>
        if command_queue_r.write_address /= command_queue_r.read_address then
          -- there are pending reads
          if port_controller_next_v.state /= WAIT_WRITE_TO_READ_DELAY then
             -- we can start read
            read_controller_next_v.state := WAIT_DELAY;
            read_controller_next_v.delay := READ_INIT_DELAY;
            --read_controller_next_v.prev_read_address := command_queue_r.commands(command_queue_r.read_address);
          end if;
        end if;
        
      when WAIT_DELAY =>
        read_controller_next_v.delay := read_controller_r.delay - 1;
        if read_controller_r.delay = 0 then
          read_controller_next_v.state := READ_DATA;
        end if;
        
      when READ_DATA =>
        local_rdata_valid_v := '1';
        if memory_v /= null then
          if memory_v(conv_integer(command_queue_r.commands(command_queue_r.read_address))) /= null then
            local_rdata_v := memory_v(conv_integer(command_queue_r.commands(command_queue_r.read_address)))(0);
          else
            local_rdata_v := (others => 'X');
          end if;
        else          
          local_rdata_v := (others => 'X');
        end if;
        command_queue_next_v.read_address := command_queue_r.read_address + 1;
        if command_queue_next_v.read_address = READ_DEPTH then
          command_queue_next_v.read_address := 0;
        end if;
        if command_queue_next_v.read_address = command_queue_r.write_address then
          -- last requested data is read -> end reading
          read_controller_next_v.state := IDLE;
        else
          -- not last read
          if command_queue_r.commands(command_queue_r.read_address) + 1 /= command_queue_r.commands(command_queue_next_v.read_address) then
            -- next address is not a successive address -> use big delay
            read_controller_next_v.state := WAIT_DELAY;
            read_controller_next_v.delay := INTER_BURST_READ_DELAY;
          else
            -- use low delay
            read_controller_next_v.state := WAIT_DELAY;
            read_controller_next_v.delay := 0;
          end if;
        end if;
      when others => null;
    end case;

--      if memory_v /= null then
--         if memory_v(2**16+2**17+2**18+2**19+2**20) /= null then	   --X"00F80000")
--             local_coarse0_v:= memory_v(2**16+2**17+2**18+2**19+2**20)(0);
--          else
--            local_coarse0_v := (others => 'X');
--          end if;
--         if memory_v(2**16+2**17+2**18+2**19+2**20+2**8) /= null then	   --X"00F80000")
--             local_coarse1_v:= memory_v(2**16+2**17+2**18+2**19+2**20+2**8)(0);
--          else
--            local_coarse1_v := (others => 'X');
--          end if;
--         if memory_v(2**16+2**17+2**18+2**19+2**20+2**9) /= null then	   --X"00F80000")
--             local_coarse2_v:= memory_v(2**16+2**17+2**18+2**19+2**20+2**9)(0);
--          else
--            local_coarse2_v := (others => 'X');
--          end if;
--      end if;

    command_queue_next     <= command_queue_next_v;
    port_controller_next   <= port_controller_next_v;
    read_controller_next   <= read_controller_next_v;
    local_rdata_valid      <= local_rdata_valid_v;
    local_rdata            <= local_rdata_v;
    local_ready            <= local_ready_v;
  end process;
  
  REG_PROC: process (clk, global_reset_n)
  begin  -- process REG_PROC
    if global_reset_n = '0' then                 -- asynchronous reset (active low)
      command_queue_r.read_address <= 0;
      command_queue_r.write_address <= 0;
      port_controller_r.state <= INIT;
      port_controller_r.read_burst <= false;
      port_controller_r.delay <= INIT_DELAY;
      port_controller_r.inter_burst_delay <= 0;
      port_controller_r.write_req_count <= 0;
      port_controller_r.read_req_count <= 0;
      read_controller_r.state <= IDLE;
      read_controller_r.prev_read_address <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      read_controller_r <= read_controller_next;
      port_controller_r <= port_controller_next;
      command_queue_r <= command_queue_next;
    end if;
  end process REG_PROC;

end test;
